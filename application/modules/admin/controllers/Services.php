<?php (defined('BASEPATH')) or exit('No direct script access allowed');

class Services extends Admin_Controller
{
    public function __construct()
    {
        parent::__construct();

        if (!$this->ion_auth->logged_in()) {
            $this->ion_auth->logout();
            redirect('auth', 'refresh');
        }

        $authorized = array('admin', 'supervisor', 'excecutives');
        if (!in_array($this->ion_auth->get_users_groups()->row()->name, $authorized)) {
            $this->session->set_flashdata('message', 'You must be an administrator to view the users page.');
            redirect('admin/dashboard');
        }

        $this->load->model('module');
        $this->load->model(array('admin/service'));
        $this->load->model(array('admin/options'));
        $this->load->model(array('admin/paciente'));
        $this->load->library('report_maker');
        $this->load->library('phpmailerlib');
        $this->load->library('servicios_lib');

        $this->load->helper("date");

    }

    public function index()
    {
        $user_id = $this->ion_auth->get_user_id();
        $group_name = $this->ion_auth->get_users_groups()->row()->name;
        $query = "SELECT DISTINCT services.* from services,services_relationship,schedule
                        where services.id = services_relationship.service_id ";

        switch ($group_name) {

            case ('supervisor'):
                $query .= " AND relation_term = 'supervisor'
                          AND relation_value = '$user_id' ";
                break;
            case ('excecutives'):
                $query .= " AND relation_term = 'excecutives'
                          AND relation_value = '$user_id' ";
                break;
        }

        $refresh = 0;

        if ($this->input->is_ajax_request()) {

            $status = $this->input->post("status");
            $luz = $this->input->post("luz");
            $supervisor = $this->input->post("supervisor");

            if ($status != "") {
                $query .= "AND services.status = '$status'";
            }
            if ($supervisor != "") {
                $query .= " AND relation_term = 'supervisor'
                          AND relation_value = '$supervisor' ";
            }
            if ($luz != "") {
                $query .= " AND schedule.nurse_id = '$luz' and schedule.service_id = services.id";
            }
            $refresh = 1;
        }

        $data['supervisors'] = $this->ion_auth->users('supervisor')->result();
        $data['colonies'] = $this->servicios_lib->get_all_colonies();

        $q = $this->db->query($query);
        $services = $q->result_array();
        $data['services'] = $services;

        $data['supervisors'] = $this->ion_auth->users('supervisor')->result();
        $data['nurses'] = $this->ion_auth->users('nurses')->result();
        $data['nurses_status'] = $this->report_maker->get_nurses_status();

        $data['services_meta'] = $this->servicios_lib->get_services_meta_for_list();
        $data['page'] = $this->config->item('ci_my_admin_template_dir_admin') . "services_list";
        $this->load->view($this->_container, $data);

        if ($refresh == 1) {
            return json_encode($data);
        }
    }

    /**
     * TODO: Si se separa services y pacientes como se manejara el dashboard.php que pasa como parametro el nurse_info que es un tipo fecha
     * 
     * Metodo que se encarga de visualizar la forma de captura de servicios
     *
     * @param  $nurse_info
     * @return void
     */
    public function create($paciente_id = null, $nurse_info = null)
    {
        $data['filtered_supervisor'] = 0;
        $data['filtered_luz'] = 0;
        $data['filtered_gender'] = 0;
        $data['filtered_colony'] = "none";
        $data['filtered_personality'] = 0;

        if ($this->input->is_ajax_request()) {

            $query = "select users.* from users,users_groups";
            $query2 = "  users_groups.user_id = users.id
                    and users_groups.group_id = (SELECT id from groups where name='nurses') ";
            if ($this->input->post('luz') != 0) {
                $query2 .= " and users.id = " . $this->input->post('luz');
                $data['filtered_luz'] = $this->input->post('luz');

            }
            if ($this->input->post('supervisor') != 0) {
                $query .= " JOIN users_meta as fs using(user_id)";
                $query2 .= "   AND (fs.meta_key = '_supervisor'
                              AND fs.meta_value =  '" . $this->input->post('supervisor') . "') ";
                $data['filtered_supervisor'] = $this->input->post('supervisor');
            }

            if ($this->input->post('colony') != "") {
                $query .= " JOIN users_meta as fc using(user_id)";
                $query2 .= "   AND (fc.meta_key = '_colony'
                              AND fc.meta_value =  '" . $this->input->post('colony') . "') ";
                $data['filtered_colony'] = $this->input->post('colony');

            }
            if ($this->input->post('gender') != "") {
                $query .= " JOIN users_meta as fg using(user_id)";
                $query2 .= "   AND (fg.meta_key = '_gender'
                              AND fg.meta_value =  '" . $this->input->post('gender') . "' )";
                $data['filtered_gender'] = $this->input->post('gender');
            }
            if ($this->input->post('personality') != "") {
                $query .= " JOIN users_meta as fp using(user_id)";
                $query2 .= "  AND (fp.meta_key = '_personality'
                              AND fp.meta_value =  '" . $this->input->post('personality') . "') ";
                $data['filtered_personality'] = $this->input->post('personality');
            }

            $query = $query . " WHERE " . $query2;
            $query = $this->db->query($query);
            $data['nurses'] = $query->result();
            $data['nurses_status'] = $this->report_maker->get_nurses_status();

            $data['page'] = $this->config->item('ci_my_admin_template_dir_admin') . "services_create";
            $this->load->view($this->_container, $data);
            return json_encode($data);

        } else {
            if(is_null($paciente_id)) {
                $this->session->set_flashdata('message_error', 'Paciente no valido.');
                redirect('admin/pacientes','refresh');
            }
            $paciente = $this->paciente->get($paciente_id);

            if(!is_object($paciente)) {
                $this->session->set_flashdata('message_error', 'Paciente no encontrado en la base de datos.');
                redirect('admin/pacientes','refresh');
            }
          
            $authorized = array('admin', 'supervisor');
            if (!in_array($this->ion_auth->get_users_groups()->row()->name, $authorized)) {
                redirect(site_url('admin/reports'), 'refresh');
            }

            if ($this->input->post('name')) {
                $this->service->insert($data);
                redirect('/admin/services', 'refresh');
            }

            $data['paciente'] = $paciente;  
            $data['nurses'] = $this->ion_auth->users('nurses')->result();
            $data['nurses_status'] = $this->report_maker->get_nurses_status();
            $data['personalities'] = $this->options->get_all('*', array('option_name' => '_personality'));
            $data['colonies'] = $this->servicios_lib->get_all_colonies();

            if ($nurse_info != null) {
                $nurse_arr = explode("-", $nurse_info);
                $data['assigned_nurse'] = $nurse_arr[0];
                $data['assigned_hour'] = $nurse_arr[1];
            } else {
                $data['assigned_nurse'] = -1;
                $data['assigned_hour'] = "00:00";
            }

            $data['lat_results'] = $this->servicios_lib->get_lat_results();
            $data['nurse_homes'] = $this->servicios_lib->get_nurse_homes();

            $data['services'] = $this->options->get_all('*', array('option_name' => '_service'));
            $data['types'] = $this->options->get_all('*', array('option_name' => '_serviceType'));
            $data['deseases'] = $this->options->get_all('*', array('option_name' => '_desease'));
            $data['service_calendar'] = true;

            $data['doctors'] = $this->ion_auth->users('doctors')->result();
            $data['supervisors'] = $this->ion_auth->users('supervisor')->result();
            $data['ejecutivos'] = $this->ion_auth->users('excecutives')->result();
            $data['date_range'] = array('Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado', 'Domingo');
            $data['time_range'] =  time_range('00:00', '24:00');

            $data['page'] = $this->config->item('ci_my_admin_template_dir_admin') . "services_create";

            $this->load->view($this->_container, $data);
        }

    }

    public function edit($paciente_id, $service_id)
    {
        $data['filtered_supervisor'] = 0;
        $data['filtered_luz'] = 0;
        $data['filtered_gender'] = 0;
        $data['filtered_colony'] = "none";
        $data['filtered_personality'] = 0;

        if ($this->input->is_ajax_request()) {
            $query = "select users.* from users,users_groups";
            $query2 = "  users_groups.user_id = users.id
                    and users_groups.group_id = (SELECT id from groups where name='nurses') ";
            if ($this->input->post('luz') != 0) {
                $query2 .= " and users.id = " . $this->input->post('luz');
                $data['filtered_luz'] = $this->input->post('luz');

            }
            if ($this->input->post('supervisor') != 0) {
                $query .= " JOIN users_meta as fs using(user_id)";
                $query2 .= "   AND (fs.meta_key = '_supervisor'
                              AND fs.meta_value =  '" . $this->input->post('supervisor') . "') ";
                $data['filtered_supervisor'] = $this->input->post('supervisor');
            }

            if ($this->input->post('colony') != "") {
                $query .= " JOIN users_meta as fc using(user_id)";
                $query2 .= "   AND (fc.meta_key = '_colony'
                              AND fc.meta_value =  '" . $this->input->post('colony') . "') ";
                $data['filtered_colony'] = $this->input->post('colony');

            }
            if ($this->input->post('gender') != "") {
                $query .= " JOIN users_meta as fg using(user_id)";
                $query2 .= "   AND (fg.meta_key = '_gender'
                              AND fg.meta_value =  '" . $this->input->post('gender') . "' )";
                $data['filtered_gender'] = $this->input->post('gender');
            }
            if ($this->input->post('personality') != "") {
                $query .= " JOIN users_meta as fp using(user_id)";
                $query2 .= "  AND (fp.meta_key = '_personality'
                              AND fp.meta_value =  '" . $this->input->post('personality') . "') ";
                $data['filtered_personality'] = $this->input->post('personality');
            }

            $query = $query . " WHERE " . $query2;
            /*echo  $query;*/
            $query = $this->db->query($query);
            $data['nurses'] = $query->result();
            $data['nurses_status'] = $this->report_maker->get_nurses_status();

            $data['page'] = $this->config->item('ci_my_admin_template_dir_admin') . "services_edit";
            $this->load->view($this->_container, $data);
            return json_encode($data);

        } else {
            $authorized = array('admin', 'supervisor');
            if (!in_array($this->ion_auth->get_users_groups()->row()->name, $authorized)) {
                redirect(site_url('admin/reports'), 'refresh');
            }
            if ($this->input->post('name')) {

                $data['name'] = $this->input->post('name');

                $data['price'] = $this->input->post('price');
                $data['cost'] = $this->input->post('cost');
                $data['model'] = $this->input->post('model');
                $data['brand_id'] = $this->input->post('brand_id');
                $data['category_id'] = $this->input->post('category_id');
                $data['tag_line'] = $this->input->post('tag_line');
                $data['features'] = $this->input->post('features');

                $this->service->update($data, $id);

                redirect('/admin/services', 'refresh');
            }

            $paciente = $this->paciente->get($paciente_id);
            $service = $this->service->get($service_id);

            $data['service_status'] = $service->status;

            $this->db->select("*");
            $this->db->from("services_meta");
            $this->db->where(array('service_id' => $service_id));
            $query = $this->db->get();
            $results = $query->result_array();

            $service_meta = array();

            foreach ($results as $result) {

                $service_meta[$result['service_meta']] = $result['service_value'];

            }

            $query_r = $query->result();
            $data['lat_results'] = $this->servicios_lib->get_lat_results();
            $data['nurse_homes'] = $this->servicios_lib->get_nurse_homes();

            $current_schedule = $this->report_maker->get_current_schedule($service_id);
            if ($current_schedule != "") {
                $data['current_schedule'] = "Asignación actual: <br/>" . $current_schedule;
            } else {
                $data['current_schedule'] = "Servicio no asignado";
            }

            $data['_schedule'] = $schedule;

            $this->db->select("relation_value");
            $this->db->from("services_relationship");
            $this->db->where(array('service_id' => $service_id, 'relation_term' => 'doctor'));
            $query = $this->db->get();
            $result = $query->result();

            $this->db->select("relation_value");
            $this->db->from("services_relationship");
            $this->db->where(array('service_id' => $service_id, 'relation_term' => 'supervisor'));
            $query = $this->db->get();
            $supervisor_result = $query->result();

            $this->db->select("relation_value");
            $this->db->from("services_relationship");
            $this->db->where(array('service_id' => $service_id, 'relation_term' => 'ejecutivo'));
            $query = $this->db->get();
            $ejecutivo_result = $query->result();

            $data['service_id'] = $service_id;

            $data['service_doctor'] = $result[0]->relation_value;
            $data['service_supervisor'] = $supervisor_result[0]->relation_value;
            $data['service_ejecutivo'] = $ejecutivo_result[0]->relation_value;
            $data['service_routines'] = json_decode($service_meta['_routines'], true);

            $service_plan_data = $this->options->get($service_meta['_service_plan']);
            $service_data = json_decode($service_plan_data->option_value);
            $data['columns'] = $service_data->daysperweek;
            $data['rows'] = $service_data->timesperday;

            $data['nurses'] = $this->ion_auth->users('nurses')->result();
            $data['nurses_status'] = $this->report_maker->get_nurses_status();
            $data['personalities'] = $this->options->get_all('*', array('option_name' => '_personality'));
            $data['colonies'] = $this->servicios_lib->get_all_colonies();

            $start_time = $service->start_date;
            // $datetime is something like: 2014-01-31 13:05:59
            $time = strtotime($start_time);
            $myFormatForView = date("m/d/Y", $time);
            $data['start_date'] = $myFormatForView;

            $data['paciente'] = $paciente;
            $data['price'] = $service_meta['_price'];
            $data['cost'] = $service_meta['_cost'];
            $data['plan'] = $service_meta['_service_plan'];
            $data['services'] = $this->options->get_all('*', array('option_name' => '_service'));
            $data['service_calendar'] = true;
            $data['types'] = $this->options->get_all('*', array('option_name' => '_serviceType'));

            $data['doctors'] = $this->ion_auth->users('doctors')->result();
            $data['supervisors'] = $this->ion_auth->users('supervisor')->result();
            $data['ejecutivos'] = $this->ion_auth->users('excecutives')->result();
            $data['date_range'] = array('Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado', 'Domingo');

            $data['time_range'] = time_range('00:00', '24:00');
            $data['service'] = $service;
            $data['service_type'] = $service->service_type;
            $data['service_controls'] = json_decode($service->control_records);

            $data['page'] = $this->config->item('ci_my_admin_template_dir_admin') . "services_edit";

            $this->load->view($this->_container, $data);

        }

    }

    public function delete($id)
    {
        $authorized = array('admin', 'supervisor');
        if (!in_array($this->ion_auth->get_users_groups()->row()->name, $authorized)) {
            redirect(site_url('admin/reports'), 'refresh');
        }
        $this->service->delete($id);

        redirect('/admin/pacientes', 'refresh');
    }

    public function medicine()
    {

        if ($this->input->is_ajax_request()) {

            if ($this->input->post('action') == 'save') {

                switch ($this->input->post('TimesPerDay')) {

                    case 1:
                        $inputs = '<div class="form-group col-md-12 col-sm-12 clockpicker"><input type="text" name="Medicine_Times[]" class="form-control Medicine_Times" /></div>';
                        break;
                    case 2:
                        $inputs = '<div class="form-group col-md-6 col-sm-6 clockpicker"><input type="text" name="Medicine_Times[]" class="form-control Medicine_Times" /></div>';
                        $inputs .= '<div class="form-group col-md-6 col-sm-6 clockpicker"><input type="text" name="Medicine_Times[]" class="form-control Medicine_Times" /></div>';

                        break;
                    case 3:
                        $inputs = '<div class="form-group col-md-4 clockpicker"><input type="text" name="Medicine_Times[]" class="form-control col-md-4 Medicine_Times" /></div>';
                        $inputs .= '<div class="form-group col-md-4 clockpicker"><input type="text" name="Medicine_Times[]" class="form-control col-md-4 Medicine_Times" /></div>';
                        $inputs .= '<div class="form-group col-md-4 clockpicker"><input type="text" name="Medicine_Times[]" class="form-control col-md-4 Medicine_Times" /></div>';

                        break;
                    case 4:
                        $inputs = '<div class="form-group col-md-3 col-sm-3 clockpicker"><input type="text" name="Medicine_Times[]" class="form-control Medicine_Times" /></div>';
                        $inputs .= '<div class="form-group col-md-3 col-sm-3 clockpicker"><input type="text" name="Medicine_Times[]" class="form-control Medicine_Times" /></div>';
                        $inputs .= '<div class="form-group col-md-3 col-sm-3 clockpicker"><input type="text" name="Medicine_Times[]" class="form-control Medicine_Times" /></div>';
                        $inputs .= '<div class="form-group col-md-3 col-sm-3 clockpicker"><input type="text" name="Medicine_Times[]" class="form-control Medicine_Times" /></div>';
                        break;
                }

                $code = '<tr>
                        <td><div class="form-group col-md-12"><input type="text" class="form-control Medicine_Names" name="Medicine_Names[]" /></div></td>
                        <td><div class="form-group col-md-12"><input type="text" class="form-control Medicine_Dosis" name="Medicine_Dosis[]" /></div></td>
                        <td><div class="form-group col-md-12"><input type="text" class="form-control Medicine_TimesperDay" name="Medicine_TimesperDay[]"  disabled value="' . $this->input->post('TimesPerDay') . '"/></div></td>
                        <td class="times">' . $inputs . '</td></tr><script type="text/javascript">$(".clockpicker").clockpicker({
                                            placement: "top",
                                            align: "left",
                                            donetext: "Listo"
                                        });</script>';
                $results = array(
                    'action' => 'append',
                    'element' => 'table-' . $this->input->post('module') . ' > tbody',
                    'code' => $code,
                    'status' => 'true',
                );

                echo json_encode($results);

            } else {

                $this->load->view($this->config->item('ci_my_admin_template_dir_admin') . "modals/_add_medicine_row");

            }

        } else {
            exit('No direct script access allowed');
        }

    }

    public function pdf($paciente_id, $service_id)
    {
        $authorized = array('admin', 'supervisor');
        if (!in_array($this->ion_auth->get_users_groups()->row()->name, $authorized)) {
            redirect(site_url('admin/reports'), 'refresh');
        }
        if ($this->input->is_ajax_request()) {
            $action = $this->input->post('action');
            if ($action == "send-get-info") {
                $this->load->view($this->config->item('ci_my_admin_template_dir_admin') . "modals/_send_report");
            } else if ($action == "save") {
                $pdfdata = base64_decode($this->input->post('pdfdata'));
                $filename = $this->input->post('filename');
                if (file_exists("uploads/" . $filename)) {
                    unlink("uploads/" . $filename);
                }

                file_put_contents("uploads/" . $filename, $pdfdata);
                $time = new DateTime();
                $datetime = $time->format("Y-m-d H:i:s");

                $query = $this->db->query("DELETE from services_meta where service_id = '" . $id . "' and service_meta = '_service_report_file'");
                $query = $this->db->query("INSERT INTO services_meta(service_id,service_meta,service_value,timestamp)
                            VALUES ('" . $id . "','_service_report_file','" . $filename . "','" . $datetime . "' )");
                $results = "success";
                echo json_encode($results);

            } else if ($action == "send") {

                $query = $this->db->query("SELECT service_value from services_meta
                                   where service_meta = '_service_report_file'
                                   and service_id = '" . $id . "'
                                   ORDER by timestamp ASC LIMIT 1");

                $results = $query->result();
                if (sizeof($results) > 0) {

                    $url = base_url("uploads/" . $results[0]->service_value);
                    $email = $this->input->post('Mail');
                    $nombre = $this->input->post('Name');
                    $message = $this->input->post('Message');
                    $subject = "[PAZ MENTAL] Envio de reportes";
                    $mail_success = $this->phpmailerlib->sendEmail($email, $nombre, $subject, $message, $url);
                    if ($mail_success == "success") {
                        $data_results = array(
                            "status" => "success",
                            "message" => "Mensaje enviado con éxito.",
                        );
                    } else {
                        $data_results = array(
                            "status" => "failure",
                            "message" => "Error al enviar el mensaje." . $mail_success,
                        );
                    }
                } else {
                    $data_results = array(
                        "status" => "failure",
                        "message" => "No se ha generado ningun perfil para este servicio. Favor de imprimir Primero.",
                    );
                }
                echo json_encode($data_results);

            } else {
                $data_array = array();
                $service_id = $this->input->post('service');
                $paciente_id = $this->input->post('paciente');

                $paciente = $this->paciente->get($paciente_id);
                $service = $this->service->get($service_id);

                // la información del familiar se recupera del paciente
                $this->db->select("*");
                $this->db->from("pacientes_relationship");
                $this->db->where(array('paciente_id' => $paciente_id, 'relation_term' => 'familiar'));
                $query = $this->db->get();
                $result = $query->result();

                $familiars = array();
                foreach ($result as $res) {
                    $familiar_data = $this->ion_auth->user($res->relation_value)->row();
                    $familiar_meta = $this->db->select("*");
                    $this->db->from("users_meta");
                    $this->db->where(array('user_id' => $res->relation_value, 'meta_key' => '_RelationType'));
                    $query = $this->db->get();
                    $result = $query->result();

                    switch ($result[0]->meta_value) {

                        case 'passive-hidden':
                            $relationVal = 'Pasivo';
                            break;
                        case 'payments-show':
                            $relationVal = 'Paga';
                            break;
                        case 'caring-hidden':
                            $relationVal = 'Cuida';
                            break;

                    }

                    $this->db->from("users_meta");
                    /*Relacion con el paciente */
                    $this->db->where(array('user_id' => $res->relation_value, 'meta_key' => '_RelationToPatient'));
                    $query = $this->db->get();
                    $result = $query->result();

                    switch ($result[0]->meta_value) {

                        case 'family-show':
                            $relationTP = 'Familiar';
                            break;
                        case 'doctor-hidden':
                            $relationTP = 'Doctor';
                            break;

                    }

                    $this->db->from("users_meta");
                    /*Calificacion luz */
                    $this->db->where(array('user_id' => $res->relation_value, 'meta_key' => '_Rating_Luz'));
                    $query = $this->db->get();
                    $ratingluz = $query->result();

                    $this->db->from("users_meta");
                    /*Calificacion servicio */
                    $this->db->where(array('user_id' => $res->relation_value, 'meta_key' => '_Rating_PM'));
                    $query = $this->db->get();
                    $ratingserv = $query->result();

                    $familiars[] = array('data' => $familiar_data, 'relationType' => $relationVal, 'relationToPatient' => $relationTP, 'calificacion_luz' => $ratingluz[0]->meta_value, 'calificacion_servicio' => $ratingserv[0]->meta_value);

                }
                $data_array['paciente_familiares'] = $this->servicios_lib->do_familiar_array($familiars);


                // ser recuperan los datos del servicio de service_meta
                $this->db->select("*");
                $this->db->from("services_meta");
                $this->db->where(array('service_id' => $service_id));

                $query = $this->db->get();
                $results = $query->result_array();
                $service_meta = array();

                foreach ($results as $result) {
                    $service_meta[$result['service_meta']] = $result['service_value'];
                }

                $current_schedule = $this->report_maker->get_current_schedule($service_id);
                if ($current_schedule != "") {
                    $data_array['current_schedule'] = "Asignación actual: <br/>" . $current_schedule;
                } else {
                    $data_array['current_schedule'] = "Servicio no asignado";
                }

                // esta información se recupera del servicio
                $data_array['service_id'] = $service_id;
                $data_array['service_doctor'] = $this->servicios_lib->get_service_reps($service_id, 'doctor');
                $data_array['service_supervisor'] = $this->servicios_lib->get_service_reps($service_id, 'supervisor');
                $data_array['service_ejecutivo'] = $this->servicios_lib->get_service_reps($service_id, 'ejecutivo');

                $data_array['service_routines'] = array([]);
                if(isset($service_meta['_routines'])) {
                    $data_array['service_routines'] = $this->servicios_lib->do_routine_array(json_decode($service_meta['_routines']));
                } 

                // esta informacion ahora se recupera de la talba de pacientes
                $data_array['disease'] = $this->servicios_lib->get_disease($paciente_id);
                $data_array['service_medicines'] = $this->servicios_lib->do_medicine_array(json_decode($service_meta['_medicines']));

                $data_array['_birthday'] = $service_meta['_birthday'];

                $service_plan_data = $this->options->get($service_meta['_service_plan']);
                $service_data = json_decode($service_plan_data->option_value);

                $start_time = $service->start_date;
                // $datetime is something like: 2014-01-31 13:05:59
                $time = strtotime($start_time);
                $myFormatForView = date("m/d/Y", $time);
                $data_array['start_date'] = $myFormatForView;

                $data_array['birthday'] = $paciente->birthday;
                $data_array['price'] = $service_meta['_price'];
                $data_array['cost'] = $service_meta['_cost'];
                $data_array['plan'] = $this->servicios_lib->get_service_plan($service_id);
                $data_array['service_calendar'] = true;
                $data_array['paciente'] = $paciente;
                $data_array['service'] = $service;
                $data_array['service_type'] = $service->service_type;
                $data_array['service_controls'] = json_decode($service->control_records);
                $data_array['service_goals'] = $paciente->goals;
                $data_array['map_lat'] = $paciente->map_lat;
                $data_array['map_long'] = $paciente->map_long;
                $data_array['logo'] = $this->servicios_lib->getDataURI("/assets/build/img/pazmental-pdf.jpg");
                $data_array['header'] = $this->servicios_lib->getDataURI("/assets/build/img/servicio-pdf.jpg");

                $data = json_encode($data_array);

                echo $data;
            }
        }

    }

    public function save()
    {

        if ($this->input->is_ajax_request()) {

            switch ($this->input->post('action')) {

                case 1:

                    $data = json_decode($this->input->post('data'));

                    $relation_ids = $data->_RelationIDs;

                    $service_type = $data->ServiceType;

                    $start_date = strtotime($data->start_date);
                    $start_date = date("Y-m-d H:i:s", $start_date);

                    $price = $data->price;
                    $cost = $data->cost;

                    $service_plan = $data->Service;

                    $tars_id = $data->tars_id;

                    $control_records = array(
                        '_recordMedicine' => $data->RecordMedicines,
                        '_recordEats' => $data->RecordEats,
                        '_recordOuts' => $data->RecordOuts,
                        '_recordPoops' => $data->RecordPoops,
                        '_recordEstimulateCoginitive' => $data->RecordEstimulateCognitive,
                        '_recordEstimulatePhisical' => $data->RecordEstimulatePhisical,
                        '_recordOthers' => $data->RecordOthers,
                        '_recordSymptoms' => $data->RecordSymptoms,
                    );

                    $service_data = array(
                        'tars_id' => $tars_id,
                        'service_type' => $service_type,
                        'start_date' => $start_date,
                        'control_records' => json_encode($control_records),
                        'status' => $data->service_status,
                        'paciente_id' => $data->paciente_id
                    );

                    $this->db->insert('services', $service_data); // creating service record

                    $service_id = $this->db->insert_id(); // retrieve last row id on database

                    /* Configurar rutinas para que tengan un día de la semana específico*/
                    $routines = array();
                    $routine_days = $data->_RoutineDays;

                    $i = 0;
                    while ($i < count($data->_Routines)) {
                        foreach ($routine_days as $rd) {
                            if (!array_key_exists($rd, $routines)) {
                                $routines[$rd] = array();
                            }
                            $routines[$rd][] = $data->_Routines[$i];
                            $i++;
                        }
                    }
                    $service_meta = array(
                        '_routines' => json_encode($routines),
                        '_price' => $price,
                        '_cost' => $cost,
                        '_service_plan' => $service_plan,
                    );

                    if ($service_id != '' && $service_id > 0) { // validate if service_id is not null and is more than 0

                        foreach ($service_meta as $meta => $value):

                            //inserting service metas

                            $this->db->insert('services_meta', array('service_id' => $service_id, 'service_meta' => $meta, 'service_value' => $value, 'timestamp' => date('Y-m-d h:i:s')));

                        endforeach;

                        $nurse_ids = $data->_NurseSchedule;

                        if (count($nurse_ids) > 0) {

                            foreach ($nurse_ids as $nurse):

                                //inserting nurse schedule and relation

                                if ($nurse[3] == 2) {
                                    // borrar esta asignación
                                    $this->db->where(array('nurse_id' => $nurse[0], 'service_id' => $service_id, 'date' => $nurse[1]));
                                    $this->db->delete('schedule');
                                }

                                $times = explode(',', $nurse[2]);
                                $lastEl = array_values(array_slice($times, -1))[0];
                                $time = strtotime($lastEl);
                                $times[] = date("h:i A", $time);

                                if(TIPO_SERVICIO_TRES_PUNTO_CINCO == $service_plan) {

                                    // guardamos la fecha en shedule
                                    $this->db->insert('schedule', array('nurse_id' => $nurse[0], 'service_id' => $service_id, 'date' => $nurse[1], 'times' => json_encode($times), 'status' => $nurse[3]));

                                    // guardamos la fecha de arranque en schedule_fecha_arranque 
                                    $this->db->insert("schedule_fechas_arranque", array('nurse_id' => $nurse[0], 'service_id' => $service_id, 'fecha_arranque' => $nurse[4]));
                                } else {
                                    $this->db->insert('schedule', array('nurse_id' => $nurse[0], 'service_id' => $service_id, 'date' => $nurse[1], 'times' => json_encode($times), 'status' => $nurse[3]));
                                }

                            endforeach;

                        }

                        if (count($data->_RelationIDs) > 0) {

                            foreach ($data->_RelationIDs as $relation => $value):

                                //inserting familiar relationship

                                $this->db->insert('services_relationship', array('service_id' => $service_id, 'relation_term' => 'familiar', 'relation_value' => $value));

                            endforeach;
                        }

                        if ($data->doctor != '') {

                            //inserting doctor relationship

                            $this->db->insert('services_relationship', array('service_id' => $service_id, 'relation_term' => 'doctor', 'relation_value' => $data->doctor));
                        }

                        $results = array(
                            'status' => 'success',
                            'action' => 'redirect',
                            'module' => "" . base_url() . 'admin' . "/pacientes",
                            'message' => 'Se registro exitosamente',
                        );

                        if ($data->supervisor != '') {

                            //inserting doctor relationship

                            $this->db->insert('services_relationship', array('service_id' => $service_id, 'relation_term' => 'supervisor', 'relation_value' => $data->supervisor));
                        }


                        if ($data->ejecutivo != '') {

                            //inserting doctor relationship

                            $this->db->insert('services_relationship', array('service_id' => $service_id, 'relation_term' => 'ejecutivo', 'relation_value' => $data->ejecutivo));
                        }

                        /* REHACER  LA AGENDA DE HOY A LA SIGUIENTE SEMANA*/
                        $schedule = $this->servicios_lib->do_schedule();

                        $results = array(
                            'status' => 'success',
                            'action' => 'redirect',
                            'module' => "" . base_url() . 'admin' . "/pacientes",
                            'message' => 'Se registro exitosamente',
                        );

                        echo json_encode($results);

                    }

                    break;

                case 2:

                    $data = json_decode($this->input->post('data'));

                    $service_id = $data->service_id;

                    $relation_ids = $data->_RelationIDs;

                    $service_type = $data->ServiceType;

                    $start_date = $data->start_date;

                    $price = $data->price;
                    $cost = $data->cost;

                    $service_plan = $data->Service;

                    $tars_id = $data->tars_id;

                    $start_date = strtotime($data->start_date);
                    $start_date = date("Y-m-d H:i:s", $start_date);

                    $control_records = array(
                        '_recordMedicine' => $data->RecordMedicines,
                        '_recordEats' => $data->RecordEats,
                        '_recordOuts' => $data->RecordOuts,
                        '_recordPoops' => $data->RecordPoops,
                        '_recordEstimulateCoginitive' => $data->RecordEstimulateCognitive,
                        '_recordEstimulatePhisical' => $data->RecordEstimulatePhisical,
                        '_recordOthers' => $data->RecordOthers,
                        '_recordSymptoms' => $data->RecordSymptoms,
                    );

                    $service_data = array(
                        'tars_id' => $tars_id,
                        'service_type' => $service_type,
                        'start_date' => $start_date,
                        'control_records' => json_encode($control_records),
                        'status' => $data->service_status,
                        'start_date' => $start_date,
                        'paciente_id' => $data->paciente_id
                    );

                    $this->db->where('id', $service_id);
                    $this->db->update('services', $service_data);

                    /* Configurar rutinas para que tengan un día de la semana específico*/
                    $routines = array();
                    $routine_days = $data->_RoutineDays;

                    
                    $i = 0;
                    while ($i < count($data->_Routines)) {
                        foreach ($routine_days as $rd) {
                            if (!array_key_exists($rd, $routines)) {
                                $routines[$rd] = array();
                            }
                            $routines[$rd][] = $data->_Routines[$i];
                            $i++;
                        }
                    }
                    
                    $service_meta = array(
                        '_routines' => json_encode($routines),
                        '_price' => $price,
                        '_cost' => $cost,
                        '_service_plan' => $service_plan,
                    );

                    $this->db->select('service_meta');
                    $this->db->from('services_meta');
                    $this->db->where('service_id', $service_id);
                    $query = $this->db->get();
                    $result = $query->result();
                    $existing_service_meta = array();
                    foreach ($result as $r) {
                        $existing_service_meta[] = $r->service_meta;
                    }

                    foreach ($service_meta as $meta => $value):
                        //inserting service metas
                        if (in_array($meta, $existing_service_meta)) {
                            $this->db->where('service_id', $service_id);
                            $this->db->where('service_meta', $meta);
                            $this->db->update('services_meta', array('service_value' => $value));
                        } else {
                            $this->db->insert('services_meta', array('service_id' => $service_id, 'service_meta' => $meta, 'service_value' => $value, 'timestamp' => date('Y-m-d h:i:s')));
                        }
                    endforeach;

                    $nurse_ids = $data->_NurseSchedule;

                    $response = "";
                    if (count($nurse_ids) > 0) {

                        foreach ($nurse_ids as $nurse):

                            //inserting nurse schedule and relation
                            $times = explode(',', $nurse[2]);

                            $this->db->select('*');
                            $this->db->from('schedule');
                            $this->db->where('date', $nurse[1]);
                            $this->db->where('service_id', $service_id);

                            $query = $this->db->get();
                            $result = $query->result();

                            foreach ($result as $r) {
                                $delete_id = $r->id;
                                if ($delete_id >= 0) {
                                    $this->db->where('id', $delete_id);
                                    $this->db->delete('schedule');
                                }
                            }

                            // para ingresar deacuerdo a la opcion que el usuario selecciono en el modal
                            if ($nurse[3] < 2) {
                                if(TIPO_SERVICIO_TRES_PUNTO_CINCO == $service_plan) {
                                    $this->db->insert('schedule', array('nurse_id' => $nurse[0], 'service_id' => $service_id, 'date' => $nurse[1], 'times' => json_encode($times), 'status' => $nurse[3]));


                                    // TODO: Preguntar a yael si la fecha schedule_fechas_arranque se tiene que eliminar al editar el servicio
                                    $this->db->delete("schedule_fechas_arranque", array(
                                        'nurse_id' => $nurse[0],
                                        'service_id' => $service_id,
                                        'DATE(fecha_arranque) >=' => $nurse[4]
                                    )); 

                                    $this->db->insert("schedule_fechas_arranque", array('nurse_id' => $nurse[0], 'service_id' => $service_id, 'fecha_arranque' => $nurse[4]));
                                } else {
                                    $this->db->insert('schedule', array('nurse_id' => $nurse[0], 'service_id' => $service_id, 'date' => $nurse[1], 'times' => json_encode($times), 'status' => $nurse[3]));
                                }
                            }

                            // para borrar deacuerdo a la opcion que el usuario selecciono en el modal
                            if ($nurse[3] == 2) {
                                $this->db->where(array('nurse_id' => $nurse[0], 'service_id' => $service_id, 'date' => $nurse[1]));
                                $this->db->delete('schedule');
                            }

                        endforeach;

                    }

                    if ($data->doctor != '') {

                        //inserting doctor relationship
                        $this->db->query("DELETE from services_relationship where service_id = '" . $service_id . "'
                            and relation_term = 'doctor'");
                        $this->db->insert('services_relationship', array('service_id' => $service_id, 'relation_term' => 'doctor', 'relation_value' => $data->doctor));
                    }

                    if ($data->supervisor != '') {
                        //inserting doctor relationship
                        $this->db->query("DELETE from services_relationship where service_id = '" . $service_id . "'
                            and relation_term = 'supervisor'");
                        $this->db->insert('services_relationship', array('service_id' => $service_id, 'relation_term' => 'supervisor', 'relation_value' => $data->supervisor));
                    }

                    if ($data->ejecutivo != '') {

                        //inserting doctor relationship
                        $this->db->query("DELETE from services_relationship where service_id = '" . $service_id . "'
                            and relation_term = 'ejecutivo'");
                        $this->db->insert('services_relationship', array('service_id' => $service_id, 'relation_term' => 'ejecutivo', 'relation_value' => $data->ejecutivo));
                    }

                    /* METER TODOS LOS DATOS EN LA AGENDA*/
                    $this->servicios_lib->do_schedule();

                    $results = array(
                        'status' => 'success',
                        'action' => 'redirect',
                        'module' => "" . base_url() . 'admin' . "/pacientes",
                        'message' => $response,
                    );

                    echo json_encode($results);

                    break;
            }

        } else {
            exit('No direct script access allowed');
        }

    }

    public function doctor()
    {

        switch ($this->input->post('action')) {
            case 'add':
                $this->load->view($this->config->item('ci_my_admin_template_dir_admin') . "modals/_doctors_create");
                break;
            case 'save':

                $username = $this->input->post('Email');
                $password = $this->input->post('Password');
                $email = $this->input->post('Email');
                $group_id = array($this->input->post('GroupID'));

                $additional_data = array(
                    'first_name' => $this->input->post('FirstName'),
                    'last_name' => $this->input->post('LastName'),
                    'username' => $this->input->post('Email'),
                    'phone' => $this->input->post('Phone'),
                );

                $user = $this->ion_auth->register($email, $password, $email, $additional_data, $group_id);

                if (!$user) {
                    $errors = $this->ion_auth->errors();
                    echo $errors;
                    die('done');
                }

                $user_data = $this->ion_auth->user($user)->row();

                $code = '<option value="' . $user_data->id . '" selected>' . $user_data->first_name . ' ' . $user_data->last_name . '</option>';

                $results = array(
                    'action' => 'append',
                    'element' => 'Asignar_Doctor',
                    'code' => $code,
                    'status' => 'true',

                );

                echo json_encode($results);

                break;
        }

    }

    public function select()
    {

        if ($this->input->is_ajax_request()) {

            if ($this->input->post('action') == 'get') {

                if ($this->input->post('module') == "price") {

                    $service_id = $this->input->post('Service');
                    if ($service_id > 0) {
                        $service = $this->options->get($service_id);
                        $data = json_decode($service->option_value);
                        /*$columns = $data->daysperweek;
                        $rows = $data->timesperday;*/
                        $price = $data->price;
                        $cost = $data->cost;

                        echo $price . "-" . $cost;

                    } else {
                        echo '<p class="alert alert-warning" id="routine_warning">Por favor selecciona un servicio a contratar.</p>';

                    }
                } else {
                    $service_id = $this->input->post('Service');
                    if ($service_id > 0) {
                        $service = $this->options->get($service_id);
                        $data = json_decode($service->option_value);
                        $columns = $data->daysperweek;
                        $rows = $data->timesperday;

                        $html = '<thead>';

                        $html .= '<tr class="headings">';

                        for ($x = 0; $x < $columns; $x++) {
                            $day = $x + 1;
                            $html .= '<th class="column-title align-center">
                                <select name="RoutineDay[]" class="form-control">
                                    <option value="Lunes">Lunes</option>
                                    <option value="Martes">Martes</option>
                                    <option value="Miercoles">Miércoles</option>
                                    <option value="Jueves">Jueves</option>
                                    <option value="Viernes">Viernes</option>
                                    <option value="Sabado">Sábado</option>
                                    <option value="Domingo">Domingo</option></select></th>';
                        }

                        $html .= '</tr>';

                        $html .= '</thead><tbody>';

                        for ($i = 0; $i < $rows; $i++) {

                            $html .= '<tr>';

                            for ($x = 0; $x < $columns; $x++) {
                                $html .= '<td id="' . $i . '_' . $x . '" style="text-align: center;">
                                        <a href="' . base_url() . 'admin/services/routines" class="routines" id="routines-' . $i . '_' . $x . '" title="Añadir Rutina" data-rel="add"><i class="fa fa-plus"></i></a>
                                </td>';
                            }

                            $html .= '</tr>';

                        }

                        $html .= '</tbody>';

                        echo $html;

                    } else {
                        echo '<p class="alert alert-warning" id="routine_warning">Por favor selecciona un servicio a contratar.</p>';
                    }
                }

            } else if ($this->input->post('action') == 'add') {

                $columns = $this->input->post('columns');
                $rows = $this->input->post('rows');

                $i = $rows + 1;

                $html = '';

                $html .= '<tr>';

                for ($x = 0; $x < $columns; $x++) {
                    $html .= '<td id="' . $i . '_' . $x . '" style="text-align: center;">
                                    <a href="' . base_url() . 'admin/services/routines" class="routines" id="routines-' . $i . '_' . $x . '" title="Añadir Rutina" data-rel="add"><i class="fa fa-plus"></i></a>
                            </td>';
                }

                $html .= '</tr>';

                $results = array(
                    'action' => 'append',
                    'element' => 'table-routines-rows',
                    'code' => $html,
                    'status' => 'true',

                );

                echo json_encode($results);

            } else {

                $this->load->view($this->config->item('ci_my_admin_template_dir_admin') . "modals/_add_routine_rows");

            }

        } else {
            exit('No direct access allowed');
        }

    }

    public function routines()
    {

        if ($this->input->is_ajax_request()) {

            if ($this->input->post('action') == 'add') {
                $data = array(
                    'rutina_tipo_actividad' => [],
                    'rutina_descripcion' => []
                );

                $data['rutina_tipo_actividad'] = $this->options->get_all('*', array('option_name' => '_rutina_tipo_actividad'));

                $d = explode("-", $this->input->post('data_routine'));

                if (sizeof($d) > 0) {
                    $explode_tipo_act = explode("_", $d[0]);
                    $explode_descripcion = explode("_", $d[1]);

                    $data['tipoActividad'] = $explode_tipo_act[0];
                    $data['hora'] = $d[2];

                    if($explode_tipo_act[0] == TIPO_ACTIVIDAD_OTRO) {
                        $data['descripcion'] = $explode_descripcion[1];
                    } else {
                        $data['descripcion'] = $explode_descripcion[0];
                    }
                    
                    if ($explode_tipo_act[0] == TIPO_ACTIVIDAD_COTIDIANA) {
                        $data['rutina_descripcion'] = $this->options->get_all('*', array('option_name' => '_rutina_descripcion_otras_actividades'));
                    }
        
                    if ($explode_tipo_act[0] == TIPO_ACTIVIDAD_ESTIMULACION_COGNITIVA) {
                        $data['rutina_descripcion'] = $this->options->get_all('*', array('option_name' => '_rutina_descripcion_estimulacion_cognitiva'));
                    }
        
                    if ($explode_tipo_act[0] == TIPO_ACTIVIDAD_ESTIMULACION_FISICA) {
                        $data['rutina_descripcion'] = $this->options->get_all('*', array('option_name' => '_rutina_descripcion_estimulacion_fisica'));
                    }
                }

                $this->load->view($this->config->item('ci_my_admin_template_dir_admin') . "modals/_add_routines", $data);

            } else {

                $str_tipo_actividad = '';
                $str_descripcion = '';
                $str_hora = '';

                $str_tipo_actividad_completo = '';
                $str_descripcion_completo = '';

                $id_tipo_actividad = 0;
                $id_descripcion = 0;

                if($this->input->post('tipoActividad')) {
                    $tipo_actividad = $this->options->get_all('*', array('id' => $this->input->post('tipoActividad')));
                    if(isset($tipo_actividad)) {
                        $option_actividad = json_decode($tipo_actividad[0]["option_value"]);
                        $str_tipo_actividad = $option_actividad->name;
                        $id_tipo_actividad = $tipo_actividad[0]['id'];

                        if($tipo_actividad[0]['id'] == TIPO_ACTIVIDAD_OTRO) {
                            $str_descripcion = $this->input->post('descripcionActividadOtro');
                        } else {
                            $descripcion = $this->options->get_all('*', array('id' => $this->input->post('descripcion')));
                            if(isset($descripcion)) {
                                $option_descripcion = json_decode($descripcion[0]["option_value"]);
                                $str_descripcion = $option_descripcion->name;
                                $id_descripcion = $descripcion[0]["id"];
                            }
                        }
                    }
                }

                if($this->input->post('hora')) {
                    $str_hora = $this->input->post('hora');
                }

                $td = $this->input->post('td');

                $code = '<a id="routines-'.$td.'" data-routine="'.$id_tipo_actividad . '_' . $str_tipo_actividad.'-'.$id_descripcion . '_' .$str_descripcion.'-'.$str_hora.'" class="routines" href="'.base_url().'admin/services/routines" title="Añadir Rutina" data-rel="add" >
                <input type="hidden" name="RoutineTipoActividad" class="RoutineTipoActividad" value="' . $id_tipo_actividad . "_" . $str_tipo_actividad . '">
               <input type="hidden" name="RoutineDescripcion" class="RoutineDescripcion" value="' . $id_descripcion . "_" .$str_descripcion.'">
               <input type="hidden" name="RoutineHora" class="RoutineHora" value="'.$str_hora.'">
               <span>' . $str_tipo_actividad . ' - ' . $str_descripcion . ' - ' . $str_hora . '</span>
               </a>
               ';

                $results = array(
                    'action' => 'append',
                    'element' => 'parent',
                    'code' => $code,
                    'td' => $td,
                    'status' => 'true',

                );

                echo json_encode($results);

            }

        } else {

        }
    }

    /**
     * Metodo que se encarga de recuperar la descripcion de la rutina por tipo de actividad
     *
     * @return json
     */
    public function descripcionPorActividad()
    {
        $data = array();
        if ($this->input->is_ajax_request()) {
            $tipoActividad = $this->input->post('tipoActividad');
            $rutinaDescripcion = '';

            if ($tipoActividad == TIPO_ACTIVIDAD_COTIDIANA) {
                $data = $this->options->get_all('*', array('option_name' => '_rutina_descripcion_otras_actividades'));
            }

            if ($tipoActividad == TIPO_ACTIVIDAD_ESTIMULACION_COGNITIVA) {
                $data = $this->options->get_all('*', array('option_name' => '_rutina_descripcion_estimulacion_cognitiva'));
            }

            if ($tipoActividad == TIPO_ACTIVIDAD_ESTIMULACION_FISICA) {
                $data = $this->options->get_all('*', array('option_name' => '_rutina_descripcion_estimulacion_fisica'));
            }

            echo json_encode($data);

        } else {
            exit('No direct access allowed');
        }
    }

    public function price()
    {

        if ($this->input->is_ajax_request()) {
            if ($this->input->post('action') == 'get') {

                $service_id = $this->input->post('Service');
                if ($service_id > 0) {
                    $service = $this->options->get($service_id);
                    $data = json_decode($service->option_value);
                    $price = $data->price;
                }

                echo $price;

            }

        } else {

        }

    }

    /**
     * Pantalla vista del servicio
     *
     * @param [type] $paciente_id
     * @param [type] $service_id
     * @return void
     */
    public function view($paciente_id, $service_id)
    {

        $data['reports_type'] = array('horarios' => 'Horarios', 'registros' => 'Registros', 'desempeno_luz' => 'Desempeño de la Luz', 'historial_reportes' => 'Historial de Reportes');

        $data['paciente_id'] = $paciente_id;
        $data['service_id'] = $service_id;

        $start_day = $this->input->post('start_date');
        $end_day = $this->input->post('end_date');

        if (!isset($start_day)) {
            $f = new DateTime("first day of this month");
            $t = new DateTime("last day of this month");
            $refresh = 0;
        } else {
            $f = new DateTime($start_day);
            $t = new DateTime($end_day);
            $refresh = 1;
        }

        $start_day = $f->format("Y-m-d");
        $end_day = $t->format("Y-m-d");

        $now = new DateTime();

        $data['from'] = $start_day;
        $data['to'] = $end_day;

        $horarios = $this->report_maker->get_horarios_services_from_tars($paciente_id, $service_id, $start_day, $end_day);

        $horarios['horas'] = array();
        $horarios['inasistencia'] = array();
        $horarios['retardo'] = array();

        $agendados = $this->report_maker->get_data_from_services($service_id, $start_day, $end_day);

        $luces_asignadas = array();

        /* HORARIOS */
        /* Calcular horas trabajadas, inasistencias, retardos  y luces asignadas */
        foreach ($agendados['entrada'] as $date => $time) {
            $real_start_time = strtotime($horarios['entrada'][$date]);
            $real_end_time = strtotime($horarios['salida'][$date]);
            $tolerancia = strtotime("+ 15 minutes", strtotime($time));

            if ($date <= $now->format("d-m-Y")) {

                if ($horarios['entrada'][$date] == "") {
                    $horarios['inasistencia'][$date] = 1;
                } else if ($real_start_time > $tolerancia) {
                    $horarios['retardo'][$date] = 1;
                }
            }

            if ($real_start_time != "" && $real_end_time != "") {
                $horarios['horas'][$date] = gmdate("H:i", $real_end_time - $real_start_time);
            }

            if (!in_array($agendados['luz'][$date], $luces_asignadas)) {
                $luces_asignadas[] = $agendados['luz'][$date];
            }
        }

        $data['horarios'] = $horarios;
        $data['agendados'] = $agendados;
        $data['luces_asignadas'] = $luces_asignadas;
        $data['current_schedule'] = $this->report_maker->get_current_schedule($service_id);
        $data['precio_de_venta'] = $this->report_maker->get_service_price($service_id);
        $data['costo'] = $this->report_maker->get_service_cost($service_id);

        /*REGISTROS*/
        $data['registros'] = $this->report_maker->get_registros_services($paciente_id, $service_id, $start_day, $end_day);

        /* DESEMPEÑO LUZ*/
        $paciente = $this->paciente->get($paciente_id);
        $service = $this->service->get($service_id);


        $name = json_decode($paciente->name);
        $data['first_name'] = $name->first_name . " " . $name->second_name;
        $data['last_name'] = $name->last_name . " " . $name->second_last_name;

        $data['desempeno_luz'] = $this->report_maker->get_desempeno_luz($service_id, date('Y-m-d', strtotime($service->start_date)), $start_day, $end_day);
        $data['dias_rutinas_arr'] = $this->report_maker->get_dias_rutinas($service_id);

        $data['tars_id'] = $paciente->tars_id;

        $query = $this->db->query("SELECT service_value from services_meta
                                   where service_meta = '_monthly_report_file'
                                   and service_id = '" . $service_id . "'
                                   ORDER by timestamp DESC");

        $data['historial'] = $query->result();

        if ($refresh == 0) {
            /*FIRST TIME */
            $data['page'] = $this->config->item('ci_my_admin_template_dir_admin') . "services_view";
            $this->load->view($this->_container, $data);

        } else {
            /*USING START - END FILTERS */
            $data['page'] = $this->config->item('ci_my_admin_template_dir_admin') . "services_view";
            $this->load->view($this->_container, $data);

            return json_encode($data);

        }

    }
}
