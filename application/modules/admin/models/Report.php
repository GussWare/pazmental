<?php (defined('BASEPATH')) or exit('No direct script access allowed');

class Report extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Metodo que se encarga de recuperar a los pacientes para revisar los reportes
     *
     * @param array $params
     * @return array
     */
    public function listado($params = array())
    {
        $this->db->select("DISTINCT pacientes.id, pacientes.nombre_familia, pacientes.name", false);
        $this->db->from("pacientes");

        if ($params['group_name'] == "familiares") {
            $this->db->join("pacientes_relationship", "pacientes.id = pacientes_relationship.paciente_id", "inner");
            $this->db->where("pacientes_relationship.relation_term", "familiar");
            $this->db->where("pacientes_relationship.relation_value", $params['user_id']);
        }

        $query = $this->db->get();

        $data = array();
        if ($query->num_rows() > 0) {
            $data = $query->result_array();
        }

        return $data;
    }

    /**
     * Metodo que se encarga de retornar los reportes diarios por pacientes
     *
     * @param array $params
     * @return void
     */
    public function get_reports_daily($params = array())
    {
        $this->db->select("pacientes.*");
        $this->db->select("services.*, services.id AS servicio_id");
        $this->db->select("reports.*, reports.service_id AS report_service_id, reports.nurse_id AS report_nurse_id");
        $this->db->from("pacientes");
        $this->db->join("services", "pacientes.id = services.paciente_id", "inner");
        $this->db->join("reports", "services.tars_id = reports.service_id", "inner");
        $this->db->where("pacientes.id", $params['paciente_id']);
        $this->db->where("DATE(reports.timestamp)", $params['timestamp']);
        $this->db->order_by("services.tars_id");
        $this->db->order_by("reports.timestamp", "ASC");

        $query = $this->db->get();
        $data = array();


        log_message("debug", "-------------------- QUERY get_reports_daily ---------------------");
        log_message("debug", $this->db->last_query());
        log_message("debug", "-------------------- QUERY get_reports_daily ---------------------");

        if($query->num_rows() > 0) {
            $data = $query->result();
        }

        return $data;
    }

    /**
     * Metodo que se encarga de recuperar todos los datos para el reporte mensual
     *
     * @param array $params
     * @return void
     */
    public function get_report_mensual($params = array()) 
    {
        $this->db->select("pacientes.*");
        $this->db->select("services.*, services.id AS servicio_id");
        $this->db->select("reports.*, reports.id AS report_id, reports.service_id AS report_service_id");
        $this->db->from("pacientes");
        $this->db->join("services", "pacientes.id = services.paciente_id", "inner");
        $this->db->join("reports", "services.tars_id = reports.service_id", "inner");
        $this->db->where("pacientes.id", $params["paciente_id"]);
        $this->db->where("DATE(reports.timestamp) >=", $params['start_day']);
        $this->db->where("DATE(reports.timestamp) <=", $params['end_day']);
        $this->db->order_by("reports.timestamp","ASC");

        $query = $this->db->get();

        log_message("debug", "-------------  QUERY REPORTE MENSUAL  ---------------");
        log_message("debug", $this->db->last_query());
        log_message("debug", "-------------  QUERY REPORTE MENSUAL  ---------------");

        $data = array();
        if($query->num_rows() > 0) {
            $data = $query->result();
        }
        return $data;
    }
}
