<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h2>
                Usuarios
                <a  href="<?= base_url('admin/users') ?>" class="btn btn-warning">Volver al Listado</a>
            </h2>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                   Actualizar Usuario
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-6">
                            <form role="form" method="POST" action="<?=base_url('admin/users/edit/'.$user->id)?>">
                                <div class="form-group">
                                    <label>User Id</label>
                                    <input class="form-control" value="<?=$user->id?>" placeholder="Auto generated" disabled="1">
                                </div>
                                <div class="form-group">
                                    <label>Username</label>
                                    <input class="form-control" value="<?=$user->username?>" placeholder="Auto generated" disabled="1">
                                </div>
                                <div class="form-group">
                                    <label>Email</label>
                                    <input class="form-control" value="<?=$user->email?>" placeholder="Enter Email" id="email" name="email">
                                </div>
                                <div class="form-group">
                                    <label>User Group</label>
                                    <select class="form-control" id="group_id" name="group_id">
                                        <?php foreach ($groups as $group): 
                                        if($group->name == "admin"  or $group->name == "excecutives" or $group->name =="supervisor"  ){?>
                                        <option value="<?=$group->id?>" <?=ui_selected_item($user_group->name,$group->name)?><?=$group->name?>><?php echo $group->description;?></option>
                                        <?php }else{ ?>
                                        <option value="<?=$group->id?>" <?=ui_selected_item($user_group->name,$group->name)?><?=$group->name?> disabled><?php echo $group->description;?></option> 
                                        
                                            <?php } endforeach; ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>First Name</label>
                                    <input class="form-control" value="<?=$user->first_name?>" placeholder="Enter Firstname" id="first_name" name="first_name">
                                </div>
                                <div class="form-group">
                                    <label>Last Name</label>
                                    <input class="form-control" value="<?=$user->last_name?>" placeholder="Enter Lastname" id="last_name" name="last_name">
                                </div>
                                <div class="form-group">
                                    <label>Phone</label>
                                    <input class="form-control" value="<?=$user->phone?>" placeholder="Enter Phone" id="phone" name="phone">
                                </div>
                                
                                <?php if(($user_group->id == 5 || $user_group->id == 6) && count($services) > 0){ ?>

                                <a href="<?php echo base_url();?>admin/users/ajax" class="bootbox btn btn-info" id="relationship" data-rel="add" title="Agregar Relación">
                                   <span class="fa fa-plus"></span> Agregar a Servicio
                                </a>

                                <h4>Servicios asignados</h4>

                                <table class="table col-md-12" id="table-relationship">
                      <thead>
                        <tr>
                          <th>ID</th>
                          <th>Nombre</th>
                        </tr>
                      </thead>
                      <tbody>
                       <?php if($services_added){
                            foreach($services_added as $service): ?>

                            <tr>
                            <td><?php echo $service->id; ?></td>
                            <td><?php echo $service->name; ?></td>

                            </tr>

                    <?php endforeach; 
                       } ?>

                     </tbody>
                </table>
                                        
                                <?php  } ?>

                                <div class="clearfix"></div>
                                <button type="submit" class="btn btn-primary">Guardar</button>
                            </form>
                        </div>


                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
<!-- /#page-wrapper -->
