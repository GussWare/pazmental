<?php (defined('BASEPATH')) OR exit('No direct script access allowed');


class Ajax extends Admin_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model(array('module'));
        $this->load->model(array('service'));
        $this->load->model(array('familiar'));
    }

    public function index() {
       
    }

    public function serviceAddFamiliar(){
	
    }
   
    public function serviceAssignNurse(){

    }

    public function serviceGetNursesList() {

    }

    public function serviceAddRoutineTable(){

    }

    public function serviceAddRoutines() {

    }
}
