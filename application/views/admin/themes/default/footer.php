  <!-- footer content -->
        <footer>
          <div class="pull-right">
           &copy; <?php echo date('Y'); ?> - Paz Mental All rights reserved.<strong> Versión: <?php echo $this->config->item('numero', 'version'); ?> </strong>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="<?php echo base_url();?>assets/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo base_url();?>assets/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo base_url();?>assets/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo base_url();?>assets/vendors/nprogress/nprogress.js"></script>
    <!-- iCheck -->
    <script src="<?php echo base_url();?>assets/vendors/iCheck/icheck.min.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="<?php echo base_url();?>assets/vendors/moment/min/moment.min.js"></script>
    <script src="<?php echo base_url();?>assets/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
    <!-- bootstrap-wysiwyg -->
    <script src="<?php echo base_url();?>assets/vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js"></script>
    <script src="<?php echo base_url();?>assets/vendors/jquery.hotkeys/jquery.hotkeys.js"></script>
    <script src="<?php echo base_url();?>assets/vendors/google-code-prettify/src/prettify.js"></script>
    <!-- jQuery Tags Input -->
    <script src="<?php echo base_url();?>assets/vendors/jquery.tagsinput/src/jquery.tagsinput.js"></script>
    <!-- Switchery -->
    <script src="<?php echo base_url();?>assets/vendors/switchery/dist/switchery.min.js"></script>
    <!-- Select2 -->
    <script src="<?php echo base_url();?>assets/vendors/select2/dist/js/select2.full.min.js"></script>
    <!-- Autosize -->
    <script src="<?php echo base_url();?>assets/vendors/autosize/dist/autosize.min.js"></script>
    <!-- jQuery autocomplete -->
    <script src="<?php echo base_url();?>assets/vendors/devbridge-autocomplete/dist/jquery.autocomplete.min.js"></script>
    <!-- Ion.RangeSlider -->
    <script src="<?php echo base_url();?>assets/vendors/ion.rangeSlider/js/ion.rangeSlider.min.js"></script>
    <!-- Bootstrap Colorpicker -->
    <script src="<?php echo base_url();?>assets/vendors/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
    <!-- jquery.inputmask -->
    <script src="<?php echo base_url();?>assets/vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>
    <!-- jQuery Knob -->
    <script src="<?php echo base_url();?>assets/vendors/jquery-knob/dist/jquery.knob.min.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="<?php echo base_url();?>assets/vendors/fullcalendar/dist/fullcalendar.min.js"></script>

    <!-- Bootstrap Clock Picker -->
     <script src="<?php echo base_url();?>assets/vendors/clockpicker-gh-pages/dist/bootstrap-clockpicker.min.js">
    <!-- starrr -->
    <script src="<?php echo base_url();?>assets/vendors/starrr/dist/starrr.js"></script>
    <script src="<?php echo base_url();?>assets/build/js/bootbox.js"></script>
    <script src="<?php echo base_url();?>assets/vendors/jQuery-Smart-Wizard/js/jquery.smartWizard.js"></script>

    <script src="<?php echo base_url();?>assets/vendors/Chart.js/dist/Chart.min.js"></script>
    <script src="<?php echo base_url();?>assets/vendors/pnotify/dist/pnotify.js"></script>
    <script src="<?php echo base_url();?>assets/vendors/pnotify/dist/pnotify.buttons.js"></script>
    <script src="<?php echo base_url();?>assets/vendors/pnotify/dist/pnotify.nonblock.js"></script>
    <script src="<?php echo base_url();?>assets/build/js/custom.min.js?v=<?php echo $this->config->item('numero', 'version'); ?>"></script>
    
    <!--MAPS -->

  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCc1EsV4I_N8PjE96ilELHDPtdBHvCT46c"></script>
  
  <script src="<?php echo base_url();?>assets/vendors/gmaps.js"></script>

    
    <!-- REPORTS -->
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqcloud/1.0.4/jqcloud-1.0.4.js"></script>
    <!-- /calendar modal -->

    <!-- PDF -->
     <script src="<?php echo base_url();?>assets/vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="<?php echo base_url();?>assets/vendors/pdfmake/build/vfs_fonts.js"></script>
    
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/build/js/custom_print.js?v=<?php echo $this->config->item('numero', 'version'); ?>"></script>
    <script src="<?php echo base_url();?>assets/vendors/domtoimage/dom-to-image.min.js"></script>
    

    <?php 

    $module_path = $this->router->fetch_class().'/'.$this->router->fetch_method();


    if($module_path == 'nurses/create'){ ?>

    <script src="<?php echo base_url();?>assets/vendors/dropzone/dist/min/dropzone.min.js"></script>
    
    <script> jQuery(document).ready(function(){
        init_dropzone();
        initMap();

    });</script>

     <?php }  if($module_path == 'services/create' || $module_path == 'services/edit'){ ?>


    
    <script> jQuery(document).ready(function(){
        
        initServiceMap();
        initMap();
        initMap_update();
        init_edit_rows();

    });</script>
    <?php } ?>

     <?php 

    $module_path = $this->router->fetch_class().'/'.$this->router->fetch_method();



      if($module_path == 'nurses/edit'){ ?>
    <script src="<?php echo base_url();?>assets/vendors/dropzone/dist/min/dropzone.min.js"></script>
    
    <script> jQuery(document).ready(function(){
        init_dropzone_update();
        initMap_update();

    });</script>
    <?php } 

    else if($module_path == 'nurses/pdf' or $module_path == 'reports/view' or $module_path == 'services'){ ?>
    <script src="<?php echo base_url();?>assets/vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="<?php echo base_url();?>assets/vendors/pdfmake/build/vfs_fonts.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/build/js/custom_print.js?v=<?php echo $this->config->item('numero', 'version'); ?>"></script>
    
    <!--<script> jQuery(document).ready(function(){
        init_dropzone_update();
        initMap_update();

    });</script>-->
    <?php }
    else if ($module_path =='reports/edit'){?>
    <script src="<?php echo base_url();?>assets/vendors/dropzone/dist/min/dropzone.min.js"></script>
    
    <script> jQuery(document).ready(function(){
	init_reports(); 
          
    });</script>
    <?php } ?>

    <!-- Custom Theme Scripts -->

    <script type="text/javascript" src="<?php echo base_url(); ?>assets/pazmental/script.js"></script>

  </body>
</html>
