
 <!-- page content -->

          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3><span class="fa fa-user-md"></span> Luces</h3>
              </div>

                <div class="panel_toolbox"><a  href="<?= base_url('admin/nurses') ?>" class="btn btn-success"><span class="fa fa-angle-double-left"></span> Regresar</a></div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Crear nueva Luz</h2>
                    
                    <div class="clearfix"></div>
                     <div class="message_center" style="color:#B22222">  </div>
                  
                  </div>

                  <div class="x_content">
                        <form role="form" method="POST" action="<?=base_url('admin/users/create')?>" id="create-nurse">
                        <h4>Datos de la Cuenta</h4>
                          <div class="row">
                                <div class="form-group col-md-4 col-sm-4">
                                    <label>User Id</label>
                                    <input class="form-control" placeholder="Auto generated" disabled="1">
                                </div>
                                <div class="form-group col-md-4 col-sm-4 div-ocultar">
                                    <label>Número de Luz</label>
                                    <input class="form-control"  name="numero_luz" id="numero_luz" >
                                </div>
                                 <div class="form-group col-md4 col-sm-4">
                                    <label>Fecha de Alta</label>
                                    <div class="control-group">
                         
                                          <div class="xdisplay_inputx form-group has-feedback">
                                            <input type="text" class="form-control has-feedback-left single_cal_picker" id="start_date" name="start_date" placeholder="Fecha de Alta" aria-describedby="inputSuccess2Status2" required>
                                            <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                            <span id="inputSuccess2Status2" class="sr-only">(success)</span>
                                          </div>
                                    </div>
                                </div>
                              </div>
                              <div class="row">
                                <div class="form-group col-md-4 col-sm-4">
                                    <label>Nombre de Usuario</label>
                                    <input class="form-control"  placeholder="Ingresa nombre de usuario" id="username" name="username"  required>
                                </div>
                                <div class="form-group col-md-4 col-sm-4">
                                    <label>Email</label>
                                    <input class="form-control" placeholder="Ingresa E-mail" id="email" name="email"  required>
                                </div>
                                 <div class="form-group col-md-4 col-sm-4">
                                    <label>Contraseña</label>
                                    <input type="password" class="form-control" placeholder="Ingresa Contraseña" id="password" name="password"  required>
                                </div>
                                <div class="form-group col-md-4 col-sm-3">
                                                            <label for="">
                            Supervisora a Cargo
                          </label>
                          <select name="supervisor" id="" class="form-control">
                            <option value="0">Ninguno</option>

                            <?php foreach($supervisors as $supervisor):
                              $name = $supervisor->first_name. ' '.$supervisor->last_name; ?>
                            <option value="<?php echo $supervisor->id; ?>"   >
                              <?php echo $name; ?>
                            </option>
                          <?php endforeach; ?>
                          </select>
                                </div>
                              <div class="form-group col-md-4 col-sm-4">
                                    <label>Grupo de Usuario</label>
                                   <select class="form-control" id="group_id" name="group_id" required>
                                        <?php foreach ($groups as $group): ?>
                                            <?php if($group->id == 3 || $group->id == 4) { ?>
                                                 <option value="<?=$group->id?>"><?=$group->description?></option>

                                            <?php } ?>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="form-group col-md-4 col-sm-4">
                                    <label>Tipo de luz</label>
                                    <select class="form-control" id="id_tipo_luz" name="id_tipo_luz" required>
                                        <?php foreach($tipos_luz AS $tipo_luz): ?>
                                          <option value="<?php echo $tipo_luz->id; ?>"><?php echo $tipo_luz->nombre; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="clearfix"></div>
                                
                        <h4>Datos Personales</h4>

                                <div class="form-group col-md-4 col-sm-4">
                                    <label>Nombre (s)</label>
                                    <input class="form-control" placeholder="Ingresa Primer y Segundo Nombre" id="first_name" name="first_name"  required>
                                </div>
                                <div class="form-group col-md-4 col-sm-4">
                                    <label>Apellidos</label>
                                    <input class="form-control" placeholder="Ingresa Apellidos" id="last_name" name="last_name"  required>
                                </div>
                                <div class="form-group col-md-4 col-sm-4">
                                    <label>Fecha de Nacimiento</label>
                                   <div class="control-group">
                                          <div class="xdisplay_inputx form-group has-feedback">
                                            <input type="text" class="form-control has-feedback-left single_cal_picker" id="" name="birth_date" placeholder="Fecha de Nacimiento" aria-describedby="inputSuccess2Status4" required>
                                            <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                            <span id="inputSuccess2Status4" class="sr-only">(success)</span>
                                          </div>
                                    </div>
                          
                                </div>
                                <div class="form-group col-md-3 col-sm-3">
                                    <label>Edad</label>
                                    <input class="form-control" placeholder="Enter age" id="age" name="age">
                                </div>
                                 <div class="form-group col-md-2 col-sm-2">
                                     <label>Género:</label>
                                          <p>
                                            M:
                                            <input type="radio" class="flat" name="pm_gender" id="genderM" value="M" checked="" required /> F:
                                            <input type="radio" class="flat" name="pm_gender" id="genderF" value="F" />
                                          </p>
                                </div>
                                 <div class="form-group col-md-4 col-sm-4">
                                    <label>Estado Civil</label>
                                    <select class="form-control" name="civil_status" required>
                                      <option value="">Selecciona Opción</option>
                                      <option value="single">Soltero (a)</option>
                                      <option value="married">Casado (a)</option>
                                      <option value="divorced">Divorciado (a)</option>
                                      <option value="widow">Viudo (a)</option>

                                    </select>

                            
                                </div>

                                <div class="form-group col-md-4 col-sm-4 div-ocultar div-ocultar">
                                    <label>CURP</label>
                                    <input class="form-control" placeholder="Enter CURP" id="curp" name="curp"  required>
                                </div>

                                 <div class="div-ocultar">
                                 <div class="form-group col-md-3 col-sm-3">
                                    <label>Número de Dependientes Económicos</label>
                                    <select name="economical_dependants"  class="form-control onChangeNurse" data-target="<?php echo base_url(); ?>admin/nurses/select" id="economical_dependants" >
                                        <option value="0">0</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>

                                    </select>
                                </div>
                                </div>

                                <div class="div-mostrar">
                                  <div class="form-group col-md-4 col-sm-4">
                                    <label>Num. Hijos</label>
                                    <input class="form-control" placeholder="Enter Hijos" id="num_hijos" name="num_hijos"  required value="<?php echo $nurse_meta['_num_hijos']; ?>">
                                  </div>
                                </div>

                                 <div class="clearfix"></div>
                                <div id="dependant-loop">
                                </div>
                                <div class="clearfix"></div>

                                

                                <div class="div-ocultar">
                                  <div class="form-group col-md-9 col-sm-9">
                                      <label>Características Físicas</label>
                                      <div class="clearfix"></div>
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                      <div  id="fisical">
                                      <?php foreach($characteristics as $char):
                                      
                                        $info = json_decode($char['option_value']); ?>
                                      
                                      <label class="checkbox-inline"><input  name="fisical" type="checkbox" value=" <?php echo $char['id']; ?>" ><?php echo $info->name; ?></label>
  
                                      <?php endforeach; ?>
                                      </div></div>

                                        
                                      </div>
                                  </div>
                                </div>

                              <div class="clearfix"></div>

                              <div class="div-mostrar">
                                <h4>Referencias</h4>
                                <div class="form-group col-md-4 col-sm-4">
                                  <label>Número Telefonico 1</label>
                                  <input type="text" class="form-control" name="referencia_phone1" placeholder="Número teléfonico" value="<?php echo $nurse_meta['_referencia_phone1']; ?>"  />
                                </div>
                                <div class="form-group col-md-8 col-sm-8">
                                  <label>Nombre Referencia 1</label>
                                  <input type="text" class="form-control" name="referencia_nombre1" placeholder="Nombre de la persona Ref. 1" value="<?php echo $nurse_meta['_referencia_nombre1']; ?>"  />
                                </div>

                                <div class="form-group col-md-4 col-sm-4">
                                  <label>Número Telefonico 2</label>
                                  <input type="text" class="form-control" name="referencia_phone2" placeholder="Número teléfonico 2" value="<?php echo $nurse_meta['_referencia_phone2']; ?>"  />
                                </div>
                                <div class="form-group col-md-8 col-sm-8">
                                  <label>Nombre Referencia 2</label>
                                  <input type="text" class="form-control" name="referencia_nombre2" placeholder="Nombre de la persona Ref. 2" value="<?php echo $nurse_meta['_referencia_nombre2']; ?>"  />
                                </div>
                              </div>
                              <div class="clearfix"></div>

                                <h4>Dirección</h4>
                                
                                <div class="form-group col-md-4 col-sm-4">
                                      <label>Dirección y Número</label>
                                    <input type="text" class="form-control" id="address" name="address" placeholder="Ingresa dirección y número" required/>
                                </div>

                                 <div class="form-group col-md-4 col-sm-4">
                                      <label>Colonia</label>
                                    <input type="text" class="form-control" id="colony" name="colony" placeholder="Ingresa Colonia" required/>
                                </div>

                                 <div class="form-group col-md-4 col-sm-4">
                                      <label>Delegación o Municipio</label>
                                    <input type="text" class="form-control" id="region" name="region" placeholder="Ingresa Región o delegación" required/>
                                </div>

                                <div class="clearfix"></div>

                                <div class="div-ocultar">
                                 <div class="form-group col-md-4 col-sm-4">
                                      <label>Ciudad</label>
                                    <input type="text" class="form-control" id="city" name="city" placeholder="Ingresa ciudad" required/>
                                </div>

                                  <div class="form-group col-md-4 col-sm-4">
                                      <label>C.P.</label>
                                    <input type="text" class="form-control" id="zip" name="zip" placeholder="Ingresa código postal" required/>
                                </div>
                                

                               <div class="clearfix"></div>
                                  <div class="col-md-3 col-sm-3"><input type="hidden"   class="form-control" placeholder="Ingresa Latitud" id="map_lat"   /></div>
                                <div class="col-md-3 col-sm-3"><input type="hidden"  class="form-control"  placeholder="Ingresa Longitud" id="map_long"   /></div>
                                 <div class="clearfix"></div>
                                  <div class="col-md-4 col-sm-4">
                               <a    title="Actualizar Mapa" class="btn btn-info updateMap">Actualizar Mapa</a>
                               </div>

                                 <div class="form-group col-md-9 col-sm-9">
                                
                              
                                <div class="clearfix"></div>

                                 <div id="map" style="height:400px; min-width:400px;margin-top:20px"></div>

                             
                              </div>
                                </div>

    
                                <div class="clearfix"></div>
                                
                                  <h4>Datos de Contacto</h4>


                                <div class="form-group col-md-4 col-sm-4">
                                    <label>Teléfono</label>
                                    <input class="form-control" placeholder="Ingresa número de teléfono" id="phone" name="phone" required>
                                </div>
                                
                                <div class="form-group col-md-4 col-sm-4">
                                    <label>Celular</label>
                                    <input class="form-control" placeholder="Ingresa número de Celular" id="mobile" name="mobile" required>
                                </div>
                                <div class="clearfix"></div>

                                  <div class="div-ocultar">
                                  <h4>Datos de Contacto en Caso de Emergencia</h4>

                                  <div class="form-group col-md-3 col-sm-3">
                                    <label>Nombre</label>
                                    <input class="form-control" placeholder="Ingresa Nombre" id="emergency_name" name="emergency_name"  required>
                                </div>
                                   <div class="form-group col-md-3 col-sm-3">
                                    <label>Apellido</label>
                                    <input class="form-control" placeholder="Ingresa Apellido" id="emergency_last_name" name="emergency_last_name"  required>
                                </div>
                                   <div class="form-group col-md-3 col-sm-3">
                                    <label>Parentezco</label>
                                    <input class="form-control" placeholder="Ingresa Parentezco" id="emergency_relation" name="emergency_relation"  required>
                                </div>
                                <div class="form-group col-md-3 col-sm-3">
                                    <label>Teléfono</label>
                                    <input class="form-control" placeholder="Ingresa número de teléfono" id="emergency_phone" name="emergency_phone" required>
                                </div>
                                </div>

                                <div class="clearfix"></div>

                             <h4>Datos del Sistema</h4>

                              <div class="div-ocultar">
                              <div class="form-group col-md-3 col-sm-3">
                                     <label>Status:</label>
                                         <select class="form-control" id="status" name="status" required>
                                
                                                 <option value="1">Activo</option>
                                                 <option value="0">Suspendida</option>
                                                 <option value="-1">Baja</option>
                                                 <option value="-2">Renuncia</option>
                                      
                                    </select>
                                </div>

                                 <div class="form-group col-md-9 col-sm-9">
                                     <label>Motivo de Baja / Renuncia</label>
                                       <textarea class="resizable_textarea form-control"  name="fire_reason" placeholder="" ></textarea>
                                
                                </div>

                                <div class="clearfix"></div>


                              <div class="form-group col-md-3 col-sm-3">
                                     <label>Fuentes de Reclutamiento:</label>
                                         <select class="form-control" id="reclutamiento" name="reclutamiento" required>
                                                <option value="">Ninguno</option>
                                    <?php foreach($recruitment_sources as $recs):
                                      $info = json_decode($recs['option_value']); ?>
                                    <option value="<?php echo $recs['id']; ?>">
                                      <?php echo $info->name; ?>
                                    </option>
                                    <?php endforeach; ?>

                                    </select>
                                </div>
                                 <div class="form-group col-md-3 col-sm-3">
                                     <label>Grados de Estudio</label>
                                         <select class="form-control" id="grado_estudios" name="grado_estudios" required>
                                      <option value="">Ninguno</option>
                                      <?php foreach($institutions as $inst):
                                      $info = json_decode($inst['option_value']); ?>
                                    <option value="<?php echo $inst['id']; ?>">
                                      <?php echo $info->name; ?>
                                    </option>
                                    <?php endforeach; ?>

                                    </select>
                                </div>
                                </div>
                                <div class="form-group col-md-2 col-sm-2">
                                     <label>¿Esta luz es confiable?:</label>
                                          <p>
                                            Sí:
                                            <input type="radio" class="flat" name="luz_confiable" id="luz_confiable_si" value="Si" checked="" required /> No:
                                            <input type="radio" class="flat" name="luz_confiable" id="luz_confiable_no" value="No" />
                                          </p>
                                </div>

                                 <div class="div-mostrar">
                                    <div class="form-group col-md-3 col-sm-3">
                                      <label>Maxima escolaridad</label>
                                      <select class="form-control" id="max_escolaridad" name="max_escolaridad" required>
                                        <option value="">Ninguno</option>
                                        <?php foreach($max_escolaridad as $value): ?>
                                          <?php $esc = json_decode($value['option_value']); ?>
                                          <option value="<?php echo $value['id']; ?>" ><?php echo $esc->name; ?></option>
                                        <?php endforeach; ?>
                                      </select>
                                    </div>
                                  </div>
                                <div class="clearfix"></div>

                                <div class="form-group col-md-12 col-sm-12">
                                    <label>Observaciones Internas:</label>
                                    <textarea class="resizable_textarea form-control"  name="observaciones_internas" placeholder="" ></textarea>
                                </div>

                                <h4>Experiencia Laboral</h4>
                                <div class="div-mostrar">
                                  <div class="form-group col-md-4 col-sm-4">
                                    <label>Años de experiencia cuidando adultos</label>
                                    <input class="form-control" placeholder="Num. Años de experiencia" id="anos_experiencia" name="anos_experiencia" value="<?php echo $nurse_meta['_anos_experiencia']; ?>"required>
                                  </div>
                                </div>
                                <div class="clearfix"></div>

                                <h5>Conocimientos y Habilidades</h5>
                                <div class="col-md-6 col-sm-6">
                                  <h5>Cuidador</h5>
                                   <?php foreach($care_skills as $cs):
                                    
                                      $info = json_decode($cs['option_value']); ?>
                                    
                                     <div class="checkbox">
                                      <label ><input name="carer_skills" type="checkbox" value=" <?php echo $cs['id']; ?>" ><?php echo $info->name; ?></label>
                                    </div>
                                    <?php endforeach; ?>
                                 
                                </div>

                                <div class="div-ocultar">
                                <div class="col-md-6 col-sm-6">
                                  <h5>Terapeuta</h5>
                                     <?php foreach($therapy_skills as $ts):
                                    
                                      $info = json_decode($ts['option_value']); ?>
                                    
                                     <div class="checkbox">
                                      <label ><input name="therapist_skills" type="checkbox" value=" <?php echo $ts['id']; ?>" ><?php echo $info->name; ?></label>
                                    </div>
                                    <?php endforeach; ?>
                                 
                                </div>
                                </div>
              
                                 <div class="clearfix"></div>

                                <br><br>

                                <div class="div-ocultar">
                                <div class="form-group col-md-6 col-sm-6">
                                    <label>Personalidad</label>
                                     <?php foreach($personalities as $pers):
                                    
                                      $info = json_decode($pers['option_value']); ?>
                                    
                                     <div class="checkbox">
                                      <label ><input name="personalities" type="checkbox" value=" <?php echo $pers['id']; ?>" ><?php echo $info->name; ?></label>
                                    </div>
                                    <?php endforeach; ?>
                                 </div>

                                 <div class="form-group col-md-6 col-sm-6">
                                    <label>Pasatiempos</label>
                                       <?php foreach($hobbies as $hobby):
                                    
                                      $info = json_decode($hobby['option_value']); ?>
                                    
                                     <div class="checkbox">
                                      <label ><input name="hobbies" type="checkbox" value=" <?php echo $hobby['id']; ?>" ><?php echo $info->name; ?></label>
                                    </div>
                                    <?php endforeach; ?>
 
                                     
                                </div>
                              </div>
                              <div class="clearfix"></div>


                              <div class="div-ocultar">
                               <div class="col-md-12 col-sm-12">
                               <h4>Datos Bancarios</h4>


                                <label for="" class="form-group col-md-4 col-sm-4" required>
                                Banco
                                  <input type="text" name="bank_name" class="form-control">
                                </label>

                                <label for="" class="form-group col-md-4 col-sm-4" required>
                                  Datos de cuenta
                                  <input type="text" name="bank_account" class="form-control">
                                </label>


                                <label for="" class="form-group col-md-4 col-sm-4" required>
                                  CLABE
                                  <input type="text" name="bank_clabe" class="form-control">
                                </label>
                                 
                               </div>
                               </div>

                                 <div class="col-md-12 col-sm-12">
                               <h4>Archivos</h4>
                                <div class="form-group col-md-3 col-sm-3">
                                  <p class="file-label"> Fotografía de Perfil <div class="dropzone" id="myDropzone_profile"   data-target="<?php echo base_url('images/upload'); ?>" data-remove="<?php echo base_url('images/remove'); ?>" data-files="<?php echo base_url('images/list_files');?>"></div>
                                  </div>

                                <div class="form-group col-md-3 col-sm-3">
                                  <p class="file-label"> IFE <div class="dropzone" id="myDropzone_IFE"   data-target="<?php echo base_url('images/upload'); ?>" data-remove="<?php echo base_url('images/remove'); ?>" data-files="<?php echo base_url('images/list_files');?>"></div>
                                  </div>
                                


                                <div class="form-group col-md-3 col-sm-3">
                                  <p class="file-label"> Comprobante de Domicilio <div class="dropzone" id="myDropzone_CDomicilio"   data-target="<?php echo base_url('images/upload'); ?>" data-remove="<?php echo base_url('images/remove'); ?>" data-files="<?php echo base_url('images/list_files');?>"></div>
                                  </div>
                                

                                <div class="form-group col-md-3 col-sm-3">
                                  <p class="file-label"> CURP <div class="dropzone" id="myDropzone_CURP"   data-target="<?php echo base_url('images/upload'); ?>" data-remove="<?php echo base_url('images/remove'); ?>" data-files="<?php echo base_url('images/list_files');?>"></div>
                                  </div>
                                

                                <div class="form-group col-md-3 col-sm-3">
                                  <p class="file-label"> Comprobante de estudios <div class="dropzone" id="myDropzone_CEstudios"   data-target="<?php echo base_url('images/upload'); ?>" data-remove="<?php echo base_url('images/remove'); ?>" data-files="<?php echo base_url('images/list_files');?>"></div>
                                  </div>
                                

                                <div class="form-group col-md-3 col-sm-3">
                                  <p class="file-label"> Acta de Nacimiento <div class="dropzone" id="myDropzone_ANacimiento"   data-target="<?php echo base_url('images/upload'); ?>" data-remove="<?php echo base_url('images/remove'); ?>" data-files="<?php echo base_url('images/list_files');?>"></div>
                                  </div>
                                
                                  <div class="div-ocultar">

                                <div class="form-group col-md-3 col-sm-3">
                                  <p class="file-label"> Seguridad Social <div class="dropzone" id="myDropzone_SSocial"   data-target="<?php echo base_url('images/upload'); ?>" data-remove="<?php echo base_url('images/remove'); ?>" data-files="<?php echo base_url('images/list_files');?>"></div>
                                  </div>
                                

                                <div class="form-group col-md-3 col-sm-3">
                                  <p class="file-label"> RFC <div class="dropzone" id="myDropzone_RFC"   data-target="<?php echo base_url('images/upload'); ?>" data-remove="<?php echo base_url('images/remove'); ?>" data-files="<?php echo base_url('images/list_files');?>"></div>
                                  </div>
                                
                                <div class="form-group col-md-3 col-sm-3">
                                  <p class="file-label"> Carta de Antecedentes no Penales <div class="dropzone" id="myDropzone_penales"   data-target="<?php echo base_url('images/upload'); ?>" data-remove="<?php echo base_url('images/remove'); ?>" data-files="<?php echo base_url('images/list_files');?>"></div>
                                  </div>

                                <div class="form-group col-md-3 col-sm-3">
                                  <p class="file-label"> Estudio Socioeconómico <div class="dropzone" id="myDropzone_ESeconomico"   data-target="<?php echo base_url('images/upload'); ?>" data-remove="<?php echo base_url('images/remove'); ?>" data-files="<?php echo base_url('images/list_files');?>"></div>
                                  </div>

                                <div class="form-group col-md-3 col-sm-3">
                                  <p class="file-label"> Otros <div class="dropzone" id="myDropzone_otros"   data-target="<?php echo base_url('images/upload'); ?>" data-remove="<?php echo base_url('images/remove'); ?>" data-files="<?php echo base_url('images/list_files');?>"></div>
                                  </div>
                                </div>
                                <div class="clearfix"></div>
                               

                                 
                                <div class="col-md-3 col-sm-3">
                      <a href="<?php echo base_url(); ?>admin/nurses/save" id="createNurse" data-rel="1" title="Crear Luz" class="btn btn-info createNurse">Guardar</a>
                    </div>
                            </form>
                  </div>
                </div>
              </div>
            </div>
          </div>

        <!-- /page content -->