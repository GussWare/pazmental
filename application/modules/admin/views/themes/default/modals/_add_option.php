<form id="">
	<label for="">Nombre</label>
	<div class="form-group">
		<input type="text" name="OptionName" class="form-control" />
	</div>
	<label for="">Descripción</label>
	<div class="form-group">
		<input type="text" name="OptionDescription" class="form-control" />
	</div>

	<?php if($module == 'services') : ?>
<label for="">Veces por Semana</label>
	<div class="form-group">
		<input type="number" name="OptionTimesWeek" class="form-control" />
	</div>
	<label for="">Horas al día</label>
	<div class="form-group">
		<input type="text" name="OptionTimesDay" class="form-control" />
	</div>
	<label for="">Precio de Venta</label>
	<div class="form-group">
		<input type="number" name="OptionPrice" class="form-control" />
	</div>
	<label for="">Costo</label>
	<div class="form-group">
		<input type="number" name="OptionCost" class="form-control" />
	</div>
	<?php endif; ?>
</form>