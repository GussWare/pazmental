<?php

defined('BASEPATH') or exit('No direct script access allowed');

if (!function_exists('get_nombre_imagen')) {

    /**
     * Metodo que se encarga de recuperar de recuperar el nombre de la imagen del string
     * ya que en nurses las imagenes se guardan con el siguiente formato ["nomnreimagen.jpg"]
     * y deceamos obtener solamente el nombre
     */
    function get_nombre_imagen($imagen = null)
    {
        if (!isset($imagen) || empty($imagen)) {
            return '';
        }

        $nombre_img = '';
        $imagen_decode = json_decode($imagen);

        if (is_array($imagen_decode)) {
            $imagen = $imagen_decode[0];
        } else {
            $imagen = $imagen_decode;
        }

        $char_inicio = substr($imagen, 0, 1);
        $char_fin = substr($imagen, -1);

        if (($char_inicio == '[') && $char_fin == ']') {
            $nombre_img = substr($imagen, 2, -2);
            if (!file_exists(DIRECTORIO_IMAGENES_NURSES . $nombre_img)) {
                $nombre_img = '';
            }
        } else {
            if (file_exists(DIRECTORIO_IMAGENES_NURSES . $imagen)) {
                $nombre_img = $imagen;
            }
        }

        return $nombre_img;
    }
}

if (!function_exists('imagen_existe')) {

    /**
     * Metodo que se encarga de verificar que una imagen existe
     *
     * @param string|array $imagen
     * @return void
     */
    function imagen_existe($imagen)
    {
        $existe = false;

        if (!isset($imagen) || empty($imagen)) {
            return $existe;
        }

        $nombre_imagen = '';
        if (is_array($imagen)) {
            $nombre_imagen = $imagen[0];
        } else {
            $nombre_imagen = $imagen;
        }

        $char_inicio = substr($nombre_imagen, 0, 1);
        $char_fin = substr($nombre_imagen, -1);

        if (($char_inicio == '[') && $char_fin == ']') {
            $nombre_imagen = substr($nombre_imagen, 2, -2);
            if (file_exists(DIRECTORIO_IMAGENES_NURSES . $nombre_imagen)) {
                $existe = true;
            }
        } else {
            if (file_exists(DIRECTORIO_IMAGENES_NURSES . $nombre_imagen)) {
                $existe = true;
            }
        }

        log_message("debug", "------ EXISTEEEE  -----");
        log_message("debug", "------ EXISTEEEE  -----");

        return $existe;
    }
}

/* End of file nombre_imagen_helper.php */
