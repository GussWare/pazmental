
<div class="report-table">

<ul class="nav nav-tabs ">

          <?php   
                              /* PERIODO DE LOS REPORTES */
                              $to = new DateTime($to);
                              $to = $to->modify('+1 day');
                              $period = new DatePeriod(
                                new DateTime($from),
                                new DateInterval('P1D'),
                                $to
                              );
          ?>
          <?php  $i = 0; foreach($reports_type as $k=>$type): $i++; ?>
              <li <?php if($i==0){echo 'class="active"';}?>><a href="#<?php echo $k;?>" data-toggle="tab"><?php echo $type;?></a></li>
          <?php endforeach; ?>
</ul>

          
                    <div class="tab-content">

                         <div class="tab-pane active" id="sintomas" >
                          <!--  Manejar multiples ejercicios iguales por dia -->
                          <table  class="table table-striped table-bordered dt-responsive nowrap" style="display: block;overflow-x: auto;white-space: nowrap;">
                            <tr><th class="headcol" > Síntoma </th>
                          <?php 
                            
                              


                              foreach($period as $p){  
                                 ?>
                                 <th> <?php echo $p->format('l \<\b\r\> d-m-Y'); ?> </th>
                              <? }
                               ?>

                             </tr>

                                <?php
        

        if(array_key_exists('sintoma',$report_data)){  
        
                                 foreach($sintomas as $sintoma){ ?>
                                      <tr>
                                        <th class="headcol" > <?php echo $sintoma; ?></th>
                                        <? foreach($period as $p){  
                                            if(array_key_exists($p->format('d-m-Y'), $report_data['sintoma'][$sintoma])) { ?>

                                              <td> 
                                              <?php foreach( $report_data['sintoma'][$sintoma][$p->format('d-m-Y')] as $r){ 
                                                $date = new DateTime($r->timestamp);
                                                $date = $date->format('H:i');
                                               ?>
                                              <?php echo $date."<br/>";}?> 
                                            
                                            </td>
                                            <?php } else{  ?>
                                               <td> </td>
                                         <?php }} ?>

                                      </tr>
                              <?php }} ?>
                            </table>
  
              <div id="sintomas1" style="width: 100%; height: 600px;"></div>
              <input id="sintomas_report" name="sintomas_report" type="hidden" value='<?php echo json_encode($report_sintoma); ?>' />
              <input id="sintomas_report_chart" name="sintomas_report_chart" type="hidden" value='<?php echo json_encode($report_hora_sintoma); ?>' />

        <div class="row" style="margin-top:100px">
              <?php foreach($sintomas as $sintoma){ ?>

        <div class="col-md-6">                <div class="sintoma_chart" id="<?php echo str_replace(' ', '_', $sintoma);?>"></div></div>

              <?php } ?>
        </div>
                </div>
              <!-- Aqui va el contenido dinámico de la tabla -->

                <div class="tab-pane " id="medicamento" >
                          <table  class="table table-striped table-bordered dt-responsive nowrap" style="display: block;overflow-x: auto;white-space: nowrap;">
                            <tr><th class="headcol"> Medicamento </th>
                          <?php 

                            
                              
                              foreach($period as $p){  
                                 ?>
                                 <th> <?php echo $p->format('l \<\b\r\> d-m-Y'); ?> </th>
                              <? }
                               ?>
                             </tr>
                                <?php    
        if(array_key_exists('medicamento',$report_data)){       
                   ksort($report_data['medicamento']);
                    
                                 foreach(array_keys($report_data['medicamento'])  as $medicamento){ ?>
                                      <tr>
                                        <th class="headcol"> <?php echo $medicamento; ?></th>
                                        <? foreach($period as $p){  
                                            if(array_key_exists($p->format('d-m-Y'),$report_data['medicamento'][$medicamento])){
                                               ?>
                                              <td>  <?php echo $report_data['medicamento'][$medicamento][$p->format('d-m-Y')] ?> </td>
                                            <?php }
                                            else{  ?>
                                               <td> </td>
                                         <?php }} ?>

                                      </tr>
                              <?php }} ?>
                            </table>

                      <div id="medicamento1" style="width: 100%; height: 600px;"></div>
          <input id="medicamento_report" name="medicamento_report" type="hidden" value='<?php echo json_encode($medicamentos); ?>' />

                         </div>

                         <div class="tab-pane" id="estimulacion_cognitiva" >
                          <table  class="table table-striped table-bordered dt-responsive nowrap" style="display: block;overflow-x: auto;white-space: nowrap;">
                            <!-- Falta poner cuál ejercicio se hizo. Manejar multiples ejercicios iguales por dia -->
                            <tr><th class="headcol"> Estimulación Cognitiva </th>
                          <?php 
                              
                              foreach($period as $p){  
                                 ?>
                                 <th> <?php echo $p->format('l \<\b\r\> d-m-Y'); ?> </th>
                              <? }
                               ?>
                             </tr>
                                <?php 
        if(array_key_exists('estimulacion_cognitiva',$report_data)){
                                 foreach(array_keys($report_data['estimulacion_cognitiva']) as $est_cognitiva){ ?>
                                      <tr>
                                        <th class="headcol"> <?php echo $est_cognitiva; ?></th>
                                        <? foreach($period as $p){  
                                            if(array_key_exists($p->format('d-m-Y'),$report_data['estimulacion_cognitiva'][$est_cognitiva])){
                                              $data=$report_data['estimulacion_cognitiva'][$est_cognitiva][$p->format('d-m-Y')];
                                              
                                              $report_meta = unserialize($data->report_meta); 
                                                ?>
                                               
                                              <td>  <?php if ($report_meta['como_le_fue'] == "Bien"){  ?> <i  style="color:#3CB371" class="fa fa-smile-o" aria-hidden="true"></i> <?php }
                                                          elseif ($report_meta['como_le_fue'] == "Regular"){   ?> <i style="color:orange" class="fa fa-meh-o"  aria-hidden="true"></i> <?php }
                                                          elseif ($report_meta['como_le_fue'] == "Mal"){  ?> <i style="color:#FA8072" class="fa fa-frown-o" aria-hidden="true"></i> <?php }?> 
                                              </td>
                                            <?php }
                                            else{  ?>
                                               <td> </td>
                                         <?php }} ?>

                                      </tr>
                              <?php }} ?>
                            </table>

              <div id="estcognitiva1" style="width: 100%; height: 600px;"></div>
              <input id="est_cognitiva" name="est_cognitiva" type="hidden" value='<?php echo json_encode($report_est_cognitiva); ?>' />
              <div id="linecognitiva1" style="width: 100%; height: 600px;"></div>
              <input id="line_cognitiva" name="line_cognitiva" type="hidden" value='<?php echo json_encode($report_line_cognitiva); ?>' />

                         </div>

                         <div class="tab-pane" id="estimulacion_fisica" >
                          <table  class="table table-striped table-bordered dt-responsive nowrap" style="display: block;overflow-x: auto;white-space: nowrap;">
                            <tr><th class="headcol"> Estimulación Física </th>
                              <!--  Manejar multiples ejercicios iguales por dia -->
                          <?php 
                              
                              foreach($period as $p){  
                                 ?>
                                 <th> <?php echo $p->format('l \<\b\r\> d-m-Y'); ?> </th>
                              <? }
                               ?>
                             </tr>
                                <?php  
        if(array_key_exists('estimulacion_fisica',$report_data)){
                                 foreach(array_keys($report_data['estimulacion_fisica']) as $est_fisica){ ?>
                                      <tr>
                                        <th class="headcol"> <?php echo $est_fisica; ?></th>
                                        <? foreach($period as $p){  
                                            if(array_key_exists($p->format('d-m-Y'),$report_data['estimulacion_fisica'][$est_fisica])){
                                              $data=$report_data['estimulacion_fisica'][$est_fisica][$p->format('d-m-Y')];
                                              
                                              $d = new DateTime($data->timestamp);
                                              $date = $d->format('H:i');
                                               ?>
                                              <td> <?php echo $date; ?> </td>
                                            <?php }
                                            else{  ?>
                                               <td> </td>
                                         <?php }} ?>

                                      </tr>
                              <?php }} ?>
                            </table>
              <div id="estfisica1" style="width: 100%; height: 600px;"></div>
              <input id="est_fisica" name="est_fisica" type="hidden" value='<?php echo json_encode($report_est_fisica); ?>' />
                         </div>

                         <div class="tab-pane" id="estimulacion_otra" >
                          <table  class="table table-striped table-bordered dt-responsive nowrap" style="display: block;overflow-x: auto;white-space: nowrap;">
                            <tr><th class="headcol"> Otras Estimulaciones </th>
                              <!--  Manejar multiples ejercicios iguales por dia -->
                          <?php 
                              
                              foreach($period as $p){  
                                 ?>
                                 <th> <?php echo $p->format('l \<\b\r\> d-m-Y'); ?> </th>
                              <? }
                               ?>
                             </tr>
                                <?php  
        if(array_key_exists('estimulacion_otra',$report_data)){
                                 foreach(array_keys($report_data['estimulacion_otra']) as $est_otra){ ?>
                                      <tr>
                                        <th  class="headcol"> <?php echo $est_otra; ?></th>
                                        <? foreach($period as $p){  
                                            if(array_key_exists($p->format('d-m-Y'),$report_data['estimulacion_otra'][$est_otra])){
                                              $data=$report_data['estimulacion_otra'][$est_otra][$p->format('d-m-Y')];
                                              
                                              $d = new DateTime($data->timestamp);
                                              $date = $d->format('H:i');
                                               ?>
                                              <td> <?php echo $date; ?> </td>
                                            <?php }
                                            else{  ?>
                                               <td> </td>
                                         <?php }} ?>

                                      </tr>
                              <?php }} ?>
                            </table>
              <div id="estotra1" style="width: 100%; height: 600px;"></div>
              <input id="est_otra" name="est_otra" type="hidden" value='<?php echo json_encode($report_est_otra); ?>' />
                         </div>
              <!-- Aqui va el contenido : Falta convertir en columnas para evacuacion y micción, falta manejar múltiples datos por celda-->
                      <div class="tab-pane" id="evacuacion" >
                          <table  class="table table-striped table-bordered dt-responsive nowrap" style="display: block;overflow-x: auto;white-space: nowrap;">
                            <tr><th class="headcol"> Evacuación - Micción </th>
                          <?php 
                              
                              foreach($period as $p){  
                                 ?>
                                 <th> <?php echo $p->format('l \<\b\r\> d-m-Y'); ?> </th>
                              <? }
                               ?>
                             </tr>
                                <?php   
        if(array_key_exists('evacuacion',$report_data)){
                                 foreach($evacuacion as $evac){ ?>
                                      <tr>
                                        <th  class="headcol"> <?php echo $evac; ?></th>
                                        <? foreach($period as $p){  
                                            if(array_key_exists($evac, $report_data['evacuacion'])){
                if(array_key_exists($p->format('d-m-Y'),$report_data['evacuacion'][$evac])){
                                                $data=$report_data['evacuacion'][$evac][$p->format('d-m-Y')];
                                                $report_meta = unserialize($data->report_meta); 
                                               ?>
                                              <td> <?php echo $report_meta['evacuacion_o_miccion']; ?> </td>
                                            <?php }
                                            else{  ?>
                                               <td> </td>
                                         <?php }} else{ ?> <td> </td> <?php }} ?>

                                      </tr>
                              <?php } } ?>
                            </table>
                    <div class="row">
         <div class="col-md-6"> <div id="evac1" style="height: 600px;"></div></div>
         <div class="col-md-6"> <div id="micc1" style="height: 600px;"></div></div>
        </div>

              <input id="evac" name="evac" type="hidden" value='<?php echo json_encode($report_evac_micc); ?>' />

                         </div>

                              <!-- Aqui va el contenido : Falta convertir en columnas para evacuacion y micción, falta manejar múltiples datos por celda-->
                      <div class="tab-pane" id="alimentacion" >
                          <table  class="table table-striped table-bordered dt-responsive nowrap" style="display: block;overflow-x: auto;white-space: nowrap;">
                            <tr><th class="headcol"> Alimentos </th>
                          <?php 
                              
                              foreach($period as $p){  
                                 ?>
                                 <th> <?php echo $p->format('l \<\b\r\> d-m-Y'); ?> </th>
                              <? }
                               ?>
                             </tr>
                                <?php 
        if(array_key_exists('alimento',$report_data)){        
                                 foreach($alimentacion as $alim){ ?>
                                      <tr>
              
                                        <th  class="headcol"> <?php echo $alim; ?></th>

                                        <?php foreach($period as $p){  

                                            if(array_key_exists($alim, $report_data['alimento'])){
                                            if(array_key_exists($p->format('d-m-Y'),$report_data['alimento'][$alim])){
                                              $data=$report_data['alimento'][$alim][$p->format('d-m-Y')];
                                              $report_meta = unserialize($data->report_meta); 

                                               ?>
                                              <td> <?php if ($report_meta['como_comio'] == "Bien"){  ?> <i  style="color:#3CB371" class="fa fa-smile-o" aria-hidden="true"></i> <?php }
                                                          elseif ($report_meta['como_comio'] == "Regular"){   ?> <i style="color:orange" class="fa fa-meh-o"  aria-hidden="true"></i> <?php }
                                                          elseif ($report_meta['como_comio'] == "Mal"){  ?> <i style="color:#FA8072" class="fa fa-frown-o" aria-hidden="true"></i> <?php }?> 
                                               </td>
                                            <?php }
                                            else{  ?>
                                               <td> </td>
                                         <?php }} else{  ?>
                                               <td> </td><?php }}  ?>

                                      </tr>
                              <?php }} ?>
                            </table>


              <div id="alimentacion1" style="width: 100%; height: 600px;"></div>

              <input id="alim" name="alim" type="hidden" value='<?php echo json_encode($report_alimentos); ?>' />
                         </div>

                                                       
                      <div class="tab-pane" id="signos_vitales" >
                          <table  class="table table-striped table-bordered dt-responsive nowrap" style="display: block;overflow-x: auto;white-space: nowrap;">
                            <tr><th class="headcol"> Signos Vitales </th>
                          <?php 

                              
                              foreach($period as $p){  
                                 ?>
                                 <th> <?php echo $p->format('l \<\b\r\> d-m-Y'); ?> </th>
                              <? }
                               ?>
                             </tr>
                                <?php 
        if(array_key_exists('signos_vitales',$report_data)){        
                                 foreach(array_keys($report_data['signos_vitales']) as $sv){ ?>
                                      <tr>
                                        <th  class="headcol"> <?php echo $sv; ?></th>

                                        <? foreach($period as $p){  
                                            if(array_key_exists($p->format('d-m-Y'),$report_data['signos_vitales'][$sv])){
                                              $data=$report_data['signos_vitales'][$sv][$p->format('d-m-Y')];
                                              $report_meta = unserialize($data->report_meta); 

                                               ?>
                                              <td> <?php echo $report_meta[$sv]; ?> 
                                               </td>
                                            <?php }
                                            else{  ?>
                                               <td> </td>
                                         <?php }} ?>

                                      </tr>
                              <?php }} ?>
                            </table>


              <input id="signos_report_chart" name="signos_report_chart" type="hidden" value='<?php echo json_encode($report_signo); ?>' />

        <div class="row" style="margin-top:100px">
              <?php foreach(array_keys($report_signo) as $signo){ ?>

        <div class="col-md-6">                <div class="signo_chart" style="height:600px" id="<?php echo str_replace(' ', '_', $signo);?>"></div></div>

              <?php } ?>
                         </div>
            </div> 



        </div>

 <div class="report-table">