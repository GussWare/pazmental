<script type="text/javascript">
    var BASE_URL = "<?php echo base_url(); ?>";
    var TIPO_ACTIVIDAD_COTIDIANA = "<?php echo TIPO_ACTIVIDAD_COTIDIANA; ?>";
    var TIPO_ACTIVIDAD_ESTIMULACION_COGNITIVA = "<?php echo TIPO_ACTIVIDAD_ESTIMULACION_COGNITIVA; ?>";
    var TIPO_ACTIVIDAD_ESTIMULACION_FISICA = "<?php echo TIPO_ACTIVIDAD_ESTIMULACION_FISICA; ?>";
    var TIPO_ACTIVIDAD_OTRO = "<?php echo TIPO_ACTIVIDAD_OTRO; ?>";
    var TIPO_SERVICIO_TRES_PUNTO_CINCO = <?php echo TIPO_SERVICIO_TRES_PUNTO_CINCO; ?>
</script>

<div class="right_col" role="main">
<?php

if (isset($page)) {
    if (isset($module)) {
        $this->load->view("$module/$page");
    } else {
        $this->load->view($page);
    }
}

?>
<div class="spiner-group">
			<div class="container-spiner"></div>
			<div class="loader-spiner"></div>
		</div>
</div>

