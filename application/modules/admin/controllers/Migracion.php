<?php (defined('BASEPATH')) or exit('No direct script access allowed');

class Migracion extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     *
     * @return void
     */
    public function index()
    {

        $this->db->select("*");
        $this->db->from("services");
        $query = $this->db->get();

        if ($query->num_rows() > 0) {

            $services = $query->result();

            $this->db->query("TRUNCATE TABLE pacientes");

            foreach ($services as $service) {

                $paciente_last_insert = null;

                // recuperamos los datos de medicinas del mapa y el cumpleaños por que ahora se guardara en la tabla de pacientes
                $this->db->select("*");
                $this->db->from("services_meta");
                $this->db->where("service_id", $service->id);
                $this->db->where_in('service_meta', [
                    '_medicines', '_map_long', '_map_lat', '_birthday',
                ]);

                $query = $this->db->get();

                $data = array();
                $data_pacientes = array();
                if ($query->num_rows() > 0) {
                    $data = $query->result();
                    foreach ($data as $service_meta) {
                        $key = substr($service_meta->service_meta, 1);
                        $data_pacientes[$key] = $service_meta->service_value;
                    }
                }

                $data_pacientes['name'] = $service->name;
                $data_pacientes['address'] = $service->address;
                $data_pacientes['city'] = $service->city;
                $data_pacientes['zip'] = $service->zip;
                $data_pacientes['state'] = $service->state;
                $data_pacientes['country'] = $service->country;
                $data_pacientes['phones'] = $service->phones;
                $data_pacientes['goals'] = $service->goals;
                $data_pacientes['ife'] = $service->ife;
                $data_pacientes['curp'] = $service->curp;
                $data_pacientes['disease'] = $service->disease;
                $data_pacientes['nombre_familia'] = $service->nombre_familia;

                // ingresamos la información de pacientes
                $this->db->insert('pacientes', $data_pacientes);

                $paciente_last_insert = $this->db->insert_id();

                // asociamos el id del paciente con el
                $this->db->update('services', array(
                    'paciente_id' => $paciente_last_insert,
                ), array('id' => $service->id));

                // actualizamos la información de los familiares en la tabla paciente relationship
                $this->db->select("*");
                $this->db->from("services_relationship");
                $this->db->where("service_id", $service->id);
                $this->db->where("relation_term", 'familiar');

                $query_familiar = $this->db->get();
                if ($query_familiar->num_rows() > 0) {
                    foreach ($query_familiar->result() as $service_familiar) {
                        $this->db->insert("pacientes_relationship", array(
                            "paciente_id" => $paciente_last_insert,
                            "relation_term" => $service_familiar->relation_term,
                            "relation_value" => $service_familiar->relation_value,
                        ));
                    }
                }

            }

        } else {
            exit("No se encontraron datos, esto no deveria pasar");
        }
    }
}
