
 <div class="">
            <div class="page-title">
               <div class="title_left">
                <h3><span class="fa fa-heartbeat"></span> Servicios</h3>
              </div>

                <div class="panel_toolbox"><a  href="<?= base_url('admin/pacientes') ?>" class="btn btn-success"><span class="fa fa-angle-double-left"></span> Regresar</a></div>
            </div>
 
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                     <h2>Actualizar Servicio</h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                      <div id="step_content">
                      <form action="" class="" id="create-service">
                         <h4 class="col-md-3">Datos Personales del Paciente</h4>
                        
                         <input type="hidden" name="service_id" class="form-control" value="<?php echo $service_id;?>">
                         <input type="hidden" name="paciente_id" class="form-control" value="<?php echo $paciente->id;?>">
                        <br/>
                         </div>
                          <div class="clearfix"></div>


                         <?php $name = json_decode($paciente->name); ?>

                              <div class="col-md-3 col-sm-3">
                                  <label for="">Nombre</label>
                                  <div class="form-group">
                                  <?php echo $name->first_name; ?>
                                  </div>
                              </div>
                              <div class="col-md-3 col-sm-3">
                                 <label for="">Segundo Nombre</label>
                                 <div class="form-group">
                                 <span id="FirstName"><?php echo $name->second_name; ?></span>
                                 </div>
                              </div>
                              <div class="col-md-3 col-sm-3">
                                 <label for="">Apellido Paterno</label>
                                 <div class="form-group">
                                 <span id="LastName"><?php echo $name->last_name; ?></span>
                                 </div>
                              </div>
                              <div class="col-md-3 col-sm-3">
                                 <label for="">Apellido Materno</label>
                                 <div class="form-group">
                                  <?php echo $name->second_last_name; ?>
                                 </div>
                              </div>
                              
                          <br>
                          <br>

                          <h4>Plan</h4>
                          <div class="col-md-4">SERVICE ID: <input type="text" name="service_id" class="form-control" value="<?php echo $service->id; ?>"  placeholder="Service ID" disabled></div>

                          <div class="col-md-4">
                              Tars ID:
                              <input type="text" name="tars_id" class="form-control" value="<?php echo $service->tars_id; ?>" placeholder="Tars ID">
                          </div>

                          <div class="col-md-4 col-sm-4">
                            <label for="">
                              Plan Contratado
                            </label>
                            <select name="Service" id="Service" class="form-control onChangeService" data-target="<?php echo base_url(); ?>admin/services/select">
                          <option value="0">Ninguno</option>
                          <?php foreach($services as $service):
                              $info = json_decode($service['option_value']); ?>
                            <option value="<?php echo $service['id']; ?>" <?php if($service['id'] == $plan) echo 'selected'; ?>>
                              <?php echo $info->name; ?>
                            </option>
                          <?php endforeach; ?>
                      </select>
                          </div>

                          <div class="col-md-4 col-sm-4">
                            <label for="">
                            Precio de Venta
                            </label>
                           <input type="text" class="form-control" name="price" value="<?php echo $price; ?>" id="precio-de-venta">
                          </div>

                          <div class="col-md-4 col-sm-4">
                            <label for="">
                            Pago diario de luz
                            </label>
                           <input type="text" class="form-control" name="cost" value="<?php echo $cost; ?>" id="costo" >
                          </div>


                         <div class="col-md-4 col-sm-4">
                          <label for="">
                            Fecha de Inicio
                          </label>
                          <div class="col-md-11 xdisplay_inputx form-group has-feedback">
                            
                      <input type="text" class="form-control has-feedback-left single_cal_picker" id="start_date" name="start_date"  value="<?php echo $start_date; ?>" />
                      <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                    
                    </div>
                        </div>
            

                      <div class="col-md-4 col-sm-4">
                        <label for="">
                       Tipo de Servicio
                        </label>
                        <select name="ServiceType" id="ServiceType" class="form-control">
                          <option value="0">Ninguno</option>
                          <?php foreach($types as $services):
                              $info = json_decode($services['option_value']); ?>
                            <option value="<?php echo $services['id']; ?>" <?php if($services['id'] == $service_type) echo 'selected'; ?>>
                              <?php echo $info->name;  ?>
                            </option>
                          <?php endforeach; ?>
                      </select>
                        </select>
                      </div>

                         <div class="col-md-4 col-sm-4">
                          <label for="">
                            Médico asignado
                          </label>
                          <select name="doctor" id="Asignar_Doctor" class="form-control onChangeDoctor" data-target="<?php echo base_url(); ?>admin/services/doctor">
                          <option value="0">Ninguno</option>
                            <?php foreach($doctors as $doctor):
                              $name = $doctor->first_name. ' '.$doctor->last_name; ?>
                            <option value="<?php echo $doctor->id; ?>" <?php if($service_doctor == $doctor->id) echo 'selected'; ?>>
                              <?php echo $name; ?>
                            </option>
                          <?php endforeach; ?>
                          <option value="-1">Otro</option>

                          </select>
                        </div>

                         <div class="col-md-4 col-sm-4">
                          <label for="">
                            Supervisora a Cargo
                          </label>
                          <select name="supervisor" id="supervisor" class="form-control">
                            <option value="0">Ninguno</option>
                            <?php foreach($supervisors as $supervisor):
                              $name = $supervisor->first_name. ' '.$supervisor->last_name; ?>
                            <option value="<?php echo $supervisor->id; ?>" <?php if($service_supervisor == $supervisor->id) echo 'selected'; ?>>
                              <?php echo $name; ?>
                            </option>
                          <?php endforeach; ?>
                          </select>
                        </div>

                  <div class="col-md-4 col-sm-4">
                          <label for="">
                            Ejecutivo de Servicio
                          </label>
                          <select name="ejecutivo" id="" class="form-control">
                            <option value="0">Ninguno</option>
                            <?php 
                              foreach($ejecutivos as $ejecutivo):
                              $name = $ejecutivo->first_name. ' '.$ejecutivo->last_name; ?>
                            <option value="<?php echo $ejecutivo->id; ?>" <?php if($service_ejecutivo == $ejecutivo->id) echo 'selected'; ?>>
                              <?php echo $name; ?>
                            </option>
                          <?php endforeach; ?>
                          </select>
                        </div>

<div class="form-group col-md-3 col-sm-3">
                        <label>Status</label>
                        <div class="control-group">
                            
                              <select name="service_status" id="service_status" class="form-control">
                                <option value="">Seleccionar</option>
                                <option value="1" <?php if ($service_status == "1"){ echo "selected";} ?> >Activo</option>
                                <option value="2" <?php if ($service_status  == "2"){ echo "selected";} ?>>Inestable</option>
                                <option value="3" <?php if ($service_status  == "3"){ echo "selected";} ?>>Suspendido</option>
                                <option value="4" <?php if ($service_status  == "4"){ echo "selected";} ?>>Cancelado</option>
                              </select>
                        </div>
                      </div>
                    
                        <div class="clearfix"></div> <br><br>  

<div class="col-sm-12 col md-12">
<br><br>  
                    </div>

<div class="col-md-12" style="text-align: right"><a href="<?php echo base_url();?>admin/services/select" class="btn btn-info addrows" id="routines-rows" data-rel="add" title="Agregar Días y Tiempos de Rutina"><span class="fa fa-plus"></span> Agregar Rutinas</a></div>
                           <br><br> 


                           <h4 class="col-md-9">Rutinas</h4>
                           
                          <br><br>


                           <table class="table table-striped jambo_table bulk_action" id="table-routines-rows">
                             <thead>


                

                    <tr class="headings">


                       <?php $rows = 0;  
                            foreach($service_routines as $key=> $value){ ?>
                            
                            <th class="column-title align-center"><select name="RoutineDay[]" class="form-control align-center">
                                    <option value="Lunes" <?php if ($key == "Lunes"){echo "selected";} ?>>Lunes</option>
                                    <option value="Martes" <?php if ($key == "Martes"){echo "selected";} ?>>Martes</option>
                                    <option value="Miercoles" <?php if ($key == "Miercoles"){echo "selected";} ?>>Miércoles</option>
                                    <option value="Jueves" <?php if ($key == "Jueves"){echo "selected";} ?>>Jueves</option>
                                    <option value="Viernes" <?php if ($key == "Viernes"){echo "selected";} ?>>Viernes</option>
                                    <option value="Sabado" <?php if ($key == "Sabado"){echo "selected";} ?>>Sábado</option>
                                    <option value="Domingo" <?php if ($key== "Domingo"){echo "selected";} ?>>Domingo</option></select>
                              </th>
                        <?php 
                            $size = sizeof($value);
                            if($size  > $rows){ 
                              $rows =$size;}
                            }

                        ?>

                    </tr>

                   

                  </thead><tbody>
   
                   <?php 
                      
                      $i=0;
                      while ($i < $rows){
                          echo '<tr>';
                           $y =0;
                          foreach($service_routines as $sr){
                            $exercise = '';
                            $explode_tipo_act =explode("_", $sr[$i][0]);
                            $explode_descripcion = explode("_", $sr[$i][1]);

                            if(count($explode_tipo_act) > 1) {
                              $exercise .= $explode_tipo_act[1] . " - ";
                            } else {
                              if($explode_tipo_act[0] != "") {
                                $exercise .= $explode_tipo_act[0]. " - ";
                              }
                            }

                            if(count($explode_descripcion) > 1) {
                              $exercise .= $explode_descripcion[1] . " - ";
                            } else {
                              if($explode_descripcion[0] != "") {
                                $exercise .= $explode_descripcion[0]. " - ";
                              }
                            }

                            $exercise .= $sr[$i][2];

                            if(trim($exercise) == ""){
                              $exercise = "<i class= 'fa fa-plus'></i>";
                            }
                            
                            $code = '<input type="hidden" name="RoutineTipoActividad" class="RoutineTipoActividad" value="'.$sr[$i][0].'">
                           <input type="hidden" name="RoutineDescripcion" class="RoutineDescripcion" value="'.$sr[$i][1].'">
                           <input type="hidden" name="RoutineHora" class="RoutineHora" value="'.$sr[$i][2].'">';
                            
                            echo '<td id="'.$i.'_'.$y.'"   style="text-align: center"><a id="routines-'.$i.'_'.$y.'" data-routine="'.$sr[$i][0].'-'.$sr[$i][1].'-'.$sr[$i][2].'" class="routines" href="'.base_url().'admin/services/routines" title="Añadir Rutina" data-rel="add" ><span>'. $code . $exercise.'</span></a></td>';
                              

                              $y+=1;
                          }
                          $i+=1;
                          echo '</tr>';
                   }


                   echo '</tbody>'; ?>
                            </table>
  <br><br>
                           
                         <h4>Asignación de Luz y Horarios</h4>
                          <br><br>
                          <div class=" col-md-9 col-sm-9" style="margin-bottom:20px"> <?php echo $current_schedule; ?></div>
    
                        <div class=" col-md-6 col-sm-6" style="margin-bottom:20px">
                        <div class="clockpicker col-md-12 col-sm-12" style="margin-bottom:20px">
                        <label for="horario_inicio_map">
                       Horario Actual de Inicio de Servicio
                        </label>
                     <input type="text" name="horario_inicio_map"  id="horario_inicio_map" class="form-control"  value="<?php echo $horario_inicio_map; ?>" /></div>
                      
                  <div class="clockpicker col-md-12 col-sm-12" style="margin-bottom:20px">
                        <label for="horario_inicio_map">
                       Horario Actual de Fin de Servicio
                        </label>
                     <input type="text" name="horario_fin_map"  id="horario_fin_map" value="<?php echo $horario_fin_map; ?>" class="form-control  " /></div>
                     <a    title="Ver Opciones" class="btn btn-info updateMarkerMap">Ver Opciones</a>
        </div>
        <div class=" col-md-6 col-sm-6" style="margin-bottom:20px">
                        <label for="dias_servicio">
                       Días Tentativos de Servicio
                        </label>
                                  <?php foreach($date_range as $d):  ?>
                                  <div class="checkbox">
                                      <label ><input name="day_availability" class="day_availability" id="<?php echo $d; ?>" type="checkbox" value="<?php echo $d; ?>" ><?php echo $d; ?></label>
                                    </div>
                                     <?php endforeach; ?>
                      
                      </div>
                          
                          <input type="hidden" id="lat_results" value='<?php echo json_encode($lat_results);?>'>
                          <input type="hidden" id="nurse_homes" value='<?php echo json_encode($nurse_homes);?>'>
<input type="hidden" id="schedule" value='<?php echo json_encode($schedule);?>'>
<div id="map_service" style="height:400px; min-width:400px;margin-top:20px"></div>

                          <?php if(!empty($nurses)): ?>
                          <h4>Opción 2: Asignación de Luz y Horarios utilizando Filtros</h4>                          
   <div id="filtering">
                        <div id="filters" class="filters col-md-12 col-sm-12">
                            
                            <div class="form-group col-md-4 col-sm-4">
                                                    <label>Seleccionar Supervisora</label>
                                                    <div class="control-group">
                                                  <select name="supervisor_filtro" id="" class="form-control">
                                          <option value="0">Ninguno</option>

                                          <?php foreach($supervisors as $supervisor):
                                            $name = $supervisor->first_name. ' '.$supervisor->last_name; ?>
                                          <option value="<?php echo $supervisor->id; ?>" <?php if ($filtered_supervisor ==$supervisor->id){echo "selected";} ?> >
                                            <?php echo $name; ?>
                                          </option>
                                        <?php endforeach; ?>
                                        </select>
                                                          
                                                    </div>
                                                </div>
                                    
                    
                                       <div class="form-group col-md-4 col-sm-4">
                                        <label>Seleccionar Luz</label>
                                                    <div class="control-group">
                                                  <select name="luz_filtro" id="luz" class="form-control">
                                          <option value="0">Ninguno</option>

                          <?php foreach($nurses as $nurse ): ?>
                                        <?php if($nurses_status[$nurse->id] > 0){ ?>
                                          <option value="<?php echo $nurse->id; ?>"  <?php if ($filtered_luz ==$nurse->id){echo "selected";} ?> >
                                            <?php echo $nurse->first_name;?> <?php echo $nurse->last_name;?>
                                          </option>
                                        <?php  } endforeach; ?>
                                        </select>
                                                          
                                                    </div>
                                                </div>

                                <div class="form-group col-md-2 col-sm-2">
                                     
                                     <label>Género:</label>
                                         <select class="form-control" name="gender_filtro" id="pm_gender"> 
                                          <option value=""  <?php if($filtered_gender == 0) echo 'selected'; ?> >Seleccionar</option>
                                             <option value="M" <?php if($filtered_gender == 'M') echo 'checked'; ?> >M </option>
                                            <option value="F" <?php if($filtered_gender == 'F') echo 'checked'; ?> >F </option>
                                          </select>
                                </div>
                    
                    <div class="form-group col-md-3 col-sm-3">
                                                    <label>Colonia</label>
                                                    <div class="control-group">
                                         
                                                          <select name="colony_filtro" id="Colony" class="form-control">
                                                          <option value=""  <?php if($filtered_colony == "none") echo 'selected'; ?> >Seleccionar</option>
                                                          <?php foreach($colonies as $colony): 
                                                            $value = $colony['colony']; ?>

                                                            <option value="<?php echo $value;?>"<?php if ($filtered_colony ==$value){echo "selected";} ?> ><?php echo $value;?></option>
                                                          <?php endforeach; ?>
                                                          </select>
                                                    </div>
                    
                                                </div>

                                                  <div class="form-group col-md-6 col-sm-6">
                                    <label>Personalidad</label>

                                     <?php foreach($personalities as $pers):
                                    
                                      $info = json_decode($pers['option_value']); ?>
                                    
                                     <div class="checkbox" style="display: inline-block;">
                                      <label ><input name="personalities_filtro" id="personalities" class="personalities" type="checkbox"  value=" <?php echo $pers['id']; ?>"  <?php if(in_array($pers['id'],$filtered_personality)) echo 'selected'; ?>><?php echo $info->name; ?></label>
                                    </div>
                                    <?php endforeach; ?>
                                 </div>



                                                <div class="col-md-3 col-sm-3">
                                                    <br> <a class="filters-toggle" href="<?php echo base_url(); ?>admin/services/edit/<?php echo $service_id; ?>" class="btn btn-default form-control" id="filter_submit2" data-rel="services_create_filter">Filtrar <span class="fa fa-filter"></span></a>
                                                </div>
                                                 
                          </div>
                        </div>
                      

                      
                      <div class="col-md-3 col-sm-3">
                        <h5>Luces Disponibles</h5>
                        <ul class="nurses-list-schedule">
                          <?php $i = 0; foreach($nurses as $nurse): ?>

                            <?php if($nurses_status[$nurse->id] > 0){ $i++;  ?>
                            <li>
                              <button href="#" style="width:auto;float:left;position: relative;left: -23px; top: 0px;"  onclick='window.open("<?php echo base_url(); ?>/admin/nurses/edit/<?php echo $nurse->id; ?>");return false;'> <i class="fa fa-eye" aria-hidden="true"></i> </button> <a style="width:90%;font-size:10px;float:right;margin-top:-27px;height: 40px;" href="<?php echo base_url(); ?>admin/nurses/schedule" data-rel="<?php echo $nurse->id; ?>" class="btn btn-default form-control <?php if($i==1) echo 'active'; ?>"><span style="width:152px;word-wrap: break-word;display:block;white-space: pre-wrap !important;text-align:center;"><?php echo $nurse->first_name;?> <?php echo $nurse->last_name;?></span></a>
                            
                            </li>
                            <?php } ?>
                            <?php endforeach; ?>
                        </ul>
                      </div>

                      <div class="col-md-9 col-sm-9">
                        <h5 class="col-md-3">Horarios Disponibles</h5> 
                             <div id="nurse-schedule-calendar" data-week-start="<?php echo date('Y-m-d',strtotime('monday this week')); ?>" data-week-end="<?php echo date('Y-m-d',strtotime('sunday this week')); ?>" class="loading">
                              <table class="table table-bordered">
                              <thead>
                                <th>Horario</th>
                                <th>Lunes</th>
                                <th>Martes</th>
                                <th>Miércoles</th>
                                <th>Jueves</th>
                                <th>Viernes</th>
                                <th>Sábado</th>
                                <th>Domingo</th>
                              </thead>
                              <tbody>
                                <?php foreach($time_range as $time): 
                                  $time_id = str_replace(':', '', $time);
                                ?>
                                  <tr id="<?php echo $time_id; ?>">
                                    <td><?php echo $time; ?><input type="hidden" id="<?php echo $nurse->id;?>"></td>
                                    <?php foreach($date_range as $date): ?>
                                      <td id="<?php echo $date; ?>">

                                          <a href="#" class="btn btn-round btn-default" data-target="<?php echo $nurse->id;?>" data-time="<?php echo $date; ?>,<?php echo $time_id; ?>"><i class="fa fa-plus"></i></a>
                                      </td>
                                    <?php endforeach; ?>
                                  </tr>
                                <?php endforeach; ?>
                              </tbody>
                              </table>
                          </div>
                      </div>


               
                        <?php else: ?>
                            <p class="alert alert-warning fade in">No se encontraron registros de luces.</p>
                        <?php endif; ?>

  <div class="clearfix"></div>
<br>
                          <br>
                                  
            <div class="clearfix"></div>

                                 <div class="col-md-6 col-sm-6">
                    <h4>Control de Registros en Bitácora</h4>
                    <p>
                      Indica cuantas veces debe reportar la luz cada categoría por día de servicio
                    </p>
                    <br><br>

                    <div class="col-md-5 col-sm-5">
                      <div class="col-form-group">
                        <label for="">Medicinas</label>
                        <input type="text" name="RecordMedicines" class="form-control" value="<?php echo $service_controls->_recordMedicine; ?>">
                      </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-5 col-sm-5">
                      <div class="col-form-group">
                        <label for="">Alimentos</label>
                        <input type="text" name="RecordEats" class="form-control" value="<?php echo $service_controls->_recordEats; ?>">
                      </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-5 col-sm-5">
                      <div class="col-form-group">
                        <label for="">Evacuaciones</label>
                        <input type="text" name="RecordOuts" class="form-control" value="<?php echo $service_controls->_recordOuts; ?>">
                      </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-5 col-sm-5">
                      <div class="col-form-group">
                        <label for="">Micciones</label>
                        <input type="text" name="RecordPoops" class="form-control"  value="<?php echo $service_controls->_recordPoops; ?>">
                      </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-5 col-sm-5">
                      <div class="col-form-group">
                        <label for="">Estimulación Cognitiva</label>
                        <input type="text" name="RecordEstimulateCognitive" class="form-control" value="<?php echo $service_controls->_recordEstimulateCoginitive; ?>">
                      </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-5 col-sm-5">
                      <div class="col-form-group">
                        <label for="">Estimulación Física</label>
                        <input type="text" name="RecordEstimulatePhisical" class="form-control" value="<?php echo $service_controls->_recordEstimulatePhisical; ?>">
                      </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-5 col-sm-5">
                      <div class="col-form-group">
                        <label for="">Otras Estimulaciones</label>
                        <input type="text" name="RecordOthers" class="form-control"  value="<?php echo $service_controls->_recordOthers; ?>">
                      </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-5 col-sm-5">
                      <div class="col-form-group">
                        <label for="">Síntomas</label>
                        <input type="text" name="RecordSymptoms" class="form-control" value="<?php echo $service_controls->_recordSymptoms; ?>">
                      </div>
                    </div>
                    <div class="clearfix"></div>
              </div>
                    

                       </form>

                  </div>













                        <div class="clearfix"></div>

<br><br>

                    <div class="col-md-9 col-sm-9">
                    </div>
                    <div class="col-md-3 col-sm-3" style="text-align:right;">
                      <a href="<?php echo base_url(); ?>admin/services/save" id="createService" data-rel="2" title="Crear Servicio" class="btn btn-info createService">Guardar</a>
                    </div>

                  </div>
                </div>
              </div>
            </div>
          </div>
