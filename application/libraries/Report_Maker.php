<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

// ---------------------------------------------------------------------------

/**
 * Report_Maker
 *
 * Yael Schwartzman
 * Access data from reports both in this db and in tars
 */

class Report_Maker
{
    public function __construct()
    {
        $CI = &get_instance();
        $this->db = $CI->db;
    }

/* OBTENER TODAS LAS CALIFICACIONES DE TODAS LAS LUCES */
    public function get_luces_calificaciones()
    {

    }

    /**
     * Metodo que se encarga de recuperar los datos del servicio de tars
     *
     * @param [type] $paciente_id
     * @param [type] $service_id
     * @param [type] $start_day
     * @param [type] $end_day
     * @return void
     */
    public function get_horarios_services_from_tars($paciente_id, $service_id, $start_day, $end_day)
    {
        $this->db->select("*");
        $this->db->from("reports");
        $this->db->join("services", "reports.service_id = services.tars_id", 'inner');
        $this->db->join("pacientes", "services.paciente_id = pacientes.id", "inner");
        $this->db->where("pacientes.id", $paciente_id);
        $this->db->where("services.id", $service_id);
        $this->db->where(" DATE(reports.timestamp) >=", $start_day);
        $this->db->where(" DATE(reports.timestamp) <=", $end_day);
        $this->db->where("(reports.report_type='inicio' OR reports.report_type='fin')", NULL, FALSE);

        $horarios = array();
        $horarios['luz'] = array();
        $horarios['entrada'] = array();
        $horarios['salida'] = array();

        $query = $this->db->get();
        $results = $query->result();

        log_message("debug", $this->db->last_query());

        foreach ($results as $r) {
            $d = new DateTime($r->timestamp);
            $date = $d->format('d-m-Y');

            if ($r->report_type == 'inicio') {
                $horarios['luz'][$date] = $this->get_nombre_luz_from_tars($r->nurse_id);
                $horarios['entrada'][$date] = $d->format("H:i");
            } else if ($r->report_type == 'fin') {
                $horarios['salida'][$date] = $d->format("H:i");
            }

        }
        return $horarios;

    }

    public function get_data_from_tars($id, $start_day, $end_day, $id_type = "services")
    {
        $horarios = array();
        $horarios['luz'] = array();
        $horarios['entrada'] = array();
        $horarios['salida'] = array();

        if ($id_type == "services") {
            $query = $this->db->query("SELECT * from reports,services where services.id ='" . $id . "' and reports.service_id = services.tars_id and DATE(timestamp) >= '" . $start_day . "' and DATE(timestamp) <='" . $end_day . "' and (report_type='inicio' OR report_type='fin')");
        } else {
            $query = $this->db->query("SELECT * from reports where  reports.nurse_id =  (SELECT meta_value from users_meta where user_id='" . $id . "' and meta_key = '_numero_luz' LIMIT 1)  and DATE(timestamp) >= '" . $start_day . "' and DATE(timestamp) <='" . $end_day . "' and (report_type='inicio' OR report_type='fin')");
        }

        $result = $query->result();
        $data['result'] = $result;

        log_message("debug", "------  query get_data_from_tars -----------");
        log_message("debug", $this->db->last_query());
        log_message("debug", "------  query get_data_from_tars -----------");

        foreach ($result as $r) {
            $d = new DateTime($r->timestamp);
            $date = $d->format('d-m-Y');

            if ($r->report_type == 'inicio') {
                $horarios['luz'][$date] = $this->get_nombre_luz_from_tars($r->nurse_id);
                $horarios['entrada'][$date] = $d->format("H:i");
            } else if ($r->report_type == 'fin') {
                $horarios['salida'][$date] = $d->format("H:i");
            }

        }
        return $horarios;
    }

    public function get_data_from_services($id, $start_day, $end_day, $id_type = "services")
    {
        $agendados = array();
        $agendados['luz'] = array();
        $agendados['entrada'] = array();
        $agendados['salida'] = array();

        if ($id_type == "services") {
            $query = $this->db->query("SELECT * from agenda,services where services.id =" . $id . " and agenda.service_id =services.id   and DATE(start_time) >= '" . $start_day . "' and DATE(end_time) <='" . $end_day . "'");
        } else {
            $query = $this->db->query("SELECT * from agenda,users where users.id =" . $id . " and agenda.nurse_id =users.id   and DATE(start_time) >= '" . $start_day . "' and DATE(end_time) <='" . $end_day . "'");
        }

        $results = $query->result();

        log_message("debug", "--------------- QUERY SERVICIOS AGENDADOS --------------");
        log_message("debug", $this->db->last_query());
        log_message("debug", "--------------- QUERY SERVICIOS AGENDADOS --------------");

        foreach ($results as $r) {

            $d = new DateTime($r->start_time);
            $ed = new DateTime($r->end_time);
            $date = $d->format('d-m-Y');

            //HERE
            $agendados['luz'][$date] = $this->get_nombre_luz_from_service($r->nurse_id);
            $agendados['entrada'][$date] = $d->format("H:i");
            $agendados['salida'][$date] = $ed->format("H:i");

        }

        return $agendados;
    }

    public function get_num_ejercicios_from_schedule($id)
    {
        $query = $this->db->query("SELECT control_records  from services  where services.id =" . $id);
        $result = $query->result();
        $controles = json_decode($result[0]->control_records);
        $sum = 0;

        log_message('debug', '---  get_num_ejercicios_from_schedule ---');
        log_message('debug', $this->db->last_query());

        if ($controles->_recordEstimulateCoginitive == "") {
            $controles->_recordEstimulateCoginitive = 0;
        }
        if ($controles->_recordEstimulatePhisical == "") {
            $controles->_recordEstimulatePhisical = 0;
        }
        if ($controles->_recordOthers == "") {
            $controles->_recordOthers = 0;
        }
        $sum += $controles->_recordEstimulateCoginitive + $controles->_recordEstimulatePhisical + $controles->_recordOthers;
        return $sum;

    }

    public function get_num_ejercicios_from_tars($id, $start_day, $end_day)
    {
        $query = $this->db->query("SELECT count(*) as count from reports,services where services.id =" . $id . " and reports.service_id = services.tars_id and DATE(timestamp) >= '" . $start_day . "' and DATE(timestamp) <='" . $end_day . "' and report_type LIKE 'estimulacion%' ");
        log_message('debug', '--- get_num_ejercicios_from_tars ---');
        log_message('debug', $this->db->last_query());
        $result = $query->result();
        return $result[0]->count;
    }

    public function get_ejercicios_from_tars($id, $start_day, $end_day)
    {
        $ejercicios = array();

        $query = $this->db->query("SELECT * from reports,services where services.id =" . $id . " and reports.service_id = services.tars_id and DATE(timestamp) >= '" . $start_day . "' and DATE(timestamp) <='" . $end_day . "' and report_type LIKE 'estimulacion%' ");
        $result = $query->result();

        log_message('debug', '--- get_ejercicios_from_tars ---');
        log_message('debug', $this->db->last_query());

        foreach ($result as $r) {
            $d = new DateTime($r->timestamp);
            $date = $d->format('d-m-Y');
            $report_meta = unserialize($r->report_meta);
            $area = "";

            if ($r->report_type == 'estimulacion_otra') {
                $area = $report_meta['otro_tipo_de_terapia'];
            } else if ($r->report_type == 'estimulacion_fisica') {
                $area = $report_meta['estimulacion_fisica'];
            } else if ($r->report_type == 'estimulacion_cognitiva') {
                $area = $report_meta['estimulacion_cognitiva'];
            }
            if (!array_key_exists($date, $ejercicios)) {
                $ejercicios[$date] = array();
            }
            $ejercicios[$date][] = $area;

        }
        return $ejercicios;
    }

    public function get_ejercicios_from_schedule($id, $start_day, $end_day)
    {
        $agendados = array();

        $query = $this->db->query("SELECT service_value  from services,services_meta where services.id =" . $id . " and services_meta.service_id =services.id   and service_meta = '_routines' ");
        $results = json_decode($query->result()[0]->service_value);

        log_message('debug', '--- get_ejercicios_from_schedule ---');
        log_message('debug', $this->db->last_query());

        $i = 0;
        $daysweek = array('Lunes' => 'Monday', 'Martes' => 'Tuesday', 'Miercoles' => 'Wednesday', 'Jueves' => 'Thursday', 'Viernes' => 'Friday', 'Sabado' => 'Saturday', 'Domingo' => 'Sunday');

        foreach ($results as $day => $excercise) {
            $day_en = $daysweek[$day];
            $agendados[$day_en] = array();
            foreach ($excercise as $e) {
                $nombre_rutina = '';
                $rutina_arr = explode('_', $e[0]);

                if (count($rutina_arr) == 1) {
                    $agendados[$day_en][] = $rutina_arr[0];
                } else if (count($rutina_arr) > 1) {
                    $agendados[$day_en][] = $rutina_arr[1];
                } else {
                    $agendados[$day_en][] = $e[0];
                }
            }

        }

        return $agendados;
    }

    public function get_medicamentos_from_tars($id, $start_day, $end_day)
    {
        /* Array de [fecha][medicina][hora_dosis1,hora_dosis2,...,hora_dosisn] */

        $medicamentos = array();

        $query = $this->db->query("SELECT * from reports,services where services.id =" . $id . " and reports.service_id = services.tars_id and DATE(timestamp) >= '" . $start_day . "' and DATE(timestamp) <='" . $end_day . "' and report_type='medicamento' ");
        $result = $query->result();

        log_message('debug', '--- get_medicamentos_from_tars ---');
        log_message('debug', $this->db->last_query());

        foreach ($result as $r) {
            $d = new DateTime($r->timestamp);
            $date = $d->format('d-m-Y');
            $report_meta = unserialize($r->report_meta);

            for ($i = 1; $i <= 3; $i++) {
                $clean_name = trim(strtolower($this->stripAccents($report_meta['que_medicina_tomo_' . $i])));
                if (strlen($clean_name != "")) {
                    if (!array_key_exists($date, $medicamentos)) {
                        $medicamentos[$date] = array();
                    }
                    if (!array_key_exists($clean_name, $medicamentos[$date])) {
                        $medicamentos[$date][$clean_name] = array();
                    }
                    $medicamentos[$date][$clean_name][] = $d->format("H:i");
                }
            }
        }
        return $medicamentos;
    }

    public function get_alimentos_from_tars($id, $start_day, $end_day)
    {
        /* Array de [fecha][medicina][hora_dosis1,hora_dosis2,...,hora_dosisn] */

        $alimentos = array();

        $query = $this->db->query("SELECT * from reports,services where services.id =" . $id . " and reports.service_id = services.tars_id and DATE(timestamp) >= '" . $start_day . "' and DATE(timestamp) <='" . $end_day . "' and report_type='alimento' ");
        $result = $query->result();

        log_message('debug', '--- get_alimentos_from_tars ---');
        log_message('debug', $this->db->last_query());

        foreach ($result as $r) {
            $d = new DateTime($r->timestamp);
            $date = $d->format('d-m-Y');
            $report_meta = unserialize($r->report_meta);

            $clean_name = $report_meta['reportar_alimentos'];
            if (!array_key_exists($date, $alimentos)) {
                $alimentos[$date] = array();
            }
            $alimentos[$date][$clean_name] = $report_meta['como_comio'];

        }
        return $alimentos;
    }

    public function get_medicamentos_from_schedule($id, $start_day, $end_day)
    {
        $agendados = array();

        $query = $this->db->query("SELECT service_value  from services,services_meta where services.id =" . $id . " and services_meta.service_id =services.id   and service_meta = '_medicines' ");
        $results = json_decode($query->result()[0]->service_value);

        log_message('debug', '--- get_medicamentos_from_schedule ---');
        log_message('debug', $this->db->last_query());

        foreach ($results as $r) {
            $clean_name = trim(strtolower($this->stripAccents($r[0])));
            $agendados[$clean_name] = array($r[3]);
        }
        return $agendados;
    }

    public function get_match_ejercicios($date, $e, $ejercicios_tars)
    {
        foreach ($ejercicios_tars as $key => $et) {
            $datetime = new DateTime($key);
            $t_date = $datetime->format("l");
            if ($t_date == $date) {
                if ($e != "") {
                    if (in_array($e, $et)) {
                        return array($key, "SI");
                    } else {
                        return array($key, "NO");
                    }
                }

            }
        }

    }

    public function get_nombre_luz_from_tars($id_luz)
    {

        $query = $this->db->query("SELECT first_name,last_name from users,users_meta where users.id = users_meta.user_id and meta_key='_numero_luz' and meta_value= " . $id_luz);
        $nurse = $query->result();
        $name = $nurse[0]->first_name . " " . $nurse[0]->last_name;
        return $name;
    }

    public function get_nombre_luz_from_service($id_luz)
    {

        $query = $this->db->query("SELECT first_name,last_name from users where users.id = " . $id_luz);
        $nurse = $query->result();
        $name = $nurse[0]->first_name . " " . $nurse[0]->last_name;
        return $name;
    }

    public function get_current_schedule($id)
    {
        $query = $this->db->query("SELECT schedule.*, users.first_name,users.last_name
                            FROM schedule,users
                            WHERE schedule.nurse_id = users.id and schedule.service_id = '$id' ");

        $schedule = $query->result();
        $current_schedule = "";
        foreach ($schedule as $s) {
            $times_array = json_decode($s->times);
            $start = $times_array[0];
            $end = new DateTime(end($times_array));
            $end = $end->modify("+30 minutes");
            $current_schedule .= "<strong> Luz: " . $s->first_name . " " . $s->last_name . "</strong> " . $s->date . ": Empieza a las " . $start . " y termina a las " . $end->format("h:i A") . "<br/>";

        }
        return $current_schedule;
    }

    public function get_short_schedule($id)
    {
        $query = $this->db->query("SELECT schedule.*, users.first_name,users.last_name
                            FROM schedule,users
                            WHERE schedule.nurse_id = users.id and schedule.service_id = '$id' ");

        $schedule = $query->result();
        $current_schedule = array();

        foreach ($schedule as $s) {
            $times_array = json_decode($s->times);
            $start = $times_array[0];
            $end = new DateTime(end($times_array));
            $end = $end->modify("+30 minutes");
            $this_name = $s->first_name . " " . $s->last_name;
            if (!array_key_exists($this_name, $current_schedule)) {
                $current_schedule[$this_name] = array();
            }
            $current_schedule[$this_name][] = $s->date . " ( " . $start . " - " . $end->format("h:i A") . ")<br/>";

        }
        return $current_schedule;
    }

    public function get_nurses_status()
    {
        $query = $this->db->query("SELECT users.id, users_meta.meta_value
                                    FROM users_meta,users
                                    WHERE  users.id = users_meta.user_id
                                    AND  users_meta.meta_key = '_status'
                                    ");

        $data = $query->result();
        $results = array();
        foreach ($data as $d) {
            $results[$d->id] = $d->meta_value;
        }
        return $results;
    }

    public function get_real_nurses($tars_id, $start_day, $end_day)
    {
        /* list of nurses that worked in this service during this period - quitar fechas fake */

        /*
        $query = $this->db->query("SELECT DISTINCT users.id, users.first_name, users.last_name
                                    FROM reports,users
                                    WHERE reports.service_id = '$tars_id'
                                    AND  users.id in (SELECT user_id from users_meta where meta_key='_numero_luz' and meta_value = reports.nurse_id)
                                    AND DATE(reports.timestamp) >= '$start_day'
                                    AND DATE(reports.timestamp) <= '$end_day'
                                    ");

        $data = $query->result();
        */

        $this->db->select("DISTINCT users.id, users.first_name, users.last_name", false);
        $this->db->from("reports");
        $this->db->join("users", "reports.nurse_id = users.id", "inner");
        $this->db->where("reports.service_id", $tars_id);
        $this->db->where("DATE(reports.timestamp) >=", $start_day);
        $this->db->where("DATE(reports.timestamp) <=", $end_day);
        $query = $this->db->get();

        log_message("debug", "----------------  query   get_real_nurses  --------------------");
        log_message("debug", $this->db->last_query());
        log_message("debug", "----------------  query   get_real_nurses  --------------------");

        $data = array();
        if($query->num_rows() > 0) {
            $data = $query->result();
        }

        return $data;
    }

    public function get_supervisor($service_id)
    {

        $query = $this->db->query("SELECT id,first_name,last_name from users where id =
                                     (SELECT relation_value from services_relationship where relation_term = 'supervisor'
                                      and service_id = $service_id)");

        $result = $query->result();

        log_message("debug", "-----------------  QUERY GET_SUPERVISOR   -------------------------");
        log_message("debug", $this->db->last_query());
        log_message("debug", "-----------------  QUERY GET_SUPERVISOR   -------------------------");

        return $result;

    }

    public function get_medico($service_id)
    {

        $query = $this->db->query("SELECT id,first_name,last_name from users where id =
                                     (SELECT relation_value from services_relationship where relation_term = 'doctor'
                                      and service_id = $service_id)");

        $result = $query->result();

        return $result;

    }

    public function get_word_cloud($words)
    {
        $word_cloud = array();
        foreach ($words as $word => $value) {
            $word_cloud[] = array("text" => $word, "weight" => $value);
        }
        return $word_cloud;
    }
    public function get_service_price($id)
    {
        $query = $this->db->query("SELECT service_value
                            FROM  services_meta, services
                            WHERE services.id = $id and services.id = services_meta.service_id
                            and service_meta = '_price'");

        $service_data = $query->row();
        setlocale(LC_MONETARY, 'en_US.UTF-8');
        //$price = money_format('%.2n', $service_data->service_value);
        $price = $service_data->service_value;
        return $price;

    }
    public function get_service_cost($id)
    {
        $query = $this->db->query("SELECT service_value
                            FROM  services_meta, services
                            WHERE services.id = $id and services.id = services_meta.service_id
                            and service_meta = '_cost'");

        $service_data = $query->row();
        setlocale(LC_MONETARY, 'en_US.UTF-8');
        //$cost = money_format('%.2n', $service_data->service_value);
        $cost = $service_data->service_value;
        return $cost;

    }

    public function get_registros($id, $start_day, $end_day, $id_type = 'services')
    {

        if ($id_type == 'services') {
            return $this->get_registros_services($id, $start_day, $end_day);

        } else {
            return $this->get_registros_nurses($id, $start_day, $end_day);

        }

    }

    public function get_registros_nurses($id, $start_day, $end_day)
    {

        $query = $this->db->query("SELECT DATE(timestamp) as dates,report_type,count(*) as count from reports where  reports.nurse_id = (SELECT meta_value from users_meta where user_id=" . $id . " and meta_key = '_numero_luz' LIMIT 1)  and DATE(timestamp) >= '" . $start_day . "' and DATE(timestamp) <='" . $end_day . "' GROUP BY report_type,date(timestamp)");

        $data = $query->result();

        $data_array = array();
        $data_array['ingresos_a_bitacora'] = array();
        $data_array['bitacora_totales'] = 0;

        foreach ($data as $d) {

            $datetime = new DateTime($d->dates);
            $date = $datetime->format('d-m-Y');

            $data_array[$d->report_type][$date] = $d->count;
            if (!array_key_exists($date, $data_array['ingresos_a_bitacora'])) {
                $data_array['ingresos_a_bitacora'][$date] = $d->count;
            } else {
                $data_array['ingresos_a_bitacora'][$date] += $d->count;
            }
            $data_array['bitacora_totales'] += $d->count;

        }
        return $data_array;
    }

    public function get_registros_services($paciente_id, $service_id, $start_day, $end_day)
    {

        //$query = $this->db->query("SELECT DATE(timestamp) as dates,report_type,count(*) as count from reports,services where services.id =" . $id . " and reports.service_id = services.tars_id and DATE(timestamp) >= '" . $start_day . "' and DATE(timestamp) <='" . $end_day . "' GROUP BY report_type,date(timestamp)");

        $this->db->select("DATE(timestamp) as dates,report_type,count(*) as count", false);
        $this->db->from("reports");
        $this->db->join("services", "reports.service_id = services.tars_id", "inner");
        $this->db->join("pacientes", "services.paciente_id = pacientes.id", "inner");
        $this->db->where("pacientes.id", $paciente_id);
        $this->db->where("services.id", $service_id);
        $this->db->where("DATE(reports.timestamp) >=", $start_day);
        $this->db->where("DATE(reports.timestamp) <=", $end_day);
        $this->db->group_by("reports.report_type, date(reports.timestamp)");

        $query = $this->db->get();

        $data = $query->result();

        $data_array = array();
        $data_array['ingresos_a_bitacora'] = array();
        $data_array['bitacora_totales'] = 0;

        foreach ($data as $d) {

            $datetime = new DateTime($d->dates);
            $date = $datetime->format('d-m-Y');

            $data_array[$d->report_type][$date]['count'] = $d->count;
            if (!array_key_exists($date, $data_array['ingresos_a_bitacora'])) {
                $data_array['ingresos_a_bitacora'][$date]['count'] = $d->count;
            } else {
                $data_array['ingresos_a_bitacora'][$date]['count'] += $d->count;
            }
            $data_array['bitacora_totales'] += $d->count;

        }

        $registros_esperados = $this->get_registros_esperados($service_id, $start_day, $end_day);

        foreach ($data as $d) {
            $datetime = new DateTime($d->dates);
            $date = $datetime->format('d-m-Y');
            if ($data_array[$d->report_type][$date]['count'] >= $registros_esperados[$d->report_type]) {
                $data_array[$d->report_type][$date]['alert'] = 0;
            } else {
                $data_array[$d->report_type][$date]['alert'] = 1;
            }

        }
        $end_day = new DateTime($end_day);
        $end_day->modify("+1 day");

        $period = new DatePeriod(
            new DateTime($start_day),
            new DateInterval('P1D'),
            $end_day
        );

        foreach ($period as $p) {
            foreach ($registros_esperados as $key => $value) {
                if ($value > 0) {

                    if (!array_key_exists($key, $data_array)) {
                        $data_array[$key] = array();
                    }
                    if (!array_key_exists($p->format('d-m-Y'), $data_array[$key])) {
                        $data_array[$key][$p->format('d-m-Y')]['count'] = "";
                        $data_array[$key][$p->format('d-m-Y')]['alert'] = 1;

                    }
                }
            }
        }

        return $data_array;
    }

    public function get_registros_esperados($id, $start_day = null, $end_day = null)
    {
        $query = $this->db->query("SELECT control_records from services where id=$id limit 1");

        log_message('debug', "---  get_registros_esperados  --");
        log_message('debug', $this->db->last_query());

        $data = $query->result();
        $control_eq = array("_recordMedicine" => "medicamento", "_recordEats" => "alimento", "_recordOuts" => "evacuacion", "_recordPoops" => 'evacuacion',
            "_recordEstimulateCoginitive" => 'estimulacion_cognitiva', "_recordEstimulatePhisical" => 'estimulacion_fisica',
            "_recordOthers" => 'estimulacion_otra', "_recordSymptoms" => 'sintoma');

        $array_control = json_decode($data[0]->control_records);

        $result = array();
        foreach ($array_control as $key => $value) {
            if (array_key_exists($control_eq[$key], $result)) {
                $result[$control_eq[$key]] += $value;
            } else {
                $result[$control_eq[$key]] = $value;
            }
        }
        return $result;
    }

    public function get_tars_id($service_id)
    {
        $query = $this->db->query("SELECT tars_id from services where id=$service_id limit 1");
        $data = $query->result();
        return $data[0]->tars_id;

    }

    public function stripAccents($str)
    {
        return strtr(utf8_decode($str), utf8_decode('àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ'), 'aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY');
    }

    /**
     * Metodo que se encarga de recuperar las rutinas y agruparlas deacuerdo a su tipo
     *
     * @param [int] $service_id
     * @param [date] $fecha_inicio
     * @return void
     */
    public function get_routines_from_service($service_id, $fecha_inicio)
    {
        $this->db->select('*');
        $this->db->from('services_meta');
        $this->db->where('service_meta', '_routines');
        $this->db->where('service_id', $service_id);

        $query = $this->db->get();
        $data = array();
        $rutinas = array();

        $daysweek = array('Lunes' => 'Monday', 'Martes' => 'Tuesday', 'Miercoles' => 'Wednesday', 'Jueves' => 'Thursday', 'Viernes' => 'Friday', 'Sabado' => 'Saturday', 'Domingo' => 'Sunday');

        if ($query->num_rows() > 0) {
            $services = $query->result();
            foreach ($services as $service_rutina) {
                $service_rutina_arr = json_decode($service_rutina->service_value, true);
                foreach ($daysweek as $dia_espanol => $dia_ingles) {
                    if (isset($service_rutina_arr[$dia_espanol])) {
                        foreach ($service_rutina_arr[$dia_espanol] as $rutina) {
                            $tipo_actividad_arr = explode("_", $rutina[0]);
                            $desc_actividad_arr = explode("_", $rutina[1]);

                            $tipo_actividad = '';
                            $desc_actividad = '';

                            if (count($tipo_actividad_arr) > 1) {
                                $tipo_actividad = $tipo_actividad_arr[1];
                            } else {
                                $tipo_actividad = $tipo_actividad_arr[0];
                            }

                            if (count($desc_actividad_arr) > 1) {
                                $desc_actividad = $desc_actividad_arr[1];
                            } else {
                                $desc_actividad = $desc_actividad_arr[0];
                            }

                            if ($tipo_actividad_arr[0] == TIPO_ACTIVIDAD_SIGNOS_VITALES) {
                                $desc_actividad = '_signos_vitales_';
                            }

                            if ($tipo_actividad_arr[0] == TIPO_ACTIVIDAD_ALIMENTO) {
                                $desc_actividad = '_alimento_';
                            }

                            if (!array_key_exists($dia_ingles, $rutinas)) {
                                $rutinas[$dia_ingles] = array();
                            }

                            if (!array_key_exists($tipo_actividad, $rutinas[$dia_ingles]) && $tipo_actividad != "") {
                                $rutinas[$dia_ingles][$tipo_actividad] = array();
                            }

                            if (!array_key_exists($desc_actividad, $rutinas[$dia_ingles][$tipo_actividad]) && $tipo_actividad != "" && $desc_actividad != "") {
                                $rutinas[$dia_ingles][$tipo_actividad][$desc_actividad] = array('conta' => 1);
                            } else {
                                if ($tipo_actividad != "" && $desc_actividad != "") {
                                    $rutinas[$dia_ingles][$tipo_actividad][$desc_actividad]['conta']++;
                                }
                            }
                        }
                    }
                }
            }
        }

        return $rutinas;
    }

    /**
     * Metodo que se encarga de recuperar las rutinas realizadas en reportes
     *
     * @param [int] $service_id
     * @param [date] $fecha_inicio
     * @param [date] $fecha_fin
     * @return void
     */
    public function get_routines_from_reportes($service_id, $fecha_inicio, $fecha_fin)
    {
        $this->db->select("reports.report_type, reports.report_meta, reports.timestamp");
        $this->db->from("reports");
        $this->db->join("services", "reports.service_id = services.tars_id", "inner");
        $this->db->where("services.id", $service_id);
        $this->db->where("timestamp >= ", $fecha_inicio);
        $this->db->where("timestamp <= ", $fecha_fin);
        $this->db->where_in("report_type", [
            'estimulacion_cognitiva',
            'estimulacion_otra',
            'estimulacion_fisica',
            'signos_vitales',
            'alimento',
        ]);
        $this->db->order_by('reports.timestamp', 'ASC');

        $query = $this->db->get();
        $data = $query->result();
        $reportes_rutinas = array();

        foreach ($data as $r) {
            //$d = new DateTime($r->timestamp);
            //$date =  $d->format('d-m-Y');
            $date = date('d-m-Y', strtotime($r->timestamp));

            $report_meta = unserialize($r->report_meta);
            $area = "";

            if ($r->report_type == 'estimulacion_otra') {
                $area = $report_meta['otro_tipo_de_terapia'];
            } else if ($r->report_type == 'estimulacion_fisica') {
                $area = $report_meta['estimulacion_fisica'];
            } else if ($r->report_type == 'estimulacion_cognitiva') {
                $area = $report_meta['estimulacion_cognitiva'];
            } else if ($r->report_type == 'signos_vitales') {
                $area = '_signos_vitales_';
            } else if ($r->report_type == 'alimento') {
                $area = '_alimento_';
            }

            if (!array_key_exists($date, $reportes_rutinas)) {
                $reportes_rutinas[$date] = [];
            }

            if (!array_key_exists($area, $reportes_rutinas[$date])) {
                $reportes_rutinas[$date][$area] = ['conta' => 1];
            } else {
                $reportes_rutinas[$date][$area]['conta']++;
            }
        }

        return $reportes_rutinas;
    }

    /**
     * Undocumented function
     *
     * @param [type] $service_id
     * @param [type] $fecha_inicio_servicio
     * @param [type] $fecha_inicio_reporte
     * @param [type] $fecha_fin_reporte
     * @return void
     */
    public function get_desempeno_luz($service_id, $fecha_inicio_servicio, $fecha_inicio_reporte, $fecha_fin_reporte)
    {
        // recuperamos tanto las rutinas en servicios como las rutinas registradas en reportes
        $rutinas_services = $this->get_routines_from_service($service_id, $fecha_inicio_servicio);
        $rutinas_reportes = $this->get_routines_from_reportes($service_id, $fecha_inicio_reporte, $fecha_fin_reporte);

        // codigo para agrupar todo en un solo array para ser presentado en la vista
        $desempeno_luz = array();
        foreach ($rutinas_services as $key => $rutina_service) {
            foreach ($rutina_service as $tipo_actividad => $actividades) {
                if (!array_key_exists($tipo_actividad, $desempeno_luz)) {
                    $desempeno_luz[$tipo_actividad] = array();
                }

                foreach ($actividades as $desc_actividad => $verificar) {
                    if (!array_key_exists($desc_actividad, $desempeno_luz[$tipo_actividad]) && $desc_actividad != "") {
                        $desempeno_luz[$tipo_actividad][$desc_actividad] = array();
                    }

                    foreach ($rutinas_reportes as $fecha => $rutina_reporte) {
                        if (date('l', strtotime($fecha)) == $key) {
                            if (array_key_exists($desc_actividad, $rutina_reporte)) {
                                if (!array_key_exists($fecha, $desempeno_luz[$tipo_actividad][$desc_actividad])) {
                                    $desempeno_luz[$tipo_actividad][$desc_actividad][$fecha] = ($rutina_reporte[$desc_actividad]['conta'] >= $verificar['conta']);
                                }
                            }
                        }
                    }
                }
            }
        }

        return $desempeno_luz;
    }

    public function get_dias_rutinas($service_id)
    {
        $this->db->select('*');
        $this->db->from('services_meta');
        $this->db->where('service_meta', '_routines');
        $this->db->where('service_id', $service_id);
        $this->db->order_by('timestamp', 'ASC');

        $query = $this->db->get();

        $data = array();
        $dias_rutinas = array();
        $daysweek = array('Lunes' => 'Monday', 'Martes' => 'Tuesday', 'Miercoles' => 'Wednesday', 'Jueves' => 'Thursday', 'Viernes' => 'Friday', 'Sabado' => 'Saturday', 'Domingo' => 'Sunday');

        if ($query->num_rows() > 0) {
            $data = $query->result();
            foreach ($data as $service) {
                $service_value = json_decode($service->service_value, true);
                foreach ($service_value as $dia_rutina => $rutinas) {
                    foreach ($rutinas as $rutina) {
                        $tipo_actividad_arr = explode("_", $rutina[0]);
                        $desc_actividad_arr = explode("_", $rutina[1]);

                        $tipo_actividad = '';
                        $desc_actividad = '';

                        if (count($tipo_actividad_arr) > 1) {
                            $tipo_actividad = $tipo_actividad_arr[1];
                        } else {
                            $tipo_actividad = $tipo_actividad_arr[0];
                        }

                        if (count($desc_actividad_arr) > 1) {
                            $desc_actividad = $desc_actividad_arr[1];
                        } else {
                            $desc_actividad = $desc_actividad_arr[0];
                        }

                        if ($tipo_actividad_arr[0] == TIPO_ACTIVIDAD_SIGNOS_VITALES) {
                            $desc_actividad = '_signos_vitales_';
                        }

                        if ($tipo_actividad_arr[0] == TIPO_ACTIVIDAD_ALIMENTO) {
                            $desc_actividad = '_alimento_';
                        }

                        if (!array_key_exists($desc_actividad, $dias_rutinas) && ($desc_actividad != "")) {
                            $dias_rutinas[$desc_actividad] = array();
                        }

                        if (array_key_exists($dia_rutina, $daysweek)) {
                            $dias_rutinas[$desc_actividad][] = $daysweek[$dia_rutina];
                        }
                    }

                }
            }
        }

        return $dias_rutinas;
    }

}
