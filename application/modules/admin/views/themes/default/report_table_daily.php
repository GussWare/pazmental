<div class="report-table">

            <div class="clearfix"></div>
            <div class="row" style="margin-top:20px">
              <div class="col-md-6">

              Hora de inicio: <strong><?php echo $hora_inicio;?></strong><br/>
              Hora de fin: <strong><?php echo $hora_fin;?></strong><br/>
              Estado anímico inicio de servicio: <strong><?php echo $sintoma_inicio;?></strong><br/>
              Estado anímico fin de servicio: <strong><?php echo $sintoma_fin;?></strong><br/>
              Nombre de la Luz: <strong><?php echo $luz_nombre;?></strong><br/>
              Comentarios generales: <strong><?php echo $comentarios_generales;?></strong><br/>
          
              
              </div>
            </div>

<div class="clearfix"></div>
            <section class="alimento-diario" style="margin-top:20px">
            <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="90%">
                <tr><th class='daily-col'>  Alimentos <i class="fa fa-pencil"  ></th><th> Desayuno</th> <th> Comida </th> <th> Cena </th> </tr>
                <tr> <td></td>
                  <td  class="col-md-3"><?php echo $alimento['Desayuno']; ?></td>
                  <td  class="col-md-3"><?php echo $alimento['Comida']; ?></td>
                  <td  class="col-md-3"><?php echo $alimento['Cena']; ?></td>
                </tr>
 
            </table>
            </section>
            <div class="clearfix"></div>

            <section class="medicamento-diario" style="margin-top:20px">
            <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="90%">
               
               <tr><th class='daily-col'> Medicamentos </th>
                <?php for($d =1; $d < $dosis ;$d++){?> 
                  <th > <?php echo "Dosis ". $d; ?></th> 
                  <?php } ?>
                </tr>
                <?php foreach(array_keys($medicamentos) as $m){?> 
                <tr>
                  <td class='daily-col'> <?php echo $m; ?> <i class="fa fa-pencil"  ></i></td> 
                
                  <?php foreach($medicamentos[$m] as $d){?> 

                  <td class="col-md-3"><?php   echo $d;  ?></td>
                  <?php } ?>
                </tr>
                <?php } ?>
 
            </table>
            </section>
            <div class="clearfix"></div>

 <section class="signos-vitales-diario" style="margin-top:20px">
            <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="90%">
                <tr><th class='daily-col'> Signos Vitales</th>
                <?php foreach($signos_horas as $hora){ ?>
                  <th> <?php echo $hora; ?> </th>
                <?php } ?>
                </tr>
                <?php foreach(array_keys($signos_vitales) as $sv){ ?>
                <tr> <th> <?php echo $sv; ?> <i class="fa fa-pencil"  ></i></td> 
                   <?php foreach($signos_vitales[$sv] as $signo){ ?> 
                      <td> <?php echo $signo; ?> </td>
                    <?php } ?>
                </tr>
                <?php } ?>
 
            </table>
            </section>
            <div class="clearfix"></div>

            <section class="estimulaciones" style="margin-top:20px">
            <table id="datatable-" class=" table table-striped  table-bordered dt-responsive nowrap" cellspacing="0" width="90%">
               <?php $table_size =0; 
                     foreach($otros_datos as $od){
                     if(sizeof($od) > $table_size){ $table_size = sizeof($od);}
                } ?>


               <tr><th class='daily-col'>Otros Datos</th></tr>
               <?php foreach(array_keys($otros_datos) as $key){ ?>

               <tr><th class='daily-col'><?php echo $key; ?> <i class="fa fa-pencil"  ></i></th>

                <?php for($i=0; $i < $table_size ; $i++){ 
                    $data = $otros_datos[$key][$i]; ?> 
                  <td> <?php echo $data; ?></td> 
                  <?php } ?>
              </tr> 
                <?php } ?>  
 
            </table>
            </section>


         
        <br><br><br>

      </div>

</div>