<?php

class Relations extends Admin_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model(array('module', 'admin/meta'));
        if (!$this->ion_auth->logged_in()) {

            $this->ion_auth->logout();
            redirect('auth', 'refresh');
        }
        $group = 'admin';

        if (!$this->ion_auth->in_group($group)) {
            $this->session->set_flashdata('message', 'You must be an administrator to view the users page.');
            redirect('admin/dashboard');
        }
    }

    public function index()
    {

    }

    public function create()
    {
        if ($this->input->is_ajax_request()) {

            if ($this->input->post('action') == 'save') {

				$this->load->library('form_validation');
                $this->form_validation->set_rules('FirstName', 'Nombres(s)', 'required');
				$this->form_validation->set_rules('Email', 'Email', 'required|is_unique[users.email]');
				$this->form_validation->set_rules('Password', 'Password', 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']');
				$this->form_validation->set_error_delimiters('<p class="error_form_validate">', '</p>');

                if (!$this->form_validation->run()) {
                    $results = array(
						'action' => 'show-modal-error',
						'title' => 'El Formulario Contiene Errores',
                        'element' => '',
                        'code' => validation_errors(),
                        'status' => 'false',
                    );
                } else {
                    $username = $this->input->post('Email');
                    $password = $this->input->post('Password');
                    $email = $this->input->post('Email');
                    $group_id = array($this->input->post('GroupID'));

                    $additional_data = array(
                        'first_name' => $this->input->post('FirstName'),
                        'last_name' => $this->input->post('LastName'),
                        'username' => $this->input->post('Email'),
                        'phone' => $this->input->post('Phone'),
                    );

                    $user = $this->ion_auth->register($email, $password, $email, $additional_data, $group_id);

                    $meta_data = array('_Relation' => 'familiar', '_RelationType' => $this->input->post('RelationType'), '_RelationToPatient' => $this->input->post('RelationToPatient'), '_ServicesPay' => json_encode($this->input->post('_ServicesPay[]')));

                    $data = [];
                    foreach ($meta_data as $meta_key => $meta_value) {
                        $data[] = array(
                            'user_id' => $user,
                            'meta_key' => $meta_key,
                            'meta_value' => $meta_value,
                        );
                    }

                    $this->db->insert_batch('users_meta', $data);

                    switch ($this->input->post('RelationType')) {

                        case 'passive-hidden':
                            $relationVal = 'Pasivo';
                            break;
                        case 'payments-show':
                            $relationVal = 'Paga';
                            break;
                        case 'caring-hidden':
                            $relationVal = 'Cuida';
                            break;

                    }

                    switch ($this->input->post('RelationToPatient')) {

                        case 'family-show':
                            $relationtopatient = 'Familiar';
                            break;
                        case 'doctor-hidden':
                            $relationtopatient = 'Doctor';
                            break;

                    }

                    $tdAllowed = array('Name' => $this->input->post('FirstName') . ' ' . $this->input->post('LastName'), 'Relation' => $relationtopatient, 'Email' => $this->input->post('Email'), 'Phone' => $this->input->post('Phone'), 'RelationType' => $relationVal);

                    $code = '<tr>';
                    foreach ($tdAllowed as $field => $value) {

                        $code .= '<td>' . $value . '</td>';
                    }
                    $code .= '
							<td class=" "><input type="hidden" name="RelationIDs[]" value="' . $user . '" class="RelationIDs"></td>
                            <td class=""></td>
                            <td class=""></td>
							</tr>';

                    $results = array(
                        'action' => 'append',
                        'element' => 'table-' . $this->input->post('module') . ' > tbody',
                        'code' => $code,
                        'status' => 'true',
                    );
                }

                echo json_encode($results);

            } else {
                $this->load->view($this->config->item('ci_my_admin_template_dir_admin') . "modals/_relations_create");
            }

        } else {
            exit('No direct script access allowed');
        }

    }
}
