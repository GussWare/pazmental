 <div class="page-title">
            <div class="row"><div class="col-md-12">
              <div class="title_left">
                <h3> Calendario  </h3>
              </div>
              </div>
            </div>
          </div>
           <div class="">
            
            <div class="row">
          
              <div class="col-md-5">
                <h4 class="name-header">  Diario de Servicios  </h4>

            </div>
            <div class="col-md-7">
                    <div class="row daily_dates">
                    <div class="col-md-1"  ><a href="<?= base_url('admin/view/'.$yesterday) ?>" ><span class="fa fa-caret-left"></span> </a> 
                        </div>

                    
                    <div class="col-md-6 col-sm-6" style="text-align:center">
                    <strong><?php echo $date; 
                         
                             $from_cal = new DateTime($date);
                              $date_cal = $from_cal->format("Y-m-d");
                              $from_cal  = $from_cal->format("m/d/Y");

                              
              ?>
                        </strong>
                    <form id="filtering">
                    <a href="#" class="servicio-detalles center" id="filters-toggle"></span> Otra Fecha</a>

                    </div>
                      <div class="col-md-1 col-sm-1">
                        <a href="<?= base_url('admin/view/'.$tomorrow) ?>" ><span class="fa fa-caret-right"></span></a>  </div>
                        <div class="clearfix"></div>
                        <div id="filters" class="hidden" ><div class="col-md-2"></div><div class="form-group col-md-4" style="text-align:center;margin-top:20px">
                                                    <div class="control-group">
                                         
                                                          <div class="xdisplay_inputx form-group has-feedback">
                                                            <input type="text" class="form-control has-feedback-left single_cal_picker" id="start_date" name="start_date" placeholder="Fecha de Alta" aria-describedby="inputSuccess2Status2">
                                                            <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                                            <span id="inputSuccess2Status2" class="sr-only">(success)</span>
                                                          </div>
                                                    </div>
                                                
                                    
                  			
                                                   <a href="<?php echo base_url(); ?>admin/view/<?php echo $id; ?>" class="btn btn-info form-control" id="filter_submit" data-rel="report_daily">Buscar</a>
                                                    </div>
                                                    
                                                </div>

                        </form>
                      </div>
            </div>
     <br/><br/>
     <div class="col-md-12" style="margin:20px">
     <form id="filtering">
          
                    <div id="filters"><div class="form-group col-md-3 col-sm-3">
                                                    <label>Supervisora</label>
                                                    <div class="control-group">
                                          				<select name="supervisor" id="supervisor" class="form-control">
							                            <option value="0">Ninguno</option>

							                            <?php foreach($supervisors as $supervisor):
							                              $name = $supervisor->first_name. ' '.$supervisor->last_name; ?>
							                            <option value="<?php echo $supervisor->id; ?>" <?php if ($filtered_supervisor ==$supervisor->id){echo "selected";} ?> >
							                              <?php echo $name; ?>
							                            </option>
							                          <?php endforeach; ?>
							                          </select>
                                                          
                                                    </div>
                                                </div>
                                    
                    
                                       <div class="form-group col-md-3 col-sm-3">
                                       	<label>Luz</label>
                                                    <div class="control-group">
                                          				<select name="luz" id="luz" class="form-control">
							                            <option value="0">Ninguno</option>

													<?php foreach($services as $nurse_info=>$service): 
							                             	$nurse_arr = explode("-",$nurse_info);?>
							                            <option value="<?php echo $nurse_arr[0]; ?>"  <?php if ($filtered_luz ==$nurse_arr[0]){echo "selected";} ?> >
							                              <?php echo $nurse_arr[1]; ?>
							                            </option>
							                          <?php endforeach; ?>
							                          </select>
                                                          
                                                    </div>
                                                </div>
                    
                    						<div class="form-group col-md-3 col-sm-3">
                                                    <label>Status</label>
                                                    <div class="control-group">
                                         
                                                          <select name="Status" id="Status" class="form-control">
                                                            <option value="">Seleccionar</option>
                                                            <option value="1">Activo</option>
                                                            <option value="2">Inestable</option>
                                                            <option value="3">Suspendido</option>
                                                            <option value="4">Cancelado</option>
                                                          </select>
                                                    </div>
                    
                                                </div>

                                                <div class="col-md-3 col-sm-3">
                                                    <br> <a class="filters-toggle" href="<?php echo base_url(); ?>admin/view/<?php echo $date_cal	; ?>" class="btn btn-default form-control" id="filter_submit2" data-rel="poeple_filter">Filtrar <span class="fa fa-filter"></span></a>
                                                </div>
                                                                    

                                  </div>
                  

</form>
</div>
     <br/><br/>
<div class="outside-report-table">

<div class="report-table">
 		<div class="col-md-12 col-sm-12"> 
 		 <table  class="table table-striped table-bordered dt-responsive nowrap table-report" style="display: block;overflow-x: auto;white-space: nowrap;">
                            <tr><th class="headcol" > Luz </th>
             <?php $hourRange = range(strtotime('08:00'), strtotime('23:50'), 60 * 60);

							foreach($hourRange as $time){
							    $date = date("H:i",$time); ?>
							    <th><?php echo $date  ?></th>
							<?php }?>
								</tr>
							 

							<?php foreach($services as $nurse_info=>$service){ 
									$nurse_arr = explode("-",$nurse_info);?>

							<tr><th class="headcol"><a href="<?= base_url('admin/nurses/view/'.$nurse_arr[0]) ?>"><?php echo $nurse_arr[1]; ?></a></th>
								<?php 
									$start_time = strtotime('08:00');
									  $end_time = strtotime('23:50');
								      $time = strtotime('08:00');
								      $i = 0;
								      $times = array_keys($service);
								      while ($time < $end_time && $i < sizeof($times)){
								      	$se  = explode("-",$times[$i]);
								      	$this_st = strtotime($se[0]);
								      	$this_et = strtotime($se[1]);

								      	/* Si hay un espacio mayor a 2 horas se pone un + para agendar servicio , si no solo se pone gris */
								      	if ($time < $this_st){
								      		$colspan = (($this_st - $time) / (60*60));
								      		$addrow = "";
								      		$difference = date("H",$this_st) - date("H",$time);
								      		if( $difference >= 2){
								      			$addrow =  '<a  href="'.base_url().'admin/services/create/'.$nurse_arr[0].'-'.date("H:i",$this_st).'"><i class="fa fa-plus"></i></a>';
								      		}?>

								      		<td style="text-align:center" colspan="<?php echo $colspan;?>"><?php echo $addrow; ?></td>
								      		<?php
								      	}
								      	
							      		$colspan = (($this_et - $this_st) /(60*60));
							      		$time = $this_et;
							      		$service_arr = $service[$times[$i]]; 
							      		$extra_info = "";

							      		/* pintar la celda de acuerdo a la asistencia */
							      		
							      		if ($service_arr['asistencia'] == 'retardo') {$bg_color = "bg-orange";} 
							      		else if ($service_arr['asistencia'] == 'inasistencia') {$bg_color = "bg-red";}  
							      		else if ($service_arr['asistencia'] == 'despues') {$bg_color = "bg-blue-sky";}  
							      		else if ($service_arr['asistencia'] == ""){ $bg_color ="bg-purple";}
							      		else if ($service_arr['asistencia'] == 'retardo-cobertura') {$bg_color = "bg-orange-striped";} 
							      		else if ($service_arr['asistencia'] == 'a-tiempo-cobertura') {$bg_color = "bg-green-striped";} 
							      		else  {$bg_color = "bg-green";}  
 
							      		if($service_arr['asistencia']!="despues"){
								      		/* poner información adicional de medicinas, ejercicios y alimentación */

								      		/* EJERCICIOS */
								      		if ($service_arr['fallo_ejercicios'] !=""){ 
									      		if ($service_arr['fallo_ejercicios'] == "SI"){
									      			$color = "rojo";
									      		}

									      		else{
									      			$color = "verde";
									      		}
									      		$extra_info.= ' <span class="dashboard-icons estimulo_cognitivo_'.$color.'"> </span> ';
									      	}

								      		/* MEDICINAS */
								      		if ($service_arr['fallo_medicamentos'] !=""){ 
									      		if ($service_arr['fallo_medicamentos'] == "SI"){
									      			$color = "rojo";
									      		}
									      		else{
									      			$color = "verde";
									      		}
									      		$extra_info.= '<span class="dashboard-icons medicinas_'.$color.'"> </span> ';
									      	}

								      		/* ALIMENTOS */
								      		if ($service_arr['fallo_alimentos'] !=""){ 
									      		if ($service_arr['fallo_alimentos'] == "SI"){
									      			$color = "rojo";
									      		}
									      		else{
									      			$color = "verde";
									      		}
									      		$extra_info.= ' <span class="dashboard-icons alimentos_'.$color.'"> </span> ';
							      			}
								      	}
							      		?>

							      		
							      		<td 
										  <?php
										 	 if($service_arr['tipo_plan'] == TIPO_SERVICIO_TRES_PUNTO_CINCO) {
												echo 'colspan="100%"';
											  }
										  ?>
										  style="text-align:center;border:1px solid #ddd !important;padding:10px;" class="<?php echo $bg_color; ?>" colspan="<?php echo $colspan;?>">
							      			<a href="<?= base_url('admin/reportsDaily/view/'. $service_arr['paciente_id']) .'/'. $date_cal?> " style="color:#FFF"><?php echo $service_arr['service_name']  ?> </a>
							      			<br/> <?php echo $extra_info; ?></td>
							      		<?php
								      	
								      	$i+=1;
										  }
										  
										if($service_arr['tipo_plan'] != TIPO_SERVICIO_TRES_PUNTO_CINCO) {
											if ($time < $end_time){
												$colspan = (($end_time - $time) / (60*60))+1;
												$difference = date("H",$end_time) - date("H",$time);
												$date = date("H:i",$this_st);
												$addrow = "";
												?>
  
												<td colspan="<?php echo $colspan;?>" style='text-align:center'>
													<?php if ($difference >= 2){ ?>
													<a href="<?= base_url('admin/services/create/'.$nurse_arr[0].'-'.$date) ?>"><i class="fa fa-plus"></i></a>
													<?php } ?> </td>
												<?php
											}
										}
									  }
									  ?>
							</tr>
						 
							

						
					</table>
					</div>
 
         </div>
    </div>
