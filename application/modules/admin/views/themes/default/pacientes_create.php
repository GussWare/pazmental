<div class="">
    <div class="page-title">
        <div class="title_left">
            <h3>
                <span class="fa fa-heartbeat"></span> Pacientes</h3>
        </div>

        <div class="panel_toolbox">
            <a href="<?php echo  base_url('admin/pacientes') ?>" class="btn btn-success">
                <span class="fa fa-angle-double-left"></span> Regresar</a>
        </div>
    </div>

    <div class="clearfix"></div>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Crear nuevo Paciente</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div id="step_content">
                        <form action="" class="" id="create-paciente">
                            <h4 class="col-md-3">Datos Personales del Paciente</h4>
                            <div class="col-md-3">
                                ID Paciente:
                                <input type="text" name="service_id" class="form-control" value="" disabled
                                    placeholder="Auto generated">
                            </div>
                            <div class="col-md-3">
                                Nombre Familia
                                <input type="text" name="nombre_familia" class="form-control" value="" placeholder="Nombre de la familia">
                            </div>
                            <div class="clearfix"></div>


                            <div class="col-md-3 col-sm-3">
                                <label for="">Nombre</label>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="FirstName" id="FirstName">
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-3">
                                <label for="">Segundo Nombre</label>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="SecondName" id="SecondName">
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-3">
                                <label for="">Apellido Paterno</label>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="LastName" id="LastName">
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-3">
                                <label for="">Apellido Materno</label>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="SecondLastname" id="SecondLastName">
                                </div>
                            </div>

                            <div class="clearfix"></div>

                    </div>
                    <div class="col-md-4 col-sm-4">
                        <label for="">Fecha de Nacimiento</label>

                        <div class=" xdisplay_inputx form-group has-feedback">
                            <input type="text" class="form-control has-feedback-left single_cal_picker" id="Birthdate" name="Birthdate" placeholder="Fecha de Nacimiento"
                                value="01/01/1980">
                            <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-3">
                        <label for="">Condición</label>
                        <div class="form-group">
                            <select name="Desease" id="Desase" class="form-control">
                                <option value="0">Ninguno</option>
                                <?php foreach($deseases as $desease):
                              $info = json_decode($desease['option_value']); ?>
                                <option value="<?php echo $desease['id']; ?>">
                                    <?php echo $info->name; ?>
                                </option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-3">
                        <label for="">Teléfono</label>
                        <div class="form-group">
                            <input type="text" name="Phone" class="form-control">
                        </div>
                    </div>

                    <div class="clearfix"></div>

                    <h4>Dirección del Servicio</h4>
                    <br>

                    <div class="col-sm-4 col-md-4">
                        <label for="">Calle</label>
                        <div class="form-group">
                            <input type="text" class="form-control" id="address" name="Street">
                        </div>
                    </div>
                    <div class="col-sm-4 col-md-4">
                        <label for="">Número Exterior</label>
                        <div class="form-group">
                            <input type="text" class="form-control" name="ExternalNumber">
                        </div>
                    </div>
                    <div class="col-sm-4 col-md-4">
                        <label for="">Número Interior</label>
                        <div class="form-group">
                            <input type="text" class="form-control" name="InternalNumber">
                        </div>
                    </div>
                    <div class="clearfix"></div>

                    <div class="col-md-3 col-sm-3">
                        <label for="">Colonia</label>
                        <div class="form-group">
                            <input type="text" class="form-control" id="colony" name="Colony">
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-3">
                        <label for="">Delegación / Municipio </label>
                        <div class="form-group">
                            <input type="text" class="form-control" id="city" name="City">
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-3">
                        <label for="">Estado</label>
                        <div class="form-group">
                            <input type="text" class="form-control" id="region" name="State">
                        </div>
                    </div>
                    <div class="form-group col-md-3 col-sm-3">
                        <label>C.P.</label>
                        <input type="text" class="form-control" id="zip" name="zip" placeholder="Ingresa código postal" required/>
                    </div>

                    <div class="clearfix"></div>


                    <div class="col-md-3 col-sm-3">
                        <input type="hidden" class="form-control" id="map_lat" name="map_lat" />
                    </div>
                    <div class="col-md-3 col-sm-3">
                        <input type="hidden" class="form-control" id="map_long" name="map_long" />
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-4 col-sm-4">
                        <a title="Actualizar Mapa" class="btn btn-info updateMap">Actualizar Mapa</a>
                    </div>

                    <div class="form-group col-md-9 col-sm-9">
                        <div class="clearfix"></div>
                        <div id="map" style="height:400px; min-width:400px;margin-top:20px"></div>
                    </div>


                    <div class="clearfix"></div>
                   
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>

                    <div class="col-sm-12 col md-12">
                        <br>
                        <br>
                        <h4>Objetivos del Mes</h4>
                        <br>
                        <br>
                        <textarea id="message" required="required" class="form-control" name="Goals" data-parsley-trigger="keyup" data-parsley-minlength="20"
                            data-parsley-maxlength="100" data-parsley-minlength-message="Come on! You need to enter at least a 20 caracters long comment.."
                            data-parsley-validation-threshold="10"> </textarea>
                        <br>
                        <br>

                        <div class="clearfix"></div>

                        <br>
                        <br>

                        <h4 class="col-md-9">Contactos que desean recibir notificaciones</h4>
                        <div class="col-md-3" style="text-align: right">
                            <a href="<?php echo base_url();?>admin/relations/create" class="btn btn-info bootbox" id="relations"
                                data-rel="add" title="Agregar Contacto">
                                <span class="fa fa-plus"></span> Agregar Contacto</a>
                        </div>
                        <br>
                        <br>
                        <br>
                        <div class="clearfix"></div>
                        <div class="table-responsive">
                            <table class="table table-striped jambo_table bulk_action" id="table-relations">
                                <thead>
                                    <tr class="headings">
                                        <th class="column-title">Nombre Completo</th>
                                        <th class="column-title">Relación</th>
                                        <th class="column-title">E-mail</th>
                                        <th class="column-title">Teléfono</th>
                                        <th class="column-title">Tipo de Pariente</th>
                                        <th class="column-title">Calificación Plan</th>
                                        <th class="column-title">Calificación Luz </th>
                                        <th class="column-title no-link last">
                                            <span class="nobr">Acción</span>
                                        </th>
                                    </tr>
                                </thead>

                                <tbody>

                                </tbody>
                            </table>
                        </div>

                        <br>
                        <br>


                        <h4 class="col-md-9">Medicamentos</h4>
                        <div class="col-md-3" style="text-align: right">
                            <a href="<?php echo base_url();?>admin/pacientes/medicine" class="btn btn-info bootbox" id="medicine"
                                data-rel="add" title="Agregar Medicamento">
                                <span class="fa fa-plus"></span> Agregar Medicamento</a>
                        </div>
                        <br>
                        <br>
                        <div class="clearfix"></div>
                        <table class="table col-md-12" id="table-medicine">
                            <thead>
                                <tr>
                                    <th>Medicamento</th>
                                    <th>Dosis</th>
                                    <th>Veces por día</th>
                                    <th>Horas</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>

                        <div class="clearfix"></div>
                        </form>

                    </div>

                    <div class="clearfix"></div>
                    <br>
                    <br>

                    <div class="col-md-9 col-sm-9">
                    </div>
                    <div class="col-md-3 col-sm-3" style="text-align:right;">
                        <a href="<?php echo base_url(); ?>admin/pacientes/save" id="createPaciente" data-rel="1" title="Crear Paciente" class="btn btn-info createPaciente">Guardar</a>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>