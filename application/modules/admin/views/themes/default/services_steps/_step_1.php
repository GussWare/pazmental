  <h4 class="col-md-9">Datos Personales del Paciente</h4> <div class="col-md-3"><input type="text" name="zoho_id" class="form-control" value="" disabled placeholder="Zoho ID"></div>


                              <div class="col-md-4 col-sm-4">
                                <label for="">CURP</label>
                                <div class="form-group"><input type="text" class="form-control" name="CURP" required></div>
                              </div>
                              <div class="col-md-4 col-sm-4">
                                <label for="">IFE</label>
                                <div class="form-group"><input type="text" class="form-control" name="IFE" required></div>
                              </div>
                              <div class="col-md-4 col-sm-4">
                               <label for="">RFC</label>
                               <div class="form-group"><input type="text" class="form-control" name="RFC" required></div>
                              </div>


                              <div class="clearfix"></div>


                              <div class="col-md-3 col-sm-3">
                                  <label for="">Nombre</label>
                                  <div class="form-group"><input type="text" class="form-control" name="FirstName"></div>
                              </div>
                              <div class="col-md-3 col-sm-3">
                                 <label for="">Segundo Nombre</label>
                                 <div class="form-group"><input type="text" class="form-control" name="SecondName"></div>
                              </div>
                              <div class="col-md-3 col-sm-3">
                                 <label for="">Apellido Paterno</label>
                                 <div class="form-group"><input type="text" class="form-control" name="LastName"></div>
                              </div>
                              <div class="col-md-3 col-sm-3">
                                 <label for="">Apellido Materno</label>
                                 <div class="form-group"><input type="text" class="form-control" name="SecondLastname"></div>
                              </div>

                              <div class="clearfix"></div>

                              <div class="col-md-4 col-sm-4">
                                <label for="">Fecha de Nacimiento</label>

                                 <div class="col-md-11 xdisplay_inputx form-group has-feedback">
                                  <input type="text" class="form-control has-feedback-left singleDatePicker" id="birthdate" name="Birtdate" placeholder="Fecha de Nacimiento" value="01/01/1980">
                                 <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                </div>
                              </div>
                              <div class="col-md-4 col-sm-4">
                                <label for="">Condición</label>
                                <div class="form-group">
                                  <select name="Desease" id="Desase" class="form-control">
                                    <option value="0">Ninguno</option>
                                    <?php foreach($deseases as $desease):
                                      $info = json_decode($desease['option_value']); ?>
                                    <option value="<?php echo $desease['id']; ?>">
                                      <?php echo $info->name; ?>
                                    </option>
                                    <?php endforeach; ?>
                                  </select>
                                </div>
                              </div>
                              <div class="col-md-4 col-sm-4">
                                 <label for="">Teléfono</label>
                                 <div class="form-group"><input type="text" name="Phone" class="form-control"></div>
                              </div>

                              <div class="clearfix"></div>

                              <h4>Dirección del Servicio</h4> <br>

                              <div class="col-sm-4 col-md-4">
                                 <label for="">Calle</label>
                                 <div class="form-group"><input type="text" class="form-control" name="Street"></div>
                              </div>
                              <div class="col-sm-4 col-md-4">
                                 <label for="">Número Exterior</label>
                                 <div class="form-group"><input type="text" class="form-control" name="ExternalNumber"></div>
                              </div>
                              <div class="col-sm-4 col-md-4">
                                  <label for="">Número Interior</label>
                                  <div class="form-group"><input type="text" class="form-control" name="InternalNumber"></div>
                              </div>
                              <div class="clearfix"></div>

                              <div class="col-md-3 col-sm-3">
                                 <label for="">Colonia</label>
                                 <div class="form-group"><input type="text" class="form-control" name="Colony"></div>
                              </div>
                              <div class="col-md-3 col-sm-3">
                                  <label for="">Delegación / Municipio </label>
                                  <div class="form-group"><input type="text" class="form-control" name="City"></div>
                              </div>
                              <div class="col-md-3 col-sm-3">
                                 <label for="">Estado</label>
                                 <div class="form-group"><input type="text" class="form-control" name="State"></div>
                              </div>
                              <div class="col-md-3 col-sm-3">
                                  <label for="">C.P</label>
                                  <div class="form-group"><input type="text" class="form-control" name="Zip"></div>
 </div>