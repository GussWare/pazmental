<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'auth';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['admin/dashboard'] = 'admin/admin';

// user groups routes
$route['admin/user-groups'] = 'admin/userGroups';
$route['admin/user-groups/create'] = 'admin/userGroups/create';
$route['admin/user-groups/edit/(:any)'] = 'admin/userGroups/edit/$1';
$route['admin/user-groups/delete/(:any)'] = 'admin/userGroups/delete/$1';

// users routes
$route['admin/users'] = 'admin/users';
$route['admin/users/create'] = 'admin/users/create';
$route['admin/users/edit/(:any)'] = 'admin/users/edit/$1';
$route['admin/users/delete/(:any)'] = 'admin/users/delete/$1';


// nurses routes
$route['admin/nurses'] = 'admin/nurses';
$route['admin/nurses/create'] = 'admin/nurses/create';
$route['admin/nurses/edit/(:any)'] = 'admin/nurses/edit/$1';

$route['admin/nurses/delete/(:any)'] = 'admin/nurses/delete/$1';
$route['admin/nurses/filter'] = 'admin/nurses/filter';
$route['admin/nurses/save'] = 'admin/nurses/save';
$route['admin/nurses/schedule'] = 'admin/nurses/schedule';






// nurses routes
$route['admin/doctors'] = 'admin/doctors';
$route['admin/doctors/create'] = 'admin/doctors/create';
$route['admin/doctors/edit/(:any)'] = 'admin/doctors/edit/$1';
$route['admin/doctors/delete/(:any)'] = 'admin/doctors/delete/$1';

// users routes
$route['admin/services'] = 'admin/services';
$route['admin/services/create'] = 'admin/services/create';
$route['admin/services/edit/(:any)'] = 'admin/services/edit/$1';
$route['admin/services/editar/(:any)'] = 'admin/services/editar/$1';
$route['admin/services/delete/(:any)'] = 'admin/services/delete/$1';


$route['admin/services/medicine'] = 'admin/services/medicine';
$route['admin/services/save'] = 'admin/services/save';
$route['admin/services/doctor'] = 'admin/services/doctor';
$route['admin/services/select'] = 'admin/services/select';
$route['admin/services/routines'] = 'admin/services/routines';






// users routes
$route['admin/relations'] = 'admin/relations';
$route['admin/relations/create'] = 'admin/relations/create';
$route['admin/relations/edit/(:any)'] = 'admin/relations/edit/$1';
$route['admin/relations/delete/(:any)'] = 'admin/relations/delete/$1';

// users routes
$route['admin/settings'] = 'admin/settings';
$route['admin/settings/services'] = 'admin/settings/services';
$route['admin/settings/delete/(:any)'] = 'admin/settings/delete/$1';


$route['admin/settings/ajax'] = 'admin/settings/ajax';
$route['upload'] = 'upload';
$route['upload/do_upload'] = 'upload/do_upload';

//Nurse settings
// user groups routes
$route['admin/nurses_settings'] = 'admin/nursesSettings';
$route['admin/nurses_settings/options'] = 'admin/nursesSettings/options';
$route['admin/nurses_settings/delete/(:any)'] = 'admin/nursesSettings/delete/$1';


$route['admin/reports'] = 'admin/reports';


$route['admin/pacientes'] = 'admin/pacientes';
$route['admin/pacientes/create'] = 'admin/pacientes/create';
$route['admin/pacientes/edit/(:any)'] = 'admin/pacientes/edit/$1';
$route['admin/pacientes/editar/(:any)'] = 'admin/pacientes/editar/$1';
$route['admin/pacientes/delete/(:any)'] = 'admin/pacientes/delete/$1';


$route['admin/pacientes/medicine'] = 'admin/pacientes/medicine';
$route['admin/pacientes/save'] = 'admin/pacientes/save';
$route['admin/pacientes/doctor'] = 'admin/pacientes/doctor';
$route['admin/pacientes/select'] = 'admin/pacientes/select';
$route['admin/pacientes/routines'] = 'admin/pacientes/routines';

$route['admin/migracion'] = 'admin/migracion/index';