<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Phpmailerlib 
{
	function __construct($config = array())
	{
		
	}

	public function load()
    {
        require_once(APPPATH."third_party/phpmailer/PHPMailer.php");
        $objMail = new \PHPMailer\PHPMailer\PHPMailer;
        return $objMail;
    }

    //Replace USERNAME, PASSWORD, and other variables accordingly 
	public function sendEmail($email,$name,$subject,$message,$attachment)
	{
	  
	        $mail = $this->load();
	  try {
	        //Server settings
	        $mail->SMTPDebug = 0;                                 // Enable verbose debug output
	        $mail->isSMTP();                                      // Set mailer to use SMTP
	        $mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
	        $mail->SMTPAuth = true;                               // Enable SMTP authentication
	        $mail->Username = 'reportes@pazmental.mx';                 // SMTP username
	        $mail->Password = 'reportes10';                           // SMTP password
	        $mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
	        $mail->Port = 465;                                    // TCP port to connect to
	        //Recipients
	        $mail->setFrom('reportes@pazmental.mx', 'Reportes Paz Mental');
	        $mail->addAddress($email, $name);     // Add a recipient
	        
	        //Attachments
	        //$mail->addAttachment($attachment);         // Add attachments
	        

	        //Content
	        $mail->isHTML(true);                                  // Set email format to HTML
	        $mail->Subject = $subject;
	        if ($attachment){
	        	$message= "<img src='https://ana.pazmental.mx/assets/build/img/pazmental-pdf.jpg' /> <br/>".
	        				$message.
	        				"<br/> <div style='font-size:22px;'> Para acceder al reporte favor de ".
	                          "<a href='".$attachment."'>ingresar aqui</a></div>" ;
	        }
	        $mail->Body = $message;
	        

	        $mail->send();
	        return 'success';
	    } catch (Exception $e) {
	        
	        return $mail->ErrorInfo;
	    }
	  }
}

