<div class="">
    <div class="page-title">
        <div class="title_left">
            <h3>
                <span class="fa fa-cog"></span> Tipos de Actividad </h3>
        </div>

    </div>

    <div class="clearfix"></div>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Configuración Tipos de Actividad</h2>

                    <div class="clearfix"></div>
                </div>
                <div class="x_content">

                    <div class="col-xs-3">
                        <!-- required for floating -->
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs tabs-left">
                            <li>
                                <a href="#desc_otras_actividades" data-toggle="tab"> Otras Actividades</a>
                            </li>
                            <li>
                                <a href="#desc_estimulacion_cognitiva" data-toggle="tab"> Estimulación Cognitiva</a>
                            </li>
                            <li>
                                <a href="#desc_estimulacion_fisica" data-toggle="tab"> Estimulación Fisica</a>
                            </li>
                        </ul>
                    </div>

                    <div class="col-xs-9">
                        <!-- Tab panes -->
                        <div class="tab-content">

                            <div class="tab-pane active" id="desc_otras_actividades">
                                <p class="lead">
                                    <span class="a-left col-md-9">Otras Actividades</span>
                                    <div class="col-md-3 a-right" style="text-align: right">
                                        <a href="<?php echo base_url();?>admin/settings/ajax" class="btn btn-info addEditRemove"
                                            data-rel="add" id="otras_actividades" title="Agregar Otras Actividades">
                                            <span class="fa fa-plus"></span>Agregar</a>
                                    </div>
                                </p>

                                <table class="table table-striped" id="table-services">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Nombre</th>
                                            <th>Descripción</th>
                                            <th> Borrar </th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if(count($desc_otras_actividades) > 0):
                                        foreach($desc_otras_actividades as $otra_actividad):
                                            $data = json_decode($otra_actividad['option_value']);
                                        ?>
                                        <tr>
                                            <td>
                                                <?php echo $otra_actividad['id'];?>
                                            </td>
                                            <td>
                                                <?php echo $data->name;?>
                                            </td>
                                            <td>
                                                <?php echo $data->description;?>
                                            </td>
                                            <td>
                                                <a href="<?= base_url('admin/TiposActividadSettings/delete/'.$otra_actividad["id"]) ?>" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title=""
                                                    data-original-title="Eliminar">
                                                    <i class="fa fa-remove"></i>
                                                </a>
                                            </td>
                                        </tr>
                                        <?php      
                                            endforeach;
                                        endif;
                                        ?>
                                    </tbody>
                                </table>

                            </div>
                            <div class="tab-pane" id="desc_estimulacion_cognitiva">
                                <p class="lead">
                                    <span class="a-left col-md-9">Estimulación Cognitiva</span>
                                    <div class="col-md-3 a-right" style="text-align: right">
                                        <a href="<?php echo base_url();?>admin/settings/ajax" class="btn btn-info addEditRemove"
                                            data-rel="add" id="estimulacion_cognitiva" title="Agregar Estimulación Cognitiva">
                                            <span class="fa fa-plus"></span>Agregar</a>
                                    </div>
                                </p>

                                <table class="table table-striped" id="table-services">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Nombre</th>
                                            <th>Descripción</th>
                                            <th> Borrar </th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if(count($desc_estimulacion_cognitiva) > 0):
                                            foreach($desc_estimulacion_cognitiva as $estimulacion_cognitova):
                                                $data = json_decode($estimulacion_cognitova['option_value']);
                                            ?>
                                        <tr>
                                            <td>
                                                <?php echo $estimulacion_cognitova['id'];?>
                                            </td>
                                            <td>
                                                <?php echo $data->name;?>
                                            </td>
                                            <td>
                                                <?php echo $data->description;?>
                                            </td>
                                            <td>
                                                <a href="<?php echo base_url('admin/TiposActividadSettings/delete/'.$estimulacion_cognitova["id"]) ?>" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title=""
                                                    data-original-title="Eliminar">
                                                    <i class="fa fa-remove"></i>
                                                </a>
                                            </td>

                                        </tr>

                                        <?php      
                                            endforeach;
                                        endif;
                                        ?>
                                    </tbody>
                                </table>

                            </div>
                            <div class="tab-pane" id="desc_estimulacion_fisica">
                                <p class="lead">
                                    <span class="a-left col-md-9">Estimulación Fisica</span>
                                    <div class="col-md-3 a-right" style="text-align: right">
                                        <a href="<?php echo base_url();?>admin/settings/ajax" class="btn btn-info addEditRemove"
                                            data-rel="add" id="estimulacion_fisica" title="Agregar Estimulación Fisica">
                                            <span class="fa fa-plus"></span>Agregar</a>
                                    </div>
                                </p>

                                <table class="table table-striped" id="table-services">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Nombre</th>
                                            <th>Descripción</th>
                                            <th> Borrar </th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if(count($desc_estimulacion_fisica) > 0):
                                            foreach($desc_estimulacion_fisica as $estimulacion_fisica):
                                                $data = json_decode($estimulacion_fisica['option_value']);
                                            ?>
                                        <tr>
                                            <td>
                                                <?php echo $estimulacion_fisica['id'];?>
                                            </td>
                                            <td>
                                                <?php echo $data->name;?>
                                            </td>
                                            <td>
                                                <?php echo $data->description;?>
                                            </td>
                                            <td>
                                                <a href="<?= base_url('admin/TiposActividadSettings/delete/'.$estimulacion_fisica["id"]) ?>" class="btn btn-danger"
                                                    data-toggle="tooltip" data-placement="top" title="" data-original-title="Eliminar">
                                                    <i class="fa fa-remove"></i>
                                                </a>
                                            </td>
                                        </tr>
                                        <?php      
                                            endforeach;
                                        endif;
                                        ?>
                                    </tbody>
                                </table>

                            </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>