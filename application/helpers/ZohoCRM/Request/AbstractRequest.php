<?php
namespace PazMental\ZohoCRMClient\Request;

use PazMental\ZohoCRMClient\Exception\NoDataException;
use PazMental\ZohoCRMClient\Exception\UnexpectedValueException;
use PazMental\ZohoCRMClient\Response\Record;
use PazMental\ZohoCRMClient\Transport\TransportRequest;

abstract class AbstractRequest implements RequestInterface
{
    /** @var TransportRequest */
    protected $request;

    public function __construct(TransportRequest $request)
    {
        $this->request = $request;
        $this->configureRequest();
    }

    /**
     * @throws UnexpectedValueException
     * @return Record[]|Field[]
     */
    public function request()
    {
        try {
            return $this->request->request();
        } catch (NoDataException $e) {
            return array();
        }
    }

    abstract protected function configureRequest();
}
