<?php (defined('BASEPATH')) or exit('No direct script access allowed');

class TiposActividadSettings extends Admin_Controller
{

    public function __construct()
    {
        parent::__construct();

        $group = 'admin';
        $this->load->model(array('module'));
        $this->load->model(array('admin/options'));

        if (!$this->ion_auth->logged_in()) {

            $this->ion_auth->logout();
            redirect('auth', 'refresh');
        }

        if (!$this->ion_auth->in_group($group)) {
            $this->session->set_flashdata('message', 'You must be an administrator to view the user groups page.');
            redirect('admin/dashboard');
        }
    }

    /**
     * Pantalla principal del catalogo de tipos de actividad settings
     * 
     * @access public 
     * @return void
     */
    public function index()
    {
        $groups = $this->ion_auth->groups()->result();

        $data['groups'] = $groups;
        $data['tipos_actividades'] = $this->options->get_all('*', array('option_name' => '_rutina_tipo_actividad'));
        $data['desc_otras_actividades'] = $this->options->get_all('*', array('option_name' => '_rutina_descripcion_otras_actividades'));
        $data['desc_estimulacion_cognitiva'] = $this->options->get_all('*', array('option_name' => '_rutina_descripcion_estimulacion_cognitiva'));
        $data['desc_estimulacion_fisica'] = $this->options->get_all('*', array('option_name' => '_rutina_descripcion_estimulacion_fisica'));
        
        $data['page'] = $this->config->item('ci_my_admin_template_dir_admin') . "tiposactividad_settings";
        $this->load->view($this->_container, $data);
    }

    /**
     * Metodo que se encarga de eliminar un registro de la tabla options
     *
     * @param [type] $id
     * @return void
     */
    public function delete($id)
    {

        $this->db->where('id', $id);
        $this->db->delete('options');

        redirect('/admin/TiposActividadSettings', 'refresh');
    }
}
