<form id="form-routines">
	<label for="">Tipo de Actividad</label>
	<div class="form-group">
		<select name="tipoActividad" class="form-control tipoActividad">
			<option value="">Seleccione</option>
			<?php foreach($rutina_tipo_actividad AS $value): ?>
				<?php $actividad = json_decode($value['option_value']); ?>
				<option <?php echo ((isset($tipoActividad)) && ($tipoActividad == $value['id'])) ? 'selected' : '' ?> value="<?php echo $value['id']; ?>"><?php echo $actividad->name; ?></option>
			<?php endforeach; ?>
		</select>
	</div>
	<label for="">Descripción</label>
	<div class="form-group">
		<div class="ocultar-rutina descActividadCotidiana">
			<select name="descripcion" class="form-control descripcionTipoActividad" style="<?php echo ((isset($tipoActividad)) && ($tipoActividad == TIPO_ACTIVIDAD_OTRO)) ? 'display:none' : ''; ?>">
				<option value="">Seleccione</option>
				<?php foreach($rutina_descripcion AS $value): ?>
					<?php $desc = json_decode($value['option_value']); ?>
				<option <?php echo ((isset($descripcion)) && ($descripcion == $value['id'])) ? 'selected' : '' ?> value="<?php echo $value['id']; ?>"><?php echo $desc->name; ?></option>
			<?php endforeach; ?>
			</select>
		</div>

		<input type="text" name="descripcionActividadOtro" class="form-control descripcionActividadOtro"  value="<?php echo $descripcion ?>" style="<?php echo ((isset($tipoActividad)) && ($tipoActividad == TIPO_ACTIVIDAD_OTRO)) ? 'display:block' : 'display:none'; ?>"/>
	</div>

	<label for="">Hora</label>
	<div class="form-group clockpicker">
		<input type="time" name="hora" class="form-control" value="<?php echo (isset($hora) ? $hora :''); ?>">
	</div>

</form>


<script>
/*
	$(document).ready(function () {
		jQuery('.clockpicker').clockpicker({
                                            placement: "top",
                                            align: "left",
											donetext: "Listo",
											init:function(){
												console.log("entro a init");
											},
											beforeShow:function(){
												console.log("entro a beforeShow");
											},
											afterDone: function(timeValue) {
												console.log("si entra afterDone");
												console.log(timeValue);
											 },
											 beforeHourSelect:function(timeValue){
												console.log("si entra beforeHourSelect");
												console.log(timeValue);
											 },
											 beforeDone:function(timeValue){
												 console.log("si entra bforeDone");
												 console.log(timeValue);
											 }
										});
									
	});
	*/
	
</script>