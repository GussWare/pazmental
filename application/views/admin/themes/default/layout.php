<?php
$data['current_user'] = $this->ion_auth->user()->row();
$data['group_name'] = $this->ion_auth->get_users_groups()->row()->name;
$condition = array('parent_id'=>0);
$modules = $this->module->get_all('*',$condition, 'modules', '', 'id');
$data['modules'] = array();
foreach($modules as $module){
    $condition = array('parent_id'=> $module['id']);
    array_push($data['modules'], array('parent'=>$module, 'children'=>$this->module->get_all('*',$condition, 'modules', '', 'id')));
}

$this->load->view($this->config->item('ci_my_admin_template_dir_admin') . 'header', $data);
$this->load->view($this->config->item('ci_my_admin_template_dir_admin') . 'content', $data);
$this->load->view($this->config->item('ci_my_admin_template_dir_admin') . 'footer', $data);
