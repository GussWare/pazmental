<style>
	.clockpicker{z-index:1151 !important;}
	.single_cal_picker{z-index:1151 !important;}

</style>
<form id="form-tars-rows">
	
	
	<?php 
		$row = json_decode($row);
		$tars_meta = unserialize($row->report_meta);
		$datetime = new DateTime($row->timestamp);
		$date =  $datetime->format("Y/m/d");
		$time = $datetime->format("H:i:s");
		$reports_id = $row->id;

	 	foreach($tars_meta as $key=>$value){ ?>
			<div class="form-group">
				<label for=""><?php echo str_replace("_"," ", $key); ?></label>
				<input type="text" class="form-control" name="<?php echo $key; ?>" value= "<?php echo $value; ?>">
			</div>
		<?php } ?>
		
		<div class="form-group col-md-6">
				<label for="">Fecha</label>
				<input type="text" class="form-control single_cal_picker" name="fecha" value= "<?php echo $date; ?>">
		</div>

	
		<div class="form-group col-md-6">
				<label for="">Hora</label>
				<input type="text" class="form-control clockpicker" name="hora" value= "<?php echo $time; ?>">
		</div>

		<input type="hidden"  name="reports_id" value= "<?php echo $reports_id; ?>">


</form>