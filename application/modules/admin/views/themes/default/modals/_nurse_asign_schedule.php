<form id="form-<?php echo $module; ?>">
	   <div class="form-group col-md-6 col-sm-6">
        <label>Hora de Inicio</label>
        <select name="startDate" id="startDateSelector" class="form-control">
        	<?php foreach($time_range as $time): ?>
        		<option value="<?php echo $time; ?>" <?php if($time_selected == $time) echo "selected"; ?>><?php echo $time; ?></option>
        	<?php endforeach; ?>
        </select>
    </div>
    <div class="form-group col-md-6 col-sm-6">
        <label>Hora de Cierre.</label>
        <?php if(!$end_time){$end_time ==$time_selected;} ?>
        <select name="endDate" id="endDateSelector" class="form-control">
        	<?php for($i=1;$i < sizeof($time_range);$i++){ ?>
        		<option value="<?php echo $time_range[$i-1]; ?>" <?php if($end_time == $time_range[$i]) echo "selected"; ?>><?php echo $time_range[$i]; ?></option>
        	<?php } ?>
        </select>
    </div>
    <div class="form-group col-md-12 col-sm-12">
        <label>Asignación</label>
      
              
          <p>
           <span class="col-md-3 left"> Reservado:
            <input type="radio" class="flat left" name="typeSchedule" id="genderM" value="0" required <?php if($assign_selected == '0') echo 'checked'; ?> /> </span>
          <span class="col-md-3 left">Asignado:  <input type="radio" class="flat left" name="typeSchedule" id="genderF" value="1" <?php if($assign_selected == '1') echo 'checked'; ?>  /></span>
          <span class="col-md-3 left">Borrar:  <input type="radio" class="flat left" name="typeSchedule" id="erase" value="2"   /></span>
          </p>
                            
    </div>

    <div class="form-group col-12">
        <label>Fecha Arranque (sólo para servicios de 24 x 3.5)</label>
        <input type="date" name="fecha_arranque" value="<?php echo $fecha_arranque; ?>"  class="form-control"/>
    </div>

    </div>
	<input type="hidden" name="nurse_id" value="<?php echo $nurse_id; ?>" />
	<input type="hidden" name="date_selected" value="<?php echo $date_selected; ?>" />

</form>