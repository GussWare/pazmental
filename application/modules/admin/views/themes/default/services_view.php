
<div class="page-title">
            <div class="row"><div class="col-md-12">
              <div class="title_left">
                <h3> Detalle de Servicios <span class="fa fa-heartbeat"></span>  </h3>
              </div>
              </div>
            </div>
            <?php if ($this->session->flashdata('message')): ?>
              <div class="col-lg-12 col-md-12">
              <div class="alert alert-info alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?=$this->session->flashdata('message')?>
              </div>
              </div>
            <?php endif; ?>
            </div>

          <div class="">
            
            <div class="row">
              <div class="col-md-5">
                <h4 class="name-header"> <span class="first-name-title"> <?php echo $first_name ?>  </span><br/>
                    <span class="last-name-title"><?php echo $last_name. "(".$tars_id.")";  ?></span></h4>

            </div>
            <div class="col-md-7">
<form id="filtering">
        
 <?php $from_cal = new DateTime($from);
                              $from_cal  = $from_cal->format("m/d/Y");
                              $to_cal = new DateTime($to);
                              $to_cal  = $to_cal->format("m/d/Y"); ?>
          
                    <div id="filters"><div class="form-group col-md-4 col-sm-4">
                                                    <label>Desde</label>
                                                    <div class="control-group">
                                         
                                                          <div class="xdisplay_inputx form-group has-feedback">
                                                            <input type="text" class="form-control has-feedback-left single_cal_picker" id="start_date" name="start_date" value="<?php echo $from_cal; ?>" aria-describedby="inputSuccess2Status2">
                                                            <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                                            <span id="inputSuccess2Status2" class="sr-only">(success)</span>
                                                          </div>
                                                    </div>
                                                </div>
                                    
                    
                                       <div class="form-group col-md-4 col-sm-4">
                                                    <label>Hasta</label>
                                                    <div class="control-group">
                                         
                                                          <div class="xdisplay_inputx form-group has-feedback">
                                                            <input type="text" class="form-control has-feedback-left single_cal_picker" id="end_date" name="end_date" value="<?php echo $to_cal; ?>" aria-describedby="inputSuccess2Status2">
                                                            <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                                            <span id="inputSuccess2Status2" class="sr-only">(success)</span>
                                                          </div>
                                                    </div>
                                                </div>
                    

                                                <div class="col-md-3 col-sm-3">
                                                    <br> <a class="filters-toggle" href="<?php echo base_url(); ?>admin/services/view/<?php echo $paciente_id; ?>/<?php echo $service_id; ?>" class="btn btn-default form-control" id="filter_submit" data-rel="services_view">Filtrar <span class="fa fa-filter"></span></a>
                                                </div>
                                                                    

                                  </div>
                  

</form>

            </div>
          </div>

              <div class="row servicio-detalles">
                <div class="col-md-3"> <strong> Luces Asignadas: </strong><br/><?php echo  implode(" / ",$luces_asignadas); ?>  </div>
                <div class="col-md-3"> <strong> Días y Horario de servicio: </strong><br><?php print_r($current_schedule);?> </div>
                <div class="col-md-3"> <strong> Precio de Venta: </strong><br/><?php echo $precio_de_venta;?> </div>
                <div class="col-md-3"> <strong> Costo: </strong><br/><?php echo $costo;?> </div>
              </div>
            </div>



<div class="outside-report-table">

<div class="report-table">

                  <ul class="nav nav-tabs ">
          <?php 

                              $to = new DateTime($to);
                              $to = $to->modify('+1 day');
                              $period = new DatePeriod(
                                new DateTime($from),
                                new DateInterval('P1D'),
                                $to
                              );

        
           $i = 0; foreach($reports_type as $k=>$type): $i++; ?>
           <?php 

                $span_class = strtolower($type);
                $span_class = strtr(utf8_decode($span_class), utf8_decode('àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ'), 'aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY');
                $span_class = str_replace(" ", "_",$span_class);

            ?>

              <li <?php if($i==0){echo 'class="active"';}?>> <span class="top-icons <?php echo $span_class;?>"></span> <a class="top-icons-a" href="#<?php echo $k;?>" data-toggle="tab"><?php echo $type;?></a></li>
          <?php endforeach; ?>
        </ul>


           
                    <div class="tab-content">

                         <div class="tab-pane active" id="horarios" >
                          <!--  Manejar multiples ejercicios iguales por dia -->
                          <table  class="table table-striped table-bordered dt-responsive nowrap table-report" >
                            <tr><th class="headcol" > Horarios </th>
                          <?php 

                              foreach($period as $p){  
                                 ?>
                                 <th> <?php echo $p->format('l \<\b\r\> d-m-Y'); ?> </th>
                              <?php }
                               ?>

                             </tr>

                                <?php
      
        
                                 foreach($horarios as $key=>$value){ ?>
                                      <tr>
                                        <th class="headcol" > <?php echo $key; ?></th>
                                        <?php foreach($period as $p){  
                                            if(array_key_exists($p->format('d-m-Y'), $horarios[$key])) { ?>

                                              <td> 
                                                
                                              <?php echo $horarios[$key][$p->format('d-m-Y')];?> 
                                            
                                            </td>
                                            <?php } else {  ?>
                                               <td> </td>
                                         <?php }} ?>

                                      </tr>
                              <?php }  ?>

                                
                            </table>

                </div>


                         <div class="tab-pane" id="registros" >
                          <!--  Manejar multiples ejercicios iguales por dia -->
                          <table  class="table table-striped table-bordered dt-responsive nowrap table-report" >
                            <tr><th class="headcol" > Registros </th>
                          <?php  
                              foreach($period as $p){  
                                 ?>
                                 <th> <?php echo $p->format('l \<\b\r\> d-m-Y'); ?> </th>
                              <?php 
                                }
                               ?>

                             </tr>

                                <?php        
                                 foreach($registros as $key=>$value){ 
                                   if($key != "bitacora_totales"){ ?>
                                      <tr>
                                        <th class="headcol" > <?php echo str_replace("_"," ",$key); ?></th>
                                        <?php foreach($period as $p){  
                                            if(array_key_exists($p->format('d-m-Y'), $registros[$key])) { ?>
                                              <?php if ($registros[$key][$p->format('d-m-Y')]['alert']){ ?>
                                                <td class="bg-red">
                                              <?php } else{  ?>
                                                <td> 
                                               <?php } ?>
                                                
                                              <?php echo $registros[$key][$p->format('d-m-Y')]['count'];?> 
                                            
                                            </td>
                                            <?php } else {  ?>
                                               <td> </td>
                                         <?php }} ?>

                                      </tr>
                              <?php 
                              } 
                            } 
                            ?>
                            </table>
                          </div>

                           <div class="tab-pane " id="desempeno_luz" >
                          <!--  Manejar multiples ejercicios iguales por dia -->
                          <table  class="table table-striped table-bordered dt-responsive nowrap table-report" >
                            <tr><th class="headcol" > Desempeño de la Luz </th>
                          <?php 
                              foreach($period as $p){  
                                 ?>
                                 <th> <?php  echo $p->format('l \<\b\r\> d-m-Y'); ?> </th>
                              <?php }
                               ?>

                             </tr>
                                <?php
                                $tipo_actividad_tmp = '';
                                 foreach($desempeno_luz as $tipo_actividad => $actividades){ 
                                    foreach($actividades AS $desc_actividad => $fechas) {
                                   ?>
                                    
                                      <tr>
                                        <th class="headcol" > 
                                        <?php 
                                          if($tipo_actividad_tmp != $tipo_actividad) {
                                            echo $tipo_actividad . '<br />';
                                          }

                                          if($desc_actividad != "_signos_vitales_" && $desc_actividad != "_alimento_") {
                                              echo $desc_actividad;
                                          }
                                        ?></th>
                                        <?php foreach($period as $p){  
                                            if(array_key_exists($p->format('d-m-Y'), $fechas)) { ?>
                                              <td> 
                                                
                                              <?php 
                                                  if( $fechas[$p->format('d-m-Y')]){
                                                      echo '<i class="fa fa-check" aria-hidden="true"></i>'; 
                                                    } else {
                                                      echo '<i class="fa fa-close" aria-hidden="true"></i>'; 
                                                    }
                                              ?> 
                                            
                                            </td>
                                            <?php } else {  ?>
                                               <td> 
                                                <?php 
                                                  if(array_key_exists($desc_actividad, $dias_rutinas_arr)) {
                                                    if(in_array($p->format('l'), $dias_rutinas_arr[$desc_actividad])) {
                                                      echo '<i class="fa fa-close" aria-hidden="true"></i>'; 
                                                    }
                                                  }
                                                ?>
                                                </td>
                                         <?php } } ?>

                                      </tr>
                              <?php 
                              $tipo_actividad_tmp = $tipo_actividad; 
                              } 
                            } ?>

                                
                            </table>

                </div>

                <div class="tab-pane " id="historial_reportes" >
                  
                  <table  class="table table-striped table-bordered dt-responsive nowrap " style="width:50%" >
                            <tr><th > Historial de Reportes Mensuales</th></tr>
                          <?php 
                              foreach($historial as $report){  
                                 ?>
                                 <tr><td> <a target="_blank" href="<?php echo base_url('uploads/'.$report->service_value); ?>">
                                    <?php echo $report->service_value; ?></a> </td></tr>
                              <?php 
                                }
                               ?>

                            </table>

                </div>
</div>
</div>