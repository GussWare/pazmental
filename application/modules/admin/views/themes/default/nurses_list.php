
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Luces <span class="fa fa-user-md"></span> </h3>
              </div>
            <?php if ($this->session->flashdata('message')): ?>
              <div class="col-lg-12 col-md-12">
              <div class="alert alert-info alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?=$this->session->flashdata('message')?>
              </div>
              </div>
            <?php endif; ?>

              
            </div>

            <div class="clearfix"></div>

            <div class="row">

              <div class="col-md-7 col-sm-7 col-xs-7">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Listado Consolidado de Luces</h2> 
                    <div class="clearfix"></div>
                     <h4 class="from-to"> <?php echo "<strong> Desde: </strong> $from <strong>Hasta:</strong> $to"; ?></h4>

                        <?php $from_cal = new DateTime($from);
                              $from_cal  = $from_cal->format("m/d/Y");
                              $to_cal = new DateTime($to);
                              $to_cal  = $to_cal->format("m/d/Y"); ?>
                    </div>
                  </div>
                </div>
                <div class="col-md-5 col-sm-5 col-xs-5">
                  <div class="panel_toolbox"><a  href="<?= base_url('admin/nurses/create') ?>" class="btn btn-success">Agregar</a></div>
                </div>
              </div>

              
              <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                  <div class="x_content">
                    <div class="panel_toolbox"><a href="#" class="btn btn-default filters-toggle" id="filters-toggle"><span class="fa fa-filter"></span> Filtros</a></div>
    <div class="clearfix"></div>
                    <div id="filters" class="hidden" >
                          <div class="row">
                                  <div class="form-group col-md-3 col-sm-3">
                                                    <label>Desde</label>
                                                    <div class="control-group">
                                         
                                                          <div class=" xdisplay_inputx form-group has-feedback">
                                                            <input type="text" class="form-control has-feedback-left single_cal_picker" id="start_date" name="start_date" value="<?php echo $from_cal; ?>" aria-describedby="inputSuccess2Status2">
                                                            <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                                            <span id="inputSuccess2Status2" class="sr-only">(success)</span>
                                                          </div>
                                                    </div>
                                                </div>
                                    
                    
                                       <div class="form-group col-md-3 col-sm-3">
                                                    <label>Hasta</label>
                                                    <div class="control-group">
                                         
                                                          <div class=" xdisplay_inputx form-group has-feedback">
                                                            <input type="text" class="form-control has-feedback-left single_cal_picker" id="start_date" name="start_date" value="<?php echo $to_cal; ?>" aria-describedby="inputSuccess2Status2">
                                                            <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                                            <span id="inputSuccess2Status2" class="sr-only">(success)</span>
                                                          </div>
                                                    </div>
                                                </div>
                    
                                                <div class="form-group col-md-3 col-sm-3">
                                                    <label>Status</label>
                                                    <div class="control-group">
                                         
                                                          <select name="Status" id="Status" class="form-control">
                                                            <option value="">Seleccionar</option>
                                                            <option value="1">Activo</option>
                                                            <option value="0">Suspendida</option>
                                                            <option value="-1">Baja</option>
                                                            <option value="-2">Renuncia</option>
                                                          </select>
                                                    </div>
                    
                                                </div>
                    
                          </div>
                          <div class="row">
                                                 <div class="form-group col-md-3 col-sm-3">
                                                    <label>Colonia</label>
                                                    <div class="control-group">
                                         
                                                          <select name="Colony" id="Colony" class="form-control">
                                                          <option value="">Seleccionar</option>
                                                          <?php foreach($colonies as $colony): ?>
                                                            <option value="<?php echo $colony['colony'];?>"><?php echo $colony['colony'];?></option>
                                                          <?php endforeach; ?>
                                                          </select>
                                                    </div>
                    
                                                </div>

                                               
                                        
                                         <div class="form-group col-md-3 col-sm-3">
                                                    <label>Supervisor</label>
                                                    <div class="control-group">
                                         
                                                          <select name="supervisor" id="supervisor" class="form-control">
                            
                                                          <option value="">Seleccionar</option>
                                                          <?php foreach($supervisors as $supervisor):
                                                            $name = $supervisor->first_name. ' '.$supervisor->last_name; ?>
                                                          <option value="<?php echo $supervisor->id; ?>">
                                                            <?php echo $name; ?>
                                                          </option>
                                                        <?php endforeach; ?>
                                                        </select>
                                                          
                                                    </div>
                    
                                                </div>
                    
                                                 <div class="form-group col-md-3 col-sm-3">
                                                    <label>Género</label>
                                                    <div class="control-group">
                                         
                                                          <select name="gender" id="gender" class="form-control">
                                                          <option value="">Seleccionar</option>
                                                         <option value="F">Femenino</option>
                                                            <option value="M">Masculino</option>
                                                          </select>
                                                    </div>
                    
                                                </div>

                                                <div class="col-md-2 col-sm-2">
                                                    <br> <a href="<?php echo base_url(); ?>admin/nurses" class="btn btn-info form-control" id="filter_submit" data-rel="nurse_list">Filtrar</a>
                                                </div>
                                              </div>

                                  <div class="row">
                              <div class="col-md-3">
                              <label>Tipo de Luz</label>
                                                    <div class="control-group">
                                                          <select name="id_tipo_luz" id="id_tipo_luz" class="form-control">
                                                          <option value="">Seleccionar</option>
                                                          <?php foreach($tipos_luz AS $tipo_luz): ?>
                                                              <option value="<?php echo $tipo_luz->id; ?>"><?php echo $tipo_luz->nombre; ?></option>
                                                          <?php endforeach; ?>
                                                          </select>
                                                    </div>
                              </div>
                                  </div>

                        
                        </div>
                  </div>


<div class="outside-report-table">


<div class="report-table">

  

                    
                    <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>Nombre Completo</th>
                          <th>Ret.</th>
                          <th>Faltas</th>
                          <th>Cob</th>
                          <th>Status</th>
                          <th>Calificación</th>
                          <th>Antigüedad</th>
                          <th>Horas.</th>
                          <th>Total <br> Visitas</th>
                          <th>Serv. <br> por día</th>
                          <th>Registros <br> bitácora</th>
                          <th>Acción</th>

                        </tr>
                      </thead>
                      <tbody>
                      <?php if (count($users)>0): ?>
                             <?php foreach ($users as $user): 
                              if ($user_meta[$user->id]['filtro'] == 0){ ?>
                              <tr class="odd gradeX">
                                <td><?=$user->first_name .' '.$user->last_name?></td>
                                <?php if(array_key_exists('retardo', $user_meta[$user->id])){?>
                                <td><?=$user_meta[$user->id]['retardo']?></td>
                                <?php } else { echo "<td></td>"; } ?>
                                <?php if(array_key_exists('inasistencia', $user_meta[$user->id])){?>
                                <td><?=$user_meta[$user->id]['inasistencia']?></td>
                                <?php } else { echo "<td></td>"; } ?>
                                <?php if(array_key_exists('coberturas', $user_meta[$user->id])){?>
                                <td><?=$user_meta[$user->id]['coberturas']?></td>
                                <?php } else { echo "<td></td>"; } ?>
                                <?php if(array_key_exists('status', $user_meta[$user->id])){?>
                                <td><?=$user_meta[$user->id]['status']?></td>
                                <?php } else { echo "<td></td>"; } ?>
                                <?php if(array_key_exists('calificacion', $user_meta[$user->id])){?>
                                <td><?=$user_meta[$user->id]['calificacion']?></td>
                                <?php } else { echo "<td></td>"; } ?>
                                <?php if(array_key_exists('antiguedad', $user_meta[$user->id])){?>
                                <td><?=$user_meta[$user->id]['antiguedad']?></td>
                                <?php } else { echo "<td></td>"; } ?>
                                <?php if(array_key_exists('horas', $user_meta[$user->id])){?>
                                <td><?=$user_meta[$user->id]['horas']?></td>
                                <?php } else { echo "<td></td>"; } ?>
                                <?php if(array_key_exists('visitas', $user_meta[$user->id])){?>
                                <td><?=$user_meta[$user->id]['visitas']?></td>
                                <?php } else { echo "<td></td>"; } ?>
                                <?php if(array_key_exists('servicios_por_dia', $user_meta[$user->id])){?>
                                <td><?=$user_meta[$user->id]['servicios_por_dia']?></td>
                                <?php } else { echo "<td></td>"; } ?>
                                <?php if(array_key_exists('registros', $user_meta[$user->id])){?>
                                <td><?=$user_meta[$user->id]['registros']?></td>
                                <?php } else { echo "<td></td>"; } ?>
                                <td>
                                  <a href="<?= base_url('admin/nurses/view/'.$user->id) ?>" class="btn btn-default btn-accion"  data-toggle="tooltip" data-placement="top" title="" data-original-title="Ver">
                                    <i class="fa fa-eye"></i> 
                                  </a>
                                  <a href="<?= base_url('admin/nurses/pdf/'.$user->id) ?>" class="btn btn-default btn-accion print_nurse_pdf"  target="_blank"   action="print" nurse-id="<?php echo $user->id; ?>" data-toggle="tooltip" data-placement="top" title="" data-original-title="Imprimir">
                                    <i class="fa fa-print"></i> 
                                  </a>
                                  <a href="<?= base_url('admin/nurses/pdf/'.$user->id) ?>" class="btn btn-default btn-accion print_nurse_pdf"   action="send-get-info" nurse-id="<?php echo $user->id; ?>" data-toggle="tooltip" data-placement="top" title="" data-original-title="Enviar">
                                    <i class="fa fa-envelope"></i> 
                                  </a>
                                  <a href="<?= base_url('admin/nurses/edit/'.$user->id) ?>" class="btn btn-default btn-accion"  data-toggle="tooltip" data-placement="top" title="" data-original-title="Editar">
                                    <i class="fa fa-edit"></i> 
                                  </a>
                                  <a href="<?= base_url('admin/nurses/delete/'.$user->id) ?>" class="btn btn-danger btn-eliminar" data-message='¿Estas seguro que deseas eliminar a la luz con nombre "<?php echo $user->first_name .' '.$user->last_name; ?>" ?'  data-toggle="tooltip" data-placement="top" title="" data-original-title="Eliminar">
                                     <i class="fa fa-remove"></i> 
                                  </a>
                   </td>
                              </tr>
                      <?php } endforeach; ?>
                      <?php else: ?>
                            <tr class="even gradeC">
                                <td>No data</td>
                                <td>No data</td>
                                <td>No data</td>
                                <td>No data</td>
                                <td>No data</td>
                                <td>No data</td>
                                <td>No data</td>
                                <td>No data</td>
                                <td>No data</td>
                                <td>No data</td>
                                <td>No data</td>
                                <td>No data</td>
                                
                              <td>
                                  <a href="#" class="btn btn-info">edit</a>  
                                  <a href="#" class="btn btn-danger">delete</a>
                              </td>
                          </tr>
                      <?php endif; ?>
                      </tbody>
                    </table>
                    
                    
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>