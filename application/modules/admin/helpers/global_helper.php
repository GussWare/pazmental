<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if( ! function_exists ('searchArrayKeyVal')){
	function searchArrayKeyVal($sKey, $id, $array) {
	   foreach ($array as $key => $val) {
	       if ($val[$sKey] == $id) {
	           return $key;
	       }
	   }
	   return false;
	}
}