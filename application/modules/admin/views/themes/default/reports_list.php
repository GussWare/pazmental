<div class="">
    <div class="page-title">
        <div class="title_left">
            <h3>
                <span class="fa fa-bar-chart"></span> Reportes</h3>
        </div>
        <?php if ($this->session->flashdata('message')): ?>
        <div class="col-lg-12 col-md-12">
            <div class="alert alert-info alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?=$this->session->flashdata('message')?>
            </div>
        </div>
        <?php endif;?>
    </div>
    <div class="clearfix"></div>
<div class="row">

    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Listado de Reportes</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="outside-report-table">
                    <div class="report-table">
                        <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>Nombre Completo</th>
                                    <th>Nombre Familia</th>
                                    <th>Reporte Diario</th>
                                    <th>Acción</th>
                                    <?php if($editable_mensual): ?>
                                    <th> Reporte Mensual </th>
                                    <th> Acción </th>
                                    <?php endif;?>
                                </tr>
                            </thead>
                            <tbody>


                                <?php if (count($pacientes_reports)): ?>
                                <?php foreach ($pacientes_reports as $key => $list): ?>
                                <tr class="odd gradeX">
                                    <td>
                                        <?php
                                            $name = json_decode($list['name']);
                                            echo $name->first_name . ' ' . $name->second_name . ' ' . $name->last_name . ' ' . $name->second_last_name;
                                        ?>
                                    </td>
                                    <td>
                                      <?php echo $list['nombre_familia'];  ?>
                                    </td>                                    

                                    <td>
                                        <a href="<?=base_url('admin/reportsDaily/pdf/' . $list["id"])?>" class="btn btn-default btn-accion print_daily_pdf"
                                            data-toggle="tooltip" report-id="<?php echo $list['id']; ?>" action="download" data-placement="top"
                                            title="" data-original-title="Descargar">
                                            Descargar
                                        </a>
                                    </td>
                                    <td>
                                        <?php if ($editable) {?>
                                        <a href="<?=base_url('admin/reportsDaily/view/' . $list["id"])?>" class="btn btn-default btn-accion" data-toggle="tooltip"
                                            data-placement="top" title="" data-original-title="Editar">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                        <?php if ($group_name != "doctors" && $group_name != "familiares") {?>
                                        <a href="<?=base_url('admin/reportsDaily/delete/' . $list["id"])?>" class="btn btn-danger" data-toggle="tooltip" data-placement="top"
                                            title="" data-original-title="Eliminar">
                                            <i class="fa fa-remove"></i>
                                        </a>
                                        <?php }?>
                                        <?php }?>
                                    </td>

                                    <?php if ($editable_mensual) {?>
                                    <td>

                                        <a href="<?=base_url('admin/reports/pdf/' . $list["id"])?>" class="btn btn-default btn-accion print_report_pdf" target="_blank"
                                            report-id="<?php echo $list['id']; ?>" action="download" data-toggle="tooltip" data-placement="top"
                                            title="" data-original-title="Descargar">
                                            Descargar
                                        </a>

                                        <a href="<?=base_url('admin/reports/pdf/' . $list["id"])?>" class="btn btn-default btn-accion print_report_pdf" report-id="<?php echo $list['id']; ?>"
                                            action="send-get-info" data-toggle="tooltip" data-placement="top" title="Enviar"
                                            data-original-title="Enviar">
                                            Enviar
                                        </a>
                                    </td>
                                    <td>
                                        <a href="<?=base_url('admin/reports/edit/' . $list["id"])?>" class="btn btn-default btn-accion" data-toggle="tooltip" data-placement="top"
                                            title="" data-original-title="Editar">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                        <?php if ($group_name != "doctors" ) {?>
                                        <a href="<?=base_url('admin/reports/delete/' . $list["id"])?>" class="btn btn-danger" data-toggle="tooltip" data-placement="top"
                                            title="" data-original-title="Eliminar">
                                            <i class="fa fa-remove"></i>
                                        </a>
                                        <?php }?>
                                        <?php }?>
                                    </td>
                                </tr>
                                <?php endforeach;?>
                                <?php else: ?>
                                <tr class="even gradeC">
                                    <td>No data</td>
                                    <td>No data</td>
                                    <td>Ver Borrador</td>
                                    <td>
                                        <a href="#" class="btn btn-default" data-toggle="tooltip" data-placement="top" title="" data-original-title="Editar">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                        <a href="#" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="" data-original-title="Eliminar">
                                            <i class="fa fa-remove"></i>
                                        </a>
                                    </td>
                                </tr>
                                <?php endif;?>
                            </tbody>
                        </table>
                    </div>

                </div>


            </div>
        </div>
    </div>
</div>
</div>


</div>
<!-- /#page-wrapper -->