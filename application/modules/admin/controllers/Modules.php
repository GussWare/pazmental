<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class Modules extends Admin_Controller {

    function __construct() {
        parent::__construct();
        if (!$this->ion_auth->logged_in()) {
   
             $this->ion_auth->logout();
             redirect('auth', 'refresh');
        }

        $this->load->model(array('module'));
    }

    public function index() {
      
        
        $this->load->view($this->config->item('ci_my_admin_template_dir_admin') . 'header', $data);


    }
}
