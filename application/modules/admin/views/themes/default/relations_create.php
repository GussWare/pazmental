 <form role="form" method="POST" name="add-relations" id="form-relations">
      <div class="form-group col-md-6 col-sm-6">
        <label>Nombre (s)</label>
        <input class="form-control" placeholder="Enter first name" id="first_name" name="FirstName"  required>
    </div>
    <div class="form-group  col-md-6 col-sm-6">
        <label>Apellidos</label>
        <input class="form-control" placeholder="Enter last name" id="last_name" name="LastName"  required>
    </div>
   
    <div class="form-group col-md-12 col-sm-12">
        <label>Password</label>
        <input type="password" class="form-control" placeholder="Enter password" id="Password" name="Password"  required>
    </div>

    
    <div class="form-group  col-md-6 col-sm-6">
        <label>Teléfono</label>
        <input class="form-control" placeholder="Enter phone number" id="Phone" name="Phone">
    </div>

     <div class="form-group  col-md-6 col-sm-6">
        <label>Email</label>
        <input class="form-control" placeholder="Enter email" id="Email" name="Email"  required>
    </div>

     <div class="form-group  col-md-12 col-sm-12">
        <label>Relación con Pariente</label>
        <input class="form-control" placeholder="" id="RelationToPatient" name="RelationToPatient"  required>
    </div>

     <div class="form-group  col-md-12 col-sm-12">
        <label>Tipo de Pariente</label>
       	<select name="RelationType" id="RelationType" class="form-control onchange">
       		<option value="passive-hidden">Pasivo</option>
       		<option value="payments-show">Paga</option>
       		<option value="caring-hidden">Cuida</option>
       	</select>
    </div>

    <div class="form-group col-md-12 col-sm-12 hidden " id="ServicesPay">
		
        <label class="control-label col-md-12 col-sm-12 col-xs-12">Selecciona los servicio que paga el familiar.</label>
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="">
            <label>
             <input type="checkbox" class="js-switch" name="ServicesPay[]" value="service" /> Servicio
            </label>
          </div>
          <div class="">
            <label>
            <input type="checkbox" class="js-switch" name="ServicesPay[]" value="goods" /> Despensa
            </label>
          </div>
          <div class="">
            <label>
            <input type="checkbox" class="js-switch" name="ServicesPay[]" value="meds" /> Medicamentos
            </label>
          </div>
          <div class="">
            <label>
            <input type="checkbox" class="js-switch" name="ServicesPay[]" value="diapers" /> Pañales
            </label>
          </div>
        </div>
                 
    </div>

        <input class="form-control" id="group_id" name="GroupID" value="5" type="hidden">
</form>