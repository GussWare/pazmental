<?php

class ReportsDaily extends Admin_Controller
{

    public function __construct()
    {
        parent::__construct();

        if (!$this->ion_auth->logged_in()) {

            $this->ion_auth->logout();
            redirect('auth', 'refresh');
        }

        $this->load->model('module');

        $this->load->model(array('admin/report'));
        $this->load->model(array('admin/paciente'));
        $this->load->model(array('admin/service'));
        $this->load->model(array('admin/options'));
        $this->load->library('phpmailerlib');
        $this->load->library('servicios_lib');

    }

    public function index()
    {

        // $data['services'] = $services;
        $data['page'] = $this->config->item('ci_my_admin_template_dir_admin') . "report_daily";
        $this->load->view($this->_container, $data);
    }

    public function pdf($id)
    {

        $datetime = new DateTime();
        $datetime->modify("-1 day");

        // TESTING $query= $this->db->query("SELECT * from reports,services where reports.service_id = services.tars_id and services.id='".$id."' and timestamp like '18-Sep-2017%' ");
        $query = $this->db->query("SELECT * from reports,services where reports.service_id = services.tars_id and services.id='" . $id . "' and DATE(timestamp) = '" . $datetime->format("Y-m-d") . "'");

        $report_data = $query->result();
        $tars_id = $report_data[0]->tars_id;
        $id_luz = "";

        $otros_datos = array();
        $medicamentos = array();
        $alimento = array();
        $signos_vitales = array();
        $signos_horas = array();
        $dosis = 0;
        $comentarios_generales = "";
        $data_array = array();
        foreach ($report_data as $r) {
            if ($id_luz == "") {
                $id_luz = $r->nurse_id;
            }
            $d = new DateTime($r->timestamp);
            $date = $d->format('d-m-Y');
            $hora = $d->format('d-m-Y H:i');

            if (!array_key_exists($r->report_type, $data_array['report_data'])) {
                $data_array['report_data'][$r->report_type] = array();
            }

            $report_meta = unserialize($r->report_meta);

            if ($r->report_type == "sintoma" || $r->report_type == "inicio" || $r->report_type == "fin") {
                if ($r->report_type == "inicio") {
                    $s = $report_meta['estado_inicial_del_paciente'];
                    $hora_inicio = $hora;
                    $sintoma_inicio = $s;
                } else if ($r->report_type == "fin") {
                    $s = $report_meta['estado_final_del_paciente'];
                    $hora_fin = $hora;
                    $sintoma_fin = $s;
                } else {
                    $s = $report_meta['reporte_de_sintomas'];
                }

                if (!array_key_exists('Sintomas', $otros_datos)) {
                    $otros_datos['Sintomas'] = array();
                }

                $otros_datos['Sintomas'][] = $s . " - " . $hora;
            } else if ($r->report_type == "medicamento") {
                for ($i = 1; $i <= 3; $i++) {
                    $clean_name = $this->stripAccents($report_meta['que_medicina_tomo_' . $i]);
                    if (strlen($clean_name != "")) {
                        $medicamentos[strtolower($clean_name)][] = $hora;
                        if (sizeof($medicamentos[strtolower($clean_name)]) > $dosis) {
                            $dosis = sizeof($medicamentos[strtolower($clean_name)]);
                        }
                    }
                }

            } else if ($r->report_type == "estimulacion_cognitiva") {

                $grade = $this->get_grade($report_meta['como_le_fue']);
                $emoticon = $this->get_emoticon_print($grade);
                $excersice = $report_meta['estimulacion_cognitiva'];
                $otros_datos['Estimulación Cognitiva'][] = array(array("text" => $excersice . " - " . $hora), $emoticon);

            } else if ($r->report_type == "estimulacion_fisica") {

                $excersice = $report_meta['estimulacion_fisica'];
                $otros_datos['Estimulación Física'][] = $excersice . " - " . $hora;
            } else if ($r->report_type == "estimulacion_otra") {
                $excersice = $report_meta['otro_tipo_de_terapia'];
                $otros_datos['Otras Estimulaciones'][] = $excersice . " - " . $hora;

            } else if ($r->report_type == "evacuacion") {
                $otros_datos[$report_meta['evacuacion_o_miccion']][] = $hora;
            } else if ($r->report_type == "alimento") {
                $grade = $this->get_grade($report_meta['como_comio']);
                $emoticon = $this->get_emoticon_print($grade);
                $alimento[$report_meta['reportar_alimentos']] = array(array("text" => $hora), $emoticon);

            } else if ($r->report_type == "signos_vitales") {
                foreach ($report_meta as $key => $value) {
                    if ($value != "") {
                        $signos_vitales[$key][$hora] = $value;
                        if (!in_array($hora, $signos_horas)) {
                            $signos_horas[] = $hora;
                        }
                    }
                }
            } else if ($r->report_type == "observacion_general") {
                $comentarios_generales .= " " . $report_meta['observacion_general'];
            }

        }

        if ($id_luz != "") {
            $data_array['luz_nombre'] = $this->get_nombre_luz($id_luz);
        } else {
            $data_array['luz_nombre'] = " No Disponible ";
        }

        if (empty($hora_inicio)) {$hora_inicio = " No Disponible ";}
        if (empty($sintoma_inicio)) {$sintoma_inicio = " No Disponible ";}
        $data_array['hora_inicio'] = $hora_inicio;
        $data_array['sintoma_inicio'] = $sintoma_inicio;

        if (empty($hora_fin)) {$hora_fin = " No Disponible ";}
        if (empty($sintoma_fin)) {$sintoma_fin = " No Disponible ";}
        $data_array['hora_fin'] = $hora_fin;
        $data_array['sintoma_fin'] = $sintoma_fin;

        if (empty($comentarios_generales)) {$comentarios_generales = " No Disponible ";}
        $data_array['comentarios_generales'] = $comentarios_generales;
        $data_array['otros_datos'] = $this->do_otros_datos_table($otros_datos);
        $data_array['signos_vitales'] = $this->do_signos_vitales_table($signos_vitales, $signos_horas);
        $data_array['medicamentos'] = $this->do_medicamento_table($dosis, $medicamentos);

        if (empty($alimento)) {
            $alimento = array("Desayuno" => array(array("text" => " No Disponible ")),
                "Comida" => array(array("text" => " No Disponible ")),
                "Cena" => array(array("text" => " No Disponible ")),
                "Liquidos" => array(array("text" => " No Disponible ")),
                "Colaciones" => array(array("text" => " No Disponible ")));
        }

        $data_array['alimento'] = $alimento;

        $date = $datetime->format('l, d M Y');
        $data_array['date'] = $date;

        $query = $this->db->query("SELECT * from services where id =" . $id);
        $service = $query->result();
        $name = json_decode($service[0]->name);

        $data_array['id'] = $id;
        $data_array['tars_id'] = $tars_id;
        $data_array['full_name'] = $name->first_name . " " . $name->second_name . " " . $name->last_name . " " . $name->second_last_name;
        $data_array['first_name'] = $name->first_name . " " . $name->second_name;
        $data_array['last_name'] = $name->last_name . " " . $name->second_last_name;
        $data_array['happy'] = $this->getDataURI("/assets/build/img/happy.jpg");
        $data_array['meh'] = $this->getDataURI("/assets/build/img/meh.jpg");
        $data_array['sad'] = $this->getDataURI("/assets/build/img/sad.jpg");
        $data_array['rd_title'] = $this->getDataURI("/assets/build/img/dr-title.jpg");
        $data_array['rd_alimentos'] = $this->getDataURI("/assets/build/img/dr-alimentos.jpg");
        $data_array['rd_medicamentos'] = $this->getDataURI("/assets/build/img/dr-medicamentos.jpg");
        $data_array['rd_signos_vitales'] = $this->getDataURI("/assets/build/img/dr-signos-vitales.jpg");
        $data_array['rd_otros_datos'] = $this->getDataURI("/assets/build/img/dr-otros-datos.jpg");

        $data_array['logo'] = $this->getDataURI("/assets/build/img/pazmental-pdf.jpg");
        $data_array['header'] = $this->getDataURI("/assets/build/img/reporte-mensual-pdf.jpg");

        echo json_encode($data_array);

    }

    public function do_otros_datos_table($otros_datos)
    {
        $table_size = 0;
        foreach ($otros_datos as $od) {
            if (sizeof($od) > $table_size) {$table_size = sizeof($od);}
        }

        $od_table = array();
        foreach (array_keys($otros_datos) as $key) {
            $row = array();
            $row[] = $key;

            for ($i = 0; $i < $table_size; $i++) {
                $data = $otros_datos[$key][$i];
                if (empty($data)) {$row[] = "";} else { $row[] = $data;}
            }
            $od_table[] = $row;

        }
        return $od_table;
    }

    public function do_medicamento_table($dosis, $medicamento)
    {
        $med = array();
        $header = array("Medicamento");
        $dosis += 1;

        for ($d = 1; $d < $dosis; $d++) {
            $header[] = "Dosis " . $d;
        }
        $med[] = $header;

        foreach (array_keys($medicamento) as $m) {
            $row = array();
            $row[] = $m;

            foreach ($medicamento[$m] as $d) {
                $row[] = $d;
            }
            $med[] = $row;
        }
        if (sizeof($med) <= 1) {
            return array();
        } else {
            return $med;
        }
    }

    public function do_signos_vitales_table($signos_vitales, $signos_horas)
    {
        $signo_array = array();
        $header = array("Signo Vital");
        foreach ($signos_horas as $hora) {
            $header[] = $hora;
        }
        $signo_array[] = $header;

        foreach (array_keys($signos_vitales) as $sv) {
            $row = array();
            $row[] = $sv;
            foreach ($signos_vitales[$sv] as $signo) {
                $row[] = $signo;
            }
            $signo_array[] = $row;
        }

        if (sizeof($signo_array) <= 1) {
            return array();
        } else {
            return $signo_array;
        }

    }

    public function getDataURI($image, $mime = '')
    {
        $image = base_url($image);
        $type = pathinfo($image, PATHINFO_EXTENSION);
        $data = file_get_contents($image);
        $dataUri = 'data:image/' . $type . ';base64,' . base64_encode($data);
        return $dataUri;
    }

    public function view($paciente_id = null, $date = null)
    {
        $action = null;
        $data = array();

        if (!$paciente_id) {
            $action = $this->input->post('action');
            $paciente_id = $this->input->post('id');
            $date = $this->input->post('date');
        }

        if (!$action) {
            $action == "view";
        }

        if ($action == "send-get-info") {

        } else {
            if ($date != "") {
                $datetime = new DateTime($date);
                $refresh = 0;
            } else {
                $start_day = $this->input->post('start_date');
                if (!isset($start_day)) {
                    $datetime = new DateTime();
                    $refresh = 1;
                } else {
                    $datetime = new DateTime($start_day);
                    $refresh = 1;
                }
            }

            $paciente = $this->paciente->get($paciente_id);

            $report_data = $this->report->get_reports_daily(array(
                "paciente_id" => $paciente_id,
                "timestamp" => $datetime->format("Y-m-d"),
            ));

            $id_luz = "";
            $datos_servicio = array();
            $otros_datos = array();
            $alimento = array();

            foreach ($report_data as $r) {

                // creamos un array con todos los servicios pero divididos por tars_id
                if (! array_key_exists($r->tars_id, $datos_servicio)) {
                    $datos_servicio[$r->tars_id] = array();

                    // para saber el plan al que pertenece el servicio
                    $datos_servicio[$r->tars_id]["service_plan"] = $this->servicios_lib->get_service_plan_id($r->servicio_id);
                }
   
                // recuperamos los valores de reports meta
                $report_meta = unserialize($r->report_meta);

                $d = new DateTime($r->timestamp);
                $hora = $d->format('H:i');

                $fecha_inicio_report = $d->format('Y-m-d H:i');

                if ($r->report_type == "sintoma" || $r->report_type == "inicio" || $r->report_type == "fin") {
                    if ($r->report_type == "inicio") {
                        $sintoma = $report_meta['estado_inicial_del_paciente'];

                        $datos_servicio[$r->tars_id]["hora_inicio"] = $hora;
                        $datos_servicio[$r->tars_id]["fecha_inicio"] = $fecha_inicio_report;
                        $datos_servicio[$r->tars_id]["nombre_luz"] = $this->get_nombre_luz($r->nurse_id);
                        $datos_servicio[$r->tars_id]["sintoma_inicio"] = $sintoma;

                    } else if ($r->report_type == "fin" && $datos_servicio[$r->tars_id]["service_plan"] != TIPO_SERVICIO_TRES_PUNTO_CINCO) {
                        $sintoma = $report_meta['estado_final_del_paciente'];
                        $datos_servicio[$r->tars_id]["sintoma_fin"] = $sintoma;
                        $datos_servicio[$r->tars_id]["hora_fin"] = $hora;
                    } else {
                        $s = $report_meta['reporte_de_sintomas'];
                    }

                    if (! array_key_exists('Sintomas', $otros_datos)) {
                        $otros_datos['Sintomas'] = array();
                    }

                    $otros_datos['Sintomas'][]= $s. " - " .$hora;

                } else if($r->report_type=="medicamento" ){

                    // pendiente de revisar
                    for($i=1;$i<=3;$i++){
                        $clean_name = $this->stripAccents($report_meta['que_medicina_tomo_'.$i]);
                        if(strlen($clean_name != "")){
                            $medicamentos[strtolower($clean_name)][]= $hora;
                            if (sizeof($medicamentos[strtolower($clean_name)]) > $dosis){
                                $dosis = sizeof($medicamentos[strtolower($clean_name)]);
                            } 
                        }
                    }
                            
                }  else if($r->report_type=="estimulacion_cognitiva" ){
                    
                    $grade = $this->get_grade($report_meta['como_le_fue']);
                    $emoticon = $this->get_emoticon($grade);
                    $excersice = $report_meta['estimulacion_cognitiva'];
                    $otros_datos['Estimulación Cognitiva'][]= $excersice." - ".$hora. " ". $emoticon;
                    
                } else if($r->report_type=="estimulacion_fisica" ){
                    
                    $excersice = $report_meta['estimulacion_fisica'];
                    $otros_datos['Estimulación Física'][]= $excersice." - ".$hora;
                }  else if($r->report_type=="estimulacion_otra" ){
                    $excersice = $report_meta['otro_tipo_de_terapia'];
                    $otros_datos['Otras Estimulaciones'][]= $excersice." - ".$hora;
                
                }  else if($r->report_type=="evacuacion" ){
                    $otros_datos[$report_meta['evacuacion_o_miccion']][]= $hora;
                } else if($r->report_type=="alimento" ){
                    $grade = $this->get_grade($report_meta['como_comio']);
                    $emoticon = $this->get_emoticon($grade);

                    if(! array_key_exists($report_meta['reportar_alimentos'], $alimento)) {
                        $alimento[$report_meta['reportar_alimentos']] = array();
                    }

                    $alimento[$report_meta['reportar_alimentos']][] = $hora. " ".$emoticon;
                                   
                }  else if($r->report_type=="signos_vitales" ){
                    // pendiente de revisar
                    foreach($report_meta as $key=>$value){
                        if ($value != ""){

                            if(! array_key_exists($key, $signos_vitales)) {
                                $signos_vitales[$key] = array();
                            }

                            if(! array_key_exists($hora, $signos_vitales[$key])) {
                                $signos_vitales[$key][$hora] = array();
                            }

                            $signos_vitales[$key][$hora][] = $value;
                            
                            if(! in_array($hora, $signos_horas)){
                                $signos_horas[]= $hora;
                            }
                        }
                    }
                } else if($r->report_type=="observacion_general") {
                    $datos_servicio[$r->tars_id]["observacion_general"] = " " . $report_meta['observacion_general'];
                } 

                // esto se tiene que ejecutar unicamente con los servicios tipo 3.5
                if(($datos_servicio[$r->tars_id]["service_plan"] == TIPO_SERVICIO_TRES_PUNTO_CINCO) && (!isset($datos_servicio[$r->tars_id]["fecha_fin"]))) {

                    $fecha_fin_reporte = $this->servicios_lib->get_plan_tres_punto_cinco_mas_un_dia($r->service_id, $fecha_inicio_report);
                    if($fecha_fin_reporte) {
                        $fecha_fin_reporte = new DateTime($fecha_fin_reporte->timestamp);
                        $datos_servicio[$r->tars_id]["fecha_fin"] = $fecha_fin_reporte->format("Y-m-d H:i");
                        $datos_servicio[$r->tars_id]["hora_fin"] = $fecha_fin_reporte->format("H:i");
                    } 
                } 
            }
            $data["datos_servicio"] = $datos_servicio;
            $data["alimento"] = $alimento;
            $data["signos_vitales"] = $signos_vitales;
            $data["signos_horas"] = $signos_horas;
            $data["otros_datos"] = $otros_datos;
            $data["medicamentos"] = $medicamentos;
            $data["dosis"] = $dosis;

            $name = json_decode($paciente->name);

            // datos del paciente
            $data['id'] = $paciente->id;
            $data['tars_id'] = $paciente->tars_id;
            $data['full_name'] = $name->first_name . " " . $name->second_name . " " . $name->last_name . " " . $name->second_last_name;
            $data['first_name'] = $name->first_name . " " . $name->second_name;
            $data['last_name'] = $name->last_name . " " . $name->second_last_name;

            // datos para datepicker filtro por fecha
            $data['yesterday'] = $datetime->format("Y-m-d");
            $date = $datetime->format('l, d M Y');
            $data['date'] = $date;
            $data['start_date'] = $datetime->format('m-d-Y');
            $data['today'] = $datetime->format("Y-m-d");
            $datetime->modify('-1 day');
            $data['yesterday'] = $datetime->format("Y-m-d");
            $datetime->modify('+2 day');
            $data['tomorrow'] = $datetime->format("Y-m-d");

            $data['sending_mail'] = false;
            if ($action == "send") {
                $data['sending_mail'] = true;
            } else {
                $user_id = $this->ion_auth->get_user_id();
                $group_name = $this->ion_auth->get_users_groups()->row()->name;
                $data['group_name'] = $group_name;

                if ($refresh == 0) {
                    $data['page'] = $this->config->item('ci_my_admin_template_dir_admin') . "report_daily";
                    $this->load->view($this->_container, $data);
                } else {
                    $data['page'] = $this->config->item('ci_my_admin_template_dir_admin') . "report_daily";
                    $this->load->view($this->_container, $data);
                }
            }
        }
    }

    public function stripAccents($str)
    {
        return strtr(utf8_decode($str), utf8_decode('àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ'), 'aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY');
    }

    public function get_grade($grade)
    {
        if ($grade == "Bien") {$grade = 100;} else if ($grade == "Regular") {$grade = 75;} else if ($grade == "Mal") {$grade = 50;}
        return $grade;
    }

    public function get_emoticon($grade)
    {
        $emo = "";
        if ($grade == 100) {$emo = '<span class="happy"> - Bien</span>';} else if ($grade == 75) {$emo = '<span class="meh"> - Regular </span>';} else if ($grade == 50) {$emo = '<span class="sad"> - Mal </span>';}
        return $emo;
    }

    public function get_emoticon_print($grade)
    {
        $emo = "";

        if ($grade == 100) {$emo = array('image' => 'happy', 'width' => 10);} else if ($grade == 75) {$emo = array('image' => 'meh', 'width' => 10);} else if ($grade == 50) {$emo = array('image' => 'sad', 'width' => 10);}
        return $emo;
    }

    public function get_nombre_luz($id_luz)
    {
        if(!$id_luz){
            return '';
        }

        $query = $this->db->query("SELECT first_name,last_name from users,users_meta where users.id = users_meta.user_id and meta_key='_numero_luz' and meta_value= " . $id_luz);

        /*
        $this->db->select("*");
        $this->db->from("users");
        $this->db->where("id", $id_luz);
        $query = $this->db->get();
        */


        log_message("debug", "--- query  get_nombre_luz ---");
        log_message("debug", $this->db->last_query());
        log_message("debug", "--- query  get_nombre_luz ---");

        $nombre_luz = '';
        if($query->num_rows() > 0) {
            $nurse = $query->result();
            $nombre_luz = $nurse[0]->first_name . " " . $nurse[0]->last_name;
        }

        return $nombre_luz;
    }
}
