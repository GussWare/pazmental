<div class="login_wrapper">
    <div class="animate form login_form">
          <section class="login_content">
            <form role="form" method="POST" action="<?= base_url('auth/reset_password/' .$code) ?>">
            <input type="hidden" value="<?php echo $user_id ?>" name="user_id" />
            <input type="hidden" value="<?php echo $code ?>" name="code" />
            <?php echo form_hidden($csrf); ?>

              <h1>Resetear</h1>
                    <?php if (isset($message) && $message != ""): ?>
                        <div class="alert alert-danger fade in">
                            <a href="#" class="close" data-dismiss="alert">&times;</a>
                            <?php echo $message; ?>
                        </div>
                    <?php endif; ?>
          
                        
                    <div class="form-group">
                                <input class="form-control" placeholder="Password" name="new" type="password" value="">
                            </div>
                            <div class="form-group">
                                <input class="form-control" placeholder="Confirmar Password" name="new_confirm" type="password" value="">
                            </div>
                          <div>
                            <br/>
                              <button class="btn btn-default submit" type="submit">Resetear</button>
                            <a class="reset_pass" href="<?php echo base_url('auth/login'); ?>">Iniciar Sesión</a>
                          </div>

              <div class="clearfix"></div>

              <div class="separator">
                <div class="clearfix"></div>
                <br />

                <div class="logo">
                 <img src="<?php echo base_url();?>/assets/build/img/pazmental-logo-gray.png" />
                  <p><?php echo date('Y'); ?> &copy All Rights Reserved.Paz Mental</p>
                </div>
              </div>
            </form>
          </section>
        </div>
      
</div>
