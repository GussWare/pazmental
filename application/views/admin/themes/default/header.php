<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Paz Mental | </title>

    <!-- Bootstrap -->
    <link href="<?php echo base_url();?>assets/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo base_url();?>assets/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?php echo base_url();?>assets/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="<?php echo base_url();?>assets/vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    
    <!-- bootstrap-progressbar -->
    <link href="<?php echo base_url();?>assets/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <!-- JQVMap -->
    <link href="<?php echo base_url();?>assets/vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>
    <!-- bootstrap-daterangepicker -->
    <link href="<?php echo base_url();?>assets/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/vendors/fullcalendar/dist/fullcalendar.min.css" rel="stylesheet">
   <link href="<?php echo base_url();?>assets/vendors/select2/dist/css/select2.min.css" rel="stylesheet">
    <!-- Switchery -->
    <link href="<?php echo base_url();?>assets/vendors/switchery/dist/switchery.min.css" rel="stylesheet">
      <link href="<?php echo base_url();?>assets/vendors/dropzone/dist/min/dropzone.min.css" rel="stylesheet">

       <!-- PNotify -->
    <link href="<?php echo base_url();?>assets/vendors/pnotify/dist/pnotify.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/vendors/pnotify/dist/pnotify.buttons.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/vendors/pnotify/dist/pnotify.nonblock.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="<?php echo base_url();?>assets/build/css/custom.min.css?v=<?php echo $this->config->item('numero', 'version'); ?>" rel="stylesheet">
    
    <!-- Bootstrap Clock Picker -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/vendors/clockpicker-gh-pages/dist/bootstrap-clockpicker.min.css">
  
    <!-- Reports -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqcloud/1.0.4/jqcloud.css">


    
  </head>

  <body class="nav-md">
    
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="<?php echo base_url('admin/dashboard');?>" class="site_title"><img src="<?php echo base_url(); ?>/assets/build/img/pazmental-logo.png" /></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <!-- <div class="profile_pic">
                <img src="images/img.jpg" alt="..." class="img-circle profile_img">
              </div> -->
              <div class="profile_info">
                <img class="profile_img" src="<?php echo base_url(); ?>/assets/build/img/profile-img.png" />
                <span class="dark-blue-txt"> BIENVENIDO(A),</span>
                <h2><span class="white-txt"><?php echo ucfirst($current_user->first_name); ?></span> <br/><span class="dark-blue-txt"> <?php echo ucfirst($current_user->last_name); ?></span></h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
               
                <ul class="nav side-menu white-txt-menu white-txt">
                <?php  if($group_name == 'admin' or $group_name == "supervisor"){ foreach($modules as $module) { ?>
    
                    <?php if(!empty($module['children'])) { ?>
                  
                             <li><a><i class="fa fa-<?php echo $module['parent']['icon']; ?>"></i> <?php echo $module['parent']['name']; ?> <span class="fa fa-caret-down"></span></a>
                            <ul class="nav child_menu white-txt-menu white-txt">
                                <?php foreach($module['children'] as $child) {
                                  echo '<li><a href="'.base_url().$child['slug'].'">'.$child['name'].'</a></li>';
                                 }
                                ?>
                            </ul>

                    <?php } else { ?>
                    <li><a href="<?=base_url().$module['parent']['slug'];?>"><i class="fa fa-<?php echo $module['parent']['icon']; ?>"></i> <?php echo $module['parent']['name']; ?></a>
                   <?php }
                    ?>
                  </li>
                   
                <?php } }?>
                 
               </ul>
              </div>

            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
              
              <a data-toggle="tooltip" data-placement="top" title="Logout" href="<?php echo base_url('auth/logout');?>">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle dark-blue-txt dark-blue-profile" data-toggle="dropdown" aria-expanded="false">
                    <?php echo ucfirst($current_user->first_name); ?> <?php echo ucfirst($current_user->last_name); ?>
                    <span class="fa fa-caret-down pm-red"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <!--<li><a href="javascript:;"> Profile</a></li>
                    <li>
                      <a href="<?php echo base_url('admin/users'); ?>">
                        <span>Settings</span>
                      </a>
                    </li>
                    <li><a href="javascript:;">Help</a></li>-->
                    <li><a href="<?php echo base_url('auth/logout');?>"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                  </ul>
                </li>

              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->