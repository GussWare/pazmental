<?php (defined('BASEPATH')) OR exit('No direct script access allowed');


class NursesSettings extends Admin_Controller {

    function __construct() {
        parent::__construct();

        $group = 'admin';
          $this->load->model(array('module'));
          $this->load->model(array('admin/options'));

          if (!$this->ion_auth->logged_in()) {
   
             $this->ion_auth->logout();
             redirect('auth', 'refresh');
        }
        
        if (!$this->ion_auth->in_group($group))
        {
            $this->session->set_flashdata('message', 'You must be an administrator to view the user groups page.');
            redirect('admin/dashboard');
        }
    }

    public function index() {    
        $groups = $this->ion_auth->groups()->result();

        $data['characteristics'] = $this->options->get_all('*',array('option_name'=>'_characterisctic'));
        $data['personalities'] = $this->options->get_all('*',array('option_name'=>'_personality'));
        $data['hobbies'] = $this->options->get_all('*',array('option_name'=>'_hobby'));
        $data['institutions'] = $this->options->get_all('*',array('option_name'=>'_institution'));
        $data['care_skills'] = $this->options->get_all('*',array('option_name'=>'_care_skill'));
        $data['therapy_skills'] = $this->options->get_all('*',array('option_name'=>'_therapy_skill'));
        $data['recruitment_sources'] = $this->options->get_all('*',array('option_name'=>'_recruitment_source'));
        $data['page'] = $this->config->item('ci_my_admin_template_dir_admin') . "settings_services";
        $data['groups'] = $groups;
        $data['page'] = $this->config->item('ci_my_admin_template_dir_admin') . "nurses_settings";
        $this->load->view($this->_container, $data);
    }

 
    /*public function create() {
        if ($this->input->post('name')) {
            $name = $this->input->post('name');
            $description = $this->input->post('description');

            $group = $this->ion_auth->create_group($name,$description);

            if(!$group)
            {
                $view_errors = $this->ion_auth->messages();
            }
            else
            {
                redirect('/admin/user-groups', 'refresh');
            }
        }
        $data['page'] = $this->config->item('ci_my_admin_template_dir_admin') . "groups_create";
        $this->load->view($this->_container, $data);
    }*/

    /*public function edit($id) {
        if ($this->input->post('name')) {
            $name = $this->input->post('name');
            $description = $this->input->post('description');
           
            $group_update = $this->ion_auth->update_group($id, $name, $description, $slug);

            redirect('/admin/user-groups', 'refresh');
        }

        $group =  $this->ion_auth->group($id)->row();

        $data['group'] = $group;
        $data['page'] = $this->config->item('ci_my_admin_template_dir_admin') . "groups_edit";
        $this->load->view($this->_container, $data);
    }
        */
    public function delete($id) {
        

        $this->db->where( 'id',$id);
        $this->db->delete('options'); 

        redirect('/admin/nurses_settings', 'refresh');
    } 
}
