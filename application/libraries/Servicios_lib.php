<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Servicios_lib
{

    public function __construct()
    {
        $this->CI = &get_instance();
    }

    /**
     * Metodo que se encarga de recuperar las colonias que se encuentran dadas de alta
     *
     * @return void
     */
    public function get_all_colonies()
    {
        $query = $this->CI->db->query("SELECT DISTINCT meta_value as colony from users_meta where meta_key='_colony'");
        $results = $query->result_array();
        return $results;
    }

    /**
     * Metodo que se encarga de recupaerar los datos de services plan
     *
     * @return void
     */
    public function get_services_meta_for_list()
    {
        $service_meta = array();

        $this->CI->db->select("*");
        $this->CI->db->from("services_meta");
        $this->CI->db->where("service_meta", "_service_plan");

        $query = $this->CI->db->get();
        $results = $query->result_array();

        /*TIPO DE PLAN*/
        foreach ($results as $result) {
            $id = $result['service_id'];
            if (!array_key_exists($id, $service_meta)) {
                $service_meta[$id] = array();
            }

            $service_plan_data = $this->CI->options->get($result['service_value']);
            if ($service_plan_data) {
                $service_data = json_decode($service_plan_data->option_value);
                $service_meta[$id]['tipo_plan'] = $service_data->name;
            }
        }

        /*LUCES Y HORARIOS*/
        $this->CI->db->select("id");
        $this->CI->db->from("services");
        $query = $this->CI->db->get();
        $results = $query->result_array();
        foreach ($results as $result) {
            $id = $result['id'];
            if (!array_key_exists($id, $service_meta)) {
                $service_meta[$id] = array();
            }

            $service_meta[$id]['schedule'] = $this->CI->report_maker->get_short_schedule($id);
        }

        return $service_meta;
    }

    /**
     * Metodo que se encarga de recuperar la información del mapa
     *
     * @return void
     */
    public function get_lat_results()
    {
        $query = $this->CI->db->query("SELECT schedule.*, users.first_name,users.last_name, t1.service_id,   t1.service_value as map_long,
                                t2.service_value as map_lat
                                FROM services_meta t1
                                JOIN services_meta t2 ON t2.service_id = t1.service_id
                                JOIN schedule ON t2.service_id = schedule.service_id and t1.service_id = schedule.service_id
                                JOIN users ON  schedule.nurse_id = users.id
                                WHERE t1.service_meta = '_map_long' and t2.service_meta ='_map_lat' ");
        $query_r = $query->result();
        $lat_results = array();
        foreach ($query_r as $q) {

            if (!array_key_exists($q->nurse_id, $lat_results)) {

                $lat_results[$q->nurse_id] = array();
            }
            if (!array_key_exists($q->date, $lat_results[$q->nurse_id])) {
                $lat_results[$q->nurse_id][$q->date] = array('start' => array(), 'end' => array());
            }

            $times_array = json_decode($q->times);
            $new_start = strtotime($times_array[0]) - (60 * 60);

            $new_start_date = date('H:i', $new_start);

            $new_end = strtotime(end($times_array)) + (60 * 60);
            $new_end_date = date('H:i', $new_end);

            $lat_results[$q->nurse_id][$q->date]['start']['time'] = $new_start_date;
            $lat_results[$q->nurse_id][$q->date]['start']['lat'] = $q->map_lat;
            $lat_results[$q->nurse_id][$q->date]['start']['lng'] = $q->map_long;
            $lat_results[$q->nurse_id][$q->date]['start']['name'] = $q->first_name . " " . $q->last_name;
            $lat_results[$q->nurse_id][$q->date]['end']['time'] = $new_end_date;
            $lat_results[$q->nurse_id][$q->date]['end']['lat'] = $q->map_lat;
            $lat_results[$q->nurse_id][$q->date]['end']['lng'] = $q->map_long;
            $lat_results[$q->nurse_id][$q->date]['end']['name'] = $q->first_name . " " . $q->last_name;
        }

        return $lat_results;
    }

    /**
     * Metodo que se encarga de recuperar las luces deacuerdo a su direccion en el mapa
     *
     * @return void
     */
    public function get_nurse_homes()
    {
        $query = $this->CI->db->query("SELECT users.first_name,users.last_name, users.id as nurse_id, t1.meta_value as map_long,
                                t2.meta_value as map_lat
                                FROM users_meta t1
                                JOIN users_meta t2 ON t2.user_id = t1.user_id
                                JOIN users ON  t1.user_id = users.id
                                WHERE t1.meta_key = '_map_long' and t2.meta_key ='_map_lat' ");
        $query_r = $query->result();
        $lat_results = array();

        foreach ($query_r as $q) {
            $lat_results[$q->nurse_id] = array('lat' => $q->map_lat, 'lng' => $q->map_long, 'name' => $q->first_name . " " . $q->last_name);
        }

        return $lat_results;

    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function do_schedule()
    {
        $begin = new DateTime('today');

        $end = new DateTime('today');
        $end = $end->modify('+8 day');

        $period = new DatePeriod(
            $begin,
            new DateInterval('P1D'),
            $end
        );

        $trans = array("Monday" => "Lunes", "Tuesday" => "Martes",
            "Wednesday" => "Miercoles", "Thursday" => "Jueves",
            "Friday" => "Viernes", "Saturday" => "Sabado",
            "Sunday" => "Domingo");

        $daysofweek = array();
        foreach ($period as $p) {

            $dayofweek = $p->format("l");
            $date = $p->format("Y-m-d");
            if (!array_key_exists($trans[$dayofweek], $daysofweek)) {
                $daysofweek[$trans[$dayofweek]] = array();
            }
            $daysofweek[$trans[$dayofweek]][] = $date;
        }

       // $query = $this->CI->db->query("DELETE from agenda where date(start_time) >= '" . $begin->format("Y-m-d") . "'");
        $query = $this->CI->db->query("SELECT schedule.*, schedule_fechas_arranque.id AS id_schedule_fecha_arranque, schedule_fechas_arranque.fecha_arranque FROM schedule INNER JOIN services ON schedule.service_id = services.id LEFT JOIN schedule_fechas_arranque ON services.id = schedule_fechas_arranque.service_id AND schedule.nurse_id = schedule_fechas_arranque.nurse_id;");

        $results = $query->result_array();
        foreach ($results as $row) {

            $day = $row['date'];
            $times = json_decode($row['times']);

            $nurse_id = $row['nurse_id'];
            $service_id = $row['service_id'];

            if ($service_id == 92)
            {
                $solo_para_pruebas = true;
                $this->CI->db->error();
            }

            $query_meta = $this->CI->db->query("SELECT * FROM services_meta WHERE service_meta = '_service_plan' AND service_id = " . $service_id . " LIMIT 1");
            if ($query_meta->num_rows() > 0) {
                $result_meta = $query_meta->result_array();
                $row_meta = $result_meta[0];
                if ($row_meta['service_value'] == TIPO_SERVICIO_TRES_PUNTO_CINCO) {
                    
                    if (isset($row['fecha_arranque'])) {
                        $date_start = new DateTime($row['fecha_arranque']);
                        $date_end = new DateTime($row['fecha_arranque']);
                        $date_end = $date_end->modify('+8 day');

                        $period_24_x_3_punto_5 = new DatePeriod(
                            $date_start,
                            new DateInterval('P2D'),
                            $date_end
                        );

                        $fecha_final = null;

                        foreach($period_24_x_3_punto_5 AS $dia) {
                            $dia_periodo = $dia->format("Y-m-d");
            
                            $start = new DateTime($dia_periodo." ".$times[0]);
                            $end = new DateTime($dia_periodo." ".end($times));
                            $fecha_final = $end->format("Y-m-d");
        
                            if(!$this->CI->db->query("INSERT INTO agenda (nurse_id, service_id, start_time, end_time) VALUES ('".$nurse_id."','".$service_id."','".$start->format("Y-m-d H:i:s")."','".$end->format("Y-m-d H:i:s")."')")){ 
                                printf("Errormessage: %s\n", $this->CI->db->error());
                                exit();
                            }
                        }

                        if(isset($fecha_final) && isset($row['id_schedule_fecha_arranque'])) {
                            if(!$this->CI->db->query("UPDATE schedule_fechas_arranque SET fecha_arranque = '".$fecha_final."' WHERE id = ". $row['id_schedule_fecha_arranque'] .";")){
                                printf("Errormessage: %s\n", $this->CI->db->error());
                                exit();
                            }
                        }
                    }
                } else {
                    foreach ($daysofweek[$day] as $d) {
                        $start = new DateTime($d . " " . $times[0]);
                        $end = new DateTime($d . " " . end($times));

                        if(!$this->CI->db->query("INSERT INTO agenda (nurse_id, service_id, start_time, end_time) VALUES ('" . $nurse_id . "','" . $service_id . "','" . $start->format("Y-m-d H:i:s") . "','" . $end->format("Y-m-d H:i:s") . "')")){
                            printf("Errormessage: %s\n", $this->CI->db->error());
                            exit();
                        }
                    }
                }
            }
        }
    }

    public function get_service_plan($id)
    {
        $query = $this->CI->db->query("SELECT option_value
                    from options, services_meta
                    where services_meta.service_meta = '_service_plan'
                    and service_id = '$id'
                    and services_meta.service_value = options.id");

        $result = $query->result();

        if (!empty($result)) {
            $service_plan = json_decode($result[0]->option_value);
            return $service_plan->name . " (" . $service_plan->description . ")";
        } else {
            return "No Asignado";
        }
    }

    public function get_service_plan_id($id)
    {
        $query = $this->CI->db->query("SELECT options.id
                    from options, services_meta
                    where services_meta.service_meta = '_service_plan'
                    and service_id = '$id'
                    and services_meta.service_value = options.id");

        $service_plan = null;

        if ($query->num_rows() > 0) {
            $result = $query->result();
            $service_plan = $result[0]->id;
        }

        return $service_plan;
    }

    /**
     * Se modifico este query para poder sacar esta información de la nueva talba de pacientes
     *
     * @param [type] $id
     * @return void
     */
    public function get_disease($id)
    {
        $query = $this->CI->db->query("SELECT option_value
                    from options, pacientes
                    where pacientes.id = '$id'
                    and   pacientes.disease = options.id");

        $result = $query->result();

        if (!empty($result)) {
            $disease = json_decode($result[0]->option_value);
            return $disease->name;
        } else {
            return "No Asignado";
        }
    }

    public function get_service_reps($id, $type)
    {
        $query = $this->CI->db->query("select first_name,last_name
                    from services_relationship,users
                    where services_relationship.service_id='$id'
                    and relation_term='$type'
                    and services_relationship.relation_value=users.id");
        $result = $query->result();

        if (!empty($result)) {
            return $result[0]->first_name . " " . $result[0]->last_name;
        } else {
            return "No Asignado";
        }
    }

    public function do_routine_array($routines)
    {

        $rows = 0;
        $table = array();
        $header = array();
        foreach ($routines as $key => $value) {
            $header[] = $key;
            $size = sizeof($value);
            if ($size > $rows) {
                $rows = $size;}
        }
        $table[] = $header;
        $i = 0;
        while ($i < $rows) {
            $row = array();
            foreach ($routines as $key => $value) {
                $ex = '';
                $tipo_actividad_arr = explode("_", $value[$i][0]);
                $descripcion_arr = explode("_", $value[$i][1]);

                if (count($tipo_actividad_arr) > 1) {
                    $ex .= $tipo_actividad_arr[1] . " ";
                } else {
                    $ex .= $tipo_actividad_arr[0] . " ";
                }

                if (count($descripcion_arr) > 1) {
                    $ex .= $descripcion_arr[1] . " ";
                } else {
                    $ex .= $descripcion_arr[0] . " ";
                }

                $ex .= $value[$i][2];

                /*
                if (trim($ex) != "") {
                $row[] = $ex;
                }
                 */

                $row[] = $ex;
            }
            $i += 1;
            /*
            if (!empty($row)) {
            $table[] = $row;
            }
             */

            $table[] = $row;
        }
        return $table;

    }

    public function do_medicine_array($medicine)
    {
        $table = array(["Medicamento", "Dosis", "Veces por día", "Horas"]);
        foreach ($medicine as $med) {
            $row = array();
            $hours = $med[3];
            $table[] = [$med[0], $med[1], $med[2], implode(",", $hours)];
        }
        return $table;
    }

    public function do_familiar_array($familiars)
    {
        $table = array(["Nombre Completo", "Relación", "Email", "Teléfono"]);
        foreach ($familiars as $fam) {
            $row = array();
            $data = $fam['data'];
            $name = $data->first_name . " " . $data->last_name;
            $table[] = [$name, $fam['relationToPatient'], $data->email, $data->phone];
        }
        return $table;
    }

    public function getDataURI($image, $mime = '')
    {
        $image = base_url($image);
        $type = pathinfo($image, PATHINFO_EXTENSION);
        $data = file_get_contents($image);
        $dataUri = 'data:image/' . $type . ';base64,' . base64_encode($data);
        return $dataUri;
    }

    /**
     * Metodo que se encarga de buscar el reporte final de un servicio con plan 3.5 
     * por regla se aumenta un dia para realizar la busqueda, esta funcion solo debe ser utilizada
     * pra los planes 3.5
     *
     * @param date $fecha
     * @return time
     */
    public function get_plan_tres_punto_cinco_mas_un_dia($report_service_id, $fecha) {

        $fecha_plan = new DateTime($fecha);
        $fecha_plan_mas_uno = $fecha_plan->modify('+1 day');
        $fecha_buscar = $fecha_plan_mas_uno->format('Y-m-d');

        $this->CI->db->select("*");
        $this->CI->db->from("reports");
        $this->CI->db->where("report_type", "fin");
        $this->CI->db->where("service_id", $report_service_id);
        $this->CI->db->where("DATE(timestamp)", $fecha_buscar);
        
        $query = $this->CI->db->get();

        log_message("debug", "------ query reporte plan_trespuntocinco");
        log_message("debug", $this->CI->db->last_query());
        log_message("debug", "------ query reporte plan_trespuntocinco");

        $data = array();
        if($query->num_rows() > 0) {
            $data = $query->result();
            $data = $data[0];
        }

        return $data;
    }

}

/* End of file Servicios_lib.php */
