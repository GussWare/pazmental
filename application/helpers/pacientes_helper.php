<?php
defined('BASEPATH') or exit('No direct script access allowed');

if (!function_exists('pacientes_agrupar_listado')) {
    /**
     * Metodo que se encarga de realizar una agrupacion de los pacientes con sus servicios
     * ya que al ser una relacion de uno a muchos la información de paciente puede verse repetida.
     *
     * La agrepuacion resultante sera como la siguiete:
     *
     *  $pacientes = array(
     *      'paciente_1' => array(
     *          array(
     *                   'id' => 1
     *                   'nombre' => '',
     *                   'direccion =>' ''
     *                   'servicios' => array(
     *                       array(
     *                           'id' => 1,
     *                           'tars_id => 1
     *                       ),
     *                       array(
     *                           'id' => 2,
     *                           'tars_id => 2
     *                       )
     *                   )
     *          )
     *      )
     *  )
     *
     * @param array $pacientes
     * @return array
     */
    function pacientes_agrupar_listado(&$pacientes)
    {

        $result = array();
        foreach ($pacientes as $key => $paciente) {

            // agrupamos la info del paciente
            if (!isset($result['paciente_' . $paciente->id])) {
                $result['paciente_' . $paciente->id] = array(
                    'id' => $paciente->id,
                    'tars_id' => $paciente->tars_id,
                    'name' => $paciente->name,
                    'address' => $paciente->address,
                    'city' => $paciente->city,
                    'zip' => $paciente->zip,
                    'state' => $paciente->state,
                    'country' => $paciente->country,
                    'phones' => $paciente->phones,
                    'goals' => $paciente->goals,
                    'ife' => $paciente->ife,
                    'curp' => $paciente->curp,
                    'disease' => $paciente->disease,
                    'nombre_familia' => $paciente->nombre_familia,
                    'services' => []
                );
            }

            // agrupamos la info de los servicios
            if ((isset($result['paciente_' . $paciente->id])) && (isset($paciente->service_id)) && (isset($paciente->tars_id))) {
                $result['paciente_' . $paciente->id]['services'][] = array(
                    'id' => $paciente->service_id,
                    'tars_id' => $paciente->tars_id,
                    'service_type' => $paciente->service_type,
                    'start_date' => $paciente->start_date,
                    'control_records' => $paciente->control_records,
                    'step' => $paciente->step,
                    'paciente_id' => $paciente->paciente_id,
                );
            }
        }

        log_message("debug", json_encode($result));
        return $result;
    }
}

if (!function_exists('crear_array_pacientes_meta')) {

    /**
     * Metodo que se encarga de generar un arreglo tipo llave => valor con los datos meta del paciente
     *
     * @param array $data
     * @return array
     */
    function crear_array_pacientes_meta($data = array())
    {
        $pacientes_meta = array();

        foreach ($data as $meta) {
            $pacientes_meta[$meta->paciente_meta] = $meta->paciente_value;
        }

        return $pacientes_meta;
    }
}

/* End of file pacientes_helper.php */
