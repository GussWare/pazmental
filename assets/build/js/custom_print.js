var dataReportPdf = {};
var actionReportPdf= '';
$(document).ready(function() {


	$("#reports_include_signos_vitales").on('click',function(e){ 
		if($(this).is(':checked')) {
			$('.content-signos-vitales').fadeIn();
		} else {
			$('.content-signos-vitales').fadeOut();
		}
	});

	
	
		$(document).on('click',".print_daily_pdf",function(e){
			e.preventDefault();

			var url = jQuery(this).attr('href');
  			var id = jQuery(this).attr('report-id');
  			var action = jQuery(this).attr('action');
			  var date = jQuery(this).attr('date');
			  var date_filter = jQuery("#start_date").val();

			  date_explode = date_filter.split("/");
			  date_final = date_explode[2] + "-"+ date_explode[0] + "-" +date_explode[1];
  			
			$.ajax({
		        url: url,
		        data: {
		         	id:id,
		         	action:action,
		         	date:date_final,
		        },
		        type: 'POST',
		        success: function (respuesta) {

		        	if(action == "send-get-info"){ 	
		        	  	bootbox.dialog({
			                title: "Enviar Reporte",
			                message: respuesta,
			                buttons: {
			                    success: {
			                        label: 'Enviar',
			                        //className: 'hidden',
			                        callback: function (a) {

			                        	a.preventDefault();
			                        	var module = "send-report";
			                        	var form = $('#form-' + module).serialize();
	  									
			                           	$.ajax({ 
			                           		url:url,
			                           		data:{
			                           			form:form,
			                           			id:id,
			                           			date:date_final,
			                           			action:"send",
			                           		},
			                           		type:'POST',
			                           		success: function (response) {
												var resp = JSON.parse(response);
				                           	 	if(resp.status=='success'){
					                           	 		new PNotify({
															title: "Enviar Reporte",
															type: "success",
															text: resp.message,
															nonblock: {
																nonblock: true
															},
															addclass: '',
															styling: 'bootstrap3'
														});
				                           	 	}
						                	}
			                   		 });
			                	}
			            	}
			            }
			        	});
			            
		        	}
		        	else{ 
		        	
		        		print_daily_pdf(respuesta,"print");
					}
				}

			});
		});

	function print_daily_pdf(respuesta,action){
		var data = JSON.parse(respuesta);
		        	var alimento = data.alimento;
		        	var content = Array();

		        	
		        	content.push({columns: [
						{
							image: data.logo,
							width: 160
						},
						[
							{image: data.rd_title,
								width:217
							},	
							{
							text: "Fecha: "+ data.date
								
							},
							{text: data.first_name, style:[ 'header1bold' ],	margin: [0, 30,0,0]
							},
							{text: data.last_name.toUpperCase(),style:[ 'header1light' ]}
						],
						
					  ],
					   columnGap: 20,
					   
					});

					content.push({columns: [
						[ 
							{text:"Hora de Inicio",style:['header2']},
							{text:data.hora_inicio,margin: [0, 0,0,30]},
							{text:"Hora de Fin",style:['header2']},
							{text:data.hora_fin,margin: [0, 0,0,30]},
							
						],
						[ 
							{text:"Estado anímico inicio de servicio:",style:['header2']},
							{text:data.sintoma_inicio,margin: [0, 0,0,30]},
							{text:"Estado anímico fin de servicio:",style:['header2']},
							{text:data.sintoma_fin,margin: [0, 0,0,30]},
							
						],
						[ 
							{text:"Nombre de la Luz:",style:['header2']},
							{text:data.luz_nombre,margin: [0, 0,0,30]}
							/*
							{text:"Comentarios generales:",style:['header2']},
							{text:data.comentarios_generales,margin: [0, 0,0,30]},
							*/
							
						],
					 					  
					 ],
					   columnGap: 20,
					   
					});


					content.push({
							image: data.rd_alimentos,
							width: 189,margin: [0, 10,0,10]
						});
					content.push({columns: [
						[ 
							{text:"Desayuno:",style:['header2']},
							{stack:alimento.Desayuno}
							
						],
						[ 
							{text:"Comida:",style:['header2']},
							{stack:alimento.Comida }
							
						],
						[ 
							{text:"Cena:",style:['header2']},
							{stack:alimento.Cena }
							
						],
					  ],
					   columnGap: 20,
					   
					});
  
					content.push({image:data.rd_medicamentos,width:189,margin: [0, 10,0,10]});
  					content.push(do_daily_medicamentos(data));

  					content.push({image:data.rd_signos_vitales,width:189,margin: [0, 10,0,10]});
  					content.push(do_daily_signos_vitales(data));

  					content.push({image:data.rd_otros_datos,width:189,margin: [0, 10,0,10]});
  					content.push(do_daily_otros_datos(data));

  			 var docDefinition = { 
			 pageOrientation: 'portrait',
			 pageSize:'legal',

			content: content,
			styles:{
			 	header1bold: 
			 		{
			 			fontSize:18,
			 			bold:true,
			 			
			 	 	},
			 	header1light: {
			 			fontSize:18 ,
			 			},
			 	header2: {fontSize:12,
			 			bold:true,
			 			 },
			 	tableStyle: {
			 		fontSize:10,

			 	}
				 
			},
			defaultStyle:{
			 	color:'#004d71',
			 	fontSize:10,
			},
			images: {
				happy:data.happy,
				meh:data.meh,
				sad:data.sad,
				checked_16:data.checked_16
			}

		};



			//pdfMake.createPdf(docDefinition).open();


			/*// print the PDF
			pdfMake.createPdf(docDefinition).print();*/

			// download the PDF
			var pdfname = 'Reporte-Diario-'+ data.first_name.trim()+'-'+data.last_name.trim()+'-'+data.date+'.pdf';
			if (action =="print"){ 
				pdfMake.createPdf(docDefinition).download(pdfname);
				return "success";

			}
			else{
				var PDFdata;
				var data;
				pdfMake.createPdf(docDefinition).getBase64(function(encodedString) {
	    		PDFdata = encodedString;
					
					$.ajax({
				        url: data.url,
				        data: {
				         	action:"save",
				         	pdfdata: PDFdata,
				         	filename: pdfname,
				        },
				        type: 'POST',
				        success: function (respuesta) {
				        	console.log("archivo guardado en servidor");
				        }
				    });
			    });
			    return "success";
			}

         
	}

	function do_daily_medicamentos(data){
		content = Array();
		medicamentos = data.medicamentos;
		if(medicamentos.length > 0){ 
			content.push({table: {
			  					 headerRows: 1,
			  					 body: data.medicamentos,
			  					 style:'tableStyle'
			  					},
			  					layout: {
			  						hLineColor:'#004d71',
			  						vLineColor:'#004d71',
			  					},
			  				});
		}
		else{
			content.push({text:"Todavía no hay datos para esta sección"});
		}
		return content;
	}

	function do_daily_signos_vitales(data){
		content = Array();
		signos_vitales = data.signos_vitales;
		if(signos_vitales.length > 0){ 
		
			content.push({table: {
		  					 headerRows: 1,
		  					 body: data.signos_vitales,
		  					 style:'tableStyle'
		  					},
		  					layout: {
		  						hLineColor:'#004d71',
		  						vLineColor:'#004d71',
		  					},
		  				});
		}
		else{
			content.push({text:"Todavía no hay datos para esta sección"});
		}
		return content;
	}

	function do_daily_otros_datos(data){
		content = Array();
		otros_datos = data.otros_datos;
		if(otros_datos.length > 0){ 
		
			content.push({table: {
		  					 headerRows: 0,
		  					 body: data.otros_datos,
		  					 style:'tableStyle'
		  					},
		  					layout: {
		  						hLineColor:'#004d71',
		  						vLineColor:'#004d71',
		  					},
		  				});
		}
		else{
			content.push({text:"Todavía no hay datos para esta sección"});
		}
		return content;
	}

	$(document).on('click', ".print_pdf", function(e){
			 e.preventDefault();


            var url = jQuery(this).attr('href');
			var paciente = jQuery(this).attr('paciente-id');
			var service = jQuery(this).attr('service-id');
			var action = jQuery(this).attr('action');

			$.ajax({
				url: url,
		        data: {
					service:service,
					paciente:paciente, 
                    action:action,
				},
				type: 'POST',
                success: function (respuesta) {

					if(action == "send-get-info"){ 	
		        	  bootbox.dialog({
			                title: "Enviar Reporte",
			                message: respuesta,
			                buttons: {
			                    success: {
			                        label: 'Enviar',
			                        //className: 'hidden',
			                        callback: function (a) {

			                        	a.preventDefault();
			                        	var module = "send-report";

	  									jQuery(this).find('form').attr('id', 'form-'+module);

			                           	 $.post(url, $('#form-' + module).serialize()+ '&action=' + 'send'+'&module='+module, function (response) {
			                           	 		
			                           	 		resp = JSON.parse(response);
			                           	 		
				                           	 	if(resp.status=='success'){
				                           	 		

				                           	 		new PNotify({
														title: "Enviar Reporte",
														type: "success",
														text: resp.message,
														nonblock: {
															nonblock: true
														},
														addclass: '',
														styling: 'bootstrap3'
													});
					                           	}
					                           	else{
  													new PNotify({
				                                    	title: "Enviar Reporte Falló",
				                                    	type: "error",
				                                    	text: resp.message,
				                                    	nonblock: {
				                                            nonblock: true
				                                    	},
				                                    	addclass: '',
				                                    	styling: 'bootstrap3'
				                                	});
					                           	
					                           	}
					                        });

			                           	 

			                        }
			                    }
			                }
			            });
		        	}
		        	else{ 
			                var data = JSON.parse(respuesta);
							//console.log(data.logo);     
							var paciente = data.paciente;                                                                                                       
                            var service = data.service;
                            var address = JSON.parse(paciente.address);
							var name = JSON.parse(paciente.name);
							var schedule = clean_current_schedule(data.current_schedule);

							var docDefinition = {

								content:[
 									{columns: [
                                                {
                                                        image: data.logo,
                                                        width: 160
                                                },
                                                [
                                                        {image: data.header,
                                                                width:217
                                                        },
                                                        {text: name.first_name+" "+name.second_name, style:[ 'header1bold' ],   margin: [0, 30,0,0],
                                                        },
                                                        {text: name.last_name.toUpperCase()+" "+name.second_last_name.toUpperCase()+" ("+service.tars_id+")",style:[ 'header1light' ]}
                                               ],
                                          ],
                                           columnGap: 20
                                        },
                                        { columns: [
                                                {text: "Fecha de Nacimiento: \n"+paciente.birthday,margin: [0, 10,0,0]},
                                                {text:" Condición: \n "+data.disease,margin: [0, 10,0,0]},
                                                {text:" Teléfono: \n "+service.phones,margin: [0, 10,0,0]},
                                                ]
                                        },
                                        {text: "Dirección del Servicio",style:[ 'header2' ],margin: [0, 20,0,5]},
                                        {text: address.street+" "+address.number+" "+address.internal+" "+address.colony},
										{text: paciente.city+" "+paciente.region+" "+paciente.country+" "+paciente.zip},
					 					{text: "Plan Contratado",style:[ 'header2' ],margin: [0, 20,0,5]},
                                        { columns: [
                                                { text: "Plan Contratado: "+data.plan+ "\n Supervisor a Cargo: "+data.service_supervisor},
                                                { text:" Médico Asignado: "+data.service_doctor+"\n"+ " Ejecutivo de Servicio: " +data.service_ejecutivo},
                                                ],
                                                columnGap:20
                                        },
                                        {text: "Objetivos del Mes",style:[ 'header2' ],margin: [0, 20,0,5]},
                                        {text: paciente.goals},
										{text: "Rutinas",style:[ 'header2' ],margin: [0, 20,0,5]},
										
										
                                        {table: {
                                                         headerRows: 1,
                                                         body: data.service_routines,
                                                         style:'tableStyle'
                                                        },
                                                        layout: {
                                                                hLineColor:'#004d71',
                                                                vLineColor:'#004d71',
                                                        },
												},
												
                                        {text: "Asignación de Luz y Horarios",style:[ 'header2' ],margin: [0, 20,0,5]},
                                        {text: schedule},
                                        {text: "Familiares que desean recibir notificaciones",style:[ 'header2' ],margin: [0, 20,0,5]},
                                        {table:{
                                                        headerRows:1,
                                                        body: data.paciente_familiares,
  											 			style:'tableStyle'
                                                        },
                                                        layout: {
                                                                hLineColor:'#004d71',
                                                                vLineColor:'#004d71',
                                                        },
                                                },
                                        {text: "Medicamentos",style:[ 'header2' ],margin: [0, 20,0,5]},
                                        {table:{
                                                        headerRows:1,
                                                        body: data.service_medicines,
                                                        style:'tableStyle'
                                                        },
                                                        layout: {
                                                                hLineColor:'#004d71',
                                                                vLineColor:'#004d71',
                                                        },
                                                },
                                        ],
                                         styles:{
                                                header1bold: 
                                                        {
                                                                fontSize:20,
                                                                bold:true,

                                                        },
                                                header1light: {
                                                	                            fontSize:20 ,
                                                                },
                                                header2: {fontSize:18,
                                                                 },
                                                tableStyle: {
                                                        margin: [2, 5, 2, 15],
                                                        paddingLeft:10,
                                                        paddingRight:10,
                                                        paddingTop:10,
                                                        paddingBottom:10,
                                                }

                                         },
                                         defaultStyle:{
                                                color:'#004d71',
                                                fontSize:6,
                                         }

                                        };



								pdfname = 'Perfil-Servicio-'+name.last_name+"-"+name.second_last_name+"-"+ service.tars_id+'.pdf';                                                                                                                      
                                pdfMake.createPdf(docDefinition).download(pdfname);

								var PDFdata;
								var data;
								pdfMake.createPdf(docDefinition).getBase64(function(encodedString) {
					    		PDFdata = encodedString;
									
									$.ajax({
								        url: url,
								        data: {
								         	action:"save",
								         	pdfdata: PDFdata,
								         	filename: pdfname,
								        },
								        type: 'POST',
								        success: function (respuesta) {
								        	console.log(respuesta);
								        	
								        }
								    });
							    });
							}

                                }
                        });
				
		});


	
	function  clean_current_schedule(data){
		data = data.split("<br/>").join("\n");
		data = data.split("<strong>").join("");
		data = data.split("</strong>").join("\n");
		return data;
	}


 		$(document).on('click', '.print_nurse_pdf', function(e){
			e.preventDefault();

			var url = jQuery(this).attr('href');
  			var nurse = jQuery(this).attr('nurse-id');
			var action = jQuery(this).attr('action');
			  
			print_nurse_pdf(url, nurse, action);
		});

		function print_nurse_pdf(url, nurse, action) {
			$.ajax({
		        url: url+"/"+action,
		        data: {
		         	nurse:nurse,
		         	action:action,
		        },
		        success: function (respuesta) {
		        	if(action == "send-get-info"){ 	
		        	  bootbox.dialog({
			                title: "Enviar Reporte",
			                message: respuesta,
			                buttons: {
			                    success: {
			                        label: 'Enviar',
			                        //className: 'hidden',
			                        callback: function (a) {
			                        	a.preventDefault();
			                        	var module = "send-report";

	  									jQuery(this).find('form').attr('id', 'form-'+module);
	  									
			                           	 $.ajax({
			                           	 	url: url ,
			                           	 	data: {form: $('#form-' + module).serialize()} ,
			                           	 	success:function(response){ 

			                           	 		resp = JSON.parse(response);
			                           	 		
				                           	 	if(resp.status=='success'){
				                           	 		

				                           	 		new PNotify({
														title: "Enviar Reporte",
														type: "success",
														text: resp.message,
														nonblock: {
															nonblock: true
														},
														addclass: '',
														styling: 'bootstrap3'
													});
					                           	}
					                           	else{
  													new PNotify({
				                                    title: "Enviar Reporte Falló",
				                                    type: "error",
				                                    text: resp.message,
				                                    nonblock: {
				                                            nonblock: true
				                                    },
				                                    addclass: '',
				                                    styling: 'bootstrap3'
				                                });
					                           	
					                           	}

					                            } 
					                            
			                        	});
			                    	}
			                	}
			            	}
			            });
			           } 
		        	else{ 
		        		
		        		var data = JSON.parse(respuesta);
		        		var nurse = data.user;
						var meta = data.nurse_meta;

						var docDefinition = {};

						if(nurse.id_tipo_luz == 1) {
							// DEFINICION PARA LA LUZ TIPO NORMAL 
							docDefinition = { 
								content:[
									{
										columns: [
										  {
											  image: data.logo,
											  width: 160
										  },
										  [
											  {image: data.header,width:217},	
											  {text: nurse.first_name, style:[ 'header1bold' ],	margin: [0, 30,0,0],},
											  {text: nurse.last_name.toUpperCase()+" ("+meta._numero_luz+")",style:[ 'header1light' ]}
										  ],
										  {
											  image: data.profile_img,
											  width: 160
										  }
										],
										   columnGap: 20
									},
								  { 
									  columns: [ 
										  {text:" Correo: \n "+nurse.email,margin: [0, 10,0,0]},
										  {text:" Teléfono: \n "+nurse.phone,margin: [0, 10,0,0]},
										  {text:" Celular: \n "+meta._mobile,margin: [0, 10,0,0]},
									  ]
								  },
									{
										text: "Datos Personales",
										style:[ 'header2' ],
										margin: [0, 20,0,5]
								  },
								  {
									  columns: [ 
										  {text: "Fecha de Nacimiento: \n"+meta._birthday,margin: [0, 10,0,0]},
										  {text:" Edad: \n "+meta._age,margin: [0, 10,0,0]},
										  {text:" Género: \n "+meta._gender,margin: [0, 10,0,0]},
									  ],
								  },
								  { 
									  columns: [ 
										  { text: "Estado Civil: \n" + data.estado_civil, margin: [0, 10,0,0]},
										  { text:" Dependientes Económicos: \n " + meta._economical_dependants, margin: [0, 10,0,0]},
										  { text:" Características Físicas: \n " + data.fisical, margin: [0, 10,0,0]},
									  ],
								  },
  
								  {text: "Dirección",style:[ 'header2' ],margin: [0, 20,0,5]},
								  {text: meta._address+" "+meta._colony},
								  {text: meta._city+" "+meta._region+" "+meta._zip},
								  {text: "Datos de Contacto en Caso de Emergencia",style:[ 'header2' ],margin: [0, 20,0,5]},
								  { columns: [
									  { text: "Nombre: "+meta._emergency_name+" "+meta._emergency_last_name},
									  { text:" Parentezco: "+meta._emergency_relation},
									  { text:" Teléfono: "+meta._emergency_phone},
									  ],
									  columnGap:20
								  },
									{text: "Datos del Sistema",style:[ 'header2' ],margin: [0, 20,0,5]},
								  { columns: [
									  { text: "Status: "+data.system_status},
									  { text:" Motivo de Baja / Renuncia: "+meta._fire_reason},
									  ],
									  columnGap:20
								  },
								  { columns: [
									  { text: "Fuentes de Reclutamiento: "+data.fuentes_reclutamiento},
									  { text:" Grado de Estudios: "+data.grado_estudios},
									  ],
									  columnGap:20
								  },
									{text: "Experiencia Laboral",style:[ 'header2' ],margin: [0, 20,0,5]},
								  { columns: [
									  { text: "Cuidador: \n"+data.carer_skills},
									  { text:" Terapeuta: \n"+data.therapist_skills},
									  ],
									  columnGap:20
								  },
								  { columns: [
									  { 	width: '50%',
										  text: "Personalidad: \n"+data.personalities},
									  {   width: '50%',
										  text:" Pasatiempos: \n"+data.hobbies},
									  ],
									  columnGap:20
								  },
						
							  ],
							  styles:{
								  header1bold: 
									  {
										  fontSize:20,
										  bold:true,
										  
									  },
								  header1light: {
										  fontSize:20 ,
										  },
								  header2: {fontSize:18,
										  },
								  tableStyle: {
									  margin: [2, 5, 2, 15],
									  paddingLeft:10,
									  paddingRight:10,
									  paddingTop:10,
									  paddingBottom:10,
								  }
							  
							  },
							  defaultStyle:{
								  color:'#004d71',
								  fontSize:12,
							  }
						};
						} else {
							// LUZ TIPO INDEPENDIENTE
								docDefinition = { 
									content:[
										{
											columns: [
											[
												{image: data.header,width:217},	
												{text: nurse.first_name, style:[ 'header1bold' ],	margin: [0, 30,0,0],},
												{text: nurse.last_name.toUpperCase(),style:[ 'header1light' ]}
											],
											{
												image: data.profile_img,
												width: 160
											}
											],
											columnGap: 20
										},
									{ 
										columns: [ 
											{text:" Correo: \n "+nurse.email,margin: [0, 10,0,0]},
											{text:" Teléfono: \n "+nurse.phone,margin: [0, 10,0,0]},
											{text:" Celular: \n "+meta._mobile,margin: [0, 10,0,0]},
										]
									},
										{
											text: "Datos Personales",
											style:[ 'header2' ],
											margin: [0, 20,0,5]
									},
									{
										columns: [ 
											{text: "Fecha de Nacimiento: \n"+meta._birthday,margin: [0, 10,0,0]},
											{text:" Edad: \n "+meta._age,margin: [0, 10,0,0]},
											{text:" Género: \n "+meta._gender,margin: [0, 10,0,0]}
										],
									},

									{
										columns: [ 
											{ text: "Estado Civil: \n" + data.estado_civil, margin: [0, 10,0,0]},
											{ text:" Número de Hijos: \n " + meta._economical_dependants, margin: [0, 10,0,0]},
											{ text:" Maxima escolaridad: \n " + data.max_escolaridad, margin: [0, 10,0,0]},
										],
									},
	
									{text: "Dirección",style:[ 'header2' ],margin: [0, 20,0,5]},
									{text: meta._address+" "+meta._colony},
									{text: meta._city+" "+meta._region+" "+meta._zip},

									{text: "Referencias",style:[ 'header2' ],margin: [0, 20,0,5]},
									{ 
										columns: [
											{ text: "Número Telefonico 1: "+meta._referencia_phone1},
											{ text:" Nombre Referencia 1: "+meta._referencia_nombre1},
										],
										columnGap:20
									},
									{ 
										columns: [
											{ text: "Número Telefonico 2: "+meta._referencia_phone2},
											{ text:" Nombre Referencia 2: "+meta._referencia_nombre2},
										],
										columnGap:20
									},
									{text: "Experiencia Laboral",style:[ 'header2' ],margin: [0, 20,0,5]},
									{
										columns:[
											{ text: "Años de experiencia cuidando adultos: " + meta._anos_experiencia},
										],
										columnGap:20
									},
									{ 
										columns: [
										{ text: "Cuidador: \n"+data.carer_skills},
										],
										columnGap:20
									}
								],
								styles:{
									header1bold: 
										{
											fontSize:20,
											bold:true,
											
										},
									header1light: {
											fontSize:20 ,
											},
									header2: {fontSize:18,
											},
									tableStyle: {
										margin: [2, 5, 2, 15],
										paddingLeft:10,
										paddingRight:10,
										paddingTop:10,
										paddingBottom:10,
									}
								
								},
								defaultStyle:{
									color:'#004d71',
									fontSize:12,
								}
							};
						}
					


 	
				 pdfname = 'Perfil-Luz-'+nurse.first_name+'-'+nurse.last_name+'.pdf';
                                pdfMake.createPdf(docDefinition).download(pdfname);

								var PDFdata;
								var data;
								pdfMake.createPdf(docDefinition).getBase64(function(encodedString) {
					    		PDFdata = encodedString;
									
									$.ajax({
								        url: url,
								        data: {
								         	action:"save",
								         	pdfdata: PDFdata,
								         	filename: pdfname,
								        },
								        type: 'POST',
								        success: function (respuesta) {
								        	console.log(respuesta);
								        	
								        }
								    });
							    });
							

                 }
             }
             });
		}

 

 		$("body").on('click','.print_report_pdf', function(e){
			
			e.preventDefault();
			disabled = jQuery(this).attr('disabled');
			if(disabled) {
				console.log("boton print_report_pdf no se puede imprimir pdf espere a que se termine de cargar los datos");
				return false;
			}

			url = jQuery(this).attr('href');
  			report = jQuery(this).attr('report-id');
			button_title = jQuery(this).attr('title');

  			if(jQuery(this).attr('action') ){
  				action = jQuery(this).attr('action');
  			}
  			else{
  				action = "create";
  			}
  			

			
			var sd = $("#from").val();
		    var ed= $("#to").val();
		    

			$.ajax({
		        url: url,
		        data: {
		         	report:report,
		         	start_date:sd,
		         	end_date:ed,
		         	action: action,
		        },
		        type: 'POST',
		        success: function (respuesta) {
		        	if(action == "send-get-info"){ 	
		        	  bootbox.dialog({
			                title: "Enviar Reporte",
			                message: respuesta,
			                buttons: {
			                    success: {
			                        label: 'Enviar',
			                        //className: 'hidden',
			                        callback: function (a) {

			                        	a.preventDefault();
			                        	var module = "send-report";

	  									jQuery(this).find('form').attr('id', 'form-'+module);


			                           	 $.post(url, $('#form-' + module).serialize()+ '&action=' + 'send'+'&module='+module, function (response) {
			                           	 		
			                           	 		resp = JSON.parse(response);
			                           	 		
				                           	 	if(resp.status=='success'){
				                           	 		

				                           	 		new PNotify({
														title: "Enviar Reporte",
														type: "success",
														text: resp.message,
														nonblock: {
															nonblock: true
														},
														addclass: '',
														styling: 'bootstrap3'
													});
					                           	 	}
					                            });

			                           	 

			                        }
			                    }
			                }
			            });
		        	}
		        	else if(action == "create"){ 	
		        		
			    		data = JSON.parse(respuesta);
			    		data['from'] = $("#from").val();
			    		data['to'] = $("#to").val();
			    		data['url'] = url
			    		try{
			    			updateReport(button_title, function(){
								if (button_title == "Guardar"){
									print_pdf(data,"save");
								}
								else{
									print_pdf(data,"download");	
								}
							});
			    		}

                        catch(err){

                               new PNotify({
                                    title: "Generar Reporte Falló",
                                    type: "error",
                                    text: err+"  Verifica que las secciones seleccionadas para imprimir en la sección de Resumen tengan información" ,
                                    nonblock: {
                                            nonblock: true
                                    },
                                    addclass: '',
                                    styling: 'bootstrap3'
                                });

                        }
                        
			    	}
			    	else{
			    			var link = document.createElement('a');
							data = JSON.parse(respuesta);
						
							if (data.status == "success"){

			    			if(button_title != "Guardar"){
				
								link.href = data.url;
								link.download = data.filename;
								link.dispatchEvent(new MouseEvent('click'));
							}
							}
						
						else{
							new PNotify({
										title: "Descargar Reporte Falló",
										type: "error",
										text: data.message,
										nonblock: {
											nonblock: true
										},
										addclass: '',
										styling: 'bootstrap3'
									});
							}
						
			    	}
		        
		        
				}
		});
 	});
		


function print_pdf(data,action){

	dataReportPdf = data;
	actionReportPdf = action;

	var node = document.getElementById('word-cloud');
	domtoimage.toPng(node).then(function (dataUrl) {

		data = dataReportPdf;
		action = actionReportPdf;

		console.log(dataUrl);
		var sintomas_chart_uri = $('#sintomas1-url').val();
		var medicamento_chart_uri = $('#medicamento1-url').val();
		var estcognitiva_chart_uri = $('#estcognitiva1-url').val();
		var estfisica_chart_uri = $('#estfisica1-url').val();
		var estotra_chart_uri = $('#estotra1-url').val();
		var micc_chart_uri = $('#micc1-url').val();
		var evac_chart_uri = $('#evac1-url').val();
		var temp_chart_uri = $('#temperatura-url').val();
		var tension_arterial_chart_uri = $('#tension_arterial-url').val();
		var presion_arterial_chart_uri = $('#presion_arterial-url').val();
		var frecuencia_cardiaca_chart_uri = $('#frecuencia_cardiaca-url').val();
		var oxigeno_chart_uri = $('#oxigeno-url').val();
		var glucosa_chart_uri = $('#glucosa-url').val();
	

	content = [];

	var incluir_reportes = $(':checkbox[name="reports_included[]"]').map(function() {
		var op = [];
		
		return op;
	});


	var incluir_reportes  = $(':checkbox[name="reports_included[]"]');
	var incluidos = [];
	
	$.each(incluir_reportes, function(){
		if(this.checked) {
			incluidos.push(this.value);
		}
	});
	console.log(incluidos);
	
	if(incluidos.indexOf('observacion_general',0 ) >=0 ){
		console.log('including resumen');
		content.push(observaciones_generales(data, dataUrl));
	}
	
	if(incluidos.indexOf('sintoma',0)>=0){
		console.log('including sintoma');
		content.push(sintomas(data,sintomas_chart_uri));
	}
	
	if(incluidos.indexOf('medicamento',0) >=0){
		console.log('including medicamento');
		content.push(medicamentos(data,medicamento_chart_uri));
	}
	if (incluidos.indexOf('estimulacion_cognitiva',0) >= 0){
		console.log('including est cognitiva');
		content.push(estimulacion_cognitiva(data,estcognitiva_chart_uri));
	}
	if(incluidos.indexOf('estimulacion_fisica',0) >= 0 ){
		console.log('including est fisica');
		content.push(estimulacion_fisica(data,estfisica_chart_uri));
	}
	if(incluidos.indexOf('estimulacion_otra',0) >= 0 ){
		console.log('including est otra');
		content.push(otras_estimulaciones(data,estotra_chart_uri));
	}
	if(incluidos.indexOf('evacuacion',0) >= 0){
		console.log('including evacuacion');
		content.push(evacuacion_miccion(data,micc_chart_uri,evac_chart_uri));	
	}
	if(incluidos.indexOf('alimento',0) >=0 ){
		console.log('including alimento');
		content.push(alimento(data));

	}
	if(incluidos.indexOf('signos_vitales',0) >=0 ){
		console.log('including signos vitales');
		content.push(signos_vitales(data,temp_chart_uri,tension_arterial_chart_uri,presion_arterial_chart_uri,frecuencia_cardiaca_chart_uri,oxigeno_chart_uri, glucosa_chart_uri));
		
	}

	var docDefinition = { 
		pageOrientation: 'landscape',
		pageSize:'legal',

	   content: content,
	   styles:{
			header1bold: 
				{
					fontSize:20,
					bold:true,
					
				 },
			header1light: {
					fontSize:20 ,
					},
			header2: {fontSize:16,
					bold:true,
					 },
			tableStyle: {
				fontSize:30,
			},
			subtext:{
				color:"#00afaa",
				bold:true,
				fontSize:14,
			},
			
	   },
	   defaultStyle:{
			color:'#004d71',
			fontSize:12,
	   },
	   images: {
		   happy:data.happy,
		   meh:data.meh,
		   sad:data.sad,
		   evacuacion:data.evacuacion,
		   miccion:data.miccion,
		   checked_16:data.checked_16
		   
	   }

   };



	   //pdfMake.createPdf(docDefinition).open();


	   /*// print the PDF
	   pdfMake.createPdf(docDefinition).print();*/

	   // download the PDF
		
	   var pdfname = 'Reporte-Mensual-'+ data.first_name.trim()+'-'+data.last_name.trim()+'-'+data.from+'-'+data.to+'.pdf';
	   
	   if (action == "download"){ 
		   pdfMake.createPdf(docDefinition).download(pdfname);
	   }
	   
	   
	   var PDFdata;
	   var data;
	   pdfMake.createPdf(docDefinition).getBase64(function(encodedString) {
	   PDFdata = encodedString;
		   
		   $.ajax({
			   url: data.url,
			   data: {
					action:"save",
					pdfdata: PDFdata,
					filename: pdfname,
			   },
			   type: 'POST',
			   success: function (respuesta) {
				   console.log(respuesta);
				   console.log("archivo guardado en servidor");

				   window.location.href = BASE_URL + 'admin/reports';
			   }
		   });
	   });
	})
	.catch(function (error) {
		console.error('oops, something went wrong!', error);
	});
	
	
	
		

		
		      
}

function signos_vitales(data,temp_chart_uri,tension_arterial_chart_uri,presion_arterial_chart_uri,frecuencia_cardiaca_chart_uri,oxigeno_chart_uri, glucosa_chart_uri){

	var signos_vitales_imprimir = [];
	var signos_vitales_incluidos = $('.checkbox_signos_vitales');
	
	$.each(signos_vitales_incluidos, function(key, value){
		if($(this).is(':checked')) {
			signos_vitales_imprimir.push($(this).val());
		}
	});

	var content = [];
	if((temp_chart_uri !="" || tension_arterial_chart_uri!= "") && (signos_vitales_imprimir.indexOf('temperatura') > -1 || signos_vitales_imprimir.indexOf('presion_arterial_sistolica') > -1)) {
		content.push({text:"",pageBreak:'before'});
		content.push({columns: [
			{
				image: data.logo,
				width: 160
			},
			[
				{image: data.header,width:217},	
				{text: data.first_name, style:[ 'header1bold' ],margin: [0, 30,0,0]},
				{text: data.last_name.toUpperCase(),style:[ 'header1light' ]}
			],
			{
				text: [ 
					{text:"Desde: "+ data.from+" Hasta: "+ data.to},
					{text: "\n Signos Vitales" ,style:[ 'header1light' ]}
				],
			},
		  ],
		   columnGap: 20,
		});

		var pagina1 = [];
		if((temp_chart_uri !="") && (signos_vitales_imprimir.indexOf('temperatura') > -1)) {
			pagina1.push({image:temp_chart_uri , width:400});
		}

		if((tension_arterial_chart_uri !="") && (signos_vitales_imprimir.indexOf('presion_arterial_sistolica') > -1)) {
			pagina1.push({image:tension_arterial_chart_uri , width:400});
		}

		content.push({columns: pagina1});
	}

	if((presion_arterial_chart_uri !="" || frecuencia_cardiaca_chart_uri!= "") && (signos_vitales_imprimir.indexOf('presion_arterial_diastolica') > -1 || signos_vitales_imprimir.indexOf('frecuencia_cardiaca') > -1)) {
		content.push({text:"",pageBreak:'before'});
		content.push({columns: [
			{
				image: data.logo,
				width: 160
			},
			[
				{image: data.header,width:217},	
				{text: data.first_name, style:[ 'header1bold' ],margin: [0, 30,0,0]},
				{text: data.last_name.toUpperCase(),style:[ 'header1light' ]}
			],
			{
				text: [ 
					{text:"Desde: "+ data.from+" Hasta: "+ data.to},
					{text: "\n Signos Vitales" ,style:[ 'header1light' ]}
				],
			},
		  ],
		   columnGap: 20,
		});

		var pagina2 = [];
		if((presion_arterial_chart_uri != "") &&  (signos_vitales_imprimir.indexOf('presion_arterial_diastolica') > -1)) {
			pagina2.push({image:presion_arterial_chart_uri , width:400});
		}

		if((frecuencia_cardiaca_chart_uri != "") && (signos_vitales_imprimir.indexOf('frecuencia_cardiaca') > -1)) {
			pagina2.push({image:frecuencia_cardiaca_chart_uri , width:400});
		}

		content.push({columns: pagina2});
	}

	if((oxigeno_chart_uri != "" || glucosa_chart_uri != "") && (signos_vitales_imprimir.indexOf('oxigeno') > -1 || signos_vitales_imprimir.indexOf('glucosa') > -1)) {
		content.push({text:"",pageBreak:'before'});
		content.push({columns: [
			{
				image: data.logo,
				width: 160
			},
			[
				{image: data.header,width:217},	
				{text: data.first_name, style:[ 'header1bold' ],margin: [0, 30,0,0]},
				{text: data.last_name.toUpperCase(),style:[ 'header1light' ]}
			],
			{
				text: [ 
					{text:"Desde: "+ data.from+" Hasta: "+ data.to},
					{text: "\n Signos Vitales" ,style:[ 'header1light' ]}
				],
			},
		  ],
		   columnGap: 20,
		});

		var pagina3 = []; 
		if((oxigeno_chart_uri != "") && (signos_vitales_imprimir.indexOf('oxigeno') > -1)) {
			pagina3.push({image:oxigeno_chart_uri , width:400});
		}

		if((glucosa_chart_uri != "") && (signos_vitales_imprimir.indexOf('glucosa') > -1)) {
			pagina3.push({image:glucosa_chart_uri , width:400});
		}

		content.push({columns: pagina3});
	}

	return content;
}
function alimento(data){
	var content = Array();
	content.push({text:"",pageBreak:'before'});

		content.push({columns: [
					{
						image: data.logo,
						width: 160
					},
					[
						{image: data.header,
							width:217
						},	
						{text: data.first_name, style:[ 'header1bold' ],	margin: [0, 30,0,0]
						},
						{text: data.last_name.toUpperCase(),style:[ 'header1light' ]}
					],
					{
						text: [ {text:"Desde: "+ data.from+" Hasta: "+ data.to},
								{text: "\n Alimentos" ,style:[ 'header1light' ]}
							  ],
					},
				  ],
				   columnGap: 20,
				   margin: [0, 0,0,20],
				});

		content.push({table: {  
						 headerRows: 1,
						 body: data.alimento_table,
						 style:'tableStyle',
						 
						},
						layout: {
							hLineColor:'#004d71',
							vLineColor:'#004d71',
						},
				});
		content.push({text:"* La tabla anterior muestra cada vez que se reportó que el paciente haya comido en el periodo reportado. Cara feliz indica que comió bien, cara enojada indica que no quiso comer o que comió poco",style:[ 'subtext' ]});
		return content;
}
function evacuacion_miccion(data,micc_chart_uri,evac_chart_uri){
	var content = Array();
	content.push({text:"",pageBreak:'before'});

		content.push({columns: [
					{
						image: data.logo,
						width: 160
					},
					[
						{image: data.header,
							width:217
						},	
						{text: data.first_name, style:[ 'header1bold' ],	margin: [0, 30,0,0]
						},
						{text: data.last_name.toUpperCase(),style:[ 'header1light' ]}
					],
					[ 
						{text:"Desde: "+ data.from+" Hasta: "+ data.to},
						{text: "\n Evacuación y Micción" ,style:[ 'header1light' ]},
						
					],

				  ],
				   columnGap: 20,
				   margin: [0, 0,0,20],
				});

		
		content.push({table: {
						 headerRows: 1,
						 body: data.evacuacion_table,
						 style:'tableStyle',
						 
						},
						layout: {
							hLineColor:'#004d71',
							vLineColor:'#004d71',
						},
				});
		content.push({image: data.guia_evacuacion, width:150,margin:[500,10,0,10]});
		content.push({text:"* La tabla anterior muestra cada vez que se reportó que el paciente haya evacuado o miccionado en el periodo reportado",style:[ 'subtext' ]});
		/*content.push({text:"",pageBreak:'before'});

		content.push({columns: [
					{
						image: data.logo,
						width: 160
					},
					[
						{image: data.header,
							width:217
						},	
						{text: data.first_name, style:[ 'header1bold' ],	margin: [0, 30,0,0]
						},
						{text: data.last_name.toUpperCase()+" ("+data.tars_id+")",style:[ 'header1light' ]}
					],
					{
						text: [ {text:"Desde: "+ data.from+" Hasta: "+ data.to},
								{text: "\n Evacuación y Micción" ,style:[ 'header1light' ]}
							  ],
					},
				  ],
				   columnGap: 20,
				   margin: [0, 0,0,20],
				});


		content.push({columns:[
						{image:micc_chart_uri,width: 400},
						{image:evac_chart_uri,width: 400},
					]});*/
		return content;
}
function otras_estimulaciones(data,estotra_chart_uri){
	var content = Array();
	content.push({text:"",pageBreak:'before'});

		content.push({columns: [
					{
						image: data.logo,
						width: 160
					},
					[
						{image: data.header,
							width:217
						},	
						{text: data.first_name, style:[ 'header1bold' ],	margin: [0, 30,0,0]
						},
						{text: data.last_name.toUpperCase(),style:[ 'header1light' ]}
					],
					{
						text: [ {text:"Desde: "+ data.from+" Hasta: "+ data.to},
								{text: "\n Otras Estimulaciones" ,style:[ 'header1light' ]}
							  ],
					},
				  ],
				   columnGap: 20,
				   margin: [0, 0,0,20],
				});

		content.push({table: {
						 headerRows: 1,
						 body: data.estimulacion_otra_table,
						 style:'tableStyle',
						 
						},
						layout: {
							hLineColor:'#004d71',
							vLineColor:'#004d71',
						},
						margin: [0, 40,0,20]
				});
		content.push({text:"*  La tabla anterior muestra los días en los cuales se llevaron a cabo cada una de las estimulaciones en el periodo reportado",style:[ 'subtext' ]});
		
		
		/*content.push({text:"",pageBreak:'before'});
		content.push({columns: [
					{
						image: data.logo,
						width: 160
					},
					[
						{image: data.header,
							width:217
						},	
						{text: data.first_name, style:[ 'header1bold' ],	margin: [0, 30,0,0]
						},
						{text: data.last_name.toUpperCase()+" ("+data.tars_id+")",style:[ 'header1light' ]}
					],
					{
						text: [ {text:"Desde: "+ data.from+" Hasta: "+ data.to},
								{text: "\n Otras Estimulaciones" ,style:[ 'header1light' ]}
							  ],
					},
				  ],
				   columnGap: 20,
				   margin: [0, 0,0,20],
				});*/

		content.push({image:estotra_chart_uri,width: 800});
		content.push({text:"* La gráfica muestra el total de veces que se llevó a cabo cada tipo de otra estimulación",style:[ 'subtext' ]});
		return content;
}
function estimulacion_fisica(data,estfisica_chart_uri){
		
		var content =  Array();
		content.push({text:"",pageBreak:'before'});

		content.push({columns: [
					{
						image: data.logo,
						width: 160
					},
					[
						{image: data.header,
							width:217
						},	
						{text: data.first_name, style:[ 'header1bold' ],	margin: [0, 30,0,0]
						},
						{text: data.last_name.toUpperCase(),style:[ 'header1light' ]}
					],
					{
						text: [ {text:"Desde: "+ data.from+" Hasta: "+ data.to},
								{text: "\n Estimulación Física" ,style:[ 'header1light' ]}
							  ],
					},
				  ],
				   columnGap: 20,
				   margin: [0, 0,0,20],
				});


		content.push({table: {
						 headerRows: 1,
						 body: data.estimulacion_fisica_table,
						 style:'tableStyle',
						 
						},
						layout: {
							hLineColor:'#004d71',
							vLineColor:'#004d71',
						},
						margin: [0, 40,0,20]
				});
		content.push({text:"*  La tabla anterior muestra los días en los cuales se llevaron a cabo cada una de las estimulaciones en el periodo reportado",style:[ 'subtext' ]});
		/*content.push({text:"",pageBreak:'before'});
		content.push({columns: [
					{
						image: data.logo,
						width: 160
					},
					[
						{image: data.header,
							width:217
						},	
						{text: data.first_name, style:[ 'header1bold' ],	margin: [0, 30,0,0]
						},
						{text: data.last_name.toUpperCase()+" ("+data.tars_id+")",style:[ 'header1light' ]}
					],
					{
						text: [ {text:"Desde: "+ data.from+" Hasta: "+ data.to},
								{text: "\n Estimulación Física" ,style:[ 'header1light' ]}
							  ],
					},
				  ],
				   columnGap: 20,
				   margin: [0, 0,0,20],
				});*/
		content.push({image:estfisica_chart_uri,width: 800});
		content.push({text:"* La gráfica muestra el total de veces que se llevó a cabo cada tipo de estimulación física",style:[ 'subtext' ]});
		return content;
}
function estimulacion_cognitiva(data,est_cognitiva_uri){
		
		var content =  Array();
		content.push({text:"",pageBreak:'before'});

		content.push({columns: [
					{
						image: data.logo,
						width: 160
					},
					[
						{image: data.header,
							width:217
						},	
						{text: data.first_name, style:[ 'header1bold' ],	margin: [0, 30,0,0]
						},
						{text: data.last_name.toUpperCase(),style:[ 'header1light' ]}
					],
					{
						text: [ {text:"Desde: "+ data.from+" Hasta: "+ data.to},
								{text: "\n Estimulación Cognitiva" ,style:[ 'header1light' ]}
							  ],
					},
				  ],
				   columnGap: 20,
				   margin: [0, 0,0,20],
				});


		content.push({table: {
						 headerRows: 1,
						 body: data.estimulacion_cognitiva_table,
						 style:'tableStyle',
						 
						},
						layout: {
							hLineColor:'#004d71',
							vLineColor:'#004d71',
						},
						margin: [0, 40,0,20]
				});
		content.push({text:"* La tabla anterior muestra el desempeño de cada ejercicio por cada función cognitiva practicada durante el periodo reportado",style:[ 'subtext' ]});
		content.push({image:est_cognitiva_uri,width: 800});
		content.push({text:"* La gráfica muestra el promedio desempeño para cada función cognitiva: 50% si lo hizo mal, 75% regular y 100% bien",style:[ 'subtext' ]});
		return content;
}

function medicamentos(data,medicamento_chart_uri){
	var content = Array();
	content.push({text:"",pageBreak:'before'});

		content.push({columns: [
					{
						image: data.logo,
						width: 160
					},
					[
						{image: data.header,
							width:217
						},	
						{text: data.first_name, style:[ 'header1bold' ],	margin: [0, 30,0,0]
						},
						{text: data.last_name.toUpperCase(),style:[ 'header1light' ]}
					],
					{
						text: [ {text:"Desde: "+ data.from+" Hasta: "+ data.to},
								{text: "\n Medicinas" ,style:[ 'header1light' ]}
							  ],
					},
				  ],
				   columnGap: 20,
				   margin: [0, 0,0,20],
				});

		content.push({table: {
						 headerRows: 1,
						 body: data.medicamento_table,
						 style:'tableStyle',
						 
						},
						layout: {
							hLineColor:'#004d71',
							vLineColor:'#004d71',
						},
						margin: [0, 40,0,20]
				});
		content.push({text:"* La tabla anterior muestra los medicamentos que se ministraton cada día del periodo reportado",style:[ 'subtext' ],pageBreak:'after'});
		content.push({columns: [
					{
						image: data.logo,
						width: 160
					},
					[
						{image: data.header,
							width:217
						},	
						{text: data.first_name, style:[ 'header1bold' ],	margin: [0, 30,0,0]
						},
						{text: data.last_name.toUpperCase(),style:[ 'header1light' ]}
					],
					{
						text: [ {text:"Desde: "+ data.from+" Hasta: "+ data.to},
								{text: "\n Medicinas" ,style:[ 'header1light' ]}
							  ],
					},
				  ],
				   columnGap: 20,
				   margin: [0, 0,0,20],
				});


		content.push({image:medicamento_chart_uri,width: 800});
		return content;
}
function sintomas(data,sintomas_chart_uri){
	var content = Array();
	content.push({text:"",pageBreak:'before'});
	content.push({columns: [
					{
						image: data.logo,
						width: 160
					},
					[
						{image: data.header,
							width:217
						},	
						{text: data.first_name, style:[ 'header1bold' ],	margin: [0, 30,0,0],
						},
						{text: data.last_name.toUpperCase(),style:[ 'header1light' ]}
					],
					{
						text: [ {text:"Desde: "+ data.from+" Hasta: "+ data.to},
								{text: "\n Síntomas" ,style:[ 'header1light' ]}
							  ],
					},
				  ],
				   columnGap: 20,
				   margin: [0, 0,0,20],
				});

		content.push({table: {
						 headerRows: 1,
						 body: data.sintoma_table,
						 style:'tableStyle',
						},
						layout: {
							hLineColor:'#004d71',
							vLineColor:'#004d71',
						},
						margin: [0, 40,0,20]
					});

		content.push({text:"* La tabla anterior muestra los días y frecuencia en los cuales se reportaron cada uno de los síntomas",style:[ 'subtext' ],pageBreak:'after'});
		content.push({columns: [
					{
						image: data.logo,
						width: 160
					},
					[
						{image: data.header,
							width:217
						},	
						{text: data.first_name, style:[ 'header1bold' ],	margin: [0, 30,0,0],
						},
						{text: data.last_name.toUpperCase(),style:[ 'header1light' ]}
					],
					{
						text: [ {text:"Desde: "+ data.from+" Hasta: "+ data.to},
								{text: "\n Síntomas" ,style:[ 'header1light' ]}
							  ],
					},
				  ],
				   columnGap: 20,
				   margin: [0, 0,0,20],
				});
		content.push({image:sintomas_chart_uri,width: 800});
		return content;
}


function observaciones_generales(data, dataUrl){
	

	
	var content = Array();

	content.push({columns: [
					{
						image: data.logo,
						width: 160
					},
					[
						{image: data.header,
							width:217
						},	
						{text: data.first_name, style:[ 'header1bold' ],	margin: [0, 30,0,0],
						},
						{text: data.last_name.toUpperCase(),style:[ 'header1light' ]}
					],
					{
						text: "Desde: "+ data.from+" Hasta: "+ data.to
					},
				  ],
				   columnGap: 20});
	
	
    	//var dataUrl = $("#word_cloud-url").val();
		data['luces']= $("#luces").val();
		data['medico']= $("#medico").val();
		data['objetivos_mes']= $("#objetivos_mes").val();
		data['objetivos_proximo_mes'] = $('#objetivos_prox_mes').val();
		data['principales_logros']= $("#principales_logros").val();
		data['principales_oportunidades']= $("#principales_oportunidades").val();
		data['analisis_resultados']= $("#analisis_resultados").val();
		data['comentarios_mes']= $("#comentarios_mes").val();
		data['informe_medico']= $("#informe_medico").val();
		data['informe_rehabilitacion']= $("#informe_rehabilitacion").val();
		data['informe_neuropsicologico']= $("#informe_neuropsicologico").val();

		columnas_izquierda = [ 
			{text: [{text:"Luces \n ",style:[ 'header2' ]},(data.luces) ],margin: [0, 20,0,5] },
			{text: [{text:"Médico Supervisor (Paz Mental) \n",style:[ 'header2' ]},data.medico ],margin: [0, 20,0,5] },
			{text: [{text:"Análisis Médico \n",style:[ 'header2' ]},data.analisis_resultados ],margin: [0, 20,0,5] },
		];

		if(data['informe_medico'] != ''){
			columnas_izquierda.push({text: [{text:"Informe Médico \n",style:[ 'header2' ]},data.informe_medico ],margin: [0, 20,0,5] });
		}

		if(data['informe_rehabilitacion'] != ''){
			columnas_izquierda.push({text: [{text:"Informe Rehabilitación \n",style:[ 'header2' ]},data.informe_rehabilitacion ],margin: [0, 20,0,5] });
		}

		columnas_derecha = [ 
			{text: [{text:"Objetivos del Mes \n",style:[ 'header2' ]},data.objetivos_mes ],margin: [0, 20,0,5] },
			{text: [{text:"Principales Logros \n",style:[ 'header2' ]},data.principales_logros ],margin: [0, 20,0,5] }, 
			{text: [{text:"Principales Oportunidades \n",style:[ 'header2' ]},data.principales_oportunidades ],margin: [0, 20,0,5] }, 
			{text: [{text:"Objetivos Próximo Mes \n",style:[ 'header2' ]},data.objetivos_proximo_mes ],margin: [0, 20,0,5] },
		];

		if(data['informe_neuropsicologico'] != ''){
			columnas_derecha.push({text: [{text:"Informe Neuropsicologico \n",style:[ 'header2' ]},data.informe_neuropsicologico ],margin: [0, 20,0,5] });
		}

		columnas = [];
		columnas.push(columnas_izquierda);
		columnas.push(columnas_derecha);

		content.push({columns: columnas});

		var com = data.comentarios_mes;
		content.push({text: [{text:"Observaciones de su Luz \n",style:[ 'header2' ]},com],margin: [0, 20,0,5]});	

		content.push({text:"",pageBreak:'before'});
		content.push({columns: [
					{
						image: data.logo,
						width: 160
					},
					[
						{image: data.header,
							width:217
						},	
						{text: data.first_name, style:[ 'header1bold' ],	margin: [0, 30,0,0],
						},
						{text: data.last_name.toUpperCase(),style:[ 'header1light' ]}
					],
					{
						text: "Desde: "+ data.from+" Hasta: "+ data.to
					},
				  ],
				   columnGap: 20});

		content.push({image:dataUrl,width:800 });
		content.push({text:"* La imagen anterior muestra de forma gráfica la frecuencia con la que se observan síntomas físicos y conductuales en el paciente",style:[ 'subtext' ]});
		return content;
	
} 

function updateReport(button_title, fnCallback) {

 
	  			var url = $("#updateReportVal").val();
	  			console.log(url);
	  			 
	  			 var $this = jQuery("#update-report")
			    , viewArr = $this.serializeArray()
			    , view = {};

 			 
			     reports_included = JSON.stringify({
   					 reports_included: $(':checkbox[name="reports_included[]"]').map(function() {
        			var op = {};
        			op[this.value] = this.checked;
        		return op;
    			}).get()
				});


			 	view['paciente_id'] = jQuery('[name="paciente_id"]').val();

	  				 $.ajax({
				        url: url,
				        data: {
				         	data: JSON.stringify(viewArr),
				         	reports_included:reports_included
	                 
				        },
					        type: 'POST',
					        success: function (respuesta) {
					        	
					        	response = JSON.parse(respuesta);

		                       	if(response.status == 'success'){
									if(button_title == 'Guardar') {
										new PNotify({
											title: "Guardar Reporte",
											type: "success",
											text: response.message,
											nonblock: {
												nonblock: true
											},
											addclass: '',
											styling: 'bootstrap3'
										});
									} else {
										new PNotify({
											title: "Guardando e imprimiendo",
											type: "success",
											text: response.message,
											nonblock: {
												nonblock: true
											},
											addclass: '',
											styling: 'bootstrap3'
										});
									}
									

									fnCallback();

									//setTimeout(function(){document.location.href = response.module}, 1000);
		                       	}

			        }
			       });
	  }


function showError(msg) {
          $('<div class="alert"> <button type="button" class="close" data-dismiss="alert">&times;</button>' +
            '<strong>Error:</strong> ' + msg + ' </div>').prependTo('#alerts');
        }

});

