<div class="login_wrapper">
    <div class="animate form login_form">
          <section class="login_content">
            <form role="form" method="POST" action="<?= base_url('auth/forgot_password') ?>">
              <h1>Recuperar</h1>
                <?php if ($message): ?>
                        <div class="alert alert-danger fade in">
                            <a href="#" class="close" data-dismiss="alert">&times;</a>
                            <?php echo $message; ?>
                        </div>
                    <?php endif; ?>
          
                        
                            <div class="form-group">
                                <input class="form-control" placeholder="E-mail" name="email" type="email" autofocus>
                            </div>

                          <div>
                            <br/>
                              <button class="btn btn-default submit" type="submit">Recuperar</button>
                            <a class="reset_pass" href="<?php echo base_url('auth/login'); ?>">Iniciar Sesión</a>
                          </div>

              <div class="clearfix"></div>

              <div class="separator">
                <div class="clearfix"></div>
                <br />

                <div class="logo">
                 <img src="<?php echo base_url();?>/assets/build/img/pazmental-logo-gray.png" />
                  <p><?php echo date('Y'); ?> &copy All Rights Reserved.Paz Mental</p>
                </div>
              </div>
            </form>
          </section>
        </div>
      
</div>
