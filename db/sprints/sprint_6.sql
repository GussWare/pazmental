CREATE TABLE `pacientes` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `tars_id` VARCHAR(255) NULL `id`,
  `name` VARCHAR(255) NULL,
  `address` VARCHAR(255) NULL,
  `city` VARCHAR(255) NULL,
  `zip` VARCHAR(255) NULL,
  `state` VARCHAR(255) NULL,
  `country` VARCHAR(255) NULL,
  `phones` VARCHAR(255) NULL,
  `goals` VARCHAR(255) NULL,
  `ife` VARCHAR(255) NULL,
  `curp` VARCHAR(255) NULL,
  `disease` VARCHAR(255) NULL,
  `nombre_familia` VARCHAR(255) NULL,
  `region` VARCHAR(255) NULL,
  `medicines` TEXT NULL,
  `map_long` VARCHAR(255) NULL,
  `map_lat` VARCHAR(255) NULL,
  `birthday` VARCHAR(255) NULL,
  PRIMARY KEY (`id`));


ALTER TABLE `services` 
ADD COLUMN `paciente_id` INT NULL AFTER `nombre_familia`;

INSERT INTO `options` (`option_name`, `option_value`) VALUES ('_service', '{\"name\":\"24x3.5\",\"description\":\"24x3.5\",\"daysperweek\":\"7\",\"timesperday\":\"24\",\"price\":\"37800\"}');

UPDATE `modules` SET `name`='Pacientes', `slug`='admin/pacientes' WHERE `id`='1';
UPDATE `modules` SET `name`='Listado Pacientes', `slug`='admin/pacientes' WHERE `id`='11'; 


CREATE TABLE `pacientes_relationship` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `paciente_id` INT NOT NULL,
  `relation_term` VARCHAR(255) NOT NULL,
  `relation_value` VARCHAR(255) NULL,
  PRIMARY KEY (`id`));

CREATE TABLE `pacientes_meta` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `paciente_id` INT NOT NULL,
  `paciente_meta` VARCHAR(45) NOT NULL,
  `paciente_value` BLOB NULL,
  `timestamp` TIMESTAMP NULL,
  PRIMARY KEY (`id`));

  CREATE TABLE `schedule_fechas_arranque` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nurse_id` INT NOT NULL,
  `service_id` INT NOT NULL,
  `fecha_arranque` TIMESTAMP NOT NULL,
  PRIMARY KEY (`id`));





