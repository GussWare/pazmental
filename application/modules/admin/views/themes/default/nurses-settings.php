 <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3><span class="fa fa-heartbeat"></span> Opciones Luces</h3>
              </div>

            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Configuración de Planes</h2>
                   
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                      
                   <div class="col-xs-3">
                      <!-- required for floating -->
                      <!-- Nav tabs -->
                      <ul class="nav nav-tabs tabs-left">
                        <li class="active"><a href="#services" data-toggle="tab">Planes</a>
                        </li>
                        <li><a href="#serviceType" data-toggle="tab">Tipos de Planes</a>
                        </li>
                        <li><a href="#deseases" data-toggle="tab">Enfermedades</a>
                        </li>
                        
                      </ul>
                    </div>

                    <div class="col-xs-9">
                      <!-- Tab panes -->
                      <div class="tab-content">
                        <div class="tab-pane active" id="services">
                          <p class="lead"><span class="a-left col-md-9">Planes a Contratar</span> <div class="col-md-3 a-right" style="text-align: right"><a href="<?php echo base_url();?>admin/settings/ajax" class="btn btn-info addEditRemove" data-rel="add" id="services" title="Agregar Servicio"><span class="fa fa-plus"></span>Agregar Planes</a></div></p>
                         
                          <table class="table table-striped" id="table-services">
                            <thead>
                              <tr>
                                <th>#</th>
                                <th>Nombre</th>
                                <th>Descripción</th>
                                <th>Precio de Venta</th>

                              </tr>
                            </thead>
                            <tbody>
                          <?php if($services!='') {

                              foreach($services as $service){
                                
                                $data = json_decode($service['option_value']);

                              ?>
<tr>
                              <td><?php echo $service['id'];?></td>
                              <td><?php echo $data->name;?></td>
                              <td><?php echo $data->description;?></td>
                              <td><?php echo $data->price;?></td>

</tr>

                          <?php      
                              }
                          }
                            ?>
                              
                            </tbody>
                          </table>
                        </div>
                        <div class="tab-pane" id="serviceType">
                           <p class="lead"><span class="a-left col-md-9">Tipos de Planes</span> <div class="col-md-3 a-right" style="text-align: right"><a href="<?php echo base_url();?>admin/settings/ajax" class="btn btn-info addEditRemove" data-rel="add" id="serviceType" title="Agregar Tipo Planes"><span class="fa fa-plus"></span>Agregar Tipo de Planes</a></div></p>
                         
                          <table class="table table-striped" id="table-services">
                            <thead>
                              <tr>
                                <th>#</th>
                                <th>Nombre</th>
                                <th>Descripción</th>

                              </tr>
                            </thead>
                            <tbody>
                          <?php if($types!='') {

                              foreach($types as $type){
                                $data = json_decode($type['option_value']);

                              ?>
<tr>
                              <td><?php echo $type['id'];?></td>
                              <td><?php echo $data->name;?></td>
                              <td><?php echo $data->description;?></td>

</tr>

                          <?php      
                              }
                          }
                            ?>
                              
                            </tbody>
                          </table>
                        </div>
                        <div class="tab-pane" id="deseases">
                             <p class="lead"><span class="a-left col-md-9">Enfermedades</span> <div class="col-md-3 a-right" style="text-align: right"><a href="<?php echo base_url();?>admin/settings/ajax" class="btn btn-info addEditRemove" data-rel="add" id="deseases" title="Agregar Enfermedad"><span class="fa fa-plus"></span>Agregar Enfermedad</a></div></p>
                         
                              <table class="table table-striped" id="table-services">
                                <thead>
                                  <tr>
                                    <th>#</th>
                                    <th>Nombre</th>
                                    <th>Descripción</th>

                                  </tr>
                                </thead>
                                <tbody>
                              <?php if($deseases!='') {

                                  foreach($deseases as $desease){
                                    $data = json_decode($desease['option_value']);

                                  ?>
                              <tr>
                                  <td><?php echo $desease['id'];?></td>
                                  <td><?php echo $data->name;?></td>
                                  <td><?php echo $data->description;?></td>

                              </tr>

                                <?php      
                                    }
                                }
                                  ?>

                                   </tbody>
                          </table>

                        </div>
                      
                      </div>
                    </div>

                    <div class="clearfix"></div>


                  </div>
                </div>
              </div>
            </div>
          </div>