<div class= "calificacion"> 
	<h2> CALIFICACION </h2>
	<h3>	de nuestros servicios </h3>

	<p>Antes de ver el reporte te pedimos que te tomes un momento para calificar a la luz que tienes asignada y a nuestros servicios.</p>

	
<form id="update_calificacion">
	<div class= "row">
		<div class="col-md-6">
			<label for="">¿Cuántas estrellas le das a la luz que tienes asignada?</label>
			<small>(1 es si estás muy insatisfecho y 6 es si estás muy satisfecho)</small>
			<div class="form-group">
				<input type="text" name="calificacion-luz" class="form-control" />
			</div>
			
			<input type="hidden" name="user_id"  id="user_id" value="<?php echo  $user_id;?>" >
			<input type="hidden" name="service_id"  id="service_id" value="<?php echo  $service_id;?>" >

		</div>
		<div class="col-md-6">
			<label for="">¿Cuántas estrellas le das al servicio que te ha proporcionado Paz Mental?</label>
			<small>(1 es si estás muy insatisfecho y 6 es si estás muy satisfecho)</small>
			<div class="form-group">
				<input type="text" name="calificacion-paz-mental" class="form-control" />
			</div>
			
		</div>
	</div>

<div class="row" id="razon-insatisfaccion-div">
<label for="">Razón por la cuál está insatisfecho </label>
	<div class="form-group">
		<select name="razon-insatisfaccion" id="razon-insatisfaccion" class="form-control">
             <option value="0">Ninguno</option>
         </select>
	</div>
</div>

<div class="row" id="razon-insatisfaccion-div">
	<label for="">Comentarios: </label>
	<textarea  rows="5" style="height: 100px;" class="form-control" name="comentarios" id="comentarios" ></textarea></div>
</div>

</form>
</div>