 <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3><span class="fa fa-cog"></span> Opciones Luces </h3>
              </div>

            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Configuración de Opciones de Luces</h2>
                   
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                      
                   <div class="col-xs-3">
                      <!-- required for floating -->
                      <!-- Nav tabs -->
                      <ul class="nav nav-tabs tabs-left">
                         
                        <li><a href="#characteristics" data-toggle="tab"> Características</a>
                        </li>
                        <li><a href="#personalities" data-toggle="tab"> Personalidad</a>
                        </li>
                        
                        <li><a href="#hobbies" data-toggle="tab"> Hobbies</a>
                        </li>
                        <li><a href="#institutions" data-toggle="tab"> Instituciones</a>
                        </li>
                        <li><a href="#care_skills" data-toggle="tab">  Habilidades de Cuidadora</a>
                        </li>
                        <li><a href="#therapy_skills" data-toggle="tab"> Habilidades de Terapeuta</a>
                        </li>
                        <li><a href="#recruitment_sources" data-toggle="tab"> Fuentes de Reclutamiento</a>
                        </li>
                        
                           
        
                        
                      </ul>
                    </div>

                    <div class="col-xs-9">
                      <!-- Tab panes -->
                      <div class="tab-content">

                         <div class="tab-pane active" id="characteristics">
                             <p class="lead"><span class="a-left col-md-9">Características</span> <div class="col-md-3 a-right" style="text-align: right"><a href="<?php echo base_url();?>admin/settings/ajax" class="btn btn-info addEditRemove" data-rel="add" id="characteristics" title="Agregar Característica"><span class="fa fa-plus"></span>Agregar Característica</a></div></p>
                         
                              <table class="table table-striped" id="table-services">
                                <thead>
                                  <tr>
                                    <th>#</th>
                                    <th>Nombre</th>
                                    <th>Descripción</th>
                                    <th> Borrar </th>

                                  </tr>
                                </thead>
                                <tbody>
                              <?php if($characteristics!='') {

                                  foreach($characteristics as $characteristic){
                                    $data = json_decode($characteristic['option_value']);

                                  ?>
                              <tr>
                                  <td><?php echo $characteristic['id'];?></td>
                                  <td><?php echo $data->name;?></td>
                                  <td><?php echo $data->description;?></td>
                                  <td>   <a href="<?= base_url('admin/nurses_settings/delete/'.$characteristic["id"]) ?>" class="btn btn-danger"  data-toggle="tooltip" data-placement="top" title="" data-original-title="Eliminar">
                                     <i class="fa fa-remove"></i> 
                                  </a></td>

                              </tr>

                                <?php      
                                    }
                                }
                                  ?>

                                   </tbody>
                          </table>

                        </div>
                        <div class="tab-pane" id="personalities">
                             <p class="lead"><span class="a-left col-md-9">Personalidad</span> <div class="col-md-3 a-right" style="text-align: right"><a href="<?php echo base_url();?>admin/settings/ajax" class="btn btn-info addEditRemove" data-rel="add" id="personalities" title="Agregar Personalidad"><span class="fa fa-plus"></span>Agregar Personalidad</a></div></p>
                         
                              <table class="table table-striped" id="table-services">
                                <thead>
                                  <tr>
                                    <th>#</th>
                                    <th>Nombre</th>
                                    <th>Descripción</th>
                                    <th> Borrar </th>

                                  </tr>
                                </thead>
                                <tbody>
                              <?php if($personalities!='') {

                                  foreach($personalities as $personality){
                                    $data = json_decode($personality['option_value']);

                                  ?>
                              <tr>
                                  <td><?php echo $personality['id'];?></td>
                                  <td><?php echo $data->name;?></td>
                                  <td><?php echo $data->description;?></td>
                                  <td>   <a href="<?= base_url('admin/nurses_settings/delete/'.$personality["id"]) ?>" class="btn btn-danger"  data-toggle="tooltip" data-placement="top" title="" data-original-title="Eliminar">
                                     <i class="fa fa-remove"></i> 
                                  </a></td>

                              </tr>

                                <?php      
                                    }
                                }
                                  ?>

                                   </tbody>
                          </table>

                        </div>
                        <div class="tab-pane" id="hobbies">
                             <p class="lead"><span class="a-left col-md-9">Hobbies</span> <div class="col-md-3 a-right" style="text-align: right"><a href="<?php echo base_url();?>admin/settings/ajax" class="btn btn-info addEditRemove" data-rel="add" id="hobbies" title="Agregar Hobby"><span class="fa fa-plus"></span>Agregar Hobby</a></div></p>
                         
                              <table class="table table-striped" id="table-services">
                                <thead>
                                  <tr>
                                    <th>#</th>
                                    <th>Nombre</th>
                                    <th>Descripción</th>
                                    <th> Borrar </th>

                                  </tr>
                                </thead>
                                <tbody>
                              <?php if($hobbies!='') {

                                  foreach($hobbies as $hobby){
                                    $data = json_decode($hobby['option_value']);

                                  ?>
                              <tr>
                                  <td><?php echo $hobby['id'];?></td>
                                  <td><?php echo $data->name;?></td>
                                  <td><?php echo $data->description;?></td>
                                  <td>   <a href="<?= base_url('admin/nurses_settings/delete/'.$hobby["id"]) ?>" class="btn btn-danger"  data-toggle="tooltip" data-placement="top" title="" data-original-title="Eliminar">
                                     <i class="fa fa-remove"></i> 
                                  </a></td>
                              </tr>

                                <?php      
                                    }
                                }
                                  ?>

                                   </tbody>
                          </table>

                        </div>
                      
                    
                        <div class="tab-pane" id="institutions">
                             <p class="lead"><span class="a-left col-md-9">Instituciones</span> <div class="col-md-3 a-right" style="text-align: right"><a href="<?php echo base_url();?>admin/settings/ajax" class="btn btn-info addEditRemove" data-rel="add" id="institutions" title="Agregar Institución"><span class="fa fa-plus"></span>Agregar Institución</a></div></p>
                         
                              <table class="table table-striped" id="table-services">
                                <thead>
                                  <tr>
                                    <th>#</th>
                                    <th>Nombre</th>
                                    <th>Descripción</th>
                                    <th> Borrar </th>


                                  </tr>
                                </thead>
                                <tbody>
                              <?php if($institutions!='') {

                                  foreach($institutions as $institution){
                                    $data = json_decode($institution['option_value']);

                                  ?>
                              <tr>
                                  <td><?php echo $institution['id'];?></td>
                                  <td><?php echo $data->name;?></td>
                                  <td><?php echo $data->description;?></td>
                                  <td>   <a href="<?= base_url('admin/nurses_settings/delete/'.$institution["id"]) ?>" class="btn btn-danger"  data-toggle="tooltip" data-placement="top" title="" data-original-title="Eliminar">
                                     <i class="fa fa-remove"></i> 
                                  </a></td>

                              </tr>

                                <?php      
                                    }
                                }
                                  ?>

                                   </tbody>
                          </table>

                        </div>
                       
                        <div class="tab-pane" id="care_skills">
                             <p class="lead"><span class="a-left col-md-9">Habilidades de Cuidado</span> <div class="col-md-3 a-right" style="text-align: right"><a href="<?php echo base_url();?>admin/settings/ajax" class="btn btn-info addEditRemove" data-rel="add" id="care_skills" title="Agregar Habilidad de Cuidado"><span class="fa fa-plus"></span>Agregar Habilidad de Cuidado</a></div></p>
                         
                              <table class="table table-striped" id="table-services">
                                <thead>
                                  <tr>
                                    <th>#</th>
                                    <th>Nombre</th>
                                    <th>Descripción</th>
                                    <th> Borrar </th>

                                  </tr>
                                </thead>
                                <tbody>
                              <?php if($therapy_skills!='') {

                                  foreach($care_skills as $care_skill){
                                    $data = json_decode($care_skill['option_value']);

                                  ?>
                              <tr>
                                  <td><?php echo $care_skill['id'];?></td>
                                  <td><?php echo $data->name;?></td>
                                  <td><?php echo $data->description;?></td>
                                  <td>   <a href="<?= base_url('admin/nurses_settings/delete/'.$care_skill["id"]) ?>" class="btn btn-danger"  data-toggle="tooltip" data-placement="top" title="" data-original-title="Eliminar">
                                     <i class="fa fa-remove"></i> 
                                  </a></td>

                              </tr>

                                <?php      
                                    }
                                }
                                  ?>

                                   </tbody>
                          </table>

                         
                    </div>
                             <div class="tab-pane" id="therapy_skills">
                             <p class="lead"><span class="a-left col-md-9">Habilidades de Terapia</span> <div class="col-md-3 a-right" style="text-align: right"><a href="<?php echo base_url();?>admin/settings/ajax" class="btn btn-info addEditRemove" data-rel="add" id="therapy_skills" title="Agregar Habilidad de Terapia"><span class="fa fa-plus"></span>Agregar Habilidad de Terapia</a></div></p>
                         
                              <table class="table table-striped" id="table-services">
                                <thead>
                                  <tr>
                                    <th>#</th>
                                    <th>Nombre</th>
                                    <th>Descripción</th>
                                    <th> Borrar </th>

                                  </tr>
                                </thead>
                                <tbody>
                              <?php if($therapy_skills!='') {

                                  foreach($therapy_skills as $therapy_skill){
                                    $data = json_decode($therapy_skill['option_value']);

                                  ?>
                              <tr>
                                  <td><?php echo $therapy_skill['id'];?></td>
                                  <td><?php echo $data->name;?></td>
                                  <td><?php echo $data->description;?></td>
                                  <td>   <a href="<?= base_url('admin/nurses_settings/delete/'.$therapy_skill["id"]) ?>" class="btn btn-danger"  data-toggle="tooltip" data-placement="top" title="" data-original-title="Eliminar">
                                     <i class="fa fa-remove"></i> 
                                  </a></td>

                              </tr>

                                <?php      
                                    }
                                }
                                  ?>

                                   </tbody>
                          </table>

                        </div>
                      
 
                     <div class="tab-pane" id="recruitment_sources">
                             <p class="lead"><span class="a-left col-md-9">Fuentes de Reclutamiento</span> <div class="col-md-3 a-right" style="text-align: right"><a href="<?php echo base_url();?>admin/settings/ajax" class="btn btn-info addEditRemove" data-rel="add" id="recruitment_sources" title="Agregar Fuente de Reclutamiento"><span class="fa fa-plus"></span>Agregar Fuente de Reclutamiento</a></div></p>
                         
                              <table class="table table-striped" id="table-services">
                                <thead>
                                  <tr>
                                    <th>#</th>
                                    <th>Nombre</th>
                                    <th>Descripción</th>
                                    <th> Borrar </th>

                                  </tr>
                                </thead>
                                <tbody>
                              <?php if($recruitment_sources!='') {

                                  foreach($recruitment_sources as $recruitment_source){
                                    $data = json_decode($recruitment_source['option_value']);

                                  ?>
                              <tr>
                                  <td><?php echo $recruitment_source['id'];?></td>
                                  <td><?php echo $data->name;?></td>
                                  <td><?php echo $data->description;?></td>
                                  <td>   <a href="<?= base_url('admin/nurses_settings/delete/'.$recruitment_source["id"]) ?>" class="btn btn-danger"  data-toggle="tooltip" data-placement="top" title="" data-original-title="Eliminar">
                                     <i class="fa fa-remove"></i> 
                                  </a></td>

                              </tr>

                                <?php      
                                    }
                                }
                                  ?>

                                   </tbody>
                          </table>

                        </div>
                       
                        </div>
                    <div class="clearfix"></div>

    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>