<div class="">
    <div class="page-title">
        <div class="title_left">
            <h3>
                <span class="fa fa-heartbeat"></span> Pacientes</h3>
        </div>

        <div class="panel_toolbox">
            <a href="<?php echo base_url('admin/pacientes') ?>" class="btn btn-success">
                <span class="fa fa-angle-double-left"></span> Regresar</a>
        </div>
    </div>

    <div class="clearfix"></div>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Actualizar Paciente</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div id="step_content">
                        <form action="" class="" id="create-paciente">
                            <h4 class="col-md-3">Datos Personales del Paciente</h4>
                            <div class="col-md-3">ID Paciente:
                                <input type="text" name="paciente_id" class="form-control" disabled value="<?php echo $paciente_id;?>"> </div>
                            
                            <div class="col-md-3">
                                Nombre Familia
                                <input type="text" name="nombre_familia" class="form-control" value="<?php echo $paciente->nombre_familia; ?>"
                                    placeholder="Nombre de la familia">
                            </div>

                            <input type="hidden" name="paciente_id" class="form-control" value="<?php echo $paciente_id;?>">
                            <br/>
                    </div>
                    <div class="clearfix"></div>


                    <?php $name = json_decode($paciente->name); ?>

                    <div class="col-md-3 col-sm-3">
                        <label for="">Nombre</label>
                        <div class="form-group">
                            <input type="text" class="form-control" name="FirstName" id="FirstName" value="<?php echo $name->first_name; ?> ">
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-3">
                        <label for="">Segundo Nombre</label>
                        <div class="form-group">
                            <input type="text" class="form-control" name="SecondName" id="SecondName" value="<?php echo $name->second_name; ?>">
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-3">
                        <label for="">Apellido Paterno</label>
                        <div class="form-group">
                            <input type="text" class="form-control" name="LastName" id="LastName" value="<?php echo $name->last_name; ?>">
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-3">
                        <label for="">Apellido Materno</label>
                        <div class="form-group">
                            <input type="text" class="form-control" name="SecondLastname" id="SecondLastName" value="<?php echo $name->second_last_name; ?>">
                        </div>
                    </div>

                    <div class="clearfix"></div>

                    <div class="col-md-4 col-sm-4">
                        <label for="">Fecha de Nacimiento</label>

                        <div class=" xdisplay_inputx form-group has-feedback">
                            <input type="text" class="form-control has-feedback-left single_cal_picker" id="Birthdate" name="Birthdate" placeholder="Fecha de Nacimiento"
                                value="<?php echo $_birthday; ?>">
                            <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <label for="">Condición</label>
                        <div class="form-group">
                            <select name="Desease" id="Desase" class="form-control">
                                <option value="0">Ninguno</option>
                                <?php foreach($deseases as $desease):
                              $info = json_decode($desease['option_value']); ?>


                                <option value="<?php echo $desease['id']; ?>" <?php if($desease['id']==$paciente->disease) echo 'selected'; ?>>
                                    <?php echo $info->name; ?>
                                </option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <label for="">Teléfono</label>
                        <div class="form-group">
                            <input type="text" name="Phone" class="form-control" value="<?php echo $paciente->phones; ?>">
                        </div>
                    </div>

                    <div class="clearfix"></div>

                    <h4>Dirección del Servicio</h4>
                    <br>


                    <?php $address = json_decode($paciente->address);  ?>

                    <div class="col-sm-4 col-md-4">
                        <label for="">Calle</label>
                        <div class="form-group">
                            <input type="text" class="form-control" name="Street" id="address" value="<?php echo $address->street; ?>">
                        </div>
                    </div>
                    <div class="col-sm-4 col-md-4">
                        <label for="">Número Exterior</label>
                        <div class="form-group">
                            <input type="text" class="form-control" name="ExternalNumber" value="<?php echo $address->number; ?>">
                        </div>
                    </div>
                    <div class="col-sm-4 col-md-4">
                        <label for="">Número Interior</label>
                        <div class="form-group">
                            <input type="text" class="form-control" name="InternalNumber" value="<?php echo $address->internal; ?>">
                        </div>
                    </div>
                    <div class="clearfix"></div>

                    <div class="col-md-3 col-sm-3">
                        <label for="">Colonia</label>
                        <div class="form-group">
                            <input type="text" class="form-control" name="colony" id="colony" value="<?php echo $address->colony; ?>">
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-3">
                        <label for="">Delegación / Municipio </label>
                        <div class="form-group">
                            <input type="text" class="form-control" name="City" id="city" value="<?php echo $paciente->city; ?>">
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-3">
                        <label for="">Estado</label>
                        <div class="form-group">
                            <input type="text" class="form-control" name="State" id="region" value="<?php echo $paciente->region; ?>">
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-3">
                        <label for="">C.P</label>
                        <div class="form-group">
                            <input type="text" class="form-control" name="Zip" id="zip" value="<?php echo $paciente->zip; ?>">
                        </div>
                    </div>

                    <div class="clearfix"></div>
                    <div class="col-md-3 col-sm-3">
                        <input type="hidden" class="form-control" id="map_lat" name="map_lat" value="<?php echo $map_lat; ?>"
                        />
                    </div>
                    <div class="col-md-3 col-sm-3">
                        <input type="hidden" class="form-control" id="map_long" name="map_long" value="<?php echo $map_long; ?>"
                        />
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-4 col-sm-4">
                        <a title="Actualizar Mapa" class="btn btn-info updateMap">Actualizar Mapa</a>
                    </div>

                    <div class="form-group col-md-9 col-sm-9">


                        <div class="clearfix"></div>

                        <div id="map" style="height:400px; min-width:400px;margin-top:20px"></div>

                    </div>

                    <div class="clearfix"></div>
                    <br>
                    <br>

                    <div class="col-sm-12 col md-12">
                        <br>
                        <br>
                        <h4>Objetivos del Mes</h4>
                        <br>
                        <br>
                        <textarea id="message" required="required" class="form-control" name="Goals" data-parsley-trigger="keyup" data-parsley-minlength="20"
                            data-parsley-maxlength="100" data-parsley-minlength-message="Come on! You need to enter at least a 20 caracters long comment.."
                            data-parsley-validation-threshold="10"><?php echo $paciente->goals;  ?></textarea>
                        <br>
                        <br>
                    </div>

                    <div class="clear"></div>
                    <div class="clearfix"></div>
                    <br>
                    <br>
                    <h4 class="col-md-9">Familiares que desean recibir notificaciones</h4>
                    <div class="col-md-3" style="text-align: right">
                        <a href="<?php echo base_url();?>admin/relations/create" class="btn btn-info bootbox" id="relations"
                            data-rel="add" title="Agregar Familiar">
                            <span class="fa fa-plus"></span> Agregar Familiar</a>
                    </div>
                    <br>
                    <br>
                    <br>
                    <div class="clearfix"></div>
                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action" id="table-relations">
                            <thead>
                                <tr class="headings">
                                    <th class="column-title">Nombre Completo</th>
                                    <th class="column-title">Relación</th>
                                    <th class="column-title">E-mail</th>
                                    <th class="column-title">Teléfono</th>
                                    <th class="column-title">Tipo de Pariente</th>
                                    <th class="column-title">Calificación Plan</th>
                                    <th class="column-title">Calificación Luz </th>
                                    <th class="column-title">Borrar</th>

                                </tr>
                            </thead>

                            <tbody>

                                <?php foreach($pacientes_familiares as $familiar): ?>
                                <tr id="familiar-row-<?php echo $familiar['data']->id;?>">
                                    <td>
                                        <?php echo $familiar['data']->first_name; ?>
                                        <?php echo $familiar['data']->last_name; ?>
                                    </td>
                                    <td>Familiar</td>
                                    <td>
                                        <?php echo $familiar['data']->email; ?>
                                    </td>
                                    <td>
                                        <?php echo $familiar['data']->phone; ?>
                                    </td>
                                    <td>
                                        <?php echo $familiar['relationType']; ?>
                                    </td>
                                    <td>
                                        <?php echo $familiar['calificacion_servicio']; ?>
                                    </td>
                                    <td>
                                        <?php echo $familiar['calificacion_luz']; ?>
                                    </td>

                                    <td>
                                        <a href="<?php echo base_url('admin/pacientes/delete_familiar/'.$paciente_id); ?>" class="btn btn-danger eliminar-familiar"
                                            data-toggle="tooltip" data-placement="top" title="Eliminar Familiar" data-rel="delete"
                                            familiar-id="<?php echo $familiar['data']->id; ?>" data-original-title="Eliminar">
                                            <i class="fa fa-remove"></i>
                                        </a>
                                        <input type="hidden" name="RelationIDs[]" value="<?php echo $familiar['data']->id; ?>"
                                            class="RelationIDs">
                                    </td>


                                </tr>

                                <?php endforeach; ?>

                            </tbody>
                        </table>
                    </div>

                    <br>
                    <br>


                    <h4 class="col-md-9">Medicamentos</h4>
                    <div class="col-md-3" style="text-align: right">
                        <a href="<?php echo base_url();?>admin/pacientes/medicine" class="btn btn-info bootbox" id="medicine"
                            data-rel="add" title="Agregar Medicamento">
                            <span class="fa fa-plus"></span> Agregar Medicamento</a>
                    </div>
                    <br>
                    <br>
                    <div class="clearfix"></div>

                    <table class="table col-md-12" id="table-medicine">
                        <thead>
                            <tr>
                                <th>Medicamento</th>
                                <th>Dosis</th>
                                <th>Veces por día</th>
                                <th>Horas</th>
                                <th>Borrar</th>
                            </tr>
                        </thead>
                        <tbody>

                            <?php foreach($service_medicines as $medicine):
                      $medicine_id = strtr(utf8_decode($medicine[0]), utf8_decode('àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ '), 'aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY-'); ?>
                            <tr id="medicine-row-<?php echo $medicine_id;?>">
                                <td>
                                    <div class="form-group col-md-12">
                                        <input type="text" class="form-control Medicine_Names" name="Medicine_Names[]" value="<?php echo $medicine[0]; ?>"
                                        />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group col-md-12">
                                        <input type="text" class="form-control Medicine_Dosis" name="Medicine_Dosis[]" value="<?php echo $medicine[1]; ?>"
                                        />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group col-md-12">
                                        <input type="text" class="form-control Medicine_TimesperDay" name="Medicine_TimesperDay[]"
                                            value="<?php echo $medicine[2]; ?>" disabled />
                                    </div>
                                </td>
                                <td>
                                    <?php foreach($medicine[3] as $med=>$value): ?>
                                    <div class="form-group col-md-12 col-sm-12 clockpicker">
                                        <input type="text" name="Medicine_Times[]" class="form-control Medicine_Times" value="<?php echo $value; ?>"
                                        />
                                    </div>
                                    <?php endforeach; ?>
                                </td>
                                <td>
                                    <a href="<?php echo base_url('admin/pacientes/delete_medicina/'.$paciente_id); ?>" class="btn btn-danger eliminar-medicinas"
                                        data-toggle="tooltip" data-placement="top" title="Eliminar Medicina" data-rel="delete"
                                        medicinas-id="<?php echo $medicine_id; ?>" data-original-title="Eliminar">
                                        <i class="fa fa-remove"></i>
                                    </a>
                                </td>
                            </tr>
                            <?php endforeach; ?>

                        </tbody>
                    </table>

                    <div class="clearfix"></div>
                    </form>
                </div>
                <div class="clearfix"></div>

                <br>
                <br>

                <div class="col-md-9 col-sm-9">
                </div>
                <div class="col-md-3 col-sm-3" style="text-align:right;">
                    <a href="<?php echo base_url(); ?>admin/pacientes/save" id="createPaciente" data-rel="2" title="Crear Paciente" class="btn btn-info createPaciente">Guardar</a>
                </div>

            </div>
        </div>
    </div>
</div>
</div>