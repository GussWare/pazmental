<form id="form-routines-rows">
	<div class="form-group">
		<label for="">Días a la semana</label>
		<input type="number" class="form-control" name="daysperweek" min="1">
	</div>
	<div class="form-group">
		<label for="">Horas al día</label>
		<input type="number" class="form-control" name="timesperday" min="1">
	</div>
</form>