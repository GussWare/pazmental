<div class="">
    <div class="page-title">
        <div class="title_left">
            <h3>
                <span class="fa fa-heartbeat"></span> Servicios</h3>
        </div>

        <div class="panel_toolbox">
            <a href="<?php echo base_url('admin/pacientes') ?>" class="btn btn-success">
                <span class="fa fa-angle-double-left"></span> Regresar</a>
        </div>
    </div>

    <div class="clearfix"></div>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Crear nuevo Servicio</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div id="step_content">
                        <form action="" class="" id="create-service">
                        <input type="hidden" id="paciente_id" name="paciente_id" class="form-control" value="<?php echo $paciente->id; ?>">

                            <h4 class="col-md-3">Datos Personales del Paciente</h4>
                            
                            
                            <br/>
                            <div class="clearfix"></div>

                            <br />

                            <div class="col-md-3 col-sm-3">
                                <label for="">Nombre</label>
                                <div class="form-group">
                                    <?php $name = json_decode($paciente->name); ?>
                                    <span id="FirstName"><?php echo $name->first_name; ?></span>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-3">
                                <label for="">Segundo Nombre</label>
                                <div class="form-group">
                                <?php echo $name->second_name; ?>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-3">
                                <label for="">Apellido Paterno</label>
                                <div class="form-group">
                                <span id="LastName"><?php echo $name->last_name; ?></span>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-3">
                                <label for="">Apellido Materno</label>
                                <div class="form-group">
                                    <?php echo $name->second_last_name; ?>
                                </div>
                            </div>

                            <div class="clearfix"></div>

                    </div>
                    <div class="clearfix"></div>

                    <h4>Plan</h4>
                    <div class="col-md-4">
                        ID Servicio:
                        <input type="text" name="service_id" class="form-control" value="" disabled
                                    placeholder="Auto generated">
                    </div>

                    <div class="col-md-4">
                        Tars ID:
                        <input type="text" name="tars_id" class="form-control" value="" placeholder="Tars ID">
                    </div>
    
                    <div class="col-md-4 col-sm-4">
                        <label for="">
                            Plan Contratado
                        </label>
                        <select name="Service" id="Service" class="form-control onChangeService" data-target="<?php echo base_url(); ?>admin/services/select">
                            <option value="0">Ninguno</option>
                            <?php foreach($services as $service):
                      $info = json_decode($service['option_value']); ?>
                            <option value="<?php echo $service['id']; ?>">
                                <?php echo $info->name; ?>
                            </option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="clearfix"></div>

                    <div class="col-md-4 col-sm-4">
                        <label for="">
                            Precio de Venta
                        </label>
                        <input type="text" class="form-control" name="price" id="precio-de-venta">
                    </div>

                    <div class="col-md-4 col-sm-4">
                        <label for="">
                            Pago diario de luz
                        </label>
                        <input type="text" class="form-control" name="cost" id="costo">
                    </div>

                    <div class="col-md-4 col-sm-4">
                        <label for="">
                            Fecha de Inicio
                        </label>
                        <div class="col-md-11 xdisplay_inputx form-group has-feedback">
                            <input type="text" class="form-control has-feedback-left single_cal_picker" id="start_date" name="start_date" placeholder="Fecha de Inicio"
                            />
                            <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>

                        </div>
                    </div>
                    <div class="clearfix"></div>


                    <div class="col-md-4 col-sm-4">
                        <label for="">
                            Tipo de Servicio
                        </label>
                        <select name="ServiceType" id="ServiceType" class="form-control">
                            <option value="0">Ninguno</option>
                            <?php foreach($types as $service):
                      $info = json_decode($service['option_value']); ?>
                            <option value="<?php echo $service['id']; ?>">
                                <?php echo $info->name; ?>
                            </option>
                            <?php endforeach; ?>
                        </select>
                    </div>


                    <div class="col-md-4 col-sm-4">
                        <label for="">
                            Médico asignado
                        </label>
                        <select name="doctor" id="Asignar_Doctor" class="form-control onChangeDoctor" data-target="<?php echo base_url(); ?>admin/services/doctor">
                            <option value="0">Ninguno</option>
                            <?php foreach($doctors as $doctor):
                            $name = $doctor->first_name. ' '.$doctor->last_name; ?>
                            <option value="<?php echo $doctor->id; ?>">
                                <?php echo $name; ?>
                            </option>
                            <?php endforeach; ?>
                            <option value="-1">Otro</option>
                        </select>
                    </div>


                    <div class="col-md-4 col-sm-4">
                        <label for="">
                            Supervisora a Cargo
                        </label>
                        <select name="supervisor" id="" class="form-control">
                            <option value="0">Ninguno</option>

                            <?php foreach($supervisors as $supervisor):
                      $name = $supervisor->first_name. ' '.$supervisor->last_name; ?>
                            <option value="<?php echo $supervisor->id; ?>">
                                <?php echo $name; ?>
                            </option>
                            <?php endforeach; ?>
                        </select>
                    </div>


                    <div class="col-md-4 col-sm-4">
                        <label for="">
                            Ejecutivo de Servicio
                        </label>
                        <select name="ejecutivo" id="" class="form-control">
                            <option value="0">Ninguno</option>
                            <?php 
                      foreach($ejecutivos as $ejecutivo):
                      $name = $ejecutivo->first_name. ' '.$ejecutivo->last_name; ?>
                            <option value="<?php echo $ejecutivo->id; ?> ">
                                <?php echo $name; ?>
                            </option>
                            <?php endforeach; ?>
                        </select>
                    </div>

                    <div class="form-group col-md-3 col-sm-3">
                        <label>Status</label>
                        <div class="control-group">

                            <select name="service_status" id="service_status" class="form-control">
                                <option value="">Seleccionar</option>
                                <option value="1"> Activo</option>
                                <option value="2"> Inestable</option>
                                <option value="3"> Suspendido</option>
                                <option value="4"> Cancelado</option>
                            </select>
                        </div>
                    </div>

                    <div class="clearfix"></div>
                    <br>
                    <br>

                    <div class="col-sm-12 col md-12">


                        <div class="col-md-12" style="text-align: right">
                            <a href="<?php echo base_url();?>admin/services/select" class="btn btn-info addrows" id="routines-rows"
                                data-rel="add" title="Agregar Días y Tiempos de Rutina">
                                <span class="fa fa-plus"></span> Agregar Rutinas</a>
                        </div>
                        <br>
                        <br>


                        <h4 class="col-md-9">Rutinas</h4>
                        <br>
                        <br>

                        <table class="table table-striped jambo_table bulk_action" id="table-routines-rows">
                        </table>
                        <br>
                        <br>


                        <h4>Opción 1: Asignación de Luz y Horarios utilizando Mapas</h4>
                        <div class=" col-md-6 col-sm-6" style="margin-bottom:20px">
                            <div class="clockpicker col-md-12 col-sm-12" style="margin-bottom:20px">
                                <label for="horario_inicio_map">
                                    Horario Tentativo de Inicio de Servicio
                                </label>
                                <input type="text" name="horario_inicio_map" id="horario_inicio_map" class="form-control  " />
                            </div>

                            <div class="clockpicker col-md-12 col-sm-12" style="margin-bottom:20px">
                                <label for="horario_inicio_map">
                                    Horario Tentativo de Fin de Servicio
                                </label>
                                <input type="text" name="horario_fin_map" id="horario_fin_map" class="form-control  " />
                            </div>
                            <a title="Ver Opciones" class="btn btn-info updateMarkerMap">Ver Opciones</a>
                        </div>
                        <div class=" col-md-6 col-sm-6" style="margin-bottom:20px">
                            <label for="dias_servicio">
                                Días Tentativos de Servicio
                            </label>
                            <?php foreach($date_range as $d):  ?>
                            <div class="checkbox">
                                <label>
                                    <input name="day_availability" class="day_availability" id="<?php echo $d; ?>" type="checkbox"
                                        value="<?php echo $d; ?>">
                                    <?php echo $d; ?>
                                </label>
                            </div>
                            <?php endforeach; ?>

                        </div>
                        <?php if(!empty($nurses)): ?>
                        <input type="hidden" id="lat_results" value='<?php echo json_encode($lat_results);?>'>
                        <input type="hidden" id="nurse_homes" value='<?php echo json_encode($nurse_homes);?>'>
                        <input type="hidden" id="schedule" value='<?php echo json_encode($schedule);?>'>
                        <div id="map_service" style="height:400px; min-width:400px;margin-top:20px"></div>
                        <h4>Opción 2: Asignación de Luz y Horarios utilizando Filtros</h4>
                        <div id="filtering">
                            <div id="filters" class="filters col-md-12 col-sm-12">

                                <div class="form-group col-md-4 col-sm-4">
                                    <label>Seleccionar Supervisora</label>
                                    <div class="control-group">
                                        <select name="supervisor" id="supervisor" class="form-control">
                                            <option value="0">Ninguno</option>

                                            <?php foreach($supervisors as $supervisor):
                                    $name = $supervisor->first_name. ' '.$supervisor->last_name; ?>
                                            <option value="<?php echo $supervisor->id; ?>" <?php if ($filtered_supervisor==$supervisor->id){echo "selected";} ?> >
                                                <?php echo $name; ?>
                                            </option>
                                            <?php endforeach; ?>
                                        </select>

                                    </div>
                                </div>


                                <div class="form-group col-md-4 col-sm-4">
                                    <label>Seleccionar Luz</label>
                                    <div class="control-group">
                                        <select name="luz" id="luz" class="form-control">
                                            <option value="0">Ninguno</option>

                                            <?php foreach($nurses as $nurse ): ?>
                                            <?php if($nurses_status[$nurse->id] > 0){ ?>
                                            <option value="<?php echo $nurse->id; ?>" <?php if ($filtered_luz==$nurse->id){echo "selected";} ?> >
                                                <?php echo $nurse->first_name;?>
                                                <?php echo $nurse->last_name;?>
                                            </option>
                                            <?php } endforeach; ?>
                                        </select>

                                    </div>
                                </div>

                                <div class="form-group col-md-2 col-sm-2">

                                    <label>Género:</label>
                                    <select class="form-control" name="pm_gender" id="pm_gender">
                                        <option value="" <?php if($filtered_gender==0 ) echo 'selected'; ?> >Seleccionar</option>
                                        <option value="M" <?php if($filtered_gender=='M' ) echo 'checked'; ?> >M </option>
                                        <option value="F" <?php if($filtered_gender=='F' ) echo 'checked'; ?> >F </option>
                                    </select>
                                </div>

                                <div class="form-group col-md-3 col-sm-3">
                                    <label>Colonia</label>
                                    <div class="control-group">

                                        <select name="Colony" id="Colony" class="form-control">
                                            <option value="" <?php if($filtered_colony=="none" ) echo 'selected'; ?> >Seleccionar</option>
                                            <?php foreach($colonies as $colony): 
                                                    $value = $colony['colony']; ?>

                                            <option value="<?php echo $value;?>" <?php if ($filtered_colony==$value){echo "selected";} ?> >
                                                <?php echo $value;?>
                                            </option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>

                                </div>

                                <div class="form-group col-md-6 col-sm-6">
                                    <label>Personalidad</label>

                                    <?php foreach($personalities as $pers):
                            
                              $info = json_decode($pers['option_value']); ?>

                                    <div class="checkbox" style="display: inline-block;">
                                        <label>
                                            <input name="personalities" id="personalities" type="checkbox" value=" <?php echo $pers['id']; ?>"
                                                <?php if(in_array($pers['id'],$filtered_personality)) echo 'selected'; ?>>
                                            <?php echo $info->name; ?>
                                        </label>
                                    </div>
                                    <?php endforeach; ?>
                                </div>



                                <div class="col-md-3 col-sm-3">
                                    <br>
                                    <a class="filters-toggle" href="<?php echo base_url(); ?>admin/services/create" class="btn btn-default form-control"
                                        id="filter_submit2" data-rel="services_create_filter">Filtrar
                                        <span class="fa fa-filter"></span>
                                    </a>
                                </div>

                            </div>
                        </div>

                        <div class="col-md-2 col-sm-2">
                            <h5>Luces Disponibles</h5>

                            <ul class="nurses-list-schedule" id="nurses-list-schedule">
                                <?php 

                    
                    if($assigend_nurse >=0){
                        $active_id = $assigned_nurse;
                    } 
                    else{
                      $active_id =-1;
                    }
                           
                    $i = 0; foreach($nurses as $nurse):   ?>
                                <?php if($nurses_status[$nurse->id] > 0){ $i++; ?>


                                <li>
                                    <button href="#" style="width:auto;float:left;position: relative;left: -23px; top: 0px;" onclick='window.open("<?php echo base_url(); ?>/admin/nurses/edit/<?php echo $nurse->id; ?>");return false;'>
                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                    </button>
                                    <a style="width:90%;font-size:10px;float:right;margin-top:-27px;height: 40px;" href="<?php echo base_url(); ?>admin/nurses/schedule"
                                        data-rel="<?php echo $nurse->id; ?>" class="btn btn-default form-control <?php if($active_id>=0){if ($active_id == $nurse->id) echo 'active';}else if($i==1) echo 'active'; ?>">
                                        <span style="width:152px;word-wrap: break-word;display:block;white-space: normal !important;text-align:center;">
                                            <?php echo trim($nurse->first_name);?><?php echo trim($nurse->last_name);?>
                                        </span>
                                    </a>

                                </li>
                                <?php } endforeach; ?>
                            </ul>
                        </div>

                        <div class="col-md-10 col-sm-10">
                            <h5 class="col-md-3">Horarios Disponibles</h5>


                            <div id="nurse-schedule-calendar" data-week-start="<?php echo date('Y-m-d',strtotime('monday this week')); ?>" data-week-end="<?php echo date('Y-m-d',strtotime('sunday this week')); ?>"
                                class="loading">
                                <table class="table table-bordered">
                                    <thead>
                                        <th>Horario</th>
                                        <th>Lunes</th>
                                        <th>Martes</th>
                                        <th>Miércoles</th>
                                        <th>Jueves</th>
                                        <th>Viernes</th>
                                        <th>Sábado</th>
                                        <th>Domingo</th>
                                    </thead>
                                    <tbody>
                                        <?php foreach($time_range as $time): 
                          $time_id = str_replace(':', '', $time);
                        ?>

                                        <tr id="<?php echo $time_id; ?>">
                                            <td>
                                                <?php echo $time; ?>
                                                <input type="hidden" id="<?php echo $nurse->id;?>">
                                            </td>
                                            <?php 
                              foreach($date_range as $d):  ?>
                                            <td id="<?php echo $d; ?>">
                                                <a href="#" class="btn btn-round btn-default" data-target="<?php echo $nurse->id;?>" data-time="<?php echo $d; ?>,<?php echo $time_id; ?>">
                                                    <i class="fa fa-plus"></i>
                                                </a>-
                                            </td>
                                            <?php endforeach; ?>
                                        </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>



                        <!--Modal-->



                        <?php else: ?>
                        <p class="alert alert-warning fade in">No se encontraron registros de luces.</p>
                        <?php endif; ?>




                        <div class="clear"></div>

                        <div class="clearfix"></div>
                        <br>
                        <br>

                        <div class="col-md-6 col-sm-6">
                            <h4>Control de Registros en Bitácora</h4>
                            <p>
                                Indica cuantas veces debe reportar la luz cada categoría por día de servicio
                            </p>
                            <br>
                            <br>

                            <div class="col-md-5 col-sm-5">
                                <div class="col-form-group">
                                    <label for="">Medicinas</label>
                                    <input type="text" name="RecordMedicines" class="form-control">
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-5 col-sm-5">
                                <div class="col-form-group">
                                    <label for="">Alimentos</label>
                                    <input type="text" name="RecordEats" class="form-control">
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-5 col-sm-5">
                                <div class="col-form-group">
                                    <label for="">Evacuaciones</label>
                                    <input type="text" name="RecordOuts" class="form-control">
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-5 col-sm-5">
                                <div class="col-form-group">
                                    <label for="">Micciones</label>
                                    <input type="text" name="RecordPoops" class="form-control">
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-5 col-sm-5">
                                <div class="col-form-group">
                                    <label for="">Estimulación Cognitiva</label>
                                    <input type="text" name="RecordEstimulateCognitive" class="form-control">
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-5 col-sm-5">
                                <div class="col-form-group">
                                    <label for="">Estimulación Física</label>
                                    <input type="text" name="RecordEstimulatePhisical" class="form-control">
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-5 col-sm-5">
                                <div class="col-form-group">
                                    <label for="">Otras Estimulaciones</label>
                                    <input type="text" name="RecordOthers" class="form-control">
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-5 col-sm-5">
                                <div class="col-form-group">
                                    <label for="">Síntomas</label>
                                    <input type="text" name="RecordSymptoms" class="form-control">
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                        </form>

                    </div>

                    <div class="clearfix"></div>
                    <br>
                    <br>

                    <div class="col-md-9 col-sm-9">
                    </div>
                    <div class="col-md-3 col-sm-3" style="text-align:right;">
                        <a href="<?php echo base_url(); ?>admin/services/save" id="createService" data-rel="1" title="Crear Servicio" class="btn btn-info createService">Guardar</a>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>