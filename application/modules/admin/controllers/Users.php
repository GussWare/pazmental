<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class Users extends Admin_Controller {

    function __construct() {
        parent::__construct();

        $group = 'admin';

          $this->load->model(array('module', 'service'));

          if (!$this->ion_auth->logged_in()) {
   
             $this->ion_auth->logout();
             redirect('auth', 'refresh');
        }
        if (!$this->ion_auth->in_group($group))
        {
            $this->session->set_flashdata('message', 'You must be an administrator to view the users page.');
            redirect('admin/dashboard');
        }
    }

    public function index() {    
        $users = $this->ion_auth->users()->result();

        $data['users'] = $users;
        $data['page'] = $this->config->item('ci_my_admin_template_dir_admin') . "users_list";
        $this->load->view($this->_container, $data);
    }

    public function create() {
        if ($this->input->post('username')) {
            $username = $this->input->post('username');
            $password = $this->input->post('password');
            $email = $this->input->post('email');
            $group_id = array( $this->input->post('group_id'));

            $additional_data = array(
                'first_name' => $this->input->post('first_name'),
                'last_name' => $this->input->post('last_name'),
                'username' => $this->input->post('username'),
                'phone' => $this->input->post('phone'),
            );

            $user = $this->ion_auth->register($email, $password, $email, $additional_data,$group_id);

            if(!$user)
            {
                $errors = $this->ion_auth->errors();
                echo $errors;
                die('done');
            }
            else
            {
                redirect('/admin/users', 'refresh');
            }


        }

        $data['groups'] = $this->ion_auth->groups()->result();
        $data['page'] = $this->config->item('ci_my_admin_template_dir_admin') . "users_create";
        $this->load->view($this->_container, $data);
    }

    public function edit($id) {
        if ($this->input->post('first_name')) {
            $data['first_name'] = $this->input->post('first_name');
            $data['last_name'] = $this->input->post('last_name');
            $data['email'] = $this->input->post('email');
            $data['phone'] = $this->input->post('phone');
            $group_id = $this->input->post('group_id');
            
            $this->ion_auth->remove_from_group('', $id);
            $this->ion_auth->add_to_group($group_id, $id);

            $group = $this->ion_auth->get_users_groups($id)->row();

            $this->ion_auth->update($id, $data);


           if($this->input->post("service_ids")!='' && is_array($this->input->post("service_ids"))){

                foreach($this->input->post("service_ids") as $service_relation){

                    $data = array(
                        'id' => '',
                        'service_id' => $service_relation,
                        'relation_term' => '_'.$group->name,
                        'relation_value'=> json_encode(array('id'=>$id))
                    );

                   
                    $this->db->insert('services_relationship', $data);

                }
            }
            
            redirect('/admin/users', 'refresh');
        }

        $this->load->helper('ui');
        $services = $this->service->get_all();
        $data['services'] = $services;
        $services_added = $this->service->get_all('*',array("relation_value"=>$id),'services_relationship');
        $services_relation = array();

        foreach($services_added as $service): 
           
            $service_data = $this->db->get('services',array('id'=>$service["service_id"]))->row();
            array_push($services_relation,$service_data);  

        endforeach; 

        $data["services_added"] = $services_relation;

        $data['groups'] = $this->ion_auth->groups()->result();
        $data['user'] = $this->ion_auth->user($id)->row();
        $data['user_group'] = $this->ion_auth->get_users_groups($id)->row();
        $data['page'] = $this->config->item('ci_my_admin_template_dir_admin') . "users_edit";
        $this->load->view($this->_container, $data);
    }

    public function delete($id) {
        $this->ion_auth->delete_user($id);

        redirect('/admin/users', 'refresh');
    }

    public function ajax() {
        if($this->input->is_ajax_request()){

            if($this->input->post('action') == 'add') {
                 $services = $this->service->get_all();
                 $data['services'] = $services;
                 $this->load->view($this->config->item('ci_my_admin_template_dir_admin') . "modals/_relations_add", $data);
            } else {



                $service = $this->service->get($this->input->post("services"));

                

                 $code = '<tr>
                        <td><input type="hidden" name="service_ids[]" value="' .$service->id.'" />' .$service->id.'</td>
                        <td>'.$service->name.'</td>
                       </tr>';
                                $results = array(
                                            'action'=>'append', 
                                            'element'=>'table-'.$this->input->post('module').' > tbody',
                                            'code'=>$code,
                                            'status'=>'true'
                                        );


                                echo json_encode($results);


            }
        } else {
            exit('Direct access not allowed');
        }
    }
}
