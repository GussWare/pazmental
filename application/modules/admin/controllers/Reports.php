<?php (defined('BASEPATH')) or exit('No direct script access allowed');
ini_set('display_errors', 1);

class Reports extends Admin_Controller
{
    public function __construct()
    {
        parent::__construct();

        if (!$this->ion_auth->logged_in()) {
            $this->load->library('session');
            $this->session->set_userdata('referred_from', current_url());
            $_SESSION['referred_from'] = current_url();
            log_message('debug', 'referred reports ' . sys_get_temp_dir() . " " . $this->session->userdata('referred_from'));
            $this->ion_auth->logout();

            redirect('auth', 'refresh');
        }

        $this->load->model('module');
        $this->load->model(array('admin/paciente'));
        $this->load->model(array('admin/service'));
        $this->load->model(array('admin/report'));
        $this->load->model(array('admin/options'));
        $this->load->library('report_maker');
        $this->load->library('phpmailerlib');
        $this->load->helper("pacientes");

    }

    public function index()
    {
        $user_id = $this->ion_auth->get_user_id();
        $group_name = $this->ion_auth->get_users_groups()->row()->name;

        $params = array(
            'group_name' => $group_name,
            'user_id' => $user_id,
        );

        $pacientes_reports = $this->report->listado($params);
        $data['pacientes_reports'] = $pacientes_reports;

        $this->db->select('*');
        $this->db->from('options');
        $this->db->where('option_name', '_serviceType');
        $query_service_type = $this->db->get();
        $arr_service_type = array();

        if ($query_service_type->num_rows() > 0) {
            $arr_service_type = $query_service_type->result();
        }

        $data['service_type'] = $arr_service_type;
        $data['user_id'] = $user_id;
        $data['group_name'] = $group_name;

        $editable = false;
        if ($group_name == 'admin' or $group_name == "supervisor" or $group_name == "doctors" or $group_name == "familiares") {
            $editable = true;
        }
        $data['editable'] = $editable;

        $editable_mensual = false;
        if ($group_name == 'admin' or $group_name == "supervisor" or $group_name == "doctors") {
            $editable_mensual = true;
        }
        $data['editable_mensual'] = $editable_mensual;

        if ($this->input->is_ajax_request()) {
            /** para cuando se usa los filtros */
            $data['page'] = $this->config->item('ci_my_admin_template_dir_admin') . "reports_list";
            $report_table_listado = $this->load->view($this->_container, $data, true);
            echo $report_table_listado;

        } else {
            $this->remove_cache();

            /** primera vez que se entra a la pantalla */
            $data['page'] = $this->config->item('ci_my_admin_template_dir_admin') . "reports_list";
            $this->load->view($this->_container, $data);
        }
    }

    public function edit($paciente_id)
    {
        $data = array();
        $refresh = 0;

        $data['user_id'] = $this->ion_auth->get_user_id();
        $data['group_name'] = $this->ion_auth->get_users_groups()->row()->name;

        $start_day = $this->input->post('start_date');
        $end_day = $this->input->post('end_date');

        if (!isset($start_day)) {
            $f = new DateTime("30 days ago");
            $t = new DateTime("today");
            $refresh = 0;
        } else {
            $f = new DateTime($start_day);
            $t = new DateTime($end_day);
            $refresh = 1;
        }

        $start_day = $f->format("Y-m-d");
        $end_day = $t->format("Y-m-d");

        $data['from'] = $start_day;
        $data['to'] = $end_day;

        $data['from_day'] = $start_day;
        $data['to_day'] = $end_day;

        $data['dias'] = $dias = array("Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado");
        $data['reports_type'] = array('sintoma' => 'Síntomas', 'medicamento' => 'Medicinas', 'estimulacion_cognitiva' => 'Estímulo Cognitivo', 'estimulacion_fisica' => 'Estímulo Físico', 'estimulacion_otra' => 'Otros Estimulos', 'evacuacion' => 'Evacuación Micción', 'alimento' => 'Alimentos', 'signos_vitales' => 'Signos Vitales', 'observacion_general' => 'Resumen');

        $paciente = $this->paciente->get($paciente_id);

        $data['paciente'] = $paciente;
        $name = json_decode($paciente->name);
        $data['full_name'] = $name->first_name . " " . $name->second_name . " " . $name->last_name . " " . $name->second_last_name;
        $data['first_name'] = $name->first_name . " " . $name->second_name;
        $data['last_name'] = $name->last_name . " " . $name->second_last_name;

        $data['report_data'] = array(
            'resumen' => []
        );

        $result = $this->report->get_report_mensual(array(
            'paciente_id' => $paciente_id,
            'start_day' => $start_day,
            'end_day' => $end_day,
        ));

        $data['evacuacion'] = array('08:00 - 12:00', '12:00 - 16:00', '16:00 - 20:00', '20:00 - 00:00', '00:00 - 04:00', '04:00 - 08:00');
        $data['alimentacion'] = array('Desayuno', 'Comida', 'Cena', 'Liquidos', 'Colacion');
        $data['signos_vitales'] = array('temperatura', 'presion_arterial_sistolica', 'presion_arterial_diastolica', 'frecuencia_cardiaca', 'oxigeno', 'glucosa');
        $pie_chart_range = ['08:00 -14:00', '14:00 - 20:00', '20:00 - 24:00', '00:00 - 08:00'];

        $services = array();
        $sintomas = array();
        $report_sintoma = array();
        $report_hora_sintoma = array();
        $report_est_cognitiva = array();
        $report_line_cognitiva = array();
        $report_est_fisica = array();
        $report_est_otra = array();
        $report_evac_micc = array();
        $report_alimentos = array();
        $report_signo = array();
        $data['report_data'] = array();
        $medicamentos = array();

        foreach ($result as $r) {

            if (!array_key_exists($r->tars_id, $services)) {
                $services[$r->tars_id] = array(
                    'tars_id' => $r->tars_id,
                    'service_id' => $r->servicio_id,
                    'service_meta' => []
                );
            }

            $d = new DateTime($r->timestamp);
            $date = $d->format('d-m-Y');

            $report_meta = unserialize($r->report_meta);

            if ($r->report_type == "sintoma" || $r->report_type == "inicio" || $r->report_type == "fin") {

                if ($r->report_type == "inicio") {
                    $s = $report_meta['estado_inicial_del_paciente'];
                } else if ($r->report_type == "fin") {
                    $s = $report_meta['estado_final_del_paciente'];
                } else {
                    $s = $report_meta['reporte_de_sintomas'];
                }

                if (!array_key_exists('sintoma', $data['report_data'])) {
                    $data['report_data']['sintoma'] = array();
                }

                if (!array_key_exists($s, $data['report_data']['sintoma'])) {
                    //inicializar array de sintomas
                    $sintomas[] = $s;

                    //Para hacer la gráfica de barras
                    $report_sintoma[$s] = 0;

                    //Para hacer los pie charts
                    $report_hora_sintoma[$s] = array();

                    //Para incluir todos los datos de sintomas a los reportes
                    $data['report_data']['sintoma'][$s] = array();

                }

                $range = $this->get_range($pie_chart_range, $this_time = $d->format("H:i"));

                if (array_key_exists($range, $report_hora_sintoma[$s])) {
                    $report_hora_sintoma[$s][$range] += 1;
                } else {
                    $report_hora_sintoma[$s][$range] = 1;
                }

                $report_sintoma[$s] += 1;

                if (!array_key_exists($date, $data['report_data']['sintoma'][$s])) {
                    $data['report_data']['sintoma'][$s][$date] = array();
                }

                $data['report_data']['sintoma'][$s][$date][] = $r;

            } else if ($r->report_type == "medicamento") {
                for ($i = 1; $i <= 3; $i++) {
                    $clean_name = trim(strtolower($this->stripAccents($report_meta['que_medicina_tomo_' . $i])));
                    if (strlen($clean_name != "")) {
                        $tuple = $d->format('H') . " hrs.";

                        if (!array_key_exists($clean_name, $medicamentos)) {
                            $medicamentos[$clean_name] = 1;
                        } else {
                            $medicamentos[$clean_name] += 1;
                        }

                        $clean_name .= " - " . $report_meta['que_dosis_' . $i] . "";

                        if (!array_key_exists($r->report_type, $data['report_data'])) {
                            $data['report_data'][$r->report_type] = array();
                        }

                        if (!array_key_exists($clean_name, $data['report_data'][$r->report_type])) {
                            $data['report_data'][$r->report_type][$clean_name] = array();
                        }

                        if (!array_key_exists($date, $data['report_data'][$r->report_type][$clean_name])) {
                            $data['report_data'][$r->report_type][$clean_name][$date]['tuple'] = array($tuple);
                            $data['report_data'][$r->report_type][$clean_name][$date]['raw_data'] = array($r);
                        } else {
                            $data['report_data'][$r->report_type][$clean_name][$date]['tuple'][] = $tuple;
                            $data['report_data'][$r->report_type][$clean_name][$date]['raw_data'][] = $r;
                        }
                    }
                }
            } else if ($r->report_type == "estimulacion_cognitiva") {
                $data['report_data'][$r->report_type][$report_meta['estimulacion_cognitiva']][$date][] = $r;
                $grade = $this->get_grade($report_meta['como_le_fue']);

                if (!array_key_exists($date, $report_line_cognitiva)) {
                    $report_line_cognitiva[$date][$report_meta['estimulacion_cognitiva']]['times'] = 1;
                    $report_line_cognitiva[$date][$report_meta['estimulacion_cognitiva']]['grade'] = $grade;
                } else {
                    if (!array_key_exists($report_meta['estimulacion_cognitiva'], $report_line_cognitiva[$date])) {
                        $report_line_cognitiva[$date][$report_meta['estimulacion_cognitiva']]['times'] = 1;
                        $report_line_cognitiva[$date][$report_meta['estimulacion_cognitiva']]['grade'] = $grade;
                    } else {
                        $report_line_cognitiva[$date][$report_meta['estimulacion_cognitiva']]['times'] = 1;
                        $report_line_cognitiva[$date][$report_meta['estimulacion_cognitiva']]['grade'] = $grade;
                    }
                }

                if (!array_key_exists($report_meta['estimulacion_cognitiva'], $report_est_cognitiva)) {
                    $report_est_cognitiva[$report_meta['estimulacion_cognitiva']]['times'] = 1;
                    $report_est_cognitiva[$report_meta['estimulacion_cognitiva']]['grade'] = $grade;
                } else {
                    $report_est_cognitiva[$report_meta['estimulacion_cognitiva']]['times'] += 1;
                    $report_est_cognitiva[$report_meta['estimulacion_cognitiva']]['grade'] += $grade;
                }
            } else if ($r->report_type == "estimulacion_fisica") {
                $data['report_data'][$r->report_type][$report_meta['estimulacion_fisica']][$date][] = $r;
                if (!array_key_exists($report_meta['estimulacion_fisica'], $report_est_fisica)) {
                    $report_est_fisica[$report_meta['estimulacion_fisica']] = 1;

                } else {
                    $report_est_fisica[$report_meta['estimulacion_fisica']] += 1;
                }
            } else if ($r->report_type == "estimulacion_otra") {
                if (!array_key_exists($report_meta['otro_tipo_de_terapia'], $report_est_otra)) {
                    $report_est_otra[$report_meta['otro_tipo_de_terapia']] = 1;
                } else {
                    $report_est_otra[$report_meta['otro_tipo_de_terapia']] += 1;
                }
                $data['report_data'][$r->report_type][$report_meta['otro_tipo_de_terapia']][$date][] = $r;
            } else if ($r->report_type == "evacuacion") {
                foreach ($data['evacuacion'] as $time) {

                    $times = explode("-", $time);
                    $this_time = $d->format("H:i");
                    if ((strtotime($times[0]) < strtotime($this_time)) && (strtotime($times[1]) >= strtotime($this_time))) {
                        $data['report_data'][$r->report_type][$time][$date][] = $r;

                        if (!array_key_exists($date, $report_evac_micc)) {
                            $report_evac_micc[$date][$report_meta['evacuacion_o_miccion']] = 1;
                        } else {
                            if (!array_key_exists($report_meta['evacuacion_o_miccion'], $report_evac_micc[$date])) {
                                $report_evac_micc[$date][$report_meta['evacuacion_o_miccion']] = 1;
                            } else {
                                $report_evac_micc[$date][$report_meta['evacuacion_o_miccion']] += 1;
                            }
                        }

                    }
                }
            } else if ($r->report_type == "alimento") {
                $data['report_data'][$r->report_type][$report_meta['reportar_alimentos']][$date][] = $r;
                $report_alimentos[$date][$report_meta['reportar_alimentos']] = $this->get_grade($report_meta['como_comio']);
            } else if ($r->report_type == "signos_vitales") {
                foreach ($report_meta as $key => $value) {
                    if ($value != "") {

                        if (!array_key_exists($r->report_type, $data['report_data'])) {
                            $data['report_data'][$r->report_type] = array();
                        }

                        if (!array_key_exists($key, $data['report_data'][$r->report_type])) {
                            $data['report_data'][$r->report_type][$key] = array();
                        }
                        if (!array_key_exists($date, $data['report_data'][$r->report_type][$key])) {
                            $data['report_data'][$r->report_type][$key][$date] = array();
                        }
                        $data['report_data'][$r->report_type][$key][$date][] = $r;

                        $ts = new DateTime($r->timestamp);
                        $timestamp = $ts->format("Y-m-d h:i");
                        if ($key == "presion_arterial_sistolica" || $key == "presion_arterial_diastolica") {

                            $report_signo['presion_arterial'][$timestamp][$key] = $value;
                        } else {
                            $report_signo[$key][$timestamp] = $value;
                        }
                    }
                }
            } else if ($r->report_type == "observacion_general") {
                foreach ($report_meta as $key => $value) {
                    if ($value != "") {
                        $data['report_data'][$r->report_type][$key][$date] = $r;
                        $observaciones_generales[] = $value;

                    }
                }
            }
        }

        $data['sintomas'] = $sintomas;
        $data['report_sintoma'] = $report_sintoma;
        $data['medicamentos'] = $medicamentos;
        $data['report_est_cognitiva'] = $report_est_cognitiva;
        $data['report_line_cognitiva'] = $report_line_cognitiva;
        $data['report_est_fisica'] = $report_est_fisica;
        $data['report_est_otra'] = $report_est_otra;
        $data['report_evac_micc'] = $report_evac_micc;
        $data['report_alimentos'] = $report_alimentos;
        $data['report_signo'] = $report_signo;
        $data['report_hora_sintoma'] = $report_hora_sintoma;

        // recuperamos los datos meta services
        foreach ($services as &$service) {

            $this->db->select("services.tars_id");
            $this->db->select("service_meta,service_value");
            $this->db->from("services");
            $this->db->join("services_meta", "services.id = services_meta.service_id", "inner");
            $this->db->where("services.id", $service['service_id']);
            $query = $this->db->get();

            log_message("debug", "---- query servicesss_meta ");
            log_message("debug", $this->db->last_query());
            log_message("debug", "---- query servicesss_meta ");

            $data_service_meta = array();
            if ($query->num_rows() > 0) {

                foreach ($query->result_array() as $result_meta) {
                    $data_service_meta[$result_meta['service_meta']] = $result_meta['service_value'];
                }

                $service['service_meta'] = $data_service_meta;
            }
        }

        // creamos los datos de la pestaña de resumen
        $report_luces = array();
        $report_supervisores = array();
        $report_medico = array();
        $report_objetivos_mes = array();
        $report_objetivos_prox_mes = array();
        $report_analisis_resultados = array();
        $report_principales_logros = array();
        $report_principales_oportunidades = array();
        $report_informe_medico = array();
        $report_informe_rehabilitacion = array();
        $report_informe_neuropsicologico = array();
        $report_notas_internas = array();
        $report_incuidos_arr = array();
        $report_comentarios_mes = array();

        foreach ($services as $key => $value) {

            // nombre de la luz
            if (isset($value['service_meta']['_report_luces'])) {
                $report_luces[] = $value['service_meta']['_report_luces'];
            } else {

                $data_luz = $this->report_maker->get_real_nurses($value['tars_id'], $start_day, $end_day);
                if(count($data_luz) > 0) {
                    $report_luces[] = $data_luz[0]->first_name . ' ' . $data_luz[0]->last_name;
                } else {
                    $report_luces[] = '';
                }
            }

            // nombre de supervisor
            if (isset($value['service_meta']['_report_supervisor'])) {
                $report_supervisores[] = $value['service_meta']['_report_supervisor'];
            } else {
                $data_supervisores = $this->report_maker->get_supervisor($value['service_id']);
                if(count($data_supervisores) > 0) {
                    $report_supervisores[] = $data_supervisores[0]->first_name . ' ' . $data_supervisores[0]->last_name;
                } else {
                    $report_supervisores[] = '';
                }
            }

            // nombre medico
            if (isset($value['service_meta']['_report_medico']) && $value['service_meta']['_report_medico'] != "") {
                $report_medico[] = $value['service_meta']['_report_medico'];
            } else {
                $medico = $this->report_maker->get_medico($value['service_id']);
                if (sizeof($medico) > 0) {
                    $report_medico[] = $medico[0]->first_name . " " . $medico[0]->last_name;
                } else {
                    $report_medico[] = "";
                }
            }

            // objetivos del mes
            if (isset($value['service_meta']['_report_objetivos_mes']) && $value['service_meta']['_report_objetivos_mes'] != '') {
                $report_objetivos_mes[] = trim($value['service_meta']['_report_objetivos_mes']);
            } else {
                $obj_servicio = $this->service->get($value['service_id']);
                if (is_object($obj_servicio)) {
                    $report_objetivos_mes[] = trim($obj_servicio->goals);
                }
            }

            // objetivos del proximo mes
            if (isset($value['service_meta']['_report_objetivos_prox_mes']) && $value['service_meta']['_report_objetivos_prox_mes'] != '') {
                $report_objetivos_prox_mes[] = trim($value['service_meta']['_report_objetivos_prox_mes']);
            }

            // comentarios del mes
            if (isset($value['service_meta']['_report_comentarios_mes']) && $value['service_meta']['_report_comentarios_mes'] != '') {
                if(is_array($value['service_meta']['_report_comentarios_mes'])) {
                    $report_comentarios_mes[] = implode(",", $value['service_meta']['_report_comentarios_mes']);
                } else {
                    $report_comentarios_mes[] = $value['service_meta']['_report_comentarios_mes'];
                }
            } else {
                // este valor se recupera de el otro foreach que todavia no esta
                $report_comentarios_mes[] = implode("\n", $observaciones_generales);;
            }

            // analisis de resultados
            if (isset($value['service_meta']['_report_analisis_resultados']) && $value['service_meta']['_report_analisis_resultados'] != '') {
                $report_analisis_resultados[] = trim($value['service_meta']['_report_analisis_resultados']);
            }

            // principales logros
            if (isset($value['service_meta']['_report_principales_logros']) && $value['service_meta']['_report_principales_logros'] != "") {
                $report_principales_logros[] = $value['service_meta']['_report_principales_logros'];
            }

            // principales oportunidades
            if (isset($value['service_meta']['_report_principales_oportunidades']) && $value['service_meta']['_report_principales_oportunidades'] != "") {
                $report_principales_oportunidades[] = $value['service_meta']['_report_principales_oportunidades'];
            }

            // informe medico
            if (isset($value['service_meta']['_report_informe_medico']) && $value['service_meta']['_report_informe_medico'] != "") {
                $report_informe_medico[] = $value['service_meta']['_report_informe_medico'];
            }

            // informe rahabilitacion
            if (isset($value['service_meta']['_report_informe_rehabilitacion']) && $value['service_meta']['_report_informe_rehabilitacion'] != "") {
                $report_informe_rehabilitacion[] = $value['service_meta']['_report_informe_rehabilitacion'];
            }

            // informe neuropsicologico
            if (isset($value['service_meta']['_report_informe_neuropsicologico']) && $value['service_meta']['_report_informe_neuropsicologico'] != "") {
                $report_informe_neuropsicologico[] = $value['service_meta']['_report_informe_neuropsicologico'];
            }

            // notas internas
            if (isset($value['service_meta']['_report_notas_internas']) && $value['service_meta']['_report_notas_internas'] != "") {
                $report_notas_internas[] = $value['service_meta']['_report_notas_internas'];
            }
        }

        $data['report_luces'] = array_filter($report_luces, "strlen");
        $data['report_supervisores'] = array_filter($report_supervisores, "strlen");
        $data['report_medico'] = array_filter($report_medico, "strlen");
        $data['report_objetivos_mes'] = array_filter($report_objetivos_mes, "strlen");
        $data['report_objetivos_prox_mes'] = array_filter($report_objetivos_prox_mes, "strlen");
        $data['report_comentarios_mes'] = array_filter($report_comentarios_mes, "strlen");
        $data['report_analisis_resultados'] = array_filter($report_analisis_resultados, "strlen");
        $data['report_principales_logros'] = array_filter($report_principales_logros, "strlen");
        $data['report_informe_medico'] = array_filter($report_informe_medico, "strlen");
        $data['report_principales_oportunidades'] = array_filter($report_principales_oportunidades, "strlen");
        $data['report_informe_rehabilitacion'] = array_filter($report_informe_rehabilitacion, "strlen");
        $data['report_informe_neuropsicologico'] = array_filter($report_informe_neuropsicologico, "strlen");
        $data['report_notas_internas'] = array_filter($report_notas_internas, "strlen");
        

        $data['report_signo'] = array();

        $data['word_cloud'] = $this->report_maker->get_word_cloud($report_sintoma);
        $data['pacientes_meta'] = crear_array_pacientes_meta($this->paciente->get_data_pacientes_meta($paciente_id));

        if (isset($data['pacientes_meta']['_report_reportes_incluidos']) && $data['pacientes_meta']['_report_reportes_incluidos'] != "") {
            $explode_reports_incluidos = explode(",", $data['pacientes_meta']['_report_reportes_incluidos']);
            foreach ($explode_reports_incluidos as $report_inculido) {
                if (!in_array($report_inculido, $report_incuidos_arr)) {
                    $report_incuidos_arr[] = $report_inculido;
                }
            }
        }
        $data['reportes_incluidos_arr'] = array_filter($report_incuidos_arr, "strlen");

        if ($refresh == 0) {

            /*FIRST TIME */
            $data['page'] = $this->config->item('ci_my_admin_template_dir_admin') . "report_monthly";
            $this->load->view($this->_container, $data);

        } else {

            /*USING START - END FILTERS */
            $data['page'] = $this->config->item('ci_my_admin_template_dir_admin') . "report_monthly";
            $this->load->view($this->_container, $data);

            return json_encode($data);
        }
    }

    public function get_range($pie_chart_range, $this_time)
    {
        $result = "";
        foreach ($pie_chart_range as $time) {
            $times = explode("-", $time);
            if ((strtotime($times[0]) < strtotime($this_time)) && (strtotime($times[1]) >= strtotime($this_time))) {
                $result = $time;
            }
        }
        return $result;
    }

    public function get_grade($grade)
    {
        if ($grade == "Bien") {$grade = 100;} else if ($grade == "Regular") {$grade = 75;} else if ($grade == "Mal") {$grade = 50;}
        return $grade;
    }

    public function edit_calificacion($user_id)
    {
        if ($this->input->is_ajax_request()) {

            if ($this->input->post('action') == 'save') {
                $data = $this->input->post();

                $service_id = $this->input->post('service_id');
                $user_id = $this->input->post('user_id');
                $meta = array();
                $meta['_Rating_Luz'] = $this->input->post('calificacion-luz');
                $meta['_Rating_PM'] = $this->input->post('calificacion-paz-mental');
                $meta['_Rating_Insatisfaction'] = $this->input->post('razon-insatisfaccion');
                $meta['_Rating_Comments'] = $this->input->post('comentarios');

                $s = array();
                $query = $this->db->query("DELETE from  users_meta where meta_key   LIKE '_Rating%' and user_id='$user_id'");

                foreach ($meta as $key => $value) {

                    $query = $this->db->query("INSERT into users_meta(user_id,meta_key,meta_value) VALUES
                                ('$user_id','$key','$value')");

                    $s[] = $this->db->affected_rows();
                }

                echo json_encode($s);

            } else {
                $data['service_id'] = $this->input->post('service_id');
                $data['user_id'] = $this->input->post('user_id');

                $this->load->view($this->config->item('ci_my_admin_template_dir_admin') . "modals/_edit_calificacion", $data);

            }

        } else {

            exit('No direct access allowed');
        }
    }

    public function pdf($paciente_id)
    {

        $authorized = array('admin', 'supervisor');
        if (!in_array($this->ion_auth->get_users_groups()->row()->name, $authorized)) {
            redirect(site_url('admin/reports'), 'refresh');
        }
        if ($this->input->is_ajax_request()) {

            $action = $this->input->post('action');

            if ($action == "create") {

                $start_day = $this->input->post('start_date');
                $end_day = $this->input->post('end_date');

                if (!isset($start_day)) {
                    $f = new DateTime("first day of this month");
                    $t = new DateTime("last day of this month");
                    $refresh = 0;
                } else {
                    $f = new DateTime($start_day);
                    $t = new DateTime($end_day);

                }

                $t = $t->modify("+1 day");
                $data_array = array();
                $data_array['start_day'] = $start_day;
                $data_array['end_day'] = $end_day;

                $data_array['user_id'] = $this->ion_auth->get_user_id();
                $data_array['group_name'] = $this->ion_auth->get_users_groups()->row()->name;
                $data_array['paciente_id'] = $paciente_id;
                $refresh = 0;

                $start_day = $f->format("Y-m-d");
                $end_day = $t->format("Y-m-d");

                $data_array['from'] = $start_day;
                $data_array['to'] = $end_day;

                $data_array['reports_type'] = array('sintoma' => 'Síntomas', 'medicamento' => 'Medicinas', 'estimulacion_cognitiva' => 'Estímulo Cognitivo', 'estimulacion_fisica' => 'Estímulo Físico', 'estimulacion_otra' => 'Otros Estimulos', 'evacuacion' => 'Evacuación Micción', 'alimento' => 'Alimentos', 'signos_vitales' => 'Signos Vitales', 'observacion_general' => 'Resumen');

                $paciente = $this->paciente->get($paciente_id);
                $data['paciente'] = $paciente;

                $name = json_decode($paciente->name);
                $data_array['full_name'] = $name->first_name . " " . $name->second_name . " " . $name->last_name . " " . $name->second_last_name;
                $data_array['first_name'] = $name->first_name . " " . $name->second_name;
                $data_array['last_name'] = $name->last_name . " " . $name->second_last_name;

                //////

                $result = $this->report->get_report_mensual(array(
                    'paciente_id' => $paciente_id,
                    'start_day' => $start_day,
                    'end_day' => $end_day,
                ));
                $data_array['result'] = $result;
                $data_array['query'] = $this->db->last_query();
                /* Hacer grid de resultados basado en tipo de reporte y fecha */

                $data_array['report_data'] = array();

                $sintomas = array();
                $medicamentos = array();
                $report_est_cognitiva = array();
                $report_line_cognitiva = array();
                $report_est_fisica = array();
                $report_est_otra = array();
                $report_evac_micc = array();
                $report_alimentos = array();
                $report_signo = array();
                $observaciones_generales = array();
                $data_array['evacuacion'] = array('08:00 - 12:00', '12:00 - 16:00', '16:00 - 20:00', '20:00 - 00:00', '00:00 - 04:00', '04:00 - 08:00');
                $data_array['alimentacion'] = array('Desayuno', 'Comida', 'Cena', 'Liquidos', 'Colaciones');
                $data_array['signos_vitales'] = array('temperatura', 'presion_arterial_sistolica', 'presion_arterial_diastolica', 'frecuencia_cardiaca', 'oxigeno', 'glucosa');
                $report_hora_sintoma = array();
                $pie_chart_range = ['08:00 -14:00', '14:00 - 20:00', '20:00 - 24:00', '00:00 - 08:00'];

                $days = array();
                $report_sintoma = array();
                $services = array();
                foreach ($result as $r) {

                    if (!array_key_exists($r->tars_id, $services)) {
                        $services[$r->tars_id] = array(
                            'tars_id' => $r->tars_id,
                            'service_id' => $r->servicio_id,
                            'service_meta' => []
                        );
                    }

                    $d = new DateTime($r->timestamp);
                    $date = $d->format('d-m-Y');

                    if (!array_key_exists($d->format("d"), $days)) {
                        $days[$d->format("d")] = array('date' => $d, 'day' => $d->format('d'));
                    }

                    if (!array_key_exists($r->report_type, $data_array['report_data'])) {
                        $data_array['report_data'][$r->report_type] = array();
                    }

                    $report_meta = unserialize($r->report_meta);
                    $data_array['report_meta'][] = $report_meta;

                    if ($r->report_type == "sintoma" || $r->report_type == "inicio" || $r->report_type == "fin") {
                        if ($r->report_type == "inicio") {
                            $s = $report_meta['estado_inicial_del_paciente'];
                        } else if ($r->report_type == "fin") {
                            $s = $report_meta['estado_final_del_paciente'];
                        } else {
                            $s = $report_meta['reporte_de_sintomas'];
                        }

                        if (!array_key_exists('sintoma', $data_array['report_data'])) {
                            $data_array['report_data']['sintoma'] = array();}

                        if (!array_key_exists($s, $data_array['report_data']['sintoma'])) {
                            //inicializar array de sintomas
                            $sintomas[] = $s;

                            //Para hacer la gráfica de barras
                            $report_sintoma[$s] = 0;

                            //Para hacer los pie charts
                            $report_hora_sintoma[$s] = array();

                            //Para incluir todos los datos de sintomas a los reportes
                            $data_array['report_data']['sintoma'][$s] = array();

                        }

                        $range = $this->get_range($pie_chart_range, $this_time = $d->format("H:i"));

                        if (array_key_exists($range, $report_hora_sintoma[$s])) {
                            $report_hora_sintoma[$s][$range] += 1;
                        } else {
                            $report_hora_sintoma[$s][$range] = 1;
                        }

                        $report_sintoma[$s] += 1;

                        if (!array_key_exists($date, $data_array['report_data']['sintoma'][$s])) {
                            $data_array['report_data']['sintoma'][$s][$date] = array();
                        }
                        $data_array['report_data']['sintoma'][$s][$date][] = $r;

                    } else if ($r->report_type == "medicamento") {
                        for ($i = 1; $i <= 3; $i++) {
                            $clean_name = trim(strtolower($this->stripAccents($report_meta['que_medicina_tomo_' . $i])));
                            if (strlen($clean_name != "")) {
                                $tuple = $d->format('H');

                                if (!array_key_exists($clean_name, $medicamentos)) {
                                    $medicamentos[$clean_name] = 1;
                                } else {
                                    $medicamentos[$clean_name] += 1;
                                }

                                $clean_name .= " - " . $report_meta['que_dosis_' . $i] . "";

                                if (!array_key_exists($clean_name, $data_array['report_data'][$r->report_type])) {
                                    $data_array['report_data'][$r->report_type][$clean_name] = array();
                                }

                                if (!array_key_exists($date, $data_array['report_data'][$r->report_type][$clean_name])) {
                                    $data_array['report_data'][$r->report_type][$clean_name][$date]['tuple'] = array($tuple);
                                    $data_array['report_data'][$r->report_type][$clean_name][$date]['raw_data'] = array($r);
                                } else {
                                    $data_array['report_data'][$r->report_type][$clean_name][$date]['tuple'][] = $tuple;
                                    $data_array['report_data'][$r->report_type][$clean_name][$date]['raw_data'][] = $r;
                                }
                            }
                        }

                    } else if ($r->report_type == "estimulacion_cognitiva") {

                        $data_array['report_data'][$r->report_type][$report_meta['estimulacion_cognitiva']][$date][] = $r;
                        $grade = $this->get_grade($report_meta['como_le_fue']);

                        if (!array_key_exists($date, $report_line_cognitiva)) {
                            $report_line_cognitiva[$date][$report_meta['estimulacion_cognitiva']]['times'] = 1;
                            $report_line_cognitiva[$date][$report_meta['estimulacion_cognitiva']]['grade'] = $grade;
                        } else {
                            if (!array_key_exists($report_meta['estimulacion_cognitiva'], $report_line_cognitiva[$date])) {
                                $report_line_cognitiva[$date][$report_meta['estimulacion_cognitiva']]['times'] = 1;
                                $report_line_cognitiva[$date][$report_meta['estimulacion_cognitiva']]['grade'] = $grade;
                            } else {
                                $report_line_cognitiva[$date][$report_meta['estimulacion_cognitiva']]['times'] = 1;
                                $report_line_cognitiva[$date][$report_meta['estimulacion_cognitiva']]['grade'] = $grade;
                            }
                        }
                        if (!array_key_exists($report_meta['estimulacion_cognitiva'], $report_est_cognitiva)) {
                            $report_est_cognitiva[$report_meta['estimulacion_cognitiva']]['times'] = 1;
                            $report_est_cognitiva[$report_meta['estimulacion_cognitiva']]['grade'] = $grade;
                        } else {
                            $report_est_cognitiva[$report_meta['estimulacion_cognitiva']]['times'] += 1;
                            $report_est_cognitiva[$report_meta['estimulacion_cognitiva']]['grade'] += $grade;
                        }

                    } else if ($r->report_type == "estimulacion_fisica") {
                        $data_array['report_data'][$r->report_type][$report_meta['estimulacion_fisica']][$date][] = $r;
                        if (!array_key_exists($report_meta['estimulacion_fisica'], $report_est_fisica)) {
                            $report_est_fisica[$report_meta['estimulacion_fisica']] = 1;
                        } else {
                            $report_est_fisica[$report_meta['estimulacion_fisica']] += 1;
                        }

                    } else if ($r->report_type == "estimulacion_otra") {
                        if (!array_key_exists($report_meta['otro_tipo_de_terapia'], $report_est_otra)) {
                            $report_est_otra[$report_meta['otro_tipo_de_terapia']] = 1;
                        } else {
                            $report_est_otra[$report_meta['otro_tipo_de_terapia']] += 1;
                        }
                        $data_array['report_data'][$r->report_type][$report_meta['otro_tipo_de_terapia']][$date][] = $r;

                    } else if ($r->report_type == "evacuacion") {
                        foreach ($data_array['evacuacion'] as $time) {
                            $times = explode("-", $time);
                            $this_time = $d->format("H:i");
                            if ((strtotime($times[0]) < strtotime($this_time)) && (strtotime($times[1]) >= strtotime($this_time))) {
                                $data_array['report_data'][$r->report_type][$time][$date][] = $r;
                                if (!array_key_exists($date, $report_evac_micc)) {
                                    $report_evac_micc[$date][$report_meta['evacuacion_o_miccion']] = 1;
                                } else {
                                    if (!array_key_exists($report_meta['evacuacion_o_miccion'], $report_evac_micc[$date])) {
                                        $report_evac_micc[$date][$report_meta['evacuacion_o_miccion']] = 1;
                                    } else {
                                        $report_evac_micc[$date][$report_meta['evacuacion_o_miccion']] += 1;
                                    }
                                }
                            }
                        }

                    } else if ($r->report_type == "alimento") {
                        foreach ($data_array['alimentacion'] as $alimentacion) {
                            $data_array['report_data'][$r->report_type][$report_meta['reportar_alimentos']][$date][] = $r;
                            $report_alimentos[$date][$report_meta['reportar_alimentos']] = $this->get_grade($report_meta['como_comio']);
                        }

                    } else if ($r->report_type == "signos_vitales") {
                        foreach ($report_meta as $key => $value) {
                            if ($value != "") {
                                $data_array['report_data'][$r->report_type][$key][$date][] = $r;
                                $report_signo[$key][$date] = $value;
                            }
                        }

                    } else if ($r->report_type == "observacion_general") {
                        foreach ($report_meta as $key => $value) {
                            if ($value != "") {
                                $data_array['report_data'][$r->report_type][$key][$date][] = $r;
                                $observaciones_generales[] = $value;
                            }
                        }
                    }
                }

                $pacientes_meta = crear_array_pacientes_meta($this->paciente->get_data_pacientes_meta($paciente_id));
                $data_array['pacientes_meta'] = $pacientes_meta;

                $days = $this->date_sort($days, "day");
                $data_array['sintoma_table'] = $this->get_sintoma_table($data_array['report_data'], $sintomas, $start_day, $end_day, $days);
                $data_array['estimulacion_cognitiva_table'] = $this->get_estimulacion_cognitiva_table($data_array['report_data'], $start_day, $end_day, $days);
                $data_array['estimulacion_otra_table'] = $this->get_estimulacion_otra_table($data_array['report_data'], $start_day, $end_day, $days);
                $data_array['signos_vitales_table'] = $this->get_signos_vitales_table($data_array['report_data'], $start_day, $end_day, $days);
                $data_array['alimento_table'] = $this->get_alimentacion_table($data_array['report_data'], $start_day, $end_day, $days);
                $data_array['evacuacion_table'] = $this->get_evacuacion_table($data_array['report_data'], $start_day, $end_day, $days);
                $data_array['medicamento_table'] = $this->get_medicamento_table($data_array['report_data'], $start_day, $end_day, $days);
                $data_array['estimulacion_fisica_table'] = $this->get_estimulacion_fisica_table($data_array['report_data'], $start_day, $end_day, $days);

                $data_array['sintomas'] = $sintomas;
                $data_array['medicamentos'] = $medicamentos;
                $data_array['report_sintoma'] = $report_sintoma;
                $data_array['report_hora_sintoma'] = $report_hora_sintoma;
                $data_array['report_est_cognitiva'] = $report_est_cognitiva;
                $data_array['report_line_cognitiva'] = $report_line_cognitiva;
                $data_array['report_est_fisica'] = $report_est_fisica;
                $data_array['report_est_otra'] = $report_est_otra;
                $data_array['report_evac_micc'] = $report_evac_micc;
                $data_array['report_alimentos'] = $report_alimentos;
                $data_array['report_signo'] = $report_signo;
                
                if (array_key_exists('_report_comentarios_mes', $pacientes_meta) && $pacientes_meta['_report_comentarios_mes'] != '') {
                    $data_array['comentarios_mes'] = $pacientes_meta['_report_comentarios_mes'];
                } else {
                    $data_array['comentarios_mes'] = implode("\n", $observaciones_generales);
                }

                if (array_key_exists('_report_luces', $pacientes_meta) && $pacientes_meta['_report_luces'] != '') {
                    $data_array['luces'] = $pacientes_meta['_report_luces'];
                } else {
                    $data_array['luces'] = '';
                }

                if (array_key_exists('_report_supervisor', $pacientes_meta) && $pacientes_meta['_report_supervisor'] != '') {
                    $data_array['supervisor'] = $pacientes_meta['_report_supervisor'];
                } else {
                    $data_array['supervisor'] = "";
                }

                if (array_key_exists('_report_medico', $pacientes_meta) && $pacientes_meta['_report_medico'] != '') {
                    $data_array['medico'] = $pacientes_meta['_report_medico'];
                } else {
                    $data_array['medico'] = "";
                }

                if (array_key_exists('_report_objetivos_mes', $pacientes_meta) && $pacientes_meta['_report_objetivos_mes'] != '') {
                    $data_array['objetivos_mes'] = $pacientes_meta['_report_objetivos_mes'];
                } else {
                    $data_array['objetivos_mes'] = '';
                }

                if (array_key_exists('_report_objetivos_prox_mes', $pacientes_meta) && $pacientes_meta['_report_objetivos_prox_mes'] != '') {
                    $data_array['objetivos_prox_mes'] = $pacientes_meta['_report_objetivos_prox_mes'];
                } else {
                    $data_array['objetivos_prox_mes'] = "";
                }

                if (array_key_exists('_report_analisis_resultados', $pacientes_meta) && $pacientes_meta['_report_analisis_resultados'] != '') {
                    $data_array['analisis_resultados'] = $pacientes_meta['_report_analisis_resultados'];
                } else {
                    $data_array['analisis_resultados'] = "";
                }

                if (array_key_exists('_report_principales_logros', $pacientes_meta) && $pacientes_meta['_report_principales_logros'] != '') {
                    $data_array['principales_logros'] = $pacientes_meta['_report_principales_logros'];
                } else {
                    $data_array['principales_logros'] = "";
                }

                if (array_key_exists('_report_principales_oportunidades', $pacientes_meta) && $pacientes_meta['_report_principales_oportunidades'] != '') {
                    $data_array['principales_oportunidades'] = $pacientes_meta['_report_principales_oportunidades'];
                } else {
                    $data_array['principales_oportunidades'] = "";
                }

                if (array_key_exists('_report_reportes_incluidos', $pacientes_meta)) {
                    $data_array['reportes_incluidos_arr'] = explode(",", $pacientes_meta['_report_reportes_incluidos']);
                } else {
                    $data_array['reportes_incluidos_arr'] = array();
                }

                $data_array['word_cloud'] = $this->report_maker->get_word_cloud($report_sintoma);
                $data_array['logo'] = $this->getDataURI("/assets/build/img/pazmental-pdf.jpg");
                $data_array['header'] = $this->getDataURI("/assets/build/img/reporte-mensual-pdf.jpg");
                $data_array['happy'] = $this->getDataURI("/assets/build/img/happy.jpg");
                $data_array['meh'] = $this->getDataURI("/assets/build/img/meh.jpg");
                $data_array['sad'] = $this->getDataURI("/assets/build/img/sad.jpg");
                $data_array['evacuacion'] = $this->getDataURI("/assets/build/img/evacuacion.jpg");
                $data_array['miccion'] = $this->getDataURI("/assets/build/img/miccion.jpg");
                $data_array['guia_evacuacion'] = $this->getDataURI("/assets/build/img/guia-evacuacion.jpg");
                $data_array['checked_16'] = $this->getDataURI("/assets/build/img/checked_16.png");

                echo json_encode($data_array);

            } else if ($action == "save") {

                // modificación para guardar report _monthly_report_file a nivel paciente

                set_time_limit(300);
                $pdfdata = base64_decode($this->input->post('pdfdata'));
                $filename = $this->input->post('filename');
                if (file_exists("uploads/" . $filename)) {
                    unlink("uploads/" . $filename);
                }

                file_put_contents("uploads/" . $filename, $pdfdata);

                $time = new DateTime();
                $datetime = $time->format("Y-m-d H:i:s");
                $this->db->query("DELETE from pacientes_meta where paciente_id = '" . $paciente_id . "' and paciente_meta = '_monthly_report_file'");
                $this->db->query("INSERT INTO pacientes_meta(paciente_id,paciente_meta,paciente_value,timestamp) VALUES ('" . $paciente_id . "','_monthly_report_file','" . $filename . "','" . $datetime . "' )");

                $results = $this->db->affected_rows();


                echo json_encode($results);

            } else if ($action == "download") {

                $query = $this->db->query("SELECT paciente_value from paciente_meta
                                   where paciente_meta = '_monthly_report_file'
                                   and paciente_id = '" . $paciente_id . "'
                                   ORDER by timestamp ASC LIMIT 1");

                $results = $query->result();
                if (sizeof($results) > 0) {
                    $data_results = array(
                        "filename" => $results[0]->paciente_value,
                        "url" => base_url("uploads/" . $results[0]->paciente_value),
                        "status" => "success",
                    );

                } else {
                    $data_results = array(
                        "status" => "failure",
                        "message" => "No se ha generado ningun reporte para este paciente.",
                    );
                }
                echo json_encode($data_results);

            } else if ($action == "send-get-info") {

                $this->load->view($this->config->item('ci_my_admin_template_dir_admin') . "modals/_send_report");
            } else if ($action == "send") {

                $query = $this->db->query("SELECT paciente_value from paciente_meta
                                   where paciente_meta = '_monthly_report_file'
                                   and paciente_id = '" . $paciente_id . "'
                                   ORDER by timestamp ASC LIMIT 1");

                $results = $query->result();
                if (sizeof($results) > 0) {

                    $data_results = array(
                        "status" => "success",
                    );

                    $url = base_url("uploads/" . $results[0]->paciente_value);
                    $email = $this->input->post('Mail');
                    $nombre = $this->input->post('Name');
                    $message = $this->input->post('Message');
                    $subject = "[PAZ MENTAL] Envio de reportes";
                    $mail_success = $this->phpmailerlib->sendEmail($email, $nombre, $subject, $message, $url);
                    if ($mail_success == "success") {
                        $data_results = array(
                            "status" => "success",
                            "message" => "Mensaje enviado con éxito.",
                        );
                    } else {
                        $data_results = array(
                            "status" => "failure",
                            "message" => "Error al enviar el mensaje." . $mail_success,
                        );
                    }
                } else {
                    $data_results = array(
                        "status" => "failure",
                        "message" => "No se ha generado ningun reporte para este paciente.",
                    );
                }
                echo json_encode($data_results);

            }

        }

    }

    public function date_sort($array, $on, $order = SORT_ASC)
    {

        $new_array = array();
        $sortable_array = array();

        if (count($array) > 0) {
            foreach ($array as $k => $v) {
                if (is_array($v)) {
                    foreach ($v as $k2 => $v2) {
                        if ($k2 == $on) {
                            $sortable_array[$k] = $v2;
                        }
                    }
                } else {
                    $sortable_array[$k] = $v;
                }
            }

            switch ($order) {
                case SORT_ASC:
                    asort($sortable_array);
                    break;
                case SORT_DESC:
                    arsort($sortable_array);
                    break;
            }

            foreach ($sortable_array as $k => $v) {
                $new_array[$k] = $array[$k];
            }
        }

        return $new_array;
    }

    public function get_sintoma_table($report_data, $sintomas, $from, $to, $period)
    {
        $to = new DateTime($to);
        $rtable = array();
        $header = array();
        $header[] = "Síntoma";
        foreach ($period as $p) {

            $header[] = $p['date']->format('d');
        }

        $rtable[] = $header;

        if (array_key_exists('sintoma', $report_data)) {
            foreach ($sintomas as $sintoma) {
                $row = array();
                $row[] = $sintoma;
                foreach ($period as $p) {
                    if (array_key_exists($p['date']->format('d-m-Y'), $report_data['sintoma'][$sintoma])) {
                        $cell = array();
                        foreach ($report_data['sintoma'][$sintoma][$p['date']->format('d-m-Y')] as $r) {
                            $date = new DateTime($r->timestamp);
                            $date = $date->format('H');
                            $id = $r->id;
                            //$cell[] = $date;
                            $cell[] = array('image' => 'checked_16', 'width' => 10);
                        }
                        $row[] = $cell;
                    } else {
                        $row[] = "";
                    }
                }
                $rtable[] = $row;
            }
        }
        return $rtable;
    }

    public function get_medicamento_table($report_data, $from, $to, $period)
    {
        $to = new DateTime($to);
        $rtable = array();
        $header = array();
        $header[] = "Medicamento";
        foreach ($period as $p) {
            $header[] = $p['date']->format('d');
        }

        $rtable[] = $header;
        if (array_key_exists('medicamento', $report_data)) {
            ksort($report_data['medicamento']);

            foreach (array_keys($report_data['medicamento']) as $medicamento) {
                $row = array();
                $row[] = $medicamento;
                foreach ($period as $p) {
                    if (array_key_exists($p['date']->format('d-m-Y'), $report_data['medicamento'][$medicamento])) {
                        $cell = array();
                        $r = $report_data['medicamento'][$medicamento][$p['date']->format('d-m-Y')]['raw_data'];
                        $value = $report_data['medicamento'][$medicamento][$p['date']->format('d-m-Y')]['tuple'];
                        for ($i = 0; $i < sizeof($r); $i++) {
                            //$cell[] =  $value[$i];
                            $cell[] = array('image' => 'checked_16', 'width' => 10);
                        }
                        //$row[]= implode("\n",$cell);
                        $row[] = $cell;
                    } else {
                        $row[] = "";
                    }
                }
                $rtable[] = $row;
            }
        }
        return $rtable;

    }

    public function get_estimulacion_cognitiva_table($report_data, $from, $to, $period)
    {
        $to = new DateTime($to);
        $rtable = array();
        $header = array();
        $header[] = "Estimulación";

        foreach ($period as $p) {
            $header[] = $p['date']->format('d');
        }
        $rtable[] = $header;
        if (array_key_exists('estimulacion_cognitiva', $report_data)) {
            foreach (array_keys($report_data['estimulacion_cognitiva']) as $est_cognitiva) {
                $row = array();
                $row[] = $est_cognitiva;
                foreach ($period as $p) {
                    if (array_key_exists($p['date']->format('d-m-Y'), $report_data['estimulacion_cognitiva'][$est_cognitiva])) {

                        $data_row = array();
                        foreach($report_data['estimulacion_cognitiva'][$est_cognitiva][$p['date']->format('d-m-Y')] AS $data) {
                            $report_meta = unserialize($data->report_meta);

                            if ($report_meta['como_le_fue'] == "Bien") {
                                $data_row[] = array('image' => 'happy', 'width' => 10);
                            }
                            if ($report_meta['como_le_fue'] == "Regular") {
                                $data_row[] = array('image' => 'meh', 'width' => 10);
                            }
                            if ($report_meta['como_le_fue'] == "Mal") {
                                $data_row[] = array('image' => 'sad', 'width' => 10);
                            }
                        }
                        $row[] = $data_row;
                    } else {
                        $row[] = "";
                    }
                }
                $rtable[] = $row;
            }
        }
        return $rtable;
    }

    public function get_estimulacion_fisica_table($report_data, $from, $to, $period)
    {
        $to = new DateTime($to);

        $rtable = array();
        $header = array();
        $header[] = "Estimulación";
        foreach ($period as $p) {
            $header[] = $p['date']->format('d');
        }
        $rtable[] = $header;
        if (array_key_exists('estimulacion_fisica', $report_data)) {
            foreach (array_keys($report_data['estimulacion_fisica']) as $est_fisica) {
                $row = array();
                $row[] = $est_fisica;
                foreach ($period as $p) {
                    if (array_key_exists($p['date']->format('d-m-Y'), $report_data['estimulacion_fisica'][$est_fisica])) {
                        $data_array = array();  
                        foreach($report_data['estimulacion_fisica'][$est_fisica][$p['date']->format('d-m-Y')] AS $data) {
                            $d = new DateTime($data->timestamp);
                            $date = $d->format('H');
                            $data_array[] = array('image' => 'checked_16', 'width' => 10);
                        }                      
                        $row[] = $data_array;
                    } else {
                        $row[] = "";
                    }
                }
                $rtable[] = $row;
            }
        }
        return $rtable;
    }

    public function get_estimulacion_otra_table($report_data, $from, $to, $period)
    {
        $to = new DateTime($to);

        $rtable = array();
        $header = array();
        $header[] = "Estimulación";
        foreach ($period as $p) {
            $header[] = $p['date']->format('d');
        }
        $rtable[] = $header;
        if (array_key_exists('estimulacion_otra', $report_data)) {
            foreach (array_keys($report_data['estimulacion_otra']) as $est_otra) {
                $row = array();
                $row[] = $est_otra;
                foreach ($period as $p) {
                    if (array_key_exists($p['date']->format('d-m-Y'), $report_data['estimulacion_otra'][$est_otra])) {
                        $data_row = array();
                        foreach($report_data['estimulacion_otra'][$est_otra][$p['date']->format('d-m-Y')] AS $data) {
                            $d = new DateTime($data->timestamp);
                            $date = $d->format('H');
                            $data_row[] = array('image' => 'checked_16', 'width' => 10);
                        }
                        $row[] = $data_row;
                    } else {
                        $row[] = "";

                    }
                }
                $rtable[] = $row;
            }
        }
        return $rtable;
    }

    public function get_evacuacion_table($report_data, $from, $to, $period)
    {
        $to = new DateTime($to);

        $rtable = array();
        $header = array();
        $header[] = "Tipo";
        $evacuacion = array('08:00 - 12:00', '12:00 - 16:00', '16:00 - 20:00', '20:00 - 00:00', '00:00 - 04:00', '04:00 - 08:00');
        foreach ($period as $p) {
            $header[] = $p['date']->format('d');
        }
        $rtable[] = $header;
        if (array_key_exists('evacuacion', $report_data)) {
            foreach ($evacuacion as $evac) {
                $row = array();
                $row[] = $evac;
                foreach ($period as $p) {
                    if (array_key_exists($evac, $report_data['evacuacion'])) {
                        if (array_key_exists($p['date']->format('d-m-Y'), $report_data['evacuacion'][$evac])) {
                            $data_row = array();

                            foreach($report_data['evacuacion'][$evac][$p['date']->format('d-m-Y')] AS $data){
                                $report_meta = unserialize($data->report_meta);
                                if ($report_meta['evacuacion_o_miccion'] == 'Miccionar') {
                                    $data_row[] = array('image' => 'miccion');
                                } else {
                                    $data_row[] = array('image' => 'evacuacion');
                                }
                            }

                            $row[] = $data_row;

                        } else {
                            $row[] = "";
                        }
                    } else {
                        $row[] = "";
                    }
                }
                $rtable[] = $row;
            }
        }
        return $rtable;
    }

    public function get_alimentacion_table($report_data, $from, $to, $period)
    {

        $to = new DateTime($to);

        $rtable = array();
        $header = array();
        $header[] = "Tipo";
        $alimentacion = array('Desayuno', 'Comida', 'Cena', 'Liquidos', 'Colaciones');
        foreach ($period as $p) {
            $header[] = $p['date']->format('d');
        }

        $rtable[] = $header;
        if (array_key_exists('alimento', $report_data)) {
            foreach ($alimentacion as $alim) {
                $row = array();
                $row[] = $alim;

                foreach ($period as $p) {
                    if (array_key_exists($alim, $report_data['alimento'])) {
                        if (array_key_exists($p['date']->format('d-m-Y'), $report_data['alimento'][$alim])) {
                            $data_array = array();
                            foreach($report_data['alimento'][$alim][$p['date']->format('d-m-Y')] AS $data) {
                                $report_meta = unserialize($data->report_meta);
                                if ($report_meta['como_comio'] == "Bien") {
                                    $data_array[] = array('image' => 'happy');
                                }
                                if ($report_meta['como_comio'] == "Regular") {
                                    $data_array[] = array('image' => 'meh');
                                }
                                if ($report_meta['como_comio'] == "Mal") {
                                    $data_array[] = array('image' => 'sad');
                                }
                            }

                            $row[] = $data_array;
                            
                        } else {
                            $row[] = "";
                        }
                    } else {
                        $row[] = "";
                    }
                }
                $rtable[] = $row;
            }
        }
        return $rtable;
    }

    public function get_signos_vitales_table($report_data, $from, $to, $period)
    {
        $to = new DateTime($to);

        $rtable = array();
        $header = array();
        $header[] = "Signo Vital";
        $signos_vitales = array('temperatura', 'presion_arterial_sistolica', 'presion_arterial_diastolica', 'frecuencia_cardiaca', 'oxigeno', 'glucosa');
        foreach ($period as $p) {
            $header[] = $p['date']->format('d');
        }
        $rtable[] = $header;
        if (array_key_exists('signos_vitales', $report_data)) {
            foreach (array_keys($report_data['signos_vitales']) as $sv) {
                $row = array();
                $row[] = $sv;
                foreach ($period as $p) {
                    if (array_key_exists($p['date']->format('d-m-Y'), $report_data['signos_vitales'][$sv])) {
                        $data_row = array();
                        foreach($report_data['signos_vitales'][$sv][$p['date']->format('d-m-Y')] AS $data) {
                            $report_meta = unserialize($data->report_meta);
                            $data_row[] = $report_meta[$sv];
                        }
                        $row[] = $data_row;
                    } else {
                        $row[] = "";
                    }
                }
                $rtable[] = $row;
            }
        }
        return $rtable;
    }

    public function getDataURI($image, $mime = '')
    {
        $image = base_url($image);
        $type = pathinfo($image, PATHINFO_EXTENSION);
        $data = file_get_contents($image);
        $dataUri = 'data:image/' . $type . ';base64,' . base64_encode($data);
        return $dataUri;
    }

    public function delete($id)
    {
        /* DELETE PDF */
    }

    public function update()
    {

        if ($this->input->is_ajax_request()) {

            $data = json_decode($this->input->post('data'));
            $reports_included = json_decode($this->input->post('reports_included'));

            $md = array();
            foreach ($data as $d) {
                $md[$d->name] = $d->value;
            }

            $paciente_id = $md['paciente_id'];

            $ri = array();
            foreach ($reports_included->reports_included as $key => $value) {
                foreach ($value as $k => $v) {
                    if ($v == 1) {
                        $ri[] = $k;
                    }
                }
            }

            $metadata = array(
                '_report_luces' => $md['luces'],
                '_report_supervisor' => $md['supervisor'],
                '_report_medico' => $md['medico'],
                '_report_objetivos_mes' => $md['objetivos_mes'],
                '_report_comentarios_mes' => $md['comentarios_mes'],
                '_report_analisis_resultados' => $md['analisis_resultados'],
                '_report_principales_logros' => $md['principales_logros'],
                '_report_principales_oportunidades' => $md['principales_oportunidades'],
                '_report_objetivos_prox_mes' => $md['objetivos_prox_mes'],
                '_report_informe_medico' => $md['informe_medico'],
                '_report_informe_rehabilitacion' => $md['informe_rehabilitacion'],
                '_report_informe_neuropsicologico' => $md['informe_neuropsicologico'],
                '_report_notas_internas' => $md['notas_internas'],
                '_report_reportes_incluidos' => implode(",", $ri));

            $this->db->select("paciente_meta, paciente_value`");
            $this->db->from("pacientes_meta");
            $this->db->where(array('paciente_id' => $paciente_id));
            $query = $this->db->get();
            $results = $query->result_array();

            $existing_meta_values = array();
            foreach ($results as $result) {

                $existing_meta_values[$result['paciente_meta']] = $result['paciente_value'];
            }

            $time = new DateTime();
            $timestamp = $time->format("Y-m-d H:i");

            foreach ($metadata as $meta => $data) {

                $insert_data = array(
                    'paciente_value' => $data,
                    'timestamp' => $timestamp,
                );

                $this->db->where('paciente_id', $paciente_id);
                $this->db->where('paciente_meta', $meta);

                if (array_key_exists($meta, $existing_meta_values)) {
                    $this->db->update('pacientes_meta', $insert_data);
                } else {
                    $insert_data = array(
                        'id' => '',
                        'paciente_id' => $paciente_id,
                        'paciente_meta' => $meta,
                        'paciente_value' => $data,
                        'timestamp' => $timestamp,
                    );

                    $this->db->insert('pacientes_meta', $insert_data);

                }
            }

            $results = array(
                'status' => 'success',
                'action' => 'redirect',
                'module' => "" . base_url() . 'admin' . "/reports",
                'message' => 'Se guardaron los cambios exitosamente',
            );

            echo json_encode($results);

        } else {
            exit('No direct access allowed');
        }
    }

    public function edit_tars($id)
    {

        if ($this->input->is_ajax_request()) {

            if ($this->input->post('action') == 'save') {
                $data = $this->input->post();

                $report_id = $this->input->post('reports_id');
                $hora = $this->input->post('hora');
                $fecha = $this->input->post('fecha');
                $metadata = array();
                
                foreach ($data as $key => $value) {
                    if ($key != 'reports_id' and $key != "hora" and $key != "fecha" and $key != "action") {
                        $metadata[$key] = $value;

                    }
                }

                $metadata = serialize($metadata);

                $query = $this->db->query("UPDATE reports set
                           report_meta = '" . $metadata . "',
                           timestamp = '" . $fecha . " " . $hora . "'
                           where id = '" . $report_id . "'");
                $s = $this->db->affected_rows();
                echo json_encode($s);

            } else {
                $query = $this->db->query("SELECT *  from reports where id = $id");
                $s = $query->result();
                $data['row'] = json_encode($s[0]);
                $this->load->view($this->config->item('ci_my_admin_template_dir_admin') . "modals/_edit_tars_rows", $data);

            }

        } else {

            exit('No direct access allowed');
        }

    }

    public function stripAccents($str)
    {
        return strtr(utf8_decode($str), utf8_decode('àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ'), 'aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY');
    }

    public function get_calificaciones($user_id, $service_id)
    {
        $query = $this->db->query("SELECT * from users_meta where meta_key = '_Rating_Luz' or meta_key = '_Rating_PM' and user_id = '$user_id'");
        $results = $query->result();

        $grade_array = array();
        foreach ($results as $row) {
            $grade_array[$row->meta_key] = $row->meta_value;
        }

        return $grade_array;

    }
}
