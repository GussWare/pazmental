<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Images extends CI_Controller
{
    private $upload_path = "./uploads/files";

    public function __construct()
    {
        parent::__construct();
        $this->load->helper("nombre_imagen");
    }

    public function index()
    {
        $this->load->view("image");
    }

    public function upload()
    {

        if (!empty($_FILES)) {

            $config['max_size'] = '15000000';

            $config["upload_path"] = $this->upload_path;

            $config["allowed_types"] = "*";

            $this->load->library('upload', $config);

            $this->upload->initialize($config);

            if ($this->upload->do_upload("file")) {

                echo $this->upload->data('file_name');

            } else {

                echo "failed to upload file(s)";
            }
        }
    }

    public function remove()
    {
        $file = $this->input->post("file");
        if ($file && file_exists($this->upload_path . "/" . $file)) {
            unlink($this->upload_path . "/" . $file);
        }
    }

    public function list_files()
    {
        $data_file = array();
        $nombre_archivo = $this->input->get("archivo");
        $nombre_files = get_nombre_imagen($nombre_archivo);

        if ($nombre_files) {
            $size = filesize($this->upload_path . "/" . $nombre_files);
            if ($size > 0) {
                $data_file[] = array(
                    'name' => $nombre_files,
                    'size' => $size,
                );
            }
        }

        header("Content-type: text/json");
        header("Content-type: application/json");
        echo json_encode($data_file);
    }

}
