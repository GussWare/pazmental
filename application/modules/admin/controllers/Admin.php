<?php (defined('BASEPATH')) or exit('No direct script access allowed');

class Admin extends Admin_Controller
{

    public function __construct()
    {

        parent::__construct();

        if (!$this->ion_auth->logged_in()) {

            $this->ion_auth->logout();
            redirect('auth', 'refresh');
        }

        $authorized = array('admin', 'supervisor', 'excecutives');
        if (!in_array($this->ion_auth->get_users_groups()->row()->name, $authorized)) {
            redirect(site_url('admin/reports'), 'refresh');
        }

        $this->load->model(array('module'));
        $this->load->model(array('service'));
        $this->load->model(array('agenda'));
        $this->load->library('report_maker');
        $this->load->library('servicios_lib');

    }

    public function index()
    {
        $datetime = new DateTime();
        // $datetime = new Datetime('2017-12-12');
        $today = $datetime->format("Y-m-d");
        $this->view($today);
    }

    public function view($date)
    {
        $user_id = $this->ion_auth->get_user_id();
        $group_name = $this->ion_auth->get_users_groups()->row()->name;

        $supervisor = null;
        $luz = null;
        $status = null;

        if ($this->input->is_ajax_request()) {
            $supervisor = $this->input->post('supervisor');
            $luz = $this->input->post('luz');
            $status = $this->input->post("status");
        }

        if ($date != "") {
            $datetime = new DateTime($date);
            $refresh = 0;
        } else {

            $start_day = $this->input->post('start_date');
            if (!isset($start_day)) {
                $datetime = new DateTime();
                $refresh = 0;
            } else {
                $datetime = new DateTime($start_day);
                $refresh = 1;
            }

        }

        $today = $datetime->format("Y-m-d");
        $date = $datetime->format('l, d M Y');
        $data['date'] = $date;
        $datetime->modify('-1 day');
        $data['yesterday'] = $datetime->format("Y-m-d");
        $datetime->modify('+2 day');
        $data['tomorrow'] = $datetime->format("Y-m-d");

        $data['services'] = $this->get_todays_service_list($today, $user_id, $group_name, $supervisor, $luz, $status);
        $data['supervisors'] = $this->ion_auth->users('supervisor')->result();
        $data['filtered_luz'] = $luz;
        $data['filtered_supervisor'] = $supervisor;

        if ($refresh == 0) {
            $data['page'] = $this->config->item('ci_my_admin_template_dir_admin') . "dashboard";
            $this->load->view($this->_container, $data);

        } else {
            $data['page'] = $this->config->item('ci_my_admin_template_dir_admin') . "dashboard";
            $this->load->view($this->_container, $data);
            return json_encode($data);
        }
    }

    public function get_todays_service_list($date, $user_id, $group_name, $supervisor, $luz, $status)
    {
        $user_id = $this->ion_auth->get_user_id();
        $group_name = $this->ion_auth->get_users_groups()->row()->name;

        $params = array(
            'supervisor' => $supervisor,
            'luz' => $luz,
            'status' => $status,
            'group_name' => $group_name,
            'date' => $date,
            'user_id' => $user_id
        );

        $results = $this->agenda->get_dashboard($params);

        $retardos = array();
        $nurse_array = array();
        $now = new DateTime();

        foreach ($results as $r) {
            $nurse = $r->nurse_name . " " . $r->nurse_last;
            $nurse_id = $r->nurse_id;
            $name_obj = json_decode($r->patient_name);
            $name = $name_obj->first_name . " " . $name_obj->second_name . " " . $name_obj->last_name . " " . $name_obj->second_last_name;
            $service_id = $r->service_id;
            $nurse_key = $nurse_id . "-" . $nurse;
            if (!array_key_exists($nurse_key, $nurse_array)) {
                $nurse_array[$nurse_key] = array();
            }

            $st = new DateTime($r->start_time);
            $et = new DateTime($r->end_time);
            if ($st < $now) {
                $retardo_o_inasistencia = $this->get_inasistencia_or_retardo($service_id, $date);
                if (($retardo_o_inasistencia != "despues")) {
                    $tars_id = $this->report_maker->get_tars_id($service_id);
                    $cobertura = $this->get_cobertura($tars_id, $date, $nurse_id);

                    if ($cobertura != "") {
                        $nurse = $cobertura->first_name . " " . $cobertura->last_name;
                        $nurse_id = $cobertura->id;
                        $nurse_key = $nurse_id . "-" . $nurse;
                        $retardo_o_inasistencia .= "-cobertura";

                    }
                }
            } else {
                $retardo_o_inasistencia = "despues";
            }
            if ($et < $now) {
                $esperados = $this->report_maker->get_registros_esperados($service_id);
                $fallo_ejercicios = $this->get_fallo_ejercicios($service_id, $date, $esperados);
                $fallo_medicamentos = $this->get_fallo_medicamentos($service_id, $date);
                $fallo_alimentos = $this->get_fallo_alimentos($service_id, $date, $esperados);
            } else {
                $fallo_ejercicios = "";
                $fallo_medicamentos = "";
                $fallo_alimentos = "";
            }
            
            $service_array = array(
                'service_id' => $service_id, 
                'service_name' => $name,
                "asistencia" => $retardo_o_inasistencia, 
                "fallo_ejercicios" => $fallo_ejercicios,
                "fallo_medicamentos" => $fallo_medicamentos, 
                "fallo_alimentos" => $fallo_alimentos, 
                "now" => $now,
                "st" => $st,
                "tipo_plan" => $this->servicios_lib->get_service_plan_id($service_id),
                "paciente_id" => $r->paciente_id
            );

            $nurse_array[$nurse_key][$st->format("H:i") . " - " . $et->format("H:i")] = $service_array;

        }

        return $nurse_array;
    }

    public function get_inasistencia_or_retardo($id, $date)
    {

        $start_day = $date;

        $horarios = $this->report_maker->get_data_from_tars($id, $start_day, $start_day);

        $agendados = $this->report_maker->get_data_from_services($id, $start_day, $start_day);

        $now = new DateTime();

        /* HORARIOS */
        /* Calcular horas trabajadas, inasistencias, retardos  y luces asignadas */
        foreach ($agendados['entrada'] as $date => $time) {
            if (array_key_exists($date, $horarios['entrada'])) {
                $real_start_time = strtotime($horarios['entrada'][$date]);
            } else {
                return "inasistencia";
            }
            $real_end_time = strtotime($horarios['salida'][$date]);

            $tolerancia = strtotime("+ 15 minutes", strtotime($time));

            if (new DateTime($date . " " . $time) > $now) {
                return "despues";
            } else if ($horarios['entrada'][$date] == "") {
                return "inasistencia";
            } else if ($real_start_time > $tolerancia) {
                return "retardo";
            } else if ($real_start_time <= $tolerancia) {
                return "a-tiempo";
            }

        }
    }

    public function get_cobertura($service_id, $date, $nurse_id)
    {
        $real_nurses = $this->report_maker->get_real_nurses($service_id, $date, $date);

        foreach ($real_nurses as $rn) {
            if ($nurse_id != $rn->id) {return $rn;}
        }
        return "";

    }

    public function get_fallo_ejercicios($id, $start_day, $esperados)
    {
        $ejercicios_tars = $this->report_maker->get_num_ejercicios_from_tars($id, $start_day, $start_day);
        $ejercicios_schedule = $this->report_maker->get_num_ejercicios_from_schedule($id);

        if ($ejercicios_tars >= $ejercicios_schedule) {
            return "NO";
        } else {
            return "SI";
        }
        /*foreach($ejercicios_schedule as $date=>$es){
    foreach($es as $e){
    $result = $this->report_maker->get_match_ejercicios($date,$e,$ejercicios_tars);
    $txt.= implode(" - ", $result);
    if($result[1] == "NO"){ return "SI"; //fallo por lo menos en un ejercicio
    }

    }
    }

    return "NO" ;//no fallo en ningun ejercicio
     */
    }

    public function get_fallo_medicamentos($id, $start_day)
    {

        $medicamentos_tars = $this->report_maker->get_medicamentos_from_tars($id, $start_day, $start_day);
        $medicamentos_schedule = $this->report_maker->get_medicamentos_from_schedule($id, $start_day, $start_day);

        /* Si no hay nada registrado en tars pero si hay medicamentos agendados */
        if ((sizeof($medicamentos_tars) == 0) & (sizeof($medicamentos_schedule) > 0)) {
            return "SI";
        }

        foreach (array_keys($medicamentos_tars) as $date) {

            foreach ($medicamentos_tars[$date] as $medicamento => $horas) {
                $real_size = sizeof($horas);
                $scheduled_size = sizeof($medicamentos_schedule[$medicamento]);

                /* Se le dió el mismo número de veces que lo que había agendado */
                if ($real_size != $scheduled_size) {
                    return "SI";
                }
            }

        }

        return "NO"; /*no fallo en ningun medicamento */
    }

    public function get_fallo_alimentos($id, $start_day, $esperados)
    {

        $alimentos_tars = $this->report_maker->get_alimentos_from_tars($id, $start_day, $start_day);
        $alimentos_esperados = (int) $esperados['alimento'];

        list($ano, $mes, $dia) = explode("-", $start_day);
        $fecha = $dia . "-" . $mes . "-" . $ano;

        if (array_key_exists($fecha, $alimentos_tars)) {

            /* Si no se cumple el numero de tars */
            if (count($alimentos_tars[$fecha]) < $alimentos_esperados) {
                return "SI"; // si hay reportes en services pero menos de los que se dio de alta en services debe estar en rojo
            } else {
                return "NO"; /*si hubo alimentación */
            }
        } else {
            if ($alimentos_esperados > 0) {
                return "SI"; // no hay reportes pero si sedio de alta en alimentos en services debe estar en rojo
            } else {
                return "NO"; // no existen reportes pero tampoco alimentos en services debe estar en verde
            }
        }

    }

}
