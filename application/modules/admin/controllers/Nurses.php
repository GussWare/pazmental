<?php

(defined('BASEPATH')) or exit('No direct script access allowed');

class Nurses extends Admin_Controller
{

    public function __construct()
    {
        parent::__construct();

        $group = 'admin';

        $this->load->model(array('module'));
        $this->load->model(array('user'));
        $this->load->model(array('schedule'));
        $this->load->model(array('service'));
        $this->load->library('report_maker');
        $this->load->library('phpmailerlib');

        $this->load->model(array('admin/options'));

        $this->load->helper("nombre_imagen");

        if (!$this->ion_auth->logged_in()) {

            $this->ion_auth->logout();
            redirect('auth', 'refresh');
        }

        $authorized = array('admin', 'supervisor', 'excecutives');
        if (!in_array($this->ion_auth->get_users_groups()->row()->name, $authorized)) {
            $this->session->set_flashdata('message', 'You must be an administrator to view the users page.');
            redirect('admin/dashboard');
        }
    }

    public function index()
    {

        $start_day = $this->input->post('start_date');
        $end_day = $this->input->post('end_date');

        if (!isset($start_day)) {
            $f = new DateTime("first day of this month");
            $t = new DateTime("last day of this month");
            $refresh = 0;
        } else {
            $f = new DateTime($start_day);
            $t = new DateTime($end_day);
            $refresh = 1;
        }

        $start_day = $f->format("Y-m-d");
        $end_day = $t->format("Y-m-d");

        $data['from'] = $start_day;
        $data['to'] = $end_day;

        $status = $this->input->post("status");
        $colony = $this->input->post("colony");
        $supervisor = $this->input->post("supervisor");
        $gender = $this->input->post("gender");
        $tipo_luz = $this->input->post("id_tipo_luz");

        $data['supervisors'] = $this->ion_auth->users('supervisor')->result();
        $data['colonies'] = $this->get_all_colonies();

        //$users = $this->ion_auth->users('nurses')->result();

        $this->db->select('u.*, ug.user_id');
        $this->db->from('users AS u');
        $this->db->join('users_groups AS ug', 'u.id = ug.user_id', 'inner');
        $this->db->join('groups AS g', 'ug.group_id = g.id', 'inner');
        $this->db->where('g.id', 3);
        if ($tipo_luz) {
            $this->db->where('u.id_tipo_luz', $tipo_luz);
        }
        $query_users = $this->db->get();
        $users = $query_users->result();

        $conta_users = count($users);

        $this->db->select('*');
        $this->db->from('tipo_luz');
        $query_tipo_luz = $this->db->get();
        $data['tipos_luz'] = $query_tipo_luz->result();

        $user_meta = array();
        $data['users'] = $users;
        $data['result_colonies'] = array();

        foreach ($users as $user) {
            $filter = 0;
            if ($status != "") {
                $query = $this->db->query("Select meta_value from users_meta where user_id = " . $user->id . " and meta_key = '_status'");
                $this_status = $query->result_array();
                if (empty($this_status)) {$filter = 1;} else if ($this_status[0]["meta_value"] != $status) {$filter = 1;}
            }
            if ($colony != "") {
                /* ME QUEDE AQUI*/
                $query = $this->db->query("Select meta_value from users_meta where user_id = " . $user->id . " and meta_key = '_colony'");
                $this_colony = $query->result_array();
                if (empty($this_colony)) {$filter = 1;} else if ($this_colony[0]["meta_value"] != $colony) {$filter = 1;}

            }
            if ($supervisor != "") {
                $query = $this->db->query("Select meta_value from users_meta where user_id = " . $user->id . " and meta_key = '_supervisor'");
                $this_supervisor = $query->result_array();
                if (empty($this_supervisor)) {$filter = 1;} else if ($this_supervisor[0]["meta_value"] != $supervisor) {$filter = 1;}

            }
            if ($gender != "") {
                $query = $this->db->query("Select meta_value from users_meta where user_id = " . $user->id . " and meta_key = '_gender'");
                $this_gender = $query->result_array();
                if (empty($this_gender)) {$filter = 1;} else if ($this_gender[0]["meta_value"] != $gender) {$filter = 1;}

            }

            $user_meta[$user->id] = array();

            if ($filter == 1) {
                $user_meta[$user->id]['filtro'] = 1;
            } else {

                $user_meta[$user->id]['filtro'] = 0;
                $horarios = $this->report_maker->get_data_from_tars($user->id, $start_day, $end_day, "nurses");
                $agendados = $this->report_maker->get_data_from_services($user->id, $start_day, $end_day, "nurses");
                $last_day = "";

                /* HORARIOS */
                /* Calcular número de servicios, días trabajados, horas trabajadas, inasistencias, retardos */
                $now = new DateTime();
                foreach ($agendados['entrada'] as $date => $time) {

                    if (!array_key_exists($date, $horarios['entrada'])) {
                        if ($date <= $now->format("d-m-Y")) {
                            if (!array_key_exists('inasistencia', $user_meta[$user->id])) {
                                $user_meta[$user->id]['inasistencia'] = 1;
                            } else {
                                $user_meta[$user->id]['inasistencia'] += 1;

                            }
                        }
                    } else {
                        $real_start_time = strtotime($horarios['entrada'][$date]);
                        if (!array_key_exists($date, $horarios['salida'])) {
                            $real_end_time = "";
                        } else {
                            $real_end_time = strtotime($horarios['salida'][$date]);
                        }
                        $tolerancia = strtotime("+ 15 minutes", strtotime($time));

                        if (!array_key_exists($date, $horarios['entrada']) or $horarios['entrada'][$date] == "") {
                            /*INASISTENCIA*/
                            if ($date <= $now->format("d-m-Y")) {
                                if (!array_key_exists('inasistencia', $user_meta[$user->id])) {
                                    $user_meta[$user->id]['inasistencia'] = 1;
                                } else {
                                    $user_meta[$user->id]['inasistencia'] += 1;

                                }

                            }

                        } else if ($real_start_time > $tolerancia) {
                            /*RETARDO*/
                            if (!array_key_exists('retardo', $user_meta[$user->id])) {
                                $user_meta[$user->id]['retardo'] = 1;
                            } else {
                                $user_meta[$user->id]['retardo'] += 1;
                            }
                        }

                        if ($real_start_time != "" && $real_end_time != "") {
                            /*HORAS TRABAJADAS*/
                            if (!array_key_exists('horas', $user_meta[$user->id])) {
                                $user_meta[$user->id]['horas'] = gmdate("H:i", $real_end_time - $real_start_time);
                            } else {
                                $user_meta[$user->id]['horas'] += gmdate("H:i", $real_end_time - $real_start_time);
                            }
                        }

                        /*TOTAL VISITAS*/
                        if (!array_key_exists('visitas', $user_meta[$user->id])) {
                            $user_meta[$user->id]['visitas'] = 1;
                        } else {
                            $user_meta[$user->id]['visitas'] += 1;
                        }

                        if ($last_day != $date) {
                            /*DIAS TRABAJADOS*/
                            if (!array_key_exists('dias', $user_meta[$user->id])) {
                                $user_meta[$user->id]['dias'] = 1;
                            } else {
                                $user_meta[$user->id]['dias'] += 1;
                            }

                        }
                        $last_day = $date;
                    }

                }
                /* COBERTURAS */
                foreach ($horarios['entrada'] as $date => $real_time) {
                    if (!array_key_exists($date, $agendados['entrada'])) {
                        $time = $agendados['entrada'][$date];
                        $real_start_time = strtotime($horarios['entrada'][$date]);
                        if (array_key_exists($date, $horarios['salida'])) {
                            $real_end_time = strtotime($horarios['salida'][$date]);
                        } else {
                            $real_end_time = "";
                        }
                        $tolerancia = strtotime("+ 15 minutes", strtotime($time));

                        if ($real_start_time > $tolerancia) {
                            /*RETARDO*/
                            if (!array_key_exists('retardo', $user_meta[$user->id])) {
                                $user_meta[$user->id]['retardo'] = 1;
                            } else {
                                $user_meta[$user->id]['retardo'] += 1;
                            }
                        }

                        if ($real_start_time != "" && $real_end_time != "") {
                            /*HORAS TRABAJADAS*/
                            if (!array_key_exists('horas', $user_meta[$user->id])) {
                                $user_meta[$user->id]['horas'] = gmdate("H:i", $real_end_time - $real_start_time);
                            } else {
                                $user_meta[$user->id]['horas'] += gmdate("H:i", $real_end_time - $real_start_time);
                            }
                        }

                        /*TOTAL VISITAS*/
                        if (!array_key_exists('visitas', $user_meta[$user->id])) {
                            $user_meta[$user->id]['visitas'] = 1;
                        } else {
                            $user_meta[$user->id]['visitas'] += 1;
                        }

                        if ($last_day != $date) {
                            /*DIAS TRABAJADOS*/
                            if (!array_key_exists('dias', $user_meta[$user->id])) {
                                $user_meta[$user->id]['dias'] = 1;
                            } else {
                                $user_meta[$user->id]['dias'] += 1;
                            }

                        }
                        if (!array_key_exists('coberturas', $user_meta[$user->id])) {
                            $user_meta[$user->id]['coberturas'] = 1;
                        } else {
                            $user_meta[$user->id]['coberturas'] += 1;
                        }

                        $last_day = $date;

                    }

                }

                $registros = $this->report_maker->get_registros($user->id, $start_day, $end_day, "nurses");
                $user_meta[$user->id]['registros'] = $registros['bitacora_totales'];

                $user_meta[$user->id]['calificacion'] = $this->get_calificacion($user->id, $start_day, $end_day);
                $user_meta[$user->id]['antiguedad'] = $this->get_antiguedad($user->id);
                $user_meta[$user->id]['ingreso_mensual'] = "TBD";
                if (array_key_exists('visitas', $user_meta[$user->id])) {
                    $user_meta[$user->id]['servicios_por_dia'] = $user_meta[$user->id]['visitas'] / $user_meta[$user->id]['dias'];
                } else {
                    $user_meta[$user->id]['servicios_por_dia'] = 0;
                }
                $user_meta[$user->id]['status'] = $this->get_status($user->id);
            }

        }

        $data['user_meta'] = $user_meta;

        if ($refresh == 0) {
            /*FIRST TIME */
            $data['page'] = $this->config->item('ci_my_admin_template_dir_admin') . "nurses_list";
            $this->load->view($this->_container, $data);

        } else {
            /*USING START - END FILTERS */
            $data['page'] = $this->config->item('ci_my_admin_template_dir_admin') . "nurses_list";
            $this->load->view($this->_container, $data);

            return json_encode($data);

        }

    }

    public function view($id)
    {
        $data['nurse_id'] = $id;
        $start_day = $this->input->post('start_date');
        $end_day = $this->input->post('end_date');

        if (!isset($start_day)) {
            $f = new DateTime("first day of this month");
            $t = new DateTime("last day of this month");
            $refresh = 0;
        } else {
            $f = new DateTime($start_day);
            $t = new DateTime($end_day);
            $refresh = 1;
        }

        $start_day = $f->format("Y-m-d");
        $end_day = $t->format("Y-m-d");

        $data['from'] = $start_day;
        $data['to'] = $end_day;
        $data['dias'] = $dias = array("Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sábado");

        $nurse_meta = $this->db->select("first_name,last_name");
        $this->db->from("users");
        $this->db->where(array('id' => $id));
        $query = $this->db->get();
        $names = $query->result_array();

        $nurse_meta = $this->db->select("*");
        $this->db->from("users_meta");
        $this->db->where(array('user_id' => $id, 'meta_key' => '_numero_luz'));
        $query = $this->db->get();
        $results = $query->result_array();

        $data['full_name'] = $names[0]['first_name'] . " " . $names[0]['last_name'] . "(" . $results[0]['meta_value'] . ")";
        $data['first_name'] = $names[0]['first_name'];
        $data['last_name'] = $names[0]['last_name'] . " (" . $results[0]['meta_value'] . ")";

        $nurse_meta = $this->db->select("*");
        $this->db->from("users_meta");
        $this->db->where(array('user_id' => $id, 'meta_key' => '_profile_images'));
        $query = $this->db->get();
        $pics = $query->result_array();

        $data['profile_img'] = $pics[0]['meta_value'];

        $horarios = $this->report_maker->get_data_from_tars($id, $start_day, $end_day, "nurses");
        $agendados = $this->report_maker->get_data_from_services($id, $start_day, $end_day, "nurses");
        $last_day = "";
        $registros = $this->report_maker->get_registros($id, $start_day, $end_day, "nurses");
        $data['registros'] = $registros['ingresos_a_bitacora'];

        $user_meta = array('Turnos Asignados' => array(),
            'Horas Trabajadas' => array(),
            'Retardos' => array(),
            'Inasistencias' => array(),
            'Coberturas' => array(),
        );
        $visitas = 0;
        $dias = array();

        /* HORARIOS */
        /* Calcular número de servicios, días trabajados, horas trabajadas, inasistencias, retardos */

        $now = new DateTime();

        foreach ($agendados['entrada'] as $date => $time) {
            $real_start_time = strtotime($horarios['entrada'][$date]);
            $real_end_time = strtotime($horarios['salida'][$date]);
            $tolerancia = strtotime("+ 15 minutes", strtotime($time));

            if ($horarios['entrada'][$date] == "") {
                if ($date <= $now->format("d-m-Y")) {

                    /*INASISTENCIA*/
                    if (!array_key_exists($date, $user_meta['Inasistencias'])) {
                        $user_meta['Inasistencias'][$date] = 1;
                    } else {
                        $user_meta['Inasistencias'][$date] += 1;

                    }
                }

            } else if ($real_start_time > $tolerancia) {
                /*RETARDO*/
                if (!array_key_exists($date, $user_meta['Retardos'])) {
                    $user_meta['Retardos'][$date] = 1;
                } else {
                    $user_meta['Retardos'][$date] += 1;
                }
            }

            if ($real_start_time != "" && $real_end_time != "") {
                /*HORAS TRABAJADAS*/
                if (!array_key_exists($date, $user_meta['Horas Trabajadas'])) {
                    $user_meta['Horas Trabajadas'][$date] = gmdate("H:i", $real_end_time - $real_start_time);
                } else {
                    $user_meta['Horas Trabajadas'][$date] += gmdate("H:i", $real_end_time - $real_start_time);
                }
            }

            /*TOTAL VISITAS*/
            if (!array_key_exists($date, $user_meta['Turnos Asignados'])) {
                $user_meta['Turnos Asignados'][$date] = 1;
                $visitas += 1;
            } else {
                $user_meta['Turnos Asignados'][$date] += 1;
                $visitas += 1;
            }

            if (!in_array($date, $dias)) {
                $dias[] = $date;

            }

        }
        /*COBERTURAS */
        foreach ($horarios['entrada'] as $date => $real_time) {
            if (!array_key_exists($date, $agendados['entrada'])) {
                $time = $agendados['entrada']['date'];
                $real_start_time = strtotime($horarios['entrada'][$date]);
                $real_end_time = strtotime($horarios['salida'][$date]);
                $tolerancia = strtotime("+ 15 minutes", strtotime($time));

                if ($real_start_time > $tolerancia) {
                    /*RETARDO*/
                    if (!array_key_exists($date, $user_meta['Retardos'])) {
                        $user_meta['Retardos'][$date] = 1;
                    } else {
                        $user_meta['Retardos'][$date] += 1;
                    }
                }

                if ($real_start_time != "" && $real_end_time != "") {
                    /*HORAS TRABAJADAS*/
                    if (!array_key_exists($date, $user_meta['Horas Trabajadas'])) {
                        $user_meta['Horas Trabajadas'][$date] = gmdate("H:i", $real_end_time - $real_start_time);
                    } else {
                        $user_meta['Horas Trabajadas'][$date] += gmdate("H:i", $real_end_time - $real_start_time);
                    }
                }

                /*TOTAL VISITAS*/
                if (!array_key_exists($date, $user_meta['Turnos Asignados'])) {
                    $user_meta['Turnos Asignados'][$date] = 1;
                    $visitas += 1;
                } else {
                    $user_meta['Turnos Asignados'][$date] += 1;
                    $visitas += 1;
                }

                if (!array_key_exists($date, $user_meta['Coberturas'])) {
                    $user_meta['Coberturas'][$date] = 1;
                } else {
                    $user_meta['Coberturas'][$date] += 1;
                }

                if (!in_array($date, $dias)) {
                    $dias[] = $date;

                }

            }

        }

        $data['calificacion'] = $this->get_calificacion($id, $start_day, $end_day);
        $data['servicios_por_dia'] = $visitas / sizeof($dias);

        $data['servicios_asignados'] = $this->get_servicios_asignados($id);

        $data['user_meta'] = $user_meta;

        if ($refresh == 0) {
            /*FIRST TIME */
            $data['page'] = $this->config->item('ci_my_admin_template_dir_admin') . "nurses_view";
            $this->load->view($this->_container, $data);

        } else {
            /*USING START - END FILTERS */
            $data['page'] = $this->config->item('ci_my_admin_template_dir_admin') . "nurses_view";
            $this->load->view($this->_container, $data);

            return json_encode($data);

        }
    }

    public function create()
    {
        if ($this->input->post('username')) {

            if (!$user) {
                $errors = $this->ion_auth->errors();
                echo $errors;
                die('done');
            } else {
                redirect('/admin/nurses', 'refresh');
            }

        }
        $data['services'] = '';
        $data['groups'] = $this->ion_auth->groups()->result();
        $data['page'] = $this->config->item('ci_my_admin_template_dir_admin') . "nurses_create";
        $data['characteristics'] = $this->options->get_all('*', array('option_name' => '_characterisctic'));
        $data['personalities'] = $this->options->get_all('*', array('option_name' => '_personality'));
        $data['hobbies'] = $this->options->get_all('*', array('option_name' => '_hobby'));
        $data['institutions'] = $this->options->get_all('*', array('option_name' => '_institution'));
        $data['care_skills'] = $this->options->get_all('*', array('option_name' => '_care_skill'));
        $data['therapy_skills'] = $this->options->get_all('*', array('option_name' => '_therapy_skill'));
        $data['recruitment_sources'] = $this->options->get_all('*', array('option_name' => '_recruitment_source'));
        $data['supervisors'] = $this->ion_auth->users('supervisor')->result();
        $data['max_escolaridad'] = $this->options->get_all('*', array('option_name' => '_max_escolaridad'));

        $this->db->select('*');
        $this->db->from('tipo_luz');
        $query_tipo_luz = $this->db->get();
        $data['tipos_luz'] = $query_tipo_luz->result();

        $this->load->view($this->_container, $data);

    }

    public function edit($id)
    {

        if ($this->input->post('first_name')) { //AILILPAULINA
            $data['first_name'] = $this->input->post('first_name');
            $data['last_name'] = $this->input->post('last_name');
            $data['email'] = $this->input->post('email');
            $data['phone'] = $this->input->post('phone');
            $current_user = $this->session->userdata();
            $data['password'] = $current_user['identity'];

            $group_id = $this->input->post('group_id');

            $this->ion_auth->remove_from_group('', $id);
            $this->ion_auth->add_to_group($group_id, $id);

            $this->ion_auth->update($id, $data);

            redirect('/admin/nurses', 'refresh');
        }

        $this->load->helper('ui');

        $data['supervisors'] = $this->ion_auth->users('supervisor')->result();
        $data['characteristics'] = $this->options->get_all('*', array('option_name' => '_characterisctic'));
        $data['personalities'] = $this->options->get_all('*', array('option_name' => '_personality'));
        $data['hobbies'] = $this->options->get_all('*', array('option_name' => '_hobby'));
        $data['institutions'] = $this->options->get_all('*', array('option_name' => '_institution'));
        $data['carer_skills'] = $this->options->get_all('*', array('option_name' => '_care_skill'));
        $data['therapy_skills'] = $this->options->get_all('*', array('option_name' => '_therapy_skill'));
        $data['recruitment_sources'] = $this->options->get_all('*', array('option_name' => '_recruitment_source'));
        $data['max_escolaridad'] = $this->options->get_all('*', array('option_name' => '_max_escolaridad'));

        $this->db->select('*');
        $this->db->from('tipo_luz');
        $query_tipo_luz = $this->db->get();
        $data['tipos_luz'] = $query_tipo_luz->result();

        $data['groups'] = $this->ion_auth->groups()->result();
        $data['user'] = $this->ion_auth->user($id)->row();

        $this->db->select("id, `meta_key`, `meta_value`");
        $this->db->from("users_meta");
        $this->db->where(array('user_id' => $id));
        $query = $this->db->get();
        $results = $query->result_array();

        $nurse_meta = array();

        foreach ($results as $result) {
            $nurse_meta[$result['meta_key']] = $result['meta_value'];
        }

        if (!array_key_exists('_numero_luz', $nurse_meta)) {
            $nurse_meta['_numero_luz'] = "";
        }
        if (!array_key_exists('_start_date', $nurse_meta)) {
            $nurse_meta['_start_date'] = "";
        }
        if (!array_key_exists('_supervisor', $nurse_meta)) {
            $nurse_meta['_supervisor'] = "";
        }

        if (!array_key_exists('_num_hijos', $nurse_meta)) {
            $nurse_meta['_num_hijos'] = "";
        }

        if (!array_key_exists('_referencia_phone1', $nurse_meta)) {
            $nurse_meta['_referencia_phone1'] = "";
        }

        if (!array_key_exists('_referencia_nombre1', $nurse_meta)) {
            $nurse_meta['_referencia_nombre1'] = "";
        }

        if (!array_key_exists('_referencia_phone2', $nurse_meta)) {
            $nurse_meta['_referencia_phone2'] = "";
        }

        if (!array_key_exists('_referencia_nombre2', $nurse_meta)) {
            $nurse_meta['_referencia_nombre2'] = "";
        }

        if (!array_key_exists('_anos_experiencia', $nurse_meta)) {
            $nurse_meta['_anos_experiencia'] = "";
        }

        if (!array_key_exists('_max_escolaridad', $nurse_meta)) {
            $nurse_meta['_max_escolaridad'] = "";
        }

        $meta_array = array();
        $selected = explode(",", $nurse_meta['_fisical']);
        foreach ($data['characteristics'] as $char) {
            if (in_array($char['id'], $selected)) {
                $char['selected'] = true;
            } else {
                $char['selected'] = false;
            }
            $meta_array[] = $char;
        }
        $nurse_meta['_fisical'] = $meta_array;

        $meta_array = array();
        $selected = explode(",", $nurse_meta['_personality']);
        foreach ($data['personalities'] as $char) {

            if (in_array($char['id'], $selected)) {
                $char['selected'] = true;
            } else {
                $char['selected'] = false;
            }
            $meta_array[] = $char;
        }
        $nurse_meta['_personalities'] = $meta_array;

        $meta_array = array();
        $selected = explode(",", $nurse_meta['_hobbies']);
        foreach ($data['hobbies'] as $char) {

            if (in_array($char['id'], $selected)) {
                $char['selected'] = true;
            } else {
                $char['selected'] = false;
            }
            $meta_array[] = $char;
        }
        $nurse_meta['_hobbies'] = $meta_array;

        $meta_array = array();
        $selected = explode(",", $nurse_meta['_carer_skills']);
        foreach ($data['carer_skills'] as $char) {

            if (in_array($char['id'], $selected)) {
                $char['selected'] = true;
            } else {
                $char['selected'] = false;
            }
            $meta_array[] = $char;
        }
        $nurse_meta['_carer_skills'] = $meta_array;

        $meta_array = array();
        $selected = explode(",", $nurse_meta['_therapist_skills']);
        foreach ($data['therapy_skills'] as $char) {

            if (in_array($char['id'], $selected)) {
                $char['selected'] = true;
            } else {
                $char['selected'] = false;
            }
            $meta_array[] = $char;
        }
        $nurse_meta['_therapist_skills'] = $meta_array;

        $this->load->helper('nombre_imagen');

        $nombre_img_perfil = get_nombre_imagen($nurse_meta['_profile_images']);
        if ($nombre_img_perfil) {
            $nurse_meta['_profile_images_nombre'] = $nombre_img_perfil;
        } else {
            $nurse_meta['_profile_images'] = "";
            $nurse_meta['_profile_images_nombre'] = "";
        }

        $nombre_img_ife = get_nombre_imagen($nurse_meta['_ife_images']);
        if ($nombre_img_ife) {
            $nurse_meta['_ife_images_nombre'] = $nombre_img_ife;
        } else {
            $nurse_meta['_ife_images'] = "";
            $nurse_meta['_ife_images_nombre'] = "";
        }

        $nombre_img_domicilio = get_nombre_imagen($nurse_meta['_CDomicilio_images']);
        if ($nombre_img_domicilio) {
            $nurse_meta['_CDomicilio_images_nombre'] = $nombre_img_domicilio;
        } else {
            $nurse_meta['_CDomicilio_images'] = "";
            $nurse_meta['_CDomicilio_images_nombre'] = "";
        }

        $nombre_img_curp = get_nombre_imagen($nurse_meta['_CURP_images']);
        if ($nombre_img_curp) {
            $nurse_meta['_CURP_images_nombre'] = $nombre_img_curp;
        } else {
            $nurse_meta['_CURP_images'] = "";
            $nurse_meta['_CURP_images_nombre'] = "";
        }

        $nombre_img_estudios = get_nombre_imagen($nurse_meta['_CEstudios_images']);
        if ($nombre_img_estudios) {
            $nurse_meta['_CEstudios_images_nombre'] = $nombre_img_estudios;
        } else {
            $nurse_meta['_CEstudios_images'] = "";
            $nurse_meta['_CEstudios_images_nombre'] = "";
        }

        $nombre_img_nacimiento = get_nombre_imagen($nurse_meta['_ANacimiento_images']);
        if ($nombre_img_nacimiento) {
            $nurse_meta['_ANacimiento_images_nombre'] = $nombre_img_nacimiento;
        } else {
            $nurse_meta['_ANacimiento_images'] = "";
            $nurse_meta['_ANacimiento_images_nombre'] = "";
        }

        $nombre_img_social = get_nombre_imagen($nurse_meta['_SSocial_images']);
        if ($nombre_img_social) {
            $nurse_meta['_SSocial_images_nombre'] = $nombre_img_social;
        } else {
            $nurse_meta['_SSocial_images'] = "";
            $nurse_meta['_SSocial_images_nombre'] = "";
        }

        $nombre_img_rfc = get_nombre_imagen($nurse_meta['_RFC_images']);
        if ($nombre_img_rfc) {
            $nurse_meta['_RFC_images_nombre'] = $nombre_img_rfc;
        } else {
            $nurse_meta['_RFC_images'] = "";
            $nurse_meta['_RFC_images_nombre'] = "";
        }

        $nombre_img_penales = get_nombre_imagen($nurse_meta['_penales_images']);
        if ($nombre_img_penales) {
            $nurse_meta['_penales_images_nombre'] = $nombre_img_penales;
        } else {
            $nurse_meta['_penales_images'] = "";
            $nurse_meta['_penales_images_nombre'] = "";
        }

        $nombre_img_seconomico = get_nombre_imagen($nurse_meta['_ESeconomico_images']);
        if ($nombre_img_seconomico) {
            $nurse_meta['_ESeconomico_images_nombre'] = $nombre_img_seconomico;
        } else {
            $nurse_meta['_ESeconomico_images'] = "";
            $nurse_meta['_ESeconomico_images_nombre'] = "";
        }

        $nombre_img_otros = get_nombre_imagen($nurse_meta['_otros_images']);
        if ($nombre_img_otros) {
            $nurse_meta['_otros_images_nombre'] = $nombre_img_otros;
        } else {
            $nurse_meta['_otros_images'] = "";
            $nurse_meta['_otros_images_nombre'] = "";
        }

        $data['nurse_id'] = $id;
        $data['nurse_meta'] = $nurse_meta;
        $data['user_group'] = $this->ion_auth->get_users_groups($id)->row();
        $data['page'] = $this->config->item('ci_my_admin_template_dir_admin') . "nurses_edit";
        $this->load->view($this->_container, $data);
    }

    public function pdf($id, $action)
    {

        $authorized = array('admin', 'supervisor');
        if (!in_array($this->ion_auth->get_users_groups()->row()->name, $authorized)) {
            redirect(site_url('admin/reports'), 'refresh');
        }

        if (!$action) {
            $action = $this->input->post('action');
        }

        if ($this->input->get("form") != "") {

            $query = $this->db->query("SELECT meta_value from users_meta
                                   where meta_key = '_nurse_report_file'
                                   and user_id = '" . $id . "'");

            $results = $query->result();
            if (sizeof($results) > 0) {

                $url = base_url("uploads/" . $results[0]->meta_value);

                /*$email = $this->input->post('Mail');
                $nombre = $this->input->post('Name');
                $message = $this->input->post('Message');*/
                $form = array();
                parse_str($this->input->get('form'), $form);
                $email = $form['Mail'];
                $nombre = $form['Name'];
                $message = $form['Message'];

                $subject = "[PAZ MENTAL] Envio de reportes";
                $mail_success = $this->phpmailerlib->sendEmail($email, $nombre, $subject, $message, $url);
                if ($mail_success == "success") {
                    $data_results = array(
                        "status" => "success",
                        "message" => "Mensaje enviado con éxito.",
                    );
                } else {
                    $data_results = array(
                        "status" => "failure",
                        "message" => "Error al enviar el mensaje." . $mail_success,
                    );
                }
            } else {
                $data_results = array(
                    "status" => "failure",
                    "message" => "No se ha generado el pdf para esta Luz. Favor de imprimir primero.",
                );
            }
            echo json_encode($data_results);

        } else {

            if ($action == "save") {
                $pdfdata = base64_decode($this->input->post('pdfdata'));
                $filename = $this->input->post('filename');
                if (file_exists("uploads/" . $filename)) {
                    unlink("uploads/" . $filename);
                }

                file_put_contents("uploads/" . $filename, $pdfdata);

                $query = $this->db->query("DELETE from users_meta where user_id = '" . $id . "' and meta_key = '_nurse_report_file'");
                $query = $this->db->query("INSERT INTO users_meta(user_id,meta_key,meta_value)
                            VALUES ('" . $id . "','_nurse_report_file','" . $filename . "')");
                $results = "success";
                echo json_encode($results);

            } else if ($action == "print") {
                $data_array = array('id' => $id);
                $data_array['nurse_supervisor'] = $this->get_service_reps($id, '_supervisor');
                $data_array['user'] = $this->ion_auth->user($id)->row();
                $nurse_meta = $this->db->select("id, `meta_key`, `meta_value`");
                $this->db->from("users_meta");
                $this->db->where(array('user_id' => $id));
                $query = $this->db->get();
                $results = $query->result_array();

                $nurse_meta = array();

                foreach ($results as $result) {

                    $nurse_meta[$result['meta_key']] = $result['meta_value'];

                }

                $data_array['nurse_meta'] = $nurse_meta;
                $data_array['fisical'] = $this->get_nurse_meta($nurse_meta['_fisical'], '_characterisctic');
                $data_array['personalities'] = $this->get_nurse_meta($nurse_meta['_personality'], '_personality');
                $data_array['hobbies'] = $this->get_nurse_meta($nurse_meta['_hobbies'], '_hobby');
                $data_array['carer_skills'] = $this->get_nurse_meta($nurse_meta['_carer_skills'], '_care_skill');
                $data_array['therapist_skills'] = $this->get_nurse_meta($nurse_meta['_therapist_skills'], '_therapy_skill');
                $data_array['estado_civil'] = $this->get_estado_civil($nurse_meta['_civil_status']);
                $data_array['system_status'] = $this->get_system_status($nurse_meta['_status']);
                $data_array['fuentes_reclutamiento'] = $this->get_nurse_meta($nurse_meta['_reclutamiento'], '_recruitment_source');
                $data_array['grado_estudios'] = $this->get_nurse_meta($nurse_meta['_grado_estudios'], '_institution');
                $data_array['logo'] = $this->getDataURI("/assets/build/img/pazmental-pdf.jpg");
                $data_array['header'] = $this->getDataURI("/assets/build/img/luz-pdf.jpg");
                $data_array['max_escolaridad'] = $this->get_max_escolaridad($nurse_meta['_max_escolaridad']);

                $profile_img = json_decode($nurse_meta["_profile_images"]);

                if (!is_array($profile_img)) {
                    $profile_img = json_decode($profile_img);
                }
                $data_array['img_url'] = $profile_img;
                $data_array['img_url2'] = base_url("uploads/files/" . $profile_img[0]);

                if (empty($profile_img)) {
                    $data_array['profile_img'] = $data_array['logo'];
                } else {
                    if (imagen_existe($profile_img[0])) {
                        $data_array['profile_img'] = $this->getDataURI("uploads/files/" . $profile_img[0]);
                    } else {
                        $data_array['profile_img'] = $data_array['logo'];
                    }
                }
                $data_array['nurse_id'] = $id;

                $data = json_encode($data_array);

                echo $data;
            } else {
                $this->load->view($this->config->item('ci_my_admin_template_dir_admin') . "modals/_send_report");

            }
        }

    }
    public function getDataURI($image, $mime = '')
    {
        $image = base_url($image);
        $type = pathinfo($image, PATHINFO_EXTENSION);
        $data = file_get_contents($image);
        $dataUri = 'data:image/' . $type . ';base64,' . base64_encode($data);
        return $dataUri;
    }

    public function get_system_status($status)
    {
        if ($status == "1") {
            return "Activo";
        } else if ($status == "0") {
            return "Suspendida";
        } else if ($status == "-1") {
            return "Baja";
        } else if ($status == "-2") {
            return "Renuncia";
        } else if (empty($status)) {
            return "No Asignado";
        }

    }

    public function get_estado_civil($civil_status)
    {
        if ($civil_status == 'single') {
            return 'Soltero (a)';
        } else if ($civil_status == 'married') {
            return 'Casado (a)';
        } else if ($civil_status == 'divorced') {
            return 'Divorciado (a)';
        } else if ($civil_status == 'widow') {
            return 'Viudo (a)';
        } else if (empty($civil_status)) {
            return "No Asignado";
        }

    }
    public function get_max_escolaridad($max_escolaridad)
    {
        $this->db->select('*');
        $this->db->from('options');
        $this->db->where('id', $max_escolaridad);

        $query = $this->db->get();

        $data = [];
        $nombre = '';
        if ($query->num_rows() > 0) {
            $data = $query->result_object();
            $escolaridad = json_decode($data[0]->option_value);
            $nombre = $escolaridad->name;
        }
        return $nombre;
    }
    public function get_service_reps($id, $type)
    {
        $query = $this->db->query("select first_name,last_name
                    from users_meta,users
                    where users_meta.user_id='$id'
                    and  meta_key='$type'
                    and users_meta.meta_value=users.id");
        $result = $query->result();

        if (!empty($result)) {
            return $result[0]->first_name . " " . $result[0]->last_name;
        } else {
            return "No Asignado";
        }
    }

    public function get_nurse_meta($data_array, $value)
    {
        if (empty($data_array)) {
            return "No Asignado";
        } else {
            $query = $this->db->query("select option_value
                        from options
                        where option_name = '$value'
                        and id in ($data_array)");
            $string = "select option_value
                        from options
                        where option_name = '$value'
                        and id in ($data_array)";
            $result = $query->result();
            $result_array = array();
            if (!empty($result)) {
                foreach ($result as $r) {
                    $ov = json_decode($r->option_value);
                    $result_array[] = $ov->name;
                }
                return $result_array;
            } else {
                return "No Asignado";
            }
        }
    }

    public function editar($id)
    {
        if ($this->input->post('first_name')) {
            $data['first_name'] = $this->input->post('first_name');
            $data['last_name'] = $this->input->post('last_name');
            $data['email'] = $this->input->post('email');
            $data['phone'] = $this->input->post('phone');
            $group_id = $this->input->post('group_id');

            $this->ion_auth->remove_from_group('', $id);
            $this->ion_auth->add_to_group($group_id, $id);

            $this->ion_auth->update($id, $data);

            redirect('/admin/nurses', 'refresh');
        }

        $this->load->helper('ui');

        $data['groups'] = $this->ion_auth->groups()->result();
        $data['user'] = $this->ion_auth->user($id)->row();
        $nurse_meta = $this->db->select("id, `meta_key`, `meta_value`");
        $this->db->from("users_meta");
        $this->db->where(array('user_id' => $id));
        $query = $this->db->get();
        $results = $query->result_array();

        $nurse_meta = array();

        foreach ($results as $result) {

            $nurse_meta[$result['meta_key']] = $result['meta_value'];

        }

        $data['nurse_id'] = $id;
        $data['nurse_meta'] = $nurse_meta;
        $data['user_group'] = $this->ion_auth->get_users_groups($id)->row();
        $data['page'] = $this->config->item('ci_my_admin_template_dir_admin') . "nurses_edit";
        $this->load->view($this->_container, $data);
    }

    public function delete($id)
    {
        $this->ion_auth->delete_user($id);

        redirect('/admin/nurses', 'refresh');
    }

    public function filter()
    {
        if ($this->input->is_ajax_request()) {

        } else {
            exit('No direct access allowed');
        }
    }

    public function save()
    {

        if ($this->input->is_ajax_request()) {

            $error_message = "";
            $validate = true;

            $data = json_decode($this->input->post('data'));
            $images = json_decode($this->input->post('images'));
            $fisical = json_decode($this->input->post('fisical'));
            $hobbies = json_decode($this->input->post('hobbies'));
            $carer_skills = json_decode($this->input->post('carer_skills'));
            $therapist_skills = json_decode($this->input->post('therapist_skills'));
            $personality = json_decode($this->input->post('personalities'));
            $map_data = json_decode($this->input->post('map_location'));

            if ($data->id_tipo_luz == '') {
                $validate = false;
                $error_message .= 'El Tipo de luz es obligatorio.<br/>';
            }

            if ($data->username == '') {
                $validate = false;
                $error_message .= 'El nombre de usuario es obligatorio.<br/>';
            }
            if ($data->email == '') {
                $validate = false;
                $error_message .= 'El email es obligatorio.<br/>';
            }
            if ($data->group_id == '') {
                $validate = false;
                $error_message .= 'El Grupo es obligatorio.<br/>';
            }
            if ($data->id_tipo_luz == '') {
                $validate = false;
                $error_message .= 'El Tipo de luz es obligatorio.<br/>';
            }
            if ($data->first_name == '') {
                $validate = false;
                $error_message .= 'Los nombres son obligatorios.<br/>';
            }
            if ($data->last_name == '') {
                $validate = false;
                $error_message .= 'Los apellidos son obligatorios.<br/>';
            }
            if ($data->age == '') {
                $validate = false;
                $error_message .= 'La Edad es obligatoria.<br/>';
            }
            if ($data->pm_gender == '') {
                $validate = false;
                $error_message .= 'El género es obligatorio.<br/>';
            }
            if ($data->civil_status == '') {
                $validate = false;
                $error_message .= 'El estado civil es obligatorio.<br/>';
            }
            if ($data->phone == '') {
                $validate = false;
                $error_message .= 'El teléfono de usuario es obligatorio.<br/>';
            }
            if ($data->address == '') {
                $validate = false;
                $error_message .= 'La dirección es obligatoria.<br/>';
            }
            if ($data->colony == '') {
                $validate = false;
                $error_message .= 'La colonia es obligatoria.<br/>';
            }
            if ($data->region == '') {
                $validate = false;
                $error_message .= 'La delegación o municipio es obligatoria.<br/>';
            }

            if (!property_exists($images, 'profiles')) {
                $validate = false;
                $error_message .= 'La imagen de perfil es obligatoria.<br/>';
            }

            if ($data->id_tipo_luz == 1) {

                if ($data->bank_name == '') {
                    $validate = false;
                    $error_message .= 'El banco es obligatorio.<br/>';
                }
                if ($data->bank_clabe == '') {
                    $validate = false;
                    $error_message .= 'La CLABE es obligatoria.<br/>';
                }
                if ($data->bank_account == '') {
                    $validate = false;
                    $error_message .= 'La cuenta de banco  es obligatoria.<br/>';
                }

                if (!property_exists($images, 'ife')) {
                    $validate = false;
                    $error_message .= 'La imagen de ife es obligatoria.<br/>';
                }
                if (!property_exists($images, 'CDomicilio')) {
                    $validate = false;
                    $error_message .= 'La imagen de comprobante de domiciio es obligatoria.<br/>';
                }
                if (!property_exists($images, 'CURP')) {
                    $validate = false;
                    $error_message .= 'La imagen de CURP es obligatoria.<br/>';
                }
                if (!property_exists($images, 'CEstudios')) {
                    $validate = false;
                    $error_message .= 'La imagen de comprobante de estudios es obligatoria.<br/>';
                }
                if (!property_exists($images, 'ANacimiento')) {
                    $validate = false;
                    $error_message .= 'La imagen de acta de nacimiento es obligatoria.<br/>';
                }
                if (!property_exists($images, 'SSocial')) {
                    $validate = false;
                    $error_message .= 'La imagen de seguro social es obligatoria.<br/>';
                }
                if (!property_exists($images, 'RFC')) {
                    $validate = false;
                    $error_message .= 'La imagen de RFC es obligatoria.<br/>';
                }
                if (!property_exists($images, 'penales')) {
                    $validate = false;
                    $error_message .= 'La imagen de Carta de antecedentes no penales es obligatoria.<br/>';
                }
                if (!property_exists($images, 'ESeconomico')) {
                    $validate = false;
                    $error_message .= 'La imagen de estudio socioeconómico es obligatoria.<br/>';
                }

                if (sizeof($map_data) < 2) {
                    $validate = false;
                    $error_message .= 'La latitud y longitud son obligatorias.<br/>';
                }

                if (!property_exists($data, 'carer_skills')) {
                    $validate = false;
                    $error_message .= 'Las habilidades de cuidador son obligatorias.<br/>';
                }
                if (!property_exists($data, 'therapist_skills')) {
                    $validate = false;
                    $error_message .= 'Las habilidades de terapeuta son obligatorias.<br/>';
                }
                if (!property_exists($data, 'personalities')) {
                    $validate = false;
                    $error_message .= 'La personalidad es obligatoria.<br/>';
                }
                if (!property_exists($data, 'hobbies')) {
                    $validate = false;
                    $error_message .= 'Los pasatiempos son obligatorios.<br/>';
                }
                if (!property_exists($data, 'fisical')) {
                    $validate = false;
                    $error_message .= 'Las características físicas son obligatorias.<br/>';
                }

                if ($data->reclutamiento == '') {
                    $validate = false;
                    $error_message .= 'Las fuentes de reclutamiento son obligatorias.<br/>';
                }
                if ($data->grado_estudios == '') {
                    $validate = false;
                    $error_message .= 'El grado de estudios es obligatorio.<br/>';
                }

                if ($data->start_date == '') {
                    $validate = false;
                    $error_message .= 'La fecha de alta es obligatioria.<br/>';
                }

                if ($data->curp == '') {
                    $validate = false;
                    $error_message .= 'El CURP es obligatorio.<br/>';
                }

                if ($data->birth_date == '') {
                    $validate = false;
                    $error_message .= 'La fecha de nacimiento es obligatoria.<br/>';
                }

                if ($data->city == '') {
                    $validate = false;
                    $error_message .= 'La ciudad o municipio es obligatoria.<br/>';
                }

                if ($data->zip == '') {
                    $validate = false;
                    $error_message .= 'El código postal es obligatorio.<br/>';
                }

                if ($data->status == '') {
                    $validate = false;
                    $error_message .= 'El status es obligatorio.<br/>';
                }

                if ($data->mobile == '') {
                    $validate = false;
                    $error_message .= 'El celular es obligatorio.<br/>';
                }

                if ($data->economical_dependants == '') {
                    $validate = false;
                    $error_message .= 'Los dependientes económicos son  obligatorios.<br/>';
                }

            }

            $results = array(
                'action' => 'append',
                'status' => 'failure',
                'element' => 'message_center',
                'code' => "",
            );

            if ($validate == false) {
                $results = array(
                    'action' => 'append',
                    'status' => 'failure',
                    'element' => 'message_center',
                    'code' => "Favor de corregir los siguientes errores: <br/>" . $error_message,
                );
            } else {

                $username = $data->username;
                $password = $data->password;
                $email = $data->email;

                if (!$this->ion_auth->email_check($email)) {

                    $group_id = array($data->group_id);

                    $additional_data = array(
                        'first_name' => $data->first_name,
                        'last_name' => $data->last_name,
                        'username' => $data->username,
                        'phone' => $data->phone,
                        'id_tipo_luz' => (int) $data->id_tipo_luz,
                    );

                    $meta_data = array(
                        '_curp' => $data->curp,
                        '_birthday' => $data->birth_date,
                        '_age' => $data->age,
                        '_gender' => $data->pm_gender,
                        '_civil_status' => $data->civil_status,
                        '_economical_dependants' => $data->economical_dependants,
                        '_address' => $data->address,
                        '_colony' => $data->colony,
                        '_region' => $data->region,
                        '_city' => $data->city,
                        '_zip' => $data->zip,
                        '_emergency_name' => $data->emergency_name,
                        '_emergency_last_name' => $data->emergency_last_name,
                        '_emergency_relation' => $data->emergency_relation,
                        '_emergency_phone' => $data->emergency_phone,
                        '_mobile' => $data->mobile,
                        '_status' => $data->status,
                        '_reclutamiento' => $data->reclutamiento,
                        '_grado_estudios' => $data->grado_estudios,
                        '_fire_reason' => $data->fire_reason,
                        '_observaciones_internas' => $data->observaciones_internas,
                        '_luz_confiable' => $data->luz_confiable,
                        '_bank_name' => $data->bank_name,
                        '_bank_account' => $data->bank_account,
                        '_bank_clabe' => $data->bank_clabe,
                        '_map_lat' => $map_data[0],
                        '_map_long' => $map_data[1],
                        '_numero_luz' => $data->numero_luz,
                        '_start_date' => $data->start_date,
                        '_supervisor' => $data->supervisor,

                        '_num_hijos' => $data->num_hijos,
                        '_referencia_phone1' => $data->referencia_phone1,
                        '_referencia_nombre1' => $data->referencia_nombre1,
                        '_referencia_phone2' => $data->referencia_phone2,
                        '_referencia_nombre2' => $data->referencia_nombre2,
                        '_anos_experiencia' => $data->anos_experiencia,
                        '_max_escolaridad' => $data->max_escolaridad,
                    );

                    if ($data->economical_dependants > 0) {
                        $meta_data["_dependant_name_0"] = $data->dependant_name_0;
                        $meta_data["_dependant_age_0"] = $data->dependant_age_0;
                        $meta_data["_dependant_dob_0"] = $data->dependant_dob_0;
                    }

                    if ($data->economical_dependants > 1) {
                        $meta_data["_dependant_name_1"] = $data->dependant_name_1;
                        $meta_data["_dependant_age_1"] = $data->dependant_age_1;
                        $meta_data["_dependant_dob_1"] = $data->dependant_dob_1;
                    }

                    if ($data->economical_dependants > 2) {
                        $meta_data["_dependant_name_2"] = $data->dependant_name_2;
                        $meta_data["_dependant_age_2"] = $data->dependant_age_2;
                        $meta_data["_dependant_dob_2"] = $data->dependant_dob_2;
                    }

                    if ($data->economical_dependants > 3) {
                        $meta_data["_dependant_name_3"] = $data->dependant_name_3;
                        $meta_data["_dependant_age_3"] = $data->dependant_age_3;
                        $meta_data["_dependant_dob_3"] = $data->dependant_dob_3;
                    }

                    if ($data->economical_dependants > 4) {
                        $meta_data["_dependant_name_4"] = $data->dependant_name_4;
                        $meta_data["_dependant_age_4"] = $data->dependant_age_4;
                        $meta_data["_dependant_dob_4"] = $data->dependant_dob_4;
                    }

                    if ($data->economical_dependants > 5) {
                        $meta_data["_dependant_name_5"] = $data->dependant_name_5;
                        $meta_data["_dependant_age_5"] = $data->dependant_age_5;
                        $meta_data["_dependant_dob_5"] = $data->dependant_dob_5;
                    }

                    $mc = array(
                        '_fisical' => $fisical->fisical,
                        '_carer_skills' => $carer_skills->carer_skills,
                        '_therapist_skills' => $therapist_skills->therapist_skills,
                        '_personality' => $personality->personalities,
                        '_hobbies' => $hobbies->hobbies,
                    );

                    foreach ($mc as $mckey => $mcvalue) {
                        $md = array();
                        foreach ($mcvalue as $key => $value) {
                            foreach ($value as $k => $v) {
                                if ($v == 1) {
                                    $md[] = $k;
                                }
                            }
                        }
                        $meta_data[$mckey] = implode(",", $md);
                    }

                    $user = $this->ion_auth->register($email, $password, $email, $additional_data, $group_id);

                    if (!$user) {
                        $errors = $this->ion_auth->errors();
                        $results = array(
                            'status' => 'error',
                            'message' => $errors,
                        );

                    } else {
                        foreach ($meta_data as $meta => $data) {
                            if (is_array($data)) {$data = serialize($data);}
                            $insert_data = array(
                                'id' => '',
                                'user_id' => $user,
                                'meta_key' => $meta,
                                'meta_value' => $data,
                            );

                            $this->db->insert('users_meta', $insert_data);
                        }

                        $mc = array(
                            '_profile_images' => $images->profiles,
                            '_ife_images' => $images->ife,
                            '_CDomicilio_images' => $images->CDomicilio,
                            '_CURP_images' => $images->CURP,
                            '_CEstudios_images' => $images->CEstudios,
                            '_ANacimiento_images' => $images->ANacimiento,
                            '_SSocial_images' => $images->SSocial,
                            '_RFC_images' => $images->RFC,
                            '_penales_images' => $images->penales,
                            '_ESeconomico_images' => $images->ESeconomico,
                            '_otros_images' => $images->otros,

                        );

                        foreach ($mc as $mckey => $mcvalue) {
                            $insert_images = array(
                                'id' => '',
                                'user_id' => $user,
                                'meta_key' => $mckey,
                                'meta_value' => json_encode($mcvalue),
                            );

                            $this->db->insert('users_meta', $insert_images);
                        }

                        $results = array(
                            'status' => 'success',
                            'action' => 'redirect',
                            'module' => "" . base_url() . 'admin' . "/nurses",
                            'message' => 'Se registro exitosamente',
                        );
                    }
                } else {
                    $results = array(
                        'status' => 'fail',
                        'code' => 'Ya existe un usuario con esa cuenta de correo',
                    );
                }
            }

            echo json_encode($results);

        } else {
            exit('No direct access allowed');
        }
    }

    public function select()
    {
        if ($this->input->is_ajax_request()) {

            if ($this->input->post('action') == 'get') {

                if ($this->input->post('module') == "dependents") {

                    $dependent_num = $this->input->post('Nurses');
                    if ($dependent_num > 0) {

                        $data = json_decode($dependent_num);

                        echo $data;

                    }
                }
            }
        } else {
            exit('No direct access allowed');
        }

    }

    public function update()
    {

        if ($this->input->is_ajax_request()) {
            $validate = true;
            $error_message = '';
            $data = json_decode($this->input->post('data'));
            $images = json_decode($this->input->post('images'));

            if ($data->username == '') {
                $validate = false;
                $error_message .= 'El nombre de usuario es obligatorio.<br/>';
            }
            if ($data->email == '') {
                $validate = false;
                $error_message .= 'El email es obligatorio.<br/>';
            }
            if ($data->group_id == '') {
                $validate = false;
                $error_message .= 'El Grupo es obligatorio.<br/>';
            }
            if ($data->id_tipo_luz == '') {
                $validate = false;
                $error_message .= 'El Tipo de luz es obligatorio.<br/>';
            }
            if ($data->first_name == '') {
                $validate = false;
                $error_message .= 'Los nombres son obligatorios.<br/>';
            }
            if ($data->last_name == '') {
                $validate = false;
                $error_message .= 'Los apellidos son obligatorios.<br/>';
            }
            if ($data->age == '') {
                $validate = false;
                $error_message .= 'La Edad es obligatoria.<br/>';
            }
            if ($data->pm_gender == '') {
                $validate = false;
                $error_message .= 'El género es obligatorio.<br/>';
            }
            if ($data->civil_status == '') {
                $validate = false;
                $error_message .= 'El estado civil es obligatorio.<br/>';
            }
            if ($data->phone == '') {
                $validate = false;
                $error_message .= 'El teléfono de usuario es obligatorio.<br/>';
            }
            if ($data->address == '') {
                $validate = false;
                $error_message .= 'La dirección es obligatoria.<br/>';
            }
            if ($data->colony == '') {
                $validate = false;
                $error_message .= 'La colonia es obligatoria.<br/>';
            }
            if ($data->region == '') {
                $validate = false;
                $error_message .= 'La delegación o municipio es obligatoria.<br/>';
            }
            if ($this->email_exist_not_id() == false) {
                $validate = false;
                $error_message .= 'El email ya esta siendo utilizado por otro usuario';
            }

            log_message("debug", "----  OBJECT IMAGES ----");
            log_message("debug", json_encode($images));

            /*
            if (($data->myDropzone_profile_files == '') || (!property_exists($images, 'profiles'))) {
            $validate = false;
            $error_message .= 'La imagen de perfil es obligatoria.<br/>';
            }*/

            $results = array(
                'status' => 'error',
                'action' => '',
                'module' => "",
                'code' => 'Datos no procesados',
            );

            if ($validate == false) {
                $results = array(
                    'status' => 'error',
                    'action' => '',
                    'module' => "",
                    'code' => $error_message,
                );
            } else {
                $user_id = $data->user_id;

                $username = $data->username;
                $password = $data->password;
                $email = $data->email;

                $group_id = array($data->group_id);
                $map_data = json_decode($this->input->post('map_location'));
                $current_user = $this->session->userdata();

                $user_data = array(
                    'username' => $username,
                    'email' => $email,
                    'first_name' => $data->first_name,
                    'last_name' => $data->last_name,
                    'username' => $data->username,
                    'phone' => $data->phone,
                    'modifiedon' => date("Y-m-d h:i:sa"),
                    'modifiedby' => $current_user['identity'],
                    'id_tipo_luz' => (int) $data->id_tipo_luz,
                );

                $meta_data = array(
                    '_curp' => $data->curp,
                    '_mobile' => $data->mobile,
                    '_birthday' => $data->birth_date,
                    '_age' => $data->age,
                    '_gender' => $data->pm_gender,
                    '_civil_status' => $data->civil_status,
                    '_economical_dependants' => $data->economical_dependants,
                    '_address' => $data->address,
                    '_colony' => $data->colony,
                    '_region' => $data->region,
                    '_city' => $data->city,
                    '_zip' => $data->zip,
                    '_emergency_name' => $data->emergency_name,
                    '_emergency_last_name' => $data->emergency_last_name,
                    '_emergency_relation' => $data->emergency_relation,
                    '_emergency_phone' => $data->emergency_phone,
                    '_status' => $data->status,
                    '_start_date' => $data->start_date,
                    '_fire_reason' => $data->fire_reason,
                    '_observaciones_internas' => $data->observaciones_internas,
                    '_luz_confiable' => $data->luz_confiable,
                    '_reclutamiento' => $data->reclutamiento,
                    '_grado_estudios' => $data->grado_estudios,
                    '_bank_name' => $data->bank_name,
                    '_bank_account' => $data->bank_account,
                    '_bank_clabe' => $data->bank_clabe,
                    '_numero_luz' => $data->numero_luz,
                    '_supervisor' => $data->supervisor,

                    '_num_hijos' => $data->num_hijos,
                    '_referencia_phone1' => $data->referencia_phone1,
                    '_referencia_nombre1' => $data->referencia_nombre1,
                    '_referencia_phone2' => $data->referencia_phone2,
                    '_referencia_nombre2' => $data->referencia_nombre2,
                    '_anos_experiencia' => $data->anos_experiencia,
                    '_max_escolaridad' => $data->max_escolaridad,
                );

                if ($data->economical_dependants > 0) {
                    $meta_data["_dependant_name_0"] = $data->dependant_name_0;
                    $meta_data["_dependant_age_0"] = $data->dependant_age_0;
                    $meta_data["_dependant_dob_0"] = $data->dependant_dob_0;
                }

                if ($data->economical_dependants > 1) {
                    $meta_data["_dependant_name_1"] = $data->dependant_name_1;
                    $meta_data["_dependant_age_1"] = $data->dependant_age_1;
                    $meta_data["_dependant_dob_1"] = $data->dependant_dob_1;
                }

                if ($data->economical_dependants > 2) {
                    $meta_data["_dependant_name_2"] = $data->dependant_name_2;
                    $meta_data["_dependant_age_2"] = $data->dependant_age_2;
                    $meta_data["_dependant_dob_2"] = $data->dependant_dob_2;
                }

                if ($data->economical_dependants > 3) {
                    $meta_data["_dependant_name_3"] = $data->dependant_name_3;
                    $meta_data["_dependant_age_3"] = $data->dependant_age_3;
                    $meta_data["_dependant_dob_3"] = $data->dependant_dob_3;
                }

                if ($data->economical_dependants > 4) {
                    $meta_data["_dependant_name_4"] = $data->dependant_name_4;
                    $meta_data["_dependant_age_4"] = $data->dependant_age_4;
                    $meta_data["_dependant_dob_4"] = $data->dependant_dob_4;
                }
                if ($data->economical_dependants > 5) {
                    $meta_data["_dependant_name_5"] = $data->dependant_name_5;
                    $meta_data["_dependant_age_5"] = $data->dependant_age_5;
                    $meta_data["_dependant_dob_5"] = $data->dependant_dob_5;
                }

                $meta_data['_map_lat'] = $map_data[0];
                $meta_data['_map_long'] = $map_data[1];

                $fisical = json_decode($this->input->post('fisical'));

                $md = array();
                foreach ($fisical->fisical as $key => $value) {
                    foreach ($value as $k => $v) {
                        if ($v == 1) {
                            $md[] = $k;
                        }
                    }
                }
                $meta_data["_fisical"] = implode(",", $md);

                $hobbies = json_decode($this->input->post('hobbies'));

                $md = array();
                foreach ($hobbies->hobbies as $key => $value) {
                    foreach ($value as $k => $v) {
                        if ($v == 1) {
                            $md[] = $k;
                        }
                    }
                }
                $meta_data["_hobbies"] = implode(",", $md);

                $carer_skills = json_decode($this->input->post('carer_skills'));

                $md = array();
                foreach ($carer_skills->carer_skills as $key => $value) {
                    foreach ($value as $k => $v) {
                        if ($v == 1) {
                            $md[] = $k;
                        }
                    }
                }
                $meta_data["_carer_skills"] = implode(",", $md);

                $therapist_skills = json_decode($this->input->post('therapist_skills'));

                $md = array();
                foreach ($therapist_skills->therapist_skills as $key => $value) {
                    foreach ($value as $k => $v) {
                        if ($v == 1) {
                            $md[] = $k;
                        }
                    }
                }
                $meta_data["_therapist_skills"] = implode(",", $md);

                $personality = json_decode($this->input->post('personalities'));
                $md = array();
                foreach ($personality->personalities as $key => $value) {
                    foreach ($value as $k => $v) {
                        if ($v == 1) {
                            $md[] = $k;
                        }
                    }
                }
                $meta_data["_personality"] = implode(",", $md);

                $nurse_meta = $this->db->select("`meta_key`");
                $this->db->from("users_meta");
                $this->db->where(array('user_id' => $user_id));
                $query = $this->db->get();
                $results = $query->result_array();

                $existing_meta_values = array();
                foreach ($results as $result) {
                    $existing_meta_values[] = $result['meta_key'];
                }

                // First we removed this user from all groups
                $this->ion_auth->remove_from_group(null, $user_id);
                // then add him to selected group
                $this->ion_auth->add_to_group($group_id, $user_id);

                $user = $this->ion_auth->update($user_id, $user_data);

                if ($user) {
                    $image_meta_keys = array(
                        'profiles' => "_profile_images",
                        'ife' => '_ife_images',
                        'CDomicilio' => '_CDomicilio_images',
                        'CURP' => '_CURP_images',
                        'CEstudios' => '_CEstudios_images',
                        'ANacimiento' => '_ANacimiento_images',
                        "SSocial" => '_SSocial_images',
                        "RFC" => "_RFC_images",
                        "penales" => "_penales_images",
                        "ESeconomico" => "_ESeconomico_images",
                        "otros" => "_otros_images",
                    );

                    foreach ($images as $key => $value) {
                        $insert_data = array(
                            'meta_value' => $value,
                        );

                        $this->db->where('user_id', $user_id);
                        $this->db->where('meta_key', $image_meta_keys[$key]);
                        $this->db->update('users_meta', $insert_data);
                        if ($this->db->affected_rows() == 0) {
                            $insert_data = array(
                                'id' => '',
                                'user_id' => $user_id,
                                'meta_key' => $image_meta_keys[$key],
                                'meta_value' => $value,
                            );

                            $this->db->insert('users_meta', $insert_data);
                        }

                    }

                    foreach ($meta_data as $meta => $data) {
                        $insert_data = array(
                            'meta_value' => $data,
                        );

                        $this->db->where('user_id', $user_id);
                        $this->db->where('meta_key', $meta);

                        if (in_array($meta, $existing_meta_values)) {
                            $this->db->update('users_meta', $insert_data);
                        } else {
                            $insert_data = array(
                                'id' => '',
                                'user_id' => $user_id,
                                'meta_key' => $meta,
                                'meta_value' => $data,
                            );

                            $this->db->insert('users_meta', $insert_data);
                        }
                    }

                    $results = array(
                        'status' => 'success',
                        'action' => 'redirect',
                        'module' => base_url('admin/nurses'),
                        'message' => 'Se guardaron los cambios exitosamente',
                    );
                } else {
                    $results = array(
                        'status' => 'error',
                        'action' => '',
                        'module' => '',
                        'code' => $this->ion_auth->errors(),
                    );
                }
            }

            echo json_encode($results);

        } else {
            exit('No direct access allowed');
        }
    }

    public function email_exist_not_id()
    {
        $data = json_decode($this->input->post('data'));

        $this->db->where('email', $data->email);
        $this->db->where_not_in('id', $data->user_id);
        $conta = $this->db->count_all_results('users');

        $valid = true;
        if ($conta > 0) {
            $valid = false;
        }

        return $valid;
    }

    public function schedule()
    {

        if ($this->input->is_ajax_request()) {

            $nurse_id = $this->input->post('nurse_id');

            $current_service = $this->input->post('current_service');

            if ($nurse_id == '') {

                $nurse_id = $this->input->post('target');
            }

            $type = $this->input->post('typeSchedule');

            $date_selected = explode(',', $this->input->post('date_selected'));

            $user_groups = $this->ion_auth->get_users_groups($nurse_id)->result();

            $start_date = $this->input->post('start_date');

            $end_date = $this->input->post('end_date');

            $date_range = array('Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado', 'Domingo');

            $time_range = time_range('00:00', '24:00');

            $html = '';

            if ($this->input->post('action') == 'asign') {
                $data['end_time'] = $date_selected[2];
                $data['time_selected'] = $date_selected[1];
                $data['date_selected'] = $date_selected[0];
                $data['nurse_id'] = $nurse_id;
                $data['time_range'] = $time_range;
                $data['module'] = $this->input->post('module');

                $this->db->select("*");
                $this->db->from("schedule_fechas_arranque");
                $this->db->where("nurse_id", $nurse_id);
                $this->db->where("service_id", $current_service);

                $query = $this->db->get();

                $data['fecha_arranque'] = '';
                if ($query->num_rows() > 0) {
                    $result_fecha = $query->result();
                    $date_fecha_arranque = new DateTime($result_fecha[0]->fecha_arranque);
                    $data['fecha_arranque'] = $date_fecha_arranque->format("Y-m-d");
                }

                $this->load->view($this->config->item('ci_my_admin_template_dir_admin') . "modals/_nurse_asign_schedule", $data);

            } else {

                if ($nurse_id != '' && $nurse_id > 0 && $user_groups[0]->name == 'nurses') {

                    switch ($this->input->post('action')) {

                        case 'get':

                            $this->db->select('`service_id`,`date`,`times`');
                            $this->db->where('date >=', $start_date);
                            $this->db->where('date <=', $end_date);
                            $this->db->where('nurse_id', $nurse_id);
                            $query = $this->db->get('schedule');

                            log_message('debug', $this->db->last_query());
                            $results = $query->result_array();
                            $results_array = array();

                            if (count($results) > 0) {

                                $results_array = array();

                                foreach ($results as $result) {
                                    $service_data = $this->service->get($result['service_id']);

                                    array_push($results_array, array(
                                        'nurse_id' => $nurse_id,
                                        'date' => $result['date'],
                                        'times' => $result['times'],
                                        'service' => json_encode(array('id' => $result['service_id'], 'name' => json_decode($service_data->name))),

                                    ));
                                }
                            }

                            echo json_encode($results_array);

                            break;
                        case 'save':

                            $date_selected = $this->input->post('date_selected');

                            $start_time = $this->input->post('startDate');

                            $start_time = strtotime("-30 minutes", strtotime($start_time));

                            $start_time = date('h:ia', $start_time);

                            $end_time = $this->input->post('endDate');

                            $time_range = time_range($start_time, $end_time);

                            //191;Lunes;08:00am;1;1;2018-06-12

                            $code = json_encode(array('times' => $time_range, 'date' => $date_selected, 'nurse_id' => $nurse_id, 'stat' => $type, 'fecha_arranque' => $this->input->post('fecha_arranque')));

                            $results = array(
                                'action' => 'append',
                                'element' => 'assign',
                                'code' => $code,
                                'status' => 'true',
                            );

                            echo json_encode($results);

                            break;
                        case 'remove':
                            break;

                        case 'load':

                            $schedule = $this->get_nurses_schedule($nurse_id);
                            $cells_drawn = array();
                            foreach ($time_range as $time):
                                $time_id = str_replace(':', '', $time);

                                $html .= '<tr id="' . $time_id . '">
					                                     <td>' . $time . '</td>';

                                foreach ($date_range as $date):
                                    if (array_key_exists($date, $schedule)) {

                                        if (array_key_exists($time, $schedule[$date])) {
                                            if ($schedule[$date][$time]['status'] == 0) {
                                                $css = "background-color:#efefef;color:#405467";
                                                $stat_txt = "reservado";} else {
                                                $css = "background-color:rgb(163, 212, 178);color:#405467";
                                                $stat_txt = "asignado";
                                            }

                                            foreach ($schedule[$date][$time]['times'] as $ts) {
                                                $time_tuple = $nurse_id . ',' . $date . ',' . $ts;
                                                $cells_drawn[] = $time_tuple;

                                            }

                                            $rowspan = sizeof($schedule[$date][$time]['times']);
                                            $end_time = end($schedule[$date][$time]['times']);

                                            if ($schedule[$date][$time]['id'] == $current_service) {
                                                $html .= '<td rowspan="' . $rowspan . '" style="' . $css . '" id="' . $date . '">

										                                                     <a href="' . base_url() . 'admin/nurses/schedule/" id="schedule_nurse" data-rel="asign" title="Asignar Luz" class="btn btn-round btn-default nursebox" data-target="' . $nurse_id . '" data-time="' . $date . ',' . $time . ',' . $end_time . '"><i class="fa fa-plus"></i></a>
										                                                      <br/>Actualmente <br/>' . $stat_txt . '</td>';
                                            } else {

                                                $html .= ' <td rowspan="' . $rowspan . '" style="' . $css . '"  id="' . $date . '">' .
                                                    '<a href="#" data-toggle="tooltip" title="' . $schedule[$date][$time]['name'] . '">' . $schedule[$date][$time]['id']
                                                    . '</a></td>';
                                            }

                                        } else {

                                            $time_tuple = $nurse_id . ',' . $date . ',' . $time;
                                            $key = array_search($time_tuple, $cells_drawn);
                                            if (!$key) {
                                                $html .= '<td id="' . $date . '">

										                                                     <a href="' . base_url() . 'admin/nurses/schedule/" id="schedule_nurse" data-rel="asign" title="Asignar Luz" class="btn btn-round btn-default nursebox" data-target="' . $nurse_id . '" data-time="' . $date . ',' . $time . '"><i class="fa fa-plus"></i></a>
										                                                      </td>';
                                            }
                                        }
                                    } else {

                                        $time_tuple = $nurse_id . ',' . $date . ',' . $time;
                                        $key = array_search($time_tuple, $cells_drawn);

                                        if (!$key) {
                                            $html .= '<td id="' . $date . '">

										                                                     <a href="' . base_url() . 'admin/nurses/schedule/" id="schedule_nurse" data-rel="asign" title="Asignar Luz" class="btn btn-round btn-default nursebox" data-target="' . $nurse_id . '" data-time="' . $date . ',' . $time . '"><i class="fa fa-plus"></i></a>
										                                                      </td>';
                                        }

                                    }
                                endforeach;
                                $html .= '</tr>';
                            endforeach;

                            echo $html;
                            break;

                    }

                }

            }
        } else {

            exit('No direct access allowed');
        }
    }

    public function get_nurses_schedule($nurse_id)
    {

        $query = $this->db->query("select schedule.*,services.name from schedule,services where nurse_id=" . $nurse_id . " and schedule.service_id = services.id");
        $results = $query->result_array();
        $schedule = array();

        foreach ($results as $r) {
            if (empty($schedule[$r['date']])) {
                $schedule[$r['date']] = array();
            }
            $times = json_decode($r['times']);
            $t = $times[0];
            $schedule[$r['date']][$t] = array();
            $schedule[$r['date']][$t]['id'] = $r['service_id'];
            $schedule[$r['date']][$t]['status'] = $r['status'];
            $schedule[$r['date']][$t]['times'] = $times;
            $name = json_decode($r['name']);
            $schedule[$r['date']][$t]['name'] = $name->first_name . " " . $name->second_name . " " . $name->last_name . " " . $name->second_last_name;

        }
        return $schedule;

    }

    public function get_status($nurse_id)
    {
        $query = $this->db->query("select meta_value from users_meta where user_id=" . $nurse_id . " and meta_key='_status' limit 1");
        $results = $query->result_array();
        $status = $results[0]['meta_value'];

        if ($status == 1) {return "Activo";} else if ($status == 0) {return "Suspendida";} else if ($status == -1) {return "Baja";} else {return "Renuncia";}

    }

    public function get_antiguedad($nurse_id)
    {
        /*ANTIGUEDAD*/
        $this->db->select("*");
        $this->db->from("users_meta");
        $this->db->where("meta_key", "_start_date");
        $this->db->where("user_id", $nurse_id);
        $query = $this->db->get();
        $results = $query->result_array();
        if (empty($results)) {
            return "N/A";
        } else {
            return $results[0]['meta_value'];
        }
    }

    public function get_calificacion($nurse_id, $start_date, $end_date)
    {
        /*calificacion*/
        $user_ids_query = "SELECT DISTINCT relation_value as user_id
                    from services_relationship
                        where relation_term = 'familiar'
                        and service_id in (SELECT DISTINCT service_id from agenda
                        where date(start_time) >= '$start_date'
                        and date(start_time) <= '$end_date'
                        and nurse_id = '$nurse_id')";

        $query = $this->db->query($user_ids_query);
        $results = $query->result_array();

        log_message("debug", "-------   query get_calificacion ------");
        log_message("debug", $this->db->last_query());
        log_message("debug", "-------   query get_calificacion ------");

        if (empty($results)) {
            return "N/A";
        } else {
            $user_ids = array();
            foreach ($results as $r) {
                if ($r['user_id'] != "") {
                    $user_ids[] = $r['user_id'];
                }
            }

            $results = array();
            if ((isset($user_ids)) && (count($user_ids) > 0)) {
                $sum_query = " SELECT sum(meta_value) as rate_sum, count(*) as rate_count from users_meta
                       where meta_key = '_Rating_Luz'
                       and user_id in (" . implode(",", $user_ids) . ") ";
                $query = $this->db->query($sum_query);
                $results = $query->result_array();
            }

            if (empty($results)) {
                return "N/A";
            } else {
                $count = sizeof($results);
                $calif = ($results[0]['rate_sum']) / ((float) $results[0]['rate_count']);
                if ($calif == "") {
                    return "N/A";
                } else {
                    return $calif;
                }
            }
        }
    }

    public function get_servicios_asignados($nurse_id)
    {
        /*ANTIGUEDAD*/
        // $query = $this->db->query("SELECT id,name,tars_id from services where id in (SELECT service_id from schedule where nurse_id = " . $nurse_id . ")");

        $this->db->select("services.id, services.tars_id", false);
        $this->db->select("pacientes.name", false);
        $this->db->from("services");
        $this->db->join("pacientes", "services.paciente_id = pacientes.id", "inner");
        $this->db->where("services.id in (SELECT service_id from schedule where nurse_id = " . $nurse_id . ")");

        $results = $query->result_array();
        $services = array();
        foreach ($results as $result) {

            $name = json_decode($result['name']);
            $services[$result['id']] = $name->last_name . " " . $name->second_last_name . "(" . $result['tars_id'] . ")";
        }

        return $services;
    }

    public function get_supervisor($id)
    {
        $query = $this->db->query("SELECT id,first_name,last_name from users where id = (SELECT meta_value from users_meta where user_id = " . $id . " and meta_key='_supervisor')");
        $results = $query->result_array();
        return $results;
    }

    public function get_all_colonies()
    {
        $query = $this->db->query("SELECT DISTINCT meta_value as colony from users_meta where meta_key='_colony'");
        $results = $query->result_array();
        return $results;
    }

    public function img_download()
    {

        $this->load->helper('download');

        $img = $this->input->post('nombre_file');

        if (empty($img)) {
            die('Imagen no valida');
        }

        force_download(DIRECTORIO_IMAGENES_NURSES . $img, null);
    }
}
