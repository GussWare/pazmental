<?php (defined('BASEPATH')) OR exit('No direct script access allowed');


class Settings extends Admin_Controller {

    function __construct() {
        parent::__construct();

        if (!$this->ion_auth->logged_in()) {
   
             $this->ion_auth->logout();
             redirect('auth', 'refresh');
        }

        $this->load->model('module');
        $this->load->model(array('admin/service'));
        $this->load->model(array('admin/options'));


    }

    public function index() {
        $data['page'] = $this->config->item('ci_my_admin_template_dir_admin') . "settings";
        $this->load->view($this->_container, $data);
    }


    public function services(){
            
            $data['services'] = $this->options->get_all('*',array('option_name'=>'_service'));
            $data['types'] = $this->options->get_all('*',array('option_name'=>'_serviceType'));
            $data['deseases'] = $this->options->get_all('*',array('option_name'=>'_desease'));

            $data['page'] = $this->config->item('ci_my_admin_template_dir_admin') . "settings_services";
            $this->load->view($this->_container, $data);
    
        

    }

    

   public function delete($id) {
        
        
        $this->db->where( 'id',$id);
        $this->db->delete('options'); 

        redirect('/admin/settings/services', 'refresh');
    } 

    public function ajax() {
        if ($this->input->is_ajax_request()) {


            switch($this->input->post('action')){

                    case 'add':
                         $data['module'] = $this->input->post('module');

                         $this->load->view($this->config->item('ci_my_admin_template_dir_admin') . "modals/_add_option", $data);
                   
                    break;
                    case 'edit':

                         $option = Options::get($this->input->post('option_id'));
                         if($option!=''){
                          $data['option'] = json_decode($option);
                         }
                         $this->load->view($this->config->item('ci_my_admin_template_dir_admin') . "modals/_edit_option", $data);

                    break;
                    case 'delete':

                            $option = Options::get($this->input->post('option_id'));
                                  
                                    $this->db->where( 'id',$option['id']);
                                    $this->db->delete('options'); 

                                    redirect('/admin/services/settings', 'refresh');
                                 
                    break;
                    case 'save':


                    if($this->input->post('module') =='services') {

                          $values = array(
                                'name'=>$this->input->post('OptionName'),
                                'description'=>$this->input->post('OptionDescription'),
                                'daysperweek'=>$this->input->post('OptionTimesWeek'),
                                'timesperday'=>$this->input->post('OptionTimesDay'),
                                'price'=>$this->input->post('OptionPrice'),
                                'cost'=>$this->input->post('OptionCost')

                        );

                    } else {

                          $values = array(
                                'name'=>$this->input->post('OptionName'),
                                'description'=>$this->input->post('OptionDescription')

                        );
                          

                    }

                      

                        $values = json_encode($values);

                        $module = $this->input->post('module');


                        switch($module){
                            case 'services':
                                $data['option_name'] = '_service';
                            break;
                            case 'serviceType':
                                $data['option_name'] = '_serviceType';
                            break;
                            case 'deseases':
                                $data['option_name'] = '_desease';
                            break;
                            case 'characteristics':
                                $data['option_name'] = '_characterisctic';
                            break;
                            case 'personalities':
                                $data['option_name'] = '_personality';
                             break;
                            case 'hobbies':
                                $data['option_name'] = '_hobby';
                             break;
                            case 'institutions':
                                $data['option_name'] = '_institution';
                             break;
                            case 'care_skills':
                                $data['option_name'] = '_care_skill';
                             break;
                            case 'therapy_skills':
                                $data['option_name'] = '_therapy_skill';
                             break;
                            case 'recruitment_sources':
                                $data['option_name'] = '_recruitment_source';
                             break;
                             case 'otras_actividades':
                             $data['option_name'] = '_rutina_descripcion_otras_actividades';
                            break;
                            case 'estimulacion_cognitiva':
                             $data['option_name'] = '_rutina_descripcion_estimulacion_cognitiva';
                            break;
                            case 'estimulacion_fisica':
                             $data['option_name'] = '_rutina_descripcion_estimulacion_fisica';
                            break;
                        }
                        

                        $options = new Options();

                        $data['option_value'] = $values;
                       $option = $options->insert($data);   

                        echo 'true';


                    break;

            }

        } else {
            exit('Stop! You can not access to this module directly');
        }
    }
}
