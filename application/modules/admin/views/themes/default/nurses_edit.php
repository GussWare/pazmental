

          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3> Luces <span class="fa fa-user-md"></span></h3>
              </div>

                <div class="panel_toolbox"><a  href="<?= base_url('admin/nurses') ?>" class="btn btn-success"><span class="fa fa-angle-double-left"></span> Regresar</a></div>
            </div>

            <div class="clearfix"></div>
            <div class="message_center" style="color:#B22222">  </div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  
                    <h2>Editar Luz</h2>
                    <div class="clearfix"></div>

                 <div class="col-md-3 col-sm-3">
                                    <?php
                                    $profile_img =  json_decode($nurse_meta["_profile_images"]);
                                     if (! is_array($profile_img)){
                                     $profile_img = json_decode($profile_img);
                      }

                                     ?>
                                  <img class="profile" style="width:300px" src="<?php echo base_url(); ?>uploads/files/<?php echo $profile_img[0] ; ?>">
                                </div>
                <div class="col-md-4">
                <h4 class="name-header"> <span class="first-name-title"> <?php echo $user->first_name ?>  </span><br/>
                    <span class="last-name-title"><?php echo $user->last_name; ?></span></h4>
                </div>
              
                    <div class="col-md-4">

                      <div class="servicio-detalles"> <strong> Ultima modificación el:</strong> <?php echo $user->modifiedon; ?> <strong>por:</strong> <?php echo $user->modifiedby; ?></div>
                    </div>
                    <div class="clearfix"></div>
                  
                  <div class="x_content">
                        <form role="form" method="POST" action="<?=base_url('admin/nurses/update')?>" id="create-nurse">
                        <h4>Datos de la Cuenta</h4>
                               
                                
                                <div class="form-group col-md-4 col-sm-4">
                                    <label>User Id</label>
                                    <input class="form-control" placeholder="Auto generated" disabled="1" value="<?php echo $nurse_id; ?>">
                                    <input type="hidden" name="user_id" value="<?php echo $nurse_id; ?>">

                                </div>
                                <div class="form-group col-md-4 col-sm-4 div-ocultar">
                                    <label>Número de Luz</label>
                                    <input class="form-control"  name="numero_luz" id="numero_luz" value="<?php echo $nurse_meta['_numero_luz']; ?>">
                                </div>
                                 <div class="form-group col-md-4 col-sm-4">
                                    <label>Fecha de Alta</label>
                                    <div class="control-group">
                         
                                          <div class=" xdisplay_inputx form-group has-feedback">
                                            <input type="text" class="form-control has-feedback-left single_cal_picker" id="start_date" name="start_date" placeholder="Fecha de Alta" value="<?php echo $nurse_meta['_start_date']; ?>" aria-describedby="inputSuccess2Status2">
                                            <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                            <span id="inputSuccess2Status2" class="sr-only">(success)</span>
                                          </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>

                                
                                <div class="form-group col-md-4 col-sm-4">
                                    <label>Nombre de Usuario</label>
                                    <input class="form-control"  placeholder="Ingresa nombre de usuario" id="username" name="username" value="<?php echo $user->username; ?>" required>
                                </div>
                                <div class="form-group col-md-4 col-sm-4">
                                      <label>Email</label>
                                      <input class="form-control" placeholder="Ingresa E-mail" id="email" name="email"  required value="<?php echo $user->email; ?>">
                                  </div>
                                 <div class="form-group col-md-4 col-sm-4">
                                    <label>Contraseña</label>
                                    <input type="password" class="form-control" placeholder="Ingresa Contraseña" id="password" name="password"  value="" required>
                                </div>
                                <div class="form-group col-md-4 col-sm-4">
                                <label for="">
                            Supervisora a Cargo
                          </label>

                          <select name="supervisor" id="" class="form-control">
                            
                            <option value="0">Ninguno</option>
                            <?php foreach($supervisors as $supervisor):
                              $name = $supervisor->first_name. ' '.$supervisor->last_name; ?>
                            <option value="<?php echo $supervisor->id; ?>" <?php if($nurse_meta['_supervisor'] == $supervisor->id) echo 'selected'; ?>>
                              <?php echo $name; ?>
                            </option>
                          <?php endforeach; ?>
                          </select>
                        </div>
                        <div class="form-group col-md-4 col-sm-4">
                                    <label>Grupo de Usuario</label>
                                   <select class="form-control" id="group_id" name="group_id">
                                        <?php foreach ($groups as $group): ?>
                                            <?php if($group->id == 3 || $group->id == 4) { ?>
                                                 <option value="<?=$group->id?>"><?=$group->description?></option>

                                            <?php } ?>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="form-group col-md-4 col-sm-4">
                                    <label>Tipo de luz</label>
                                    <select class="form-control" id="id_tipo_luz" name="id_tipo_luz" required>
                                        <option value="">Seleccione</option>
                                        <?php foreach($tipos_luz AS $tipo_luz): ?>
                                          <option <?php echo ($user->id_tipo_luz == $tipo_luz->id) ? 'selected' : '' ?> value="<?php echo $tipo_luz->id; ?>"><?php echo $tipo_luz->nombre; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="clearfix"></div>
                                </div>
                        <h4>Datos Personales</h4>
                               
                                <div class="form-group col-md-4 col-sm-4">
                                    <label>Nombre (s)</label>
                                    <input class="form-control" placeholder="Ingresa Primer y Segundo Nombre" id="first_name" name="first_name"  required value="<?php echo $user->first_name; ?>">
                                </div>
                                <div class="form-group col-md-4 col-sm-4">
                                    <label>Apellidos</label>
                                    <input class="form-control" placeholder="Ingresa Apellidos" id="last_name" name="last_name" value="<?php echo $user->last_name; ?>" required>
                                </div>
                                <div class="form-group col-md-4 col-sm-4">
                                    <label>Fecha de Nacimiento</label>
                                   <div class="control-group">
                                          <div class=" xdisplay_inputx form-group has-feedback">
                                            <input type="text" class="form-control has-feedback-left single_cal_picker" id="" name="birth_date" placeholder="Fecha de Nacimiento" aria-describedby="inputSuccess2Status4" value="<?php echo $nurse_meta['_birthday']; ?>">
                                            <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                            <span id="inputSuccess2Status4" class="sr-only">(success)</span>
                                          </div>
                                    </div>
                          
                                </div>
                                <div class="form-group col-md-3 col-sm-3">
                                    <label>Edad</label>
                                    <input class="form-control" placeholder="Enter age" id="age" name="age" value="<?php echo $nurse_meta['_age']; ?>">
                                </div>
                                 <div class="form-group col-md-2 col-sm-2">
                                     
                                     <label>Género:</label>
                                         <select class="form-control" name="pm_gender"> 
                                             <option value="M" <?php if($nurse_meta['_gender'] == 'M'){ echo 'selected'; } ?> >M </option>
                                            <option value="F" <?php if($nurse_meta['_gender'] == 'F'){ echo 'selected'; } ?> >F </option>
                                          </select>
                                </div>
                                 <div class="form-group col-md-4 col-sm-4">
                                    <label>Estado Civil</label>
                                    <select class="form-control" name="civil_status">
                                      <option value="">Selecciona Opción</option>
                                      <option value="single" <?php if($nurse_meta['_civil_status'] == 'single') echo 'selected'; ?> >Soltero (a)</option>
                                      <option value="married"  <?php if($nurse_meta['_civil_status'] == 'married') echo 'selected'; ?>>Casado (a)</option>
                                      <option value="divorced"  <?php if($nurse_meta['_civil_status'] == 'divorced') echo 'selected'; ?>>Divorciado (a)</option>
                                      <option value="widow"  <?php if($nurse_meta['_civil_status'] == 'widow') echo 'selected'; ?>>Viudo (a)</option>

                                    </select>

                            
                                </div>
                                <div class="form-group col-md-4 col-sm-4 div-ocultar">
                                    <label>CURP</label>
                                    <input class="form-control" placeholder="Enter CURP" id="curp" name="curp"  required value="<?php echo $nurse_meta['_curp']; ?>">
                                </div>

                                <div class="div-ocultar">
                                 <div class="form-group col-md-4 col-sm-4">
                                    <label>Dependientes Económicos</label>
                                   
                                            <select name="economical_dependants"  class="form-control onChangeNurse" data-target="<?php echo base_url(); ?>admin/nurses/select" id="economical_dependants" >
                                        <option value="0" <?php if($nurse_meta['_economical_dependants'] == '0') echo 'selected'; ?>>0</option>
                                        <option value="1" <?php if($nurse_meta['_economical_dependants'] == '1') echo 'selected'; ?>>1</option>
                                        <option value="2" <?php if($nurse_meta['_economical_dependants'] == '2') echo 'selected'; ?>>2</option>
                                        <option value="3" <?php if($nurse_meta['_economical_dependants'] == '3') echo 'selected'; ?>>3</option>
                                        <option value="4" <?php if($nurse_meta['_economical_dependants'] == '4') echo 'selected'; ?>>4</option>
                                        <option value="5" <?php if($nurse_meta['_economical_dependants'] == '5') echo 'selected'; ?>>5</option>

                                    </select>
                                </div>
                                </div>

                                <div class="div-mostrar">
                                  <div class="form-group col-md-4 col-sm-4">
                                    <label>Num. Hijos</label>
                                    <input class="form-control" placeholder="Enter Hijos" id="num_hijos" name="num_hijos"  required value="<?php echo $nurse_meta['_num_hijos']; ?>">
                                  </div>
                                </div>
                                
                                
                                 <div class="clearfix"></div>
                                  
                                  <?php  if($nurse_meta['_economical_dependants'] > 0 && $user->id_tipo_luz != 2) {?>
                                       <div id='dependent0'>
                                       <div class='form-group col-md-3 col-sm-3'>
                                        <label>Nombre</label>
                                        <input type='text' class='form-control' name='dependant_name_0' value='<?php echo $nurse_meta['_dependant_name_0']; ?>'/>
                                      </div>
                                      <div class='form-group col-md-3 col-sm-3'>
                                        <label>Edad</label>
                                        <input type='text' class='form-control' name='dependant_age_0' value='<?php echo $nurse_meta['_dependant_age_0']; ?>' />
                                        </div>
                                      <div class='form-group col-md-3 col-sm-3'>
                                      <label>Fecha de Nacimiento</label> 
                                      <input type='date' class='form-control' name='dependant_dob_0' value='<?php echo $nurse_meta['_dependant_dob_0']; ?>'/>
                                     </div></div>
                                      <div class='clearfix'></div>
                                    
                                    <?php   }?>
                                     <?php  if($nurse_meta['_economical_dependants'] > 1 && $user->id_tipo_luz != 2) {?>
                                       <div id='dependent1'>
                                        <div class='form-group col-md-3 col-sm-3'>
                                        <label>Nombre</label>
                                          <input type='text' class='form-control' name='dependant_name_1' value='<?php echo $nurse_meta['_dependant_name_1']; ?>'/>
                                      </div>
                                      <div class='form-group col-md-3 col-sm-3'>
                                        <label>Edad</label>
                                        <input type='text' class='form-control' name='dependant_age_1' value='<?php echo $nurse_meta['_dependant_age_1']; ?>' />
                                        </div>
                                      <div class='form-group col-md-3 col-sm-3'>
                                      <label>Fecha de Nacimiento</label> 
                                      <input type='date' class='form-control' name='dependant_dob_1' value='<?php echo $nurse_meta['_dependant_dob_1']; ?>'/>
                                     </div></div>
                                      <div class='clearfix'></div>
                                      
                                    <?php   }?>
                                
                                 <?php  if($nurse_meta['_economical_dependants'] > 2 && $user->id_tipo_luz != 2) {?>
                                       <div id='dependent2'>
                                       <div class='form-group col-md-3 col-sm-3'>
                                        <label>Nombre</label>
                                        <input type='text' class='form-control' name='dependant_name_2' value='<?php echo $nurse_meta['_dependant_name_2']; ?>'/>
                                      </div>
                                      <div class='form-group col-md-3 col-sm-3'>
                                        <label>Edad</label>
                                        <input type='text' class='form-control' name='dependant_age_2' value='<?php echo $nurse_meta['_dependant_age_2']; ?>' />
                                        </div>
                                      <div class='form-group col-md-3 col-sm-3'>
                                      <label>Fecha de Nacimiento</label> 
                                      <input type='date' class='form-control' name='dependant_dob_2' value='<?php echo $nurse_meta['_dependant_dob_2']; ?>'/>
                                     </div></div>
                                      <div class='clearfix'></div>
                                    
                                    <?php   }?>

                                           <?php  if($nurse_meta['_economical_dependants'] > 3 && $user->id_tipo_luz != 2) {?>
                                           <div id='dependent3'>
                                       <div class='form-group col-md-3 col-sm-3'>
                                        <label>Nombre</label>
                                        <input type='text' class='form-control' name='dependant_name_3' value='<?php echo $nurse_meta['_dependant_name_3']; ?>'/>
                                      </div>
                                      <div class='form-group col-md-3 col-sm-3'>
                                        <label>Edad</label>
                                        <input type='text' class='form-control' name='dependant_age_3' value='<?php echo $nurse_meta['_dependant_age_3']; ?>' />
                                        </div>
                                      <div class='form-group col-md-3 col-sm-3'>
                                      <label>Fecha de Nacimiento</label> 
                                      <input type='date' class='form-control' name='dependant_dob_3' value='<?php echo $nurse_meta['_dependant_dob_3']; ?>'/>
                                     </div></div>
                                      <div class='clearfix'></div>
                                      <?php   }?>
                                             <?php  if($nurse_meta['_economical_dependants'] > 4 && $user->id_tipo_luz != 2) {?>
                                       <div id='dependent4'>
                                       <div class='form-group col-md-3 col-sm-3'>
                                        <label>Nombre</label>
                                        <input type='text' class='form-control' name='dependant_name_4' value='<?php echo $nurse_meta['_dependant_name_4']; ?>'/>
                                      </div>
                                      <div class='form-group col-md-3 col-sm-3'>
                                        <label>Edad</label>
                                        <input type='text' class='form-control' name='dependant_age_4' value='<?php echo $nurse_meta['_dependant_age_4']; ?>' />
                                        </div>
                                      <div class='form-group col-md-3 col-sm-3'>
                                      <label>Fecha de Nacimiento</label> 
                                      <input type='date' class='form-control' name='dependant_dob_4' value='<?php echo $nurse_meta['_dependant_dob_4']; ?>'/>
                                     </div></div>
                                      <div class='clearfix'></div>
                                    
                                    <?php   }?>
                                    <div id="dependant-loop"></div>
                                    

                                <div class="clearfix"></div>

                                

                                <div class="div-ocultar">
                                <div class="form-group col-md-9 col-sm-9">
                                    <label>Características Físicas</label>
                                    <div class="clearfix"></div>
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                    <?php foreach($nurse_meta['_fisical'] as $char):
                                      
                                      $info = json_decode($char['option_value']); ?>
                                    
                                     <label class="checkbox-inline"><input  name="fisical" type="checkbox" value=" <?php echo $char['id']; ?>"  <?php if ($char['selected'] ==1 ) echo "checked"; ?> ><?php echo $info->name; ?></label>
 
                                    <?php endforeach; ?>                                    </div>
                                </div>
                                </div>

                              <div class="clearfix"></div>

                              <div class="div-mostrar">
                                <h4>Referencias</h4>
                                <div class="form-group col-md-4 col-sm-4">
                                  <label>Número Telefonico 1</label>
                                  <input type="text" class="form-control" name="referencia_phone1" placeholder="Número teléfonico" value="<?php echo $nurse_meta['_referencia_phone1']; ?>"  />
                                </div>
                                <div class="form-group col-md-8 col-sm-8">
                                  <label>Nombre Referencia 1</label>
                                  <input type="text" class="form-control" name="referencia_nombre1" placeholder="Nombre de la persona Ref. 1" value="<?php echo $nurse_meta['_referencia_nombre1']; ?>"  />
                                </div>

                                <div class="form-group col-md-4 col-sm-4">
                                  <label>Número Telefonico 2</label>
                                  <input type="text" class="form-control" name="referencia_phone2" placeholder="Número teléfonico 2" value="<?php echo $nurse_meta['_referencia_phone2']; ?>"  />
                                </div>
                                <div class="form-group col-md-8 col-sm-8">
                                  <label>Nombre Referencia 2</label>
                                  <input type="text" class="form-control" name="referencia_nombre2" placeholder="Nombre de la persona Ref. 2" value="<?php echo $nurse_meta['_referencia_nombre2']; ?>"  />
                                </div>
                              </div>
                              <div class="clearfix"></div>

                                <h4>Dirección</h4>

                                <div class="form-group col-md-4 col-sm-4">
                                      <label>Dirección y Número</label>
                                    <input type="text" class="form-control" name="address" placeholder="Ingresa dirección y número" value="<?php echo $nurse_meta['_address']; ?>"  />
                                </div>

                                 <div class="form-group col-md-4 col-sm-4">
                                      <label>Colonia</label>
                                    <input type="text" class="form-control" name="colony" placeholder="Ingresa Colonia"  value="<?php echo $nurse_meta['_colony']; ?>"  />
                                </div>

                                 <div class="form-group col-md-4 col-sm-4">
                                      <label>Delegación o Municipio</label>
                                    <input type="text" class="form-control" name="region" placeholder="Ingresa Región o delegación"  value="<?php echo $nurse_meta['_region']; ?>"  />
                                </div>

                                <div class="clearfix"></div>

                                <div class="div-ocultar">
                                  <div class="form-group col-md-4 col-sm-4">
                                        <label>Ciudad</label>
                                      <input type="text" class="form-control" name="city" placeholder="Ingresa ciudad"  value="<?php echo $nurse_meta['_city']; ?>"  />
                                  </div>

                                    <div class="form-group col-md-4 col-sm-4">
                                        <label>C.P.</label>
                                      <input type="text" class="form-control" name="zip" placeholder="Ingresa código postal"  value="<?php echo $nurse_meta['_zip']; ?>" />
                                  </div>
                                    <div class="form-group col-md-9 col-sm-9">
                                    <input type="hidden" id="map_long" value= '<?php echo $nurse_meta["_map_long"]; ?>' />
                                    <input type="hidden" id="map_lat" value= '<?php echo $nurse_meta["_map_lat"]; ?>' />
                                  <div id="map" style="height:400px; min-width:400px"></div>

                              
                                  
                                  </div>
                                </div>
    
                                <div class="clearfix"></div>

                                
                                  <h4>Datos de Contacto</h4>
                                  <div class="form-group col-md-4 col-sm-4">
                                      <label>Teléfono</label>
                                      <input class="form-control" placeholder="Ingresa número de teléfono" id="phone" name="phone" value="<?php echo $user->phone; ?>">
                                  </div>
                                  
                                  <div class="form-group col-md-4 col-sm-4">
                                      <label>Celular</label>
                                      <input class="form-control" placeholder="Ingresa número de Celular" id="mobile" name="mobile" value="<?php echo $nurse_meta['_mobile']; ?>">
                                  </div>

                                <div class="clearfix"></div>

                                <div class="div-ocultar">
                                  <h4>Datos de Contacto en Caso de Emergencia</h4>

                                    <div class="form-group col-md-3 col-sm-3">
                                      <label>Nombre</label>
                                      <input class="form-control" placeholder="Ingresa Nombre" id="emergency_name" name="emergency_name"  value="<?php echo $nurse_meta['_emergency_name']; ?>" required>
                                  </div>
                                    <div class="form-group col-md-3 col-sm-3">
                                      <label>Apellido</label>
                                      <input class="form-control" placeholder="Ingresa Apellido" id="emergency_last_name" name="emergency_last_name" value="<?php echo $nurse_meta['_emergency_last_name']; ?>" required>
                                  </div>
                                    <div class="form-group col-md-3 col-sm-3">
                                      <label>Parentezco</label>
                                      <input class="form-control" placeholder="Ingresa Parentezco" id="emergency_relation" name="emergency_relation" value="<?php echo $nurse_meta['_emergency_relation']; ?>" required>
                                  </div>
                                  <div class="form-group col-md-3 col-sm-3">
                                      <label>Teléfono</label>
                                      <input class="form-control" placeholder="Ingresa número de teléfono" id="emergency_phone" name="emergency_phone" value="<?php echo $nurse_meta['_emergency_phone']; ?>"required>
                                  </div>
         
                                </div>
                                <div class="clearfix"></div>

                            
                              <h4>Datos del Sistema</h4>
                              <div class="div-ocultar">
                                <div class="form-group col-md-3 col-sm-3">
                                      <label>Status:</label>
                                          <select class="form-control" id="status" name="status">
                                  
                                                  <option value="1" <?php if($nurse_meta['_status'] == '1') echo 'selected'; ?>>Activo</option>
                                                  <option value="0" <?php if($nurse_meta['_status'] == '0') echo 'selected'; ?>>Suspendida</option>
                                                  <option value="-1" <?php if($nurse_meta['_status'] == '-1') echo 'selected'; ?>>Baja</option>
                                                  <option value="-2" <?php if($nurse_meta['_status'] == '-2') echo 'selected'; ?>>Renuncia</option>
                                        
                                      </select>
                                  </div>

                                  <div class="form-group col-md-9 col-sm-9">
                                      <label>Motivo de Baja / Renuncia</label>
                                        <textarea class="resizable_textarea form-control"  name="fire_reason" placeholder=""><?php echo $nurse_meta['_fire_reason']; ?></textarea>
                                  
                                  </div>

                                  <div class="clearfix"></div>

                                <div class="form-group col-md-3 col-sm-3">
                                      <label>Fuentes de Reclutamiento:</label>
                                      <select class="form-control" id="reclutamiento" name="reclutamiento" required>
                                            <option value="">Ninguno</option>
                                            <?php foreach($recruitment_sources as $recs):
                                              $info = json_decode($recs['option_value']); ?>
                                            <option value="<?php echo $recs['id']; ?>" <?php if($nurse_meta['_reclutamiento'] == $recs['id']){ echo 'selected';} ?>>
                                              <?php echo $info->name; ?>
                                            </option>
                                            <?php endforeach; ?>

                                      </select>
                                        
                                  </div>
                                  <div class="form-group col-md-3 col-sm-3">
                                      <label>Grados de Estudio</label>
                                              <select class="form-control" id="grado_estudios" name="grado_estudios" required>
                                        <option value="">Ninguno</option>
                                        <?php foreach($institutions as $inst):
                                        $info = json_decode($inst['option_value']); ?>
                                      <option value="<?php echo $inst['id']; ?>" <?php if($nurse_meta['_grado_estudios'] == $inst['id']) echo 'selected';?>  >
                                        <?php echo $info->name; ?>
                                      </option>
                                      <?php endforeach; ?>

                                      </select>
                                  </div>
                                </div>

                                <div class="form-group col-md-2 col-sm-2">
                                     <label>¿Esta luz es confiable?:</label>
                                          <p>
                                            Sí:
                                            <input type="radio"  name="luz_confiable" id="luz_confiable_si" value="Si" <?php echo ($nurse_meta['_luz_confiable'] == "Si") ? 'checked="checked"' : ''; ?>  /> 
                                            No:
                                            <input type="radio"  name="luz_confiable" id="luz_confiable_no" value="No" <?php echo ($nurse_meta['_luz_confiable'] == "No") ? 'checked="checked"' : ''; ?> />
                                          </p>
                                </div>

                                <div class="div-mostrar">
                                    <div class="form-group col-md-3 col-sm-3">
                                      <label>Maxima escolaridad</label>
                                      <select class="form-control" id="max_escolaridad" name="max_escolaridad" required>
                                        <option value="">Ninguno</option>
                                        <?php foreach($max_escolaridad as $value): ?>
                                          <?php $esc = json_decode($value['option_value']); ?>
                                          <option value="<?php echo $value['id']; ?>" <?php if($nurse_meta['_max_escolaridad'] == $value['id']) echo 'selected';?>  ><?php echo $esc->name; ?></option>
                                        <?php endforeach; ?>
                                      </select>
                                    </div>
                                  </div>
                                <div class="clearfix"></div>

                                <div class="form-group col-md-12 col-sm-12">
                                    <label>Observaciones Internas:</label>
                                    <textarea class="resizable_textarea form-control"  name="observaciones_internas" placeholder="" ><?php echo $nurse_meta['_observaciones_internas'];?></textarea>
                                </div>

                                <h4>Experiencia Laboral</h4>

                                <div class="div-mostrar">
                                  <div class="form-group col-md-4 col-sm-4">
                                    <label>Años de experiencia cuidando adultos</label>
                                    <input class="form-control" placeholder="Num. Años de experiencia" id="anos_experiencia" name="anos_experiencia" value="<?php echo $nurse_meta['_anos_experiencia']; ?>"required>
                                  </div>
                                </div>

                                <div class="clearfix"></div>

                                <h5>Conocimientos y Habilidades</h5>
                                <div class="col-md-6 col-sm-6">
                                  <h5>Cuidador</h5>
                                  <?php foreach($nurse_meta['_carer_skills'] as $cs):
                                    
                                      $info = json_decode($cs['option_value']); ?>
                                    
                                     <div class="checkbox">
                                      <label ><input name="carer_skills" type="checkbox" value=" <?php echo $cs['id']; ?>" <?php if ($cs['selected']==1) echo "checked";?> ><?php echo $info->name; ?></label>
                                    </div>
                                    <?php endforeach; ?>
                                </div>

                                <div class="div-ocultar">
                                  <div class="col-md-6 col-sm-6">
                                    <h5>Terapeuta</h5>
                                    <?php foreach($nurse_meta['_therapist_skills'] as $ts):
                                      
                                        $info = json_decode($ts['option_value']); ?>
                                      
                                      <div class="checkbox">
                                        <label ><input name="therapist_skills" type="checkbox" value=" <?php echo $ts['id']; ?>" <?php if ($ts['selected']==1) echo "checked";?> ><?php echo $info->name; ?></label>
                                      </div>
                                      <?php endforeach; ?>
                                  
                                  </div>
                                </div>
              
                                 <div class="clearfix"></div>

                                <br><br>

                                <div class="div-ocultar">
                                <div class="form-group col-md-6 col-sm-6">
                                    <label>Personalidad</label>
                                    <div class="clearfix"></div>
                                    <?php foreach($nurse_meta['_personalities'] as $pers):
                                    
                                      $info = json_decode($pers['option_value']); ?>
                                    
                                     <div class="checkbox">
                                      <label ><input name="personalities" type="checkbox" value=" <?php echo $pers['id']; ?>" <?php if ($pers['selected']==1) echo "checked";?>  ><?php echo $info->name; ?></label>
                                    </div>
                                    <?php endforeach; ?>
                                </div>

                                 <div class="form-group col-md-6 col-sm-6">
                                    <label>Pasatiempos</label>
                                    <div class="clearfix"></div>
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                    <?php foreach($nurse_meta['_hobbies'] as $hobby):
                                    
                                      $info = json_decode($hobby['option_value']); ?>
                                    
                                     <div class="checkbox">
                                      <label ><input name="hobbies" type="checkbox" value=" <?php echo $hobby['id']; ?>" <?php if ($hobby['selected']==1) echo "checked";?> ><?php echo $info->name; ?></label>
                                    </div>
                                    <?php endforeach; ?>
                                    </div>
                                </div>
                                </div>
                              <div class="clearfix"></div>


                               <div class="col-md-12 col-sm-12">

                               <div class="div-ocultar">
                                  <h4>Datos Bancarios</h4>


                                    <label for="" class="form-group col-md-4 col-sm-4">
                                    Banco
                                      <input type="text" name="bank_name" class="form-control" value="<?php echo $nurse_meta['_bank_name']; ?>">
                                    </label>

                                    <label for="" class="form-group col-md-4 col-sm-4">
                                      Datos de cuenta
                                      <input type="text" name="bank_account" class="form-control" value="<?php echo $nurse_meta['_bank_account']; ?>">
                                    </label>


                                    <label for="" class="form-group col-md-4 col-sm-4">
                                      CLABE
                                      <input type="text" name="bank_clabe" class="form-control" value="<?php echo $nurse_meta['_bank_clabe']; ?>">
                                    </label>
                                    
                                  </div>
                               </div>



                            <div class="col-md-12 col-sm-12">
                               <h4>Archivos</h4>  
                                <input type="hidden" name="myDropzone_profile_files" id="myDropzone_profile_files" value= '<?php echo $nurse_meta["_profile_images"]; ?>' />
                                <input type="hidden" name="myDropzone_IFE_files" id="myDropzone_IFE_files" value= '<?php echo $nurse_meta["_ife_images"]; ?>' />
                                <input type="hidden" name="myDropzone_CDomicilio_files" id="myDropzone_CDomicilio_files" value= '<?php echo $nurse_meta["_CDomicilio_images"]; ?>' />
                                <input type="hidden" name="myDropzone_CURP_files" id="myDropzone_CURP_files" value= '<?php echo $nurse_meta["_CURP_images"]; ?>' />
                                <input type="hidden" name="myDropzone_CEstudios_files" id="myDropzone_CEstudios_files" value= '<?php echo $nurse_meta["_CEstudios_images"]; ?>' />
                                <input type="hidden" name="myDropzone_ANacimiento_files" id="myDropzone_ANacimiento_files" value= '<?php echo $nurse_meta["_ANacimiento_images"]; ?>' />
                                <input type="hidden" name="myDropzone_SSocial_files" id="myDropzone_SSocial_files" value= '<?php echo $nurse_meta["_SSocial_images"]; ?>' />
                                <input type="hidden" name="myDropzone_RFC_files" id="myDropzone_RFC_files" value= '<?php echo $nurse_meta["_RFC_images"]; ?>' />
                                <input type="hidden" name="myDropzone_penales_files" id="myDropzone_penales_files" value= '<?php echo $nurse_meta["_penales_images"]; ?>' />
                                <input type="hidden" name="myDropzone_ESeconomico_files" id="myDropzone_ESeconomico_files" value= '<?php echo $nurse_meta["_ESeconomico_images"]; ?>' />
                                <input type="hidden" name="myDropzone_otros_files" id="myDropzone_otros_files" value= '<?php echo $nurse_meta["_otros_images"]; ?>' />

                                <div class="form-group col-md-3 col-sm-3 text-center">
                                  <p class="file-label"> Fotografía de Perfil <div class="dropzone" id="myDropzone_profile_update"   data-target="<?php echo base_url('images/upload'); ?>" data-remove="<?php echo base_url('images/remove'); ?>" data-files="<?php echo base_url('images/list_files');?>"></div>
                                  <?php if($nurse_meta["_profile_images_nombre"]) : ?> 
                                    <a href="#" data-nombre-file="<?php echo $nurse_meta["_profile_images_nombre"] ?>" class="btn btn-info btn-descargar-file">
                                    <i class="fa fa-download"></i>
                                    Descargar</a>
                                  <?php endif; ?>
                                </div>


                                <div class="form-group col-md-3 col-sm-3 text-center">
                                  <p class="file-label"> IFE <div class="dropzone" id="myDropzone_IFE_update"   data-target="<?php echo base_url('images/upload'); ?>" data-remove="<?php echo base_url('images/remove'); ?>" data-files="<?php echo base_url('images/list_files');?>"></div>
                                  <?php if($nurse_meta["_ife_images_nombre"]) : ?> 
                                    <a href="#" data-nombre-file="<?php echo $nurse_meta["_ife_images_nombre"] ?>" class="btn btn-info btn-descargar-file">
                                    <i class="fa fa-download"></i>
                                    Descargar</a>
                                  <?php endif; ?>
                                  </div>
                                


                                <div class="form-group col-md-3 col-sm-3 text-center">
                                  <p class="file-label"> Comprobante de Domicilio <div class="dropzone" id="myDropzone_CDomicilio_update"   data-target="<?php echo base_url('images/upload'); ?>" data-remove="<?php echo base_url('images/remove'); ?>" data-files="<?php echo base_url('images/list_files');?>"></div>
                                  <?php if($nurse_meta["_CDomicilio_images_nombre"]) : ?> 
                                    <a href="#" data-nombre-file="<?php echo $nurse_meta["_CDomicilio_images_nombre"] ?>" class="btn btn-info btn-descargar-file">
                                    <i class="fa fa-download"></i>
                                    Descargar</a>
                                  <?php endif; ?>
                                  </div>
                                
                                
                                <div class="form-group col-md-3 col-sm-3 text-center">
                                  <p class="file-label"> CURP <div class="dropzone" id="myDropzone_CURP_update"   data-target="<?php echo base_url('images/upload'); ?>" data-remove="<?php echo base_url('images/remove'); ?>" data-files="<?php echo base_url('images/list_files');?>"></div>
                                  <?php if($nurse_meta["_CURP_images_nombre"]) : ?> 
                                    <a href="#" data-nombre-file="<?php echo $nurse_meta["_CURP_images_nombre"] ?>" class="btn btn-info btn-descargar-file">
                                    <i class="fa fa-download"></i>
                                    Descargar</a>
                                  <?php endif; ?>
                                  </div>
                                

                                <div class="form-group col-md-3 col-sm-3 text-center">
                                  <p class="file-label"> Comprobante de estudios <div class="dropzone" id="myDropzone_CEstudios_update"   data-target="<?php echo base_url('images/upload'); ?>" data-remove="<?php echo base_url('images/remove'); ?>" data-files="<?php echo base_url('images/list_files');?>"></div>
                                  <?php if($nurse_meta["_CEstudios_images_nombre"]) : ?> 
                                    <a href="#" data-nombre-file="<?php echo $nurse_meta["_CEstudios_images_nombre"] ?>" class="btn btn-info btn-descargar-file">
                                    <i class="fa fa-download"></i>
                                    Descargar</a>
                                  <?php endif; ?>
                                  </div>
                                

                                <div class="form-group col-md-3 col-sm-3 text-center">
                                  <p class="file-label"> Acta de Nacimiento <div class="dropzone" id="myDropzone_ANacimiento_update"   data-target="<?php echo base_url('images/upload'); ?>" data-remove="<?php echo base_url('images/remove'); ?>" data-files="<?php echo base_url('images/list_files');?>"></div>
                                  <?php if($nurse_meta["_ANacimiento_images_nombre"]) : ?> 
                                    <a href="#" data-nombre-file="<?php echo $nurse_meta["_ANacimiento_images_nombre"] ?>" class="btn btn-info btn-descargar-file">
                                    <i class="fa fa-download"></i>
                                    Descargar</a>
                                  <?php endif; ?>
                                  </div>
                                
                                  <div class="div-ocultar">
                                <div class="form-group col-md-3 col-sm-3 text-center">
                                  <p class="file-label"> Seguridad Social <div class="dropzone" id="myDropzone_SSocial_update"   data-target="<?php echo base_url('images/upload'); ?>" data-remove="<?php echo base_url('images/remove'); ?>" data-files="<?php echo base_url('images/list_files');?>"></div>
                                  <?php if($nurse_meta["_SSocial_images_nombre"]) : ?> 
                                    <a href="#" data-nombre-file="<?php echo $nurse_meta["_SSocial_images_nombre"] ?>" class="btn btn-info btn-descargar-file">
                                    <i class="fa fa-download"></i>
                                    Descargar</a>
                                  <?php endif; ?>
                                  </div>
                                  </div>
                                
                                  <div class="div-ocultar">
                                <div class="form-group col-md-3 col-sm-3 text-center">
                                  <p class="file-label"> RFC <div class="dropzone" id="myDropzone_RFC_update"   data-target="<?php echo base_url('images/upload'); ?>" data-remove="<?php echo base_url('images/remove'); ?>" data-files="<?php echo base_url('images/list_files');?>"></div>
                                  <?php if($nurse_meta["_RFC_images_nombre"]) : ?> 
                                    <a href="#" data-nombre-file="<?php echo $nurse_meta["_RFC_images_nombre"] ?>" class="btn btn-info btn-descargar-file">
                                    <i class="fa fa-download"></i>
                                    Descargar</a>
                                  <?php endif; ?>
                                  </div>
                                  </div>
                                
                                  <div class="div-ocultar">
                                <div class="form-group col-md-3 col-sm-3 text-center">
                                  <p class="file-label"> Carta de Antecedentes no Penales <div class="dropzone" id="myDropzone_penales_update"   data-target="<?php echo base_url('images/upload'); ?>" data-remove="<?php echo base_url('images/remove'); ?>" data-files="<?php echo base_url('images/list_files');?>"></div>
                                  <?php if($nurse_meta["_penales_images_nombre"]) : ?> 
                                    <a href="#" data-nombre-file="<?php echo $nurse_meta["_penales_images_nombre"] ?>"  class="btn btn-info btn-descargar-file">
                                    <i class="fa fa-download"></i>
                                    Descargar</a>
                                  <?php endif; ?>
                                  </div>
                                  </div>

                                 <div class="div-ocultar">
                                <div class="form-group col-md-3 col-sm-3 text-center">
                                  <p class="file-label"> Estudio Socioeconómico <div class="dropzone" id="myDropzone_ESeconomico_update"   data-target="<?php echo base_url('images/upload'); ?>" data-remove="<?php echo base_url('images/remove'); ?>" data-files="<?php echo base_url('images/list_files');?>"></div>
                                  <?php if($nurse_meta["_ESeconomico_images_nombre"]) : ?> 
                                    <a  href="#" data-nombre-file="<?php echo $nurse_meta["_ESeconomico_images_nombre"] ?>"  class="btn btn-info btn-descargar-file">
                                    <i class="fa fa-download"></i>
                                    Descargar</a>
                                  <?php endif; ?>
                                  </div>
                                  </div>

                                 <div class="div-ocultar">
                                <div class="form-group col-md-3 col-sm-3 text-center">
                                  <p class="file-label"> Otros <div class="dropzone" id="myDropzone_otros_update"   data-target="<?php echo base_url('images/upload'); ?>" data-remove="<?php echo base_url('images/remove'); ?>" data-files="<?php echo base_url('images/list_files');?>"></div>
                                  <?php if($nurse_meta["_otros_images_nombre"]) : ?> 
                                    <a target ="_blank" href="#" data-nombre-file="<?php echo $nurse_meta["_otros_images_nombre"] ?>" class="btn btn-info btn-descargar-file">
                                    <i class="fa fa-download"></i>
                                    Descargar</a>
                                  <?php endif; ?>
                                  </div>
                                  </div>
                                
                                <div class="clearfix"></div>



                                <br />
                                <br />
                                <br />


                                <div class="col-md-3 col-sm-3">
                      <a href="<?php echo base_url(); ?>admin/nurses/update" id="createNurse" data-rel="2" title="Crear Luz" class="btn btn-info createNurse">Guardar</a>
                    </div>
                            </form>
                  </div>
                </div>
              </div>
            </div>
          </div>

<div class="contenedor-forma-file" style="display:none;"></div>
        <!-- /page content -->
