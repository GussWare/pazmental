 <div class="login_wrapper">
    <div class="animate form login_form">
          <section class="login_content">
            <form role="form" method="POST" action="<?= base_url('auth/login') ?>">
              <h1>Login Form</h1>
                <?php if (isset($message) && $message != ""): ?>
                        <div class="alert alert-danger fade in">
                            <a href="#" class="close" data-dismiss="alert">&times;</a>
                            <?= $message ?>
                        </div>
                    <?php endif; ?>

                <?php if (isset($message_success) && $message_success != ""): ?>
                        <div class="alert alert-success fade in">
                            <a href="#" class="close" data-dismiss="alert">&times;</a>
                            <?= $message_success ?>
                        </div>
                    <?php endif; ?>
          
                        
                            <div class="form-group">
                                <input class="form-control" placeholder="E-mail" name="email" type="email" autofocus>
                            </div>
                            <div class="form-group">
                                <input class="form-control" placeholder="Password" name="password" type="password" value="">
                            </div>
                          <div>
                             <?php echo form_error('g-recaptcha-response','<div style="color:red;">','</div>'); ?>
                            <div style="text-align:center" class="g-recaptcha" data-sitekey="6Le2xzsUAAAAAKM6YvXAgmvgKwQ45_NDUyiKreAi"></div> 
                            <br/>
                              <button class="btn btn-default submit" type="submit">Login</button>
                            <a class="reset_pass" href="<?php echo base_url('auth/forgot_password'); ?>">Lost your password?</a>
                          </div>

              <div class="clearfix"></div>

              <div class="separator">
                <div class="clearfix"></div>
                <br />

                <div class="logo">
                 <img src="<?php echo base_url();?>/assets/build/img/pazmental-logo-gray.png" />
                  <p><?php echo date('Y'); ?> &copy All Rights Reserved.Paz Mental</p>
                </div>
              </div>
            </form>
          </section>
        </div>
      
</div>
