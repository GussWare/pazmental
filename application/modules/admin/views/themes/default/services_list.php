
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Servicios <span class="fa fa-heartbeat"></span>  </h3>
              </div>
            <?php if ($this->session->flashdata('message')): ?>
              <div class="col-lg-12 col-md-12">
              <div class="alert alert-info alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?=$this->session->flashdata('message')?>
              </div>
              </div>
            <?php endif; ?>

              
            </div>

            <div class="clearfix"></div>

            <div class="row">

              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">

                  <div class="x_title">
                    <h2>Listado de Servicios</h2>
                    <?php 
                                       /* PARA CONTROLAR ACCESOS*/
                                    $editable = false;
                                    $disabled = "disabled";
                                    $edit_data ="";
                                    
                                    if($group_name == 'admin' or $group_name == "supervisor"){
                                        $editable = true; 
                                        $disabled = "";
                                        $edit_data = "edit_data";
                                      }
                            if($editable){ ?>
                    <div class="panel_toolbox"><a  href="<?= base_url('admin/services/create') ?>" class="btn btn-success">Agregar</a></div>
                     <?php } ?>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <div class="panel_toolbox"><a href="#" class="btn btn-default filters-toggle" id="filters-toggle"><span class="fa fa-filter"></span> Filtros</a></div>
    <div class="clearfix"></div>
                    <div id="filters" class="hidden" >
                          <div class="row">
                                  
                    
                                                <div class="form-group col-md-3 col-sm-3">
                                                    <label>Status</label>
                                                    <div class="control-group">
                                         
                                                          <select name="Status" id="Status" class="form-control">
                                                            <option value="">Seleccionar</option>
                                                            <option value="1">Activo</option>
                                                            <option value="2">Inestable</option>
                                                            <option value="3">Suspendido</option>
                                                            <option value="4">Cancelado</option>
                                                          </select>
                                                    </div>
                    
                                                </div>
                  

                                               
                                        
                                         <div class="form-group col-md-3 col-sm-3">
                                                    <label>Supervisor</label>
                                                    <div class="control-group">
                                         
                                                          <select name="supervisor" id="supervisor" class="form-control">
                            
                                                          <option value="">Seleccionar</option>
                                                          <?php foreach($supervisors as $supervisor):
                                                            $name = $supervisor->first_name. ' '.$supervisor->last_name; ?>
                                                          <option value="<?php echo $supervisor->id; ?>">
                                                            <?php echo $name; ?>
                                                          </option>
                                                        <?php endforeach; ?>
                                                        </select>
                                                          
                                                    </div>
                    
                                                </div>
                    
                                                  <div class="form-group col-md-4 col-sm-4">
                                                   <label>Luz</label>
                                                    <div class="control-group">
                                                      <select name="luz" id="luz" class="form-control">
                                                        <option value="">Ninguno</option>

                                                        <?php foreach($nurses as $nurse ): ?>
                                                        <option value="<?php echo $nurse->id; ?>"  <?php if ($filtered_luz ==$nurse->id){echo "selected";} ?> >
                                                            <?php echo $nurse->first_name;?> <?php echo $nurse->last_name;?>
                                                        </option>
                                                        <?php endforeach; ?>
                                                      </select>
                                                          
                                                    </div>
                                                </div>

                    
                                                </div>

                                                <div class="col-md-2 col-sm-2">
                                                    <br> <a href="<?php echo base_url(); ?>admin/services" class="btn btn-info form-control" id="filter_submit" data-rel="nurse_list">Filtrar</a>
                                                </div>
                                              </div>

                                  </div>
                  </div>

<div class="outside-report-table">

  <div class="report-table">
  
                    <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>Nombre Completo</th>
                          <th>Familia</th>
                          <th>Tipo de Plan</th>
                          <th>Horarios</th>
                          <th>Teléfonos.</th>
                          <th>Acción</th>

                        </tr>
                      </thead>
                      <tbody>
                      <?php if (count($services)): ?>
                                    <?php 


                                    foreach ($services as $key => $list):   ?>
                              <tr class="odd gradeX">
                               <td>
                               <?php 
                                    $name = json_decode($list['name']);

                                    echo "<span class='first-name-table'>". $name->first_name.' '. $name->second_name.' </span> <br/> <span class="last-name-table">'. $name->last_name.' '. $name->second_last_name .'</span>'; ?>
                               </td>
                                    <td><?php echo $list['nombre_familia'];?></td>
                                    <td><?php echo $services_meta[$list['id']]['tipo_plan']; ?></td>
                                    <td><?php 
                                              foreach($services_meta[$list['id']]['schedule'] as $nurse=>$hours){
                                                echo "<strong> ".$nurse." </strong> <br/>";
                                                echo implode("",$hours);
                                              } ?></td>
                                    
                                    <td><?php echo $list['phones']; ?></td>
                                <td>
                                  <a href="<?= base_url('admin/services/view/'.$list["id"]) ?>" class="btn btn-default btn-accion"  data-toggle="tooltip" data-placement="top" title="" data-original-title="Ver">
                                    <i class="fa fa-eye"></i> 
                                  </a>
                                  <a href="<?= base_url('admin/services/pdf/'.$list["id"]) ?>" class="btn btn-default btn-accion print_pdf" action="print" target="_blank" service-id="<?php echo $list['id']; ?>" data-toggle="tooltip" data-placement="top" title="" data-original-title="Imprimir">
                                    <i class="fa fa-print"></i> 
                                  </a>
                                  <a href="<?= base_url('admin/services/pdf/'.$list["id"]) ?>" class="btn btn-default btn-accion print_pdf" action="send-get-info"  target="_blank" service-id="<?php echo $list['id']; ?>" data-toggle="tooltip" data-placement="top" title="" data-original-title="Enviar">
                                    <i class="fa fa-envelope"></i> 
                                  </a>
                                  <?php if($editable){ ?>
                                  <a href="<?= base_url('admin/services/edit/'.$list["id"]) ?>" class="btn btn-default btn-accion"  data-toggle="tooltip" data-placement="top" title="" data-original-title="Editar">
                                    <i class="fa fa-edit"></i> 
                                  </a>
                                  <a href="<?= base_url('admin/services/delete/'.$list["id"]) ?>" class="btn btn-danger btn-eliminar" data-message="¿Estas seguro que deseas eleminiar el servicio con codigo <?php echo $list['tars_id']; ?> ?" data-toggle="tooltip" data-placement="top" title="" data-original-title="Eliminar">
                                     <i class="fa fa-remove"></i> 
                                  </a>
                                  <?php } ?>
                   </td>
                              </tr>
                      <?php endforeach; ?>
                      <?php else: ?>
                            <tr class="even gradeC">
                                <td>No data</td>
                                <td>No data</td>
                               <td>
                                  <a href="#" class="btn btn-default btn-accion"  data-toggle="tooltip" data-placement="top" title="" data-original-title="Editar">
                                    <i class="fa fa-edit"></i> 
                                  </a>
                                  <a href="#" class="btn btn-danger btn-accion"  data-toggle="tooltip" data-placement="top" title="" data-original-title="Eliminar">
                                     <i class="fa fa-remove"></i> 
                                  </a>
                   </td>
                          </tr>
                      <?php endif; ?>
                      </tbody>
                    </table>
                  </div>
              </div>
                    
                    
                  </div>
                </div>
              </div>
            </div>
          </div>
        
</div>
<!-- /#page-wrapper -->