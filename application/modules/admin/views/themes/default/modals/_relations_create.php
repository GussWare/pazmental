 <form role="form" method="POST" name="add-relations" id="form-relations">
      <div class="form-group col-md-6 col-sm-6">
        <label>Nombre (s)</label>
        <input class="form-control" placeholder="Enter first name" id="first_name" name="FirstName"  required>
    </div>
    <div class="form-group  col-md-6 col-sm-6">
        <label>Apellidos</label>
        <input class="form-control" placeholder="Enter last name" id="last_name" name="LastName"  required>
    </div>
   
        <div class="form-group  col-md-6 col-sm-6">
        <label>Email</label>
        <input class="form-control" placeholder="Enter email" id="Email" name="Email"  required>
    </div>
    
    <div class="form-group col-md-6 col-sm-6">
        <label>Password</label>
        <input type="password" class="form-control" placeholder="Enter password" id="Password" name="Password"  required>
    </div>




    <div class="form-group  col-md-6 col-sm-6">
        <label>Teléfono</label>
        <input class="form-control" placeholder="Enter phone number" id="Phone" name="Phone">
    </div>

    <div class="form-group col-md-6 col-sm-6">
        <label>Relación con el Paciente</label>
        <select name="RelationToPatient" id="RelationToPatient" class="form-control onchange">
            <option value="family-show">Familiar</option>
            <option value="doctor-hidden">Doctor</option>
        </select>
    </div>

    <div class="form-group col-md-12">
        <label>Tipo de Relación</label>
        <select name="RelationType" id="RelationType" class="form-control onchange">
            
            <option value="passive-hidden">Pasivo</option>
            <option value="payments-show">Paga</option>
            <option value="caring-hidden">Cuida</option>

        </select>
    </div>

     <div class="form-group hidden">

                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <div class="">
                            <label>
                              <input type="checkbox" class="js-switch" name="_ServicesPay[]" value="services" /> Servicios
                            </label>
                          </div>
                          <div class="">
                            <label>
                              <input type="checkbox" class="js-switch" name="_ServicesPay[]" value="food"/> Despensa
                            </label>
                          </div>
                          <div class="">
                            <label>
                              <input type="checkbox" class="js-switch" name="_ServicesPay[]" value="medicines" /> Medicamentos
                            </label>
                          </div>
                          <div class="">
                            <label>
                              <input type="checkbox" class="js-switch" name="_ServicesPay[]"  value="diapers" /> Pañales
                            </label>
                          </div>
                        </div>
   
                 
    </div>

        <input class="form-control" id="group_id" name="GroupID" value="5" type="hidden">
</form>