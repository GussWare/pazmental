          <div class="page-title">
            <div class="row"><div class="col-md-12">
              <div class="title_left">
                <?php if ($sending_mail){echo "<img src='https://ana.pazmental.mx/assets/build/img/pazmental-pdf.jpg' />";} ?>
                <h3 <?php if ($sending_mail){echo "style='font-size:25px;color:#00afaa;'";} ?>> Reporte Diario   </h3>
              </div>
              </div>
            </div>
          </div>
           <div class="">
            
            <div class="row">
          
              <div class="col-md-5">
                <h4 class="name-header" <?php if ($sending_mail){echo "style='font-size:22px;color:#00afaa;'";} ?>><span class="first-name-title"> <?php echo $first_name ?>  </span><br/>
                    <span class="last-name-title"><?php echo $last_name;  ?></span></h4>

            </div>
            
            <div class="col-md-7">
                    <div class="row daily_dates">
                    <div class="col-md-1"  ><a href="<?= base_url('admin/reportsDaily/view/'.$id.'/'.$yesterday) ?>" ><span class="fa fa-caret-left"></span> </a> 
                        </div>


                    <?php if($sending_mail){ $align = "left";}else{$align = "center";} ?>
                    <div class="col-md-6 col-sm-6" style="text-align:<?php echo $align; ?>">
                    <strong><?php echo $date; 
                         
                             $from_cal = new DateTime($date);
                              $from_cal  = $from_cal->format("m/d/Y");
                              
                            ?>
                        </strong>
                    <?php if(!$sending_mail){ ?>
                    <form id="filtering">

                    <a href="#" class="servicio-detalles center" id="filters-toggle"></span> Otra Fecha</a>

                    </div>
                      <div class="col-md-1 col-sm-1">
                        <a href="<?= base_url('admin/reportsDaily/view/'.$id.'/'.$tomorrow) ?>" ><span class="fa fa-caret-right"></span></a>  </div>
                        <div class="clearfix"></div>
                        <div id="filters" class="hidden" ><div class="col-md-2"></div><div class="form-group col-md-4" style="text-align:center;margin-top:20px">
                                                    <div class="control-group">
                                         
                                                          <div class="xdisplay_inputx form-group has-feedback">
                                                            <input type="text" value="<?php echo $start_date ?>" class="form-control has-feedback-left single_cal_picker" id="start_date" name="start_date" placeholder="Fecha de Alta" aria-describedby="inputSuccess2Status2">
                                                            <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                                            <span id="inputSuccess2Status2" class="sr-only">(success)</span>
                                                          </div>
                                                    </div>
                                                
                                    
                  
                                                   <a href="<?php echo base_url(); ?>admin/reportsDaily/view/<?php echo $id; ?>" class="btn btn-info form-control" id="filter_submit" data-rel="report_daily">Buscar</a>
                                                    </div>
                                                    
                                                </div>

                        </form>
                        <?php } ?>
                      </div>
            
            

          </div>
     


<div class="outside-report-table">
  <div class="report-table">
            <div class="clearfix"></div> 

            <?php foreach($datos_servicio AS $tars_id =>  $servicio): ?>
            
                <div class="row servicio-detalles"  style="margin-top:20px">
                    <div class="col-md-4"> <strong>  Hora de inicio: </strong><br/>
                        <?php
                        if($servicio["service_plan"] == TIPO_SERVICIO_TRES_PUNTO_CINCO):
                          echo $servicio['fecha_inicio'];
                        else:
                          echo $servicio['hora_inicio'];
                        endif;
                         ?>  
                    </div>
                    <div class="col-md-4"> <strong>  Estado anímico inicio de servicio: </strong><br/><?php echo $servicio['sintoma_inicio'];?></div>
                    <div class="col-md-4"> <strong> Nombre de la Luz: </strong><br/><?php echo $servicio['nombre_luz'];?>  </div>
                </div>


                <div class="row servicio-detalles"  style="margin-top:20px">
                    <div class="col-md-4"> <strong>  Hora de fin: </strong><br/>
                        <?php
                        if($servicio["service_plan"] == TIPO_SERVICIO_TRES_PUNTO_CINCO):
                          echo $servicio['fecha_fin'];
                        else:
                          echo $servicio['hora_fin'];
                        endif;
                         ?>  
                    </div>
                    <div class="col-md-4"> <strong>  Estado anímico fin de servicio: </strong><br/><?php echo $servicio['sintoma_fin'];?> </div>

                    <?php if(isset($group_name) && $group_name != "familiares"): ?>
                      <div class="col-md-4"> <strong> Comentarios generales: </strong><br/><?php echo $servicio['comentarios_generales'];?>  </div>
                    <?php endif; ?>
                </div>

            <?php endforeach; ?>

<div class="clearfix"></div>

            <section class="alimento-diario" style="margin-top:20px">
               <div class="page-title"><div class="title_left"> <h6 <?php if ($sending_mail){echo "style='font-size:22px;color:#00afaa;'";} ?>> Alimentos <span class="side-icons alimentacion"></span></h6></div></div>
               <div class="clearfix"></div>
            <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap table-report-daily " style="text-align:left;<?php if ($sending_mail){ echo 'background-color:#dbd9d6;padding:15px';} ?>" cellspacing="0" width="90%">
                <tr><th> Desayuno</th> <th> Comida </th> <th> Cena </th> <th> Líquidos </th> <th> Colaciones </th> </tr>
                <tr>  
                  <td  class="col-md-3">
                    <?php 
                      if(isset($alimento['Desayuno'])): 
                        foreach($alimento['Desayuno'] AS $desayuno):
                          echo $desayuno;
                        endforeach;
                      endif; 
                    ?>
                  </td>
                  <td  class="col-md-3">
                    <?php 
                      if(isset($alimento['Comida'])): 
                        foreach($alimento['Comida'] AS $comida):
                          echo $comida;
                        endforeach;
                      endif; 
                    ?>
                  </td>
                  <td  class="col-md-3">
                    <?php 
                      if(isset($alimento['Cena'])): 
                        foreach($alimento['Cena'] AS $cena):
                          echo $cena;
                        endforeach;
                      endif; 
                    ?>
                  </td>
                  <td  class="col-md-3">
                    <?php 
                      if(isset($alimento['Liquidos'])): 
                        foreach($alimento['Liquidos'] AS $liquidos):
                          echo $liquidos;
                        endforeach;
                      endif; 
                    ?>
                  </td>
                  <td  class="col-md-3">
                    <?php 
                      if(isset($alimento['Colacion'])): 
                        foreach($alimento['Colacion'] AS $colacion):
                          echo $colacion;
                        endforeach;
                      endif; 
                    ?>
                  </td>
                </tr>
            </table>
            </section>
            <div class="clearfix"></div>

            <section class="medicamento-diario" style="margin-top:20px">
              <div class="page-title"><div class="title_left"> <h6 <?php if ($sending_mail){echo "style='font-size:22px;color:#00afaa;'";} ?>> Medicamentos <span class="side-icons medicinas"></span></h6></div></div>
              <div class="clearfix"></div>
            <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap table-report-daily" style="text-align:left;<?php if ($sending_mail){ echo 'background-color:#dbd9d6;padding:15px';} ?>" cellspacing="0" width="90%">
               
               <tr> <th > Medicamento</th>  
                <?php for($d =1; $d <= $dosis ;$d++){?> 
                  <th > <?php echo "Dosis ". $d; ?></th> 
                  <?php } ?>
                </tr>
                <?php foreach(array_keys($medicamentos) as $m){?> 
                <tr>
                  <td class='daily-col'> <?php echo $m; ?> </i></td> 
                
                  <?php foreach($medicamentos[$m] as $d){?> 

                  <td class="col-md-3"><?php   echo $d;  ?></td>
                  <?php } ?>
                </tr>
                <?php } ?>
 
            </table>
            </section>
            <div class="clearfix"></div>

            <section class="signos-vitales-diario" style="margin-top:20px">
            <div class="page-title"><div class="title_left"> <h6 <?php if ($sending_mail){echo "style='font-size:22px;color:#00afaa;'";} ?>> Signos Vitales <span class="side-icons signos-vitales"></span></h6></div></div>
            <div class="clearfix"></div>
            <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap table-report-daily" style="text-align:left;<?php if ($sending_mail){ echo 'background-color:#dbd9d6;padding:15px';} ?>" cellspacing="0" width="90%">
                <tr><th> Signo Vital </th>
                <?php foreach($signos_horas as $hora){ ?>
                  <th> <?php echo $hora; ?> </th>
                <?php } ?>
                </tr>
                <?php foreach(array_keys($signos_vitales) as $sv){ ?>
                <tr> <th> <?php echo $sv; ?> </td> 
                   <?php foreach($signos_vitales[$sv] as $hora => $signo){ ?> 
                      <td> <?php foreach($signo AS $valor): echo $valor ."<br />"; endforeach; ?> </td>
                    <?php } ?>
                </tr>
                <?php } ?>
 
            </table>
            </section>
            <div class="clearfix"></div>

            <section class="estimulaciones" style="margin-top:20px">
              <div class="page-title"><div class="title_left"> <h6 <?php if ($sending_mail){echo "style='font-size:22px;color:#00afaa;'";} ?>> Otros Datos <span class="side-icons otros-datos"></span></h6></div></div>
            <table id="datatable-" class=" table table-striped  table-bordered dt-responsive nowrap table-report-daily table-report" style="text-align:left;width:91%;<?php if ($sending_mail){ echo 'background-color:#dbd9d6;padding:15px';} ?>" cellspacing="0" width="90%">
               <?php 
                    $table_size =0; 
                    foreach($otros_datos as $od){
                      if(sizeof($od) > $table_size){ 
                        $table_size = sizeof($od);
                      }
                  } 
                ?>


               
               <?php foreach(array_keys($otros_datos) as $key){ ?>

               <tr><th class='daily-col'><?php echo $key; ?> </th>

                <?php for($i=0; $i < $table_size ; $i++){ 
                    $data = $otros_datos[$key][$i]; ?> 
                  <td> <?php echo $data; ?></td> 
                  <?php } ?>
              </tr> 
                <?php } ?>  
 
            </table>

             

            </section>
            </div>
            </div>

            <?php if(!$sending_mail){ ?>
            <div class="col-md-9 col-sm-9" style="text-align:right;">
                       
                      <a href="<?= base_url('admin/reportsDaily/view/'.$list["id"]) ?>" class="btn btn-info print_daily_pdf"  data-toggle="tooltip" report-id="<?php echo $id; ?>"   date="<?php echo $today; ?>" action="send-get-info" data-placement="top" title="" data-original-title="Enviar"> Enviar </a>

            </div>
            <?php }  ?>

         
        <br><br><br>

      </div>
    </div>
  </div>