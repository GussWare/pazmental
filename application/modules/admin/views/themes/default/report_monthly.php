<?php
  ob_end_flush();
  ob_implicit_flush();
?>
          
          <div class="">
            <div class="page-title">
               <div class="title_left">
                <h3> Reporte Mensual <span class="fa fa-bar-chart"></span>  </h3>
              
              </div>

            </div>

            <div class="clearfix"></div>
              <div class="row">
              <div class="col-md-5">
                <h4 class="name-header"> <span class="first-name-title"> <?php echo $first_name ?>  </span><br/>
                    <span class="last-name-title"><?php echo $last_name;  ?></span></h4>

            </div>
          <div class="col-md-7">

<form id="filtering">
           <?php $from_cal = new DateTime($from);
                              $from_cal  = $from_cal->format("m/d/Y");
                              $to_cal = new DateTime($to);
                              $to_cal  = $to_cal->format("m/d/Y");

                  /* PARA CONTROLAR ACCESOS*/
                  $editable = false;
                  $disabled = "disabled";
                  $edit_data ="";
                  $save = false;
                  
                  if($group_name == 'admin' or $group_name == "supervisor"){
                      $editable = true; 
                      $disabled = "";
                      $edit_data = "edit_data";
                      $save = true;
                    }
                     if($group_name == 'doctors'){
                      $disabled = "";
                      $save = true;

                    }


            ?>
         
         <div id="filters"><div class="form-group col-md-4 col-sm-4">
                                                    <label>Desde</label>
                                                    <div class="control-group">
                                         
                                                          <div class="xdisplay_inputx form-group has-feedback">
                                                            <input type="text" class="form-control has-feedback-left single_cal_picker" id="start_date" name="start_date" value="<?php echo $from_cal; ?>" aria-describedby="inputSuccess2Status2">
                                                            <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                                            <span id="inputSuccess2Status2" class="sr-only">(success)</span>
                                                          </div>
                                                    </div>
                                                </div>
                                    
                    
                                       <div class="form-group col-md-4 col-sm-4">
                                                    <label>Hasta</label>
                                                    <div class="control-group">
                                         
                                                          <div class="xdisplay_inputx form-group has-feedback">
                                                            <input type="text" class="form-control has-feedback-left single_cal_picker" id="end_date" name="end_date" value="<?php echo $to_cal; ?>" aria-describedby="inputSuccess2Status2">
                                                            <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                                            <span id="inputSuccess2Status2" class="sr-only">(success)</span>
                                                          </div>
                                                    </div>
                                                </div>
                    

                                                <div class="col-md-3 col-sm-3">
                                                     <br> <a  class="filters-toggle" href="<?php echo base_url(); ?>admin/reports/edit/<?php echo $paciente->id; ?>" class="btn btn-info form-control "  id="filter_submit" data-rel="reports">Filtrar <span class="fa fa-filter"></span></a>
                                                    
                                                </div>
                                                                    

                                  </div>
                  

</form>

            </div>
          </div>
</div>  
<div class="outside-report-table">

  <div class="report-table">

                  <ul class="nav nav-tabs nav-tabs-resumen">
          <?php   
                              $to = new DateTime($to);
                              $to = $to->modify('+1 day');
                              $period = new DatePeriod(
                                new DateTime($from),
                                new DateInterval('P1D'),
                                $to
                              );


          ?>
          <?php  $i = 0; foreach($reports_type as $k=>$type): $i++; ?>
           <?php 

                $span_class = strtolower($type);
                $span_class = strtr(utf8_decode($span_class), utf8_decode('àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ.'), 'aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY ');
                $span_class = str_replace(" ", "_",$span_class);

            ?>

              <li <?php if($i==0){echo 'class="active"';}?>> <span class="top-icons <?php echo $span_class;?>"></span> <a class="top-icons-a" href="#<?php echo $k;?>" data-toggle="tab"><?php echo $type;?></a></li>
          <?php endforeach; ?>
        </ul>

              

           
                    <div class="tab-content">

                         <div class="tab-pane " id="sintoma" >
                          <!--  Manejar multiples ejercicios iguales por dia -->
                          <table  class="table table-striped table-bordered dt-responsive nowrap table-report" style="display: block;overflow-x: auto;white-space: nowrap;">
                            <tr><th class="headcol" > Síntoma </th>
                          <?php 
                            
                              


                              foreach($period as $p){  
                                 
                                 ?>

                                 <th> <?php echo $dias[$p->format('w')]. "<br/>";
                                  echo $p->format('d-m-Y'); ?> </th>
                              <?php }
                               ?>

                             </tr>

                                <?php
        

        if(array_key_exists('sintoma',$report_data)){  
        
                                 foreach($sintomas as $sintoma){ ?>
                                      <tr>
                                        <th class="headcol" > <?php echo $sintoma; ?></th>
                                        <?php foreach($period as $p){  
                                            if(array_key_exists($p->format('d-m-Y'), $report_data['sintoma'][$sintoma])) { ?>

                                              <td> 
                                              <?php foreach( $report_data['sintoma'][$sintoma][$p->format('d-m-Y')] as $r){ 
                                                $date = new DateTime($r->timestamp);
                                                $date = $date->format('H:i');
                                                $id  = $r->id;
                                               ?>

                                              <span class="<?php echo $edit_data; ?>" data-rel= "<?php echo base_url();?>admin/reports/edit_tars/<?php echo $r->report_id; ?>" title="Editar Dato"  report-type="<?php echo $r->report_type;?>" service-id="<?php echo $r->servicio_id; ?>"> <?php echo $date."<br/>";?> </span>
                                              <?php } ?>
                                            </td>
                                            <?php } else{  ?>
                                               <td> </td>
                                         <?php }} ?>

                                      </tr>
                              <?php }} ?>
                            </table>
  
              <div id="sintomas1" style="width: 100%; height: 600px;"></div>
              <input <?php echo $disabled; ?> id="sintomas_report" name="sintomas_report" type="hidden" value='<?php echo json_encode($report_sintoma); ?>' />
              <input <?php echo $disabled; ?> id="sintomas_report_chart" name="sintomas_report_chart" type="hidden" value='<?php echo json_encode($report_hora_sintoma); ?>' />

        <div class="row" style="margin-top:100px">
              <?php foreach($sintomas as $sintoma){ ?>

        <div class="col-md-6">                <div class="sintoma_chart" id="<?php echo str_replace(' ', '_', $sintoma);?>"></div></div>

              <?php } ?>
        </div>
                </div>
              <!-- Aqui va el contenido dinámico de la tabla -->

                <div class="tab-pane " id="medicamento" >
                          <table  class="table table-striped table-bordered dt-responsive nowrap table-report" style="display: block;overflow-x: auto;white-space: nowrap;">
                            <tr><th class="headcol"> Medicamento </th>
                          <?php 

                            
                              
                              foreach($period as $p){  
                                 ?>
                                 <th> <?php echo $dias[$p->format('w')]. "<br/>";
                                  echo $p->format('d-m-Y'); ?> </th>
                              <?php }
                               ?>
                             </tr>
                                <?php    
        if(array_key_exists('medicamento',$report_data)){        
                   ksort($report_data['medicamento']);
                                  
                                 foreach(array_keys($report_data['medicamento'])  as $medicamento){ ?>
                                      <tr>
                                        <th class="headcol"> <?php echo $medicamento; ?></th>
                                        <?php foreach($period as $p){  
                                            if(array_key_exists($p->format('d-m-Y'),$report_data['medicamento'][$medicamento])){
                                              $r = $report_data['medicamento'][$medicamento][$p->format('d-m-Y')]['raw_data'];
                                              $value = $report_data['medicamento'][$medicamento][$p->format('d-m-Y')]['tuple'];
                                               ?>
                                              <td>  
                                                <?php for($i = 0; $i < sizeof($r);$i++){ ?>
                                                  <span class="<?php echo $edit_data; ?>" data-rel= "<?php echo base_url();?>admin/reports/edit_tars/<?php echo $r[$i]->report_id; ?>" title="Editar Dato"  report-type="<?php echo $r[$i]->report_type;?>" service-id="<?php echo $r[$i]->servicio_id; ?>">  <?php echo $value[$i]; ?> </span> 
                                                  <br/>
                                                <?php } ?>
                                                </td>
                                            <?php }  else{  ?>
                                               <td> </td>
                                         <?php } } ?>

                                      </tr>
                              <?php } } ?>
                            </table>

                      <div id="medicamento1" style="width: 100%; height: 600px;"></div>
          <input <?php echo $disabled; ?> id="medicamento_report" name="medicamento_report" type="hidden" value='<?php echo json_encode($medicamentos); ?>' />

                         </div>

                         <div class="tab-pane" id="estimulacion_cognitiva" >
                          <table  class="table table-striped table-bordered dt-responsive nowrap table-report" style="display: block;overflow-x: auto;white-space: nowrap;">
                            <!-- Falta poner cuál ejercicio se hizo. Manejar multiples ejercicios iguales por dia -->
                            <tr><th class="headcol"> Estimulación Cognitiva </th>
                          <?php 
                              
                              foreach($period as $p){  
                                 ?>
                                 <th> <?php echo $dias[$p->format('w')]. "<br/>";
                                  echo $p->format('d-m-Y'); ?> </th>
                              <?php }
                               ?>
                             </tr>
                                <?php 
        if(array_key_exists('estimulacion_cognitiva',$report_data)){
                                 foreach(array_keys($report_data['estimulacion_cognitiva']) as $est_cognitiva){ ?>
                                      <tr>
                                        <th class="headcol"> <?php echo $est_cognitiva; ?></th>
                                        <?php foreach($period as $p){  
                                            if(array_key_exists($p->format('d-m-Y'),$report_data['estimulacion_cognitiva'][$est_cognitiva])){
                                              //$data=$report_data['estimulacion_cognitiva'][$est_cognitiva][$p->format('d-m-Y')];
                                              //$report_meta = unserialize($data->report_meta); 
                                                ?>
                                               
                                              <td>    
                                                  <?php 
                                                    foreach($report_data['estimulacion_cognitiva'][$est_cognitiva][$p->format('d-m-Y')] AS $data): 
                                                      $report_meta = unserialize($data->report_meta);   
                                                  ?>
                                                    <span class="<?php echo $edit_data; ?>" data-rel= "<?php echo base_url();?>admin/reports/edit_tars/<?php echo $data->report_id; ?>" title="Editar Dato"  report-type="<?php echo $data->report_type;?>" service-id="<?php echo $data->servicio_id; ?>"><?php if ($report_meta['como_le_fue'] == "Bien"){  ?><span class="happy"></span><?php }
                                                          elseif ($report_meta['como_le_fue'] == "Regular"){   ?> <span class="meh"></span></i> <?php }
                                                          elseif ($report_meta['como_le_fue'] == "Mal"){  ?> <span class="sad"></span> <?php }?> </span>

                                                  <?php endforeach; ?>
                                              </td>
                                            <?php }
                                            else{  ?>
                                               <td> </td>
                                         <?php }} ?>

                                      </tr>
                              <?php }} ?>
                            </table>

              <div id="estcognitiva1" style="width: 100%; height: 600px;"></div>
              <input <?php echo $disabled; ?> id="est_cognitiva" name="est_cognitiva" type="hidden" value='<?php echo json_encode($report_est_cognitiva); ?>' />
              <div id="linecognitiva1" style="width: 100%; height: 600px;"></div>
              <input <?php echo $disabled; ?> id="line_cognitiva" name="line_cognitiva" type="hidden" value='<?php echo json_encode($report_line_cognitiva); ?>' />

                         </div>

                         <div class="tab-pane" id="estimulacion_fisica" >
                          <table  class="table table-striped table-bordered dt-responsive nowrap table-report" style="display: block;overflow-x: auto;white-space: nowrap;">
                            <tr><th class="headcol"> Estimulación Física </th>
                              <!--  Manejar multiples ejercicios iguales por dia -->
                          <?php 
                              
                              foreach($period as $p){  
                                 ?>
                                 <th> <?php echo $dias[$p->format('w')]. "<br/>";
                                  echo $p->format('d-m-Y'); ?> </th>
                              <?php }
                               ?>
                             </tr>
                                <?php  
        if(array_key_exists('estimulacion_fisica',$report_data)){
                                 foreach(array_keys($report_data['estimulacion_fisica']) as $est_fisica){ ?>
                                      <tr>
                                        <th class="headcol"> <?php echo $est_fisica; ?></th>
                                        <?php foreach($period as $p){  
                                            if(array_key_exists($p->format('d-m-Y'),$report_data['estimulacion_fisica'][$est_fisica])){
                                              ?>
                                              <td> 
                                              <?php 
                                              foreach($report_data['estimulacion_fisica'][$est_fisica][$p->format('d-m-Y')] AS $data):
                                                $d = new DateTime($data->timestamp);
                                                $date = $d->format('H:i');
                                               ?>
                                              <span class="<?php echo $edit_data; ?>" data-rel= "<?php echo base_url();?>admin/reports/edit_tars/<?php echo $data->report_id; ?>" title="Editar Dato"  report-type="<?php echo $data->report_type;?>" service-id="<?php echo $data->servicio_id; ?>"><?php echo $date; ?><br /></span> 
                                              <?php  endforeach; ?>
                                              </td>
                                            <?php 
                                             
                                            }
                                            else{  ?>
                                               <td> </td>
                                         <?php }} ?>

                                      </tr>
                              <?php }} ?>
                            </table>
              <div id="estfisica1" style="width: 100%; height: 600px;"></div>
              <input <?php echo $disabled; ?> id="est_fisica" name="est_fisica" type="hidden" value='<?php echo json_encode($report_est_fisica); ?>' />
                         </div>

                         <div class="tab-pane" id="estimulacion_otra" >
                          <table  class="table table-striped table-bordered dt-responsive nowrap table-report" style="display: block;overflow-x: auto;white-space: nowrap;">
                            <tr><th class="headcol"> Otras Estimulaciones </th>
                              <!--  Manejar multiples ejercicios iguales por dia -->
                          <?php 
                              
                              foreach($period as $p){  
                                 ?>
                                 <th> <?php echo $dias[$p->format('w')]. "<br/>";
                                  echo $p->format('d-m-Y'); ?> </th>
                              <?php }
                               ?>
                             </tr>
                                <?php  
        if(array_key_exists('estimulacion_otra',$report_data)){
                                 foreach(array_keys($report_data['estimulacion_otra']) as $est_otra){ ?>
                                      <tr>
                                        <th  class="headcol"> <?php echo $est_otra; ?></th>
                                        <?php foreach($period as $p){  
                                            if(array_key_exists($p->format('d-m-Y'),$report_data['estimulacion_otra'][$est_otra])){
                                              $data=$report_data['estimulacion_otra'][$est_otra][$p->format('d-m-Y')];
                                               ?>
                                              <td>  
                                                <?php foreach($report_data['estimulacion_otra'][$est_otra][$p->format('d-m-Y')] AS $data): 
                                                   $d = new DateTime($data->timestamp);
                                                   $date = $d->format('H:i');
                                                  ?>
                                                <span class="<?php echo $edit_data; ?>" data-rel= "<?php echo base_url();?>admin/reports/edit_tars/<?php echo $data->report_id; ?>" title="Editar Dato"  report-type="<?php echo $data->report_type;?>" service-id="<?php echo $data->servicio_id; ?>"><?php echo $date; ?> <br /></span>
                                                <?php endforeach; ?>
                                              </td>
                                            <?php }
                                            else{  ?>
                                               <td> </td>
                                         <?php }} ?>

                                      </tr>
                              <?php }} ?>
                            </table>
              <div id="estotra1" style="width: 100%; height: 600px;"></div>
              <input <?php echo $disabled; ?> id="est_otra" name="est_otra" type="hidden" value='<?php echo json_encode($report_est_otra); ?>' />
                         </div>
              <!-- Aqui va el contenido : Falta convertir en columnas para evacuacion y micción, falta manejar múltiples datos por celda-->
                      <div class="tab-pane" id="evacuacion" >
                          <table  class="table table-striped table-bordered dt-responsive nowrap table-report" style="display: block;overflow-x: auto;white-space: nowrap;">
                            <tr><th class="headcol"> Evacuación - Micción </th>
                          <?php 
                              
                              foreach($period as $p){  
                                 ?>
                                 <th> <?php echo $dias[$p->format('w')]. "<br/>";
                                  echo $p->format('d-m-Y'); ?> </th>
                              <?php }
                               ?>
                             </tr>
                                <?php   
        if(array_key_exists('evacuacion',$report_data)){
                                 foreach($evacuacion as $evac){ ?>
                                      <tr>
                                        <th  class="headcol"> <?php echo $evac; ?></th>
                                        <?php foreach($period as $p){  
                                            if(array_key_exists($evac, $report_data['evacuacion'])){
                if(array_key_exists($p->format('d-m-Y'),$report_data['evacuacion'][$evac])){
                                               ?>
                                              <td>
                                              <?php foreach($report_data['evacuacion'][$evac][$p->format('d-m-Y')] AS $data): 
                                                  $report_meta = unserialize($data->report_meta); 
                                                ?>  
                                                <span class="<?php echo $edit_data; ?>" data-rel= "<?php echo base_url();?>admin/reports/edit_tars/<?php echo $data->report_id; ?>" title="Editar Dato"  report-type="<?php echo $data->report_type;?>" service-id="<?php echo $data->servicio_id; ?>"><?php echo $report_meta['evacuacion_o_miccion']; ?> <br />
                                              <?php endforeach; ?>
                                              </td></td>
                                            <?php }
                                            else{  ?>
                                               <td> </td>
                                         <?php }} else{ ?> <td> </td> <?php }} ?>

                                      </tr>
                              <?php } } ?>
                            </table>
                    <div class="row">
         <div class="col-md-6"> <div id="evac1" style="height: 600px;"></div></div>
         <div class="col-md-6"> <div id="micc1" style="height: 600px;"></div></div>
        </div>

              <input <?php echo $disabled; ?> id="evac" name="evac" type="hidden" value='<?php echo json_encode($report_evac_micc); ?>' />

                         </div>

                              <!-- Aqui va el contenido : Falta convertir en columnas para evacuacion y micción, falta manejar múltiples datos por celda-->
                      <div class="tab-pane" id="alimento" >
                          <table  class="table table-striped table-bordered dt-responsive nowrap table-report" style="display: block;overflow-x: auto;white-space: nowrap;">
                            <tr><th class="headcol"> Alimentos </th>
                          <?php 
                              
                              foreach($period as $p){  
                                 ?>
                                  <th> <?php echo $dias[$p->format('w')]. "<br/>";
                                  echo $p->format('d-m-Y'); ?> </th>
                              <?php }
                               ?>
                             </tr>
                                <?php 
        if(array_key_exists('alimento',$report_data)){        
                                 foreach($alimentacion as $alim){ ?>
                                      <tr>
              
                                        <th  class="headcol"> <?php echo $alim; ?></th>

                                        <?php foreach($period as $p){  

                                            if(array_key_exists($alim, $report_data['alimento'])){
                                            if(array_key_exists($p->format('d-m-Y'),$report_data['alimento'][$alim])){
                                               ?>
                                              <td>
                                                <?php foreach($report_data['alimento'][$alim][$p->format('d-m-Y')] AS $data): 
                                                    $report_meta = unserialize($data->report_meta); 
                                                  ?>  
                                                  <span class="<?php echo $edit_data; ?>" data-rel= "<?php echo base_url();?>admin/reports/edit_tars/<?php echo $data->report_id; ?>" title="Editar Dato"  report-type="<?php echo $data->report_type;?>" service-id="<?php echo $data->servicio_id; ?>"><?php if ($report_meta['como_comio'] == "Bien"){  ?> <span class="happy"></span> <?php }
                                                          elseif ($report_meta['como_comio'] == "Regular"){   ?> <span class="meh"></span></i> <?php }
                                                          elseif ($report_meta['como_comio'] == "Mal"){  ?> <span class="sad"></span> <?php }?> </span>
                                                 <?php endforeach; ?>
                                               </td>
                                            <?php }
                                            else{  ?>
                                               <td> </td>
                                         <?php }} else{  ?>
                                               <td> </td><?php }}  ?>

                                      </tr>
                              <?php }} ?>
                            </table>


              <div id="alimentacion1" style="width: 100%; height: 600px;"></div>

              <input <?php echo $disabled; ?> id="alim" name="alim" type="hidden" value='<?php echo json_encode($report_alimentos); ?>' />
                         </div>

                                                       
                      <div class="tab-pane" id="signos_vitales" >
                          <table  class="table table-striped table-bordered dt-responsive nowrap table-report" style="display: block;overflow-x: auto;white-space: nowrap;">
                            <tr><th class="headcol"> Signos Vitales </th>
                          <?php 

                              
                              foreach($period as $p){  
                                 ?>
                                  <th> <?php echo $dias[$p->format('w')]. "<br/>";
                                  echo $p->format('d-m-Y'); ?> </th>
                              <?php }
                               ?>
                             </tr>
                                <?php 
        if(array_key_exists('signos_vitales',$report_data)){        
                                 foreach(array_keys($report_data['signos_vitales']) as $sv){ ?>
                                      <tr>
                                        <th  class="headcol"> <?php echo $sv; ?></th>

                                        <?php foreach($period as $p){  
                                            if(array_key_exists($p->format('d-m-Y'),$report_data['signos_vitales'][$sv])){ ?>
                                              <td> 

                                              <?php

                                                   foreach ($report_data['signos_vitales'][$sv][$p->format('d-m-Y')]  as $data){ 
                                                  $report_meta = unserialize($data->report_meta); 

                                               ?>
                                              <span class="<?php echo $edit_data; ?>" data-rel= "<?php echo base_url();?>admin/reports/edit_tars/<?php echo $data->report_id; ?>" title="Editar Dato"  report-type="<?php echo $data->report_type;?>" service-id="<?php echo $data->servicio_id; ?>"> - <?php echo $report_meta[$sv]; ?> - </span> 
                                               <?php } ?>
                                               </td>
                                            <?php }
                                            else{  ?>
                                               <td> </td>
                                         <?php }} ?>

                                      </tr>
                              <?php }} ?>
                            </table>

                           


              <input <?php echo $disabled; ?> id="signos_report_chart" name="signos_report_chart" type="hidden" value='<?php echo json_encode($report_signo); ?>' />

        <div class="row" style="margin-top:100px">
              <?php foreach(array_keys($report_signo) as $signo){ ?>

        <div class="col-md-6">                <div class="signo_chart" style="height:600px" id="<?php echo str_replace(' ', '_', $signo);?>"></div></div>

              <?php } ?>
                         </div>
            
            </div> 

            
      

      <div class="tab-pane active"   id="observacion_general" >

              <br/>
              <br/>
         
            <form   id="update-report">
              <input <?php echo $disabled; ?> type="hidden" id="paciente_id" name="paciente_id" value="<?php echo $paciente->id; ?>" />
              <div class="row">
                <div class="col-md-2">
                  <label for="">Luces</label>
                </div>
                <div class="col-md-5">
                  <div class="form-group"><span><?php echo implode(", " ,$report_luces) ; ?><span></div>
                  <div class="form-group"><input <?php echo $disabled; ?> type="text" class="form-control" name="luces" id="luces" value="<?php echo (isset($pacientes_meta['_report_luces'])) ? $pacientes_meta['_report_luces'] : ''; ?>"></div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-2">

                  <label for="">Supervisor</label>
                  </div>
                <div class="col-md-5">
                  <div class="form-group"><span><?php echo implode(", ",$report_supervisores); ?>"<span></div>
                  <div class="form-group"><input <?php echo $disabled; ?> type="text" class="form-control" name="supervisor" id="supervisor" value="<?php echo (isset($pacientes_meta['_report_supervisor'])) ? $pacientes_meta['_report_supervisor'] : ''; ?>"></div>
                </div>
              </div>   
               <div class="row">
                <div class="col-md-2">
                  <label for="">Médico</label>
                  </div>
                <div class="col-md-5">
                  <div class="form-group"><span><?php echo  implode(", ", $report_medico); ?>"</span></div>
                  <div class="form-group"><input <?php echo $disabled; ?> type="text" class="form-control" name="medico" id="medico" value="<?php echo (isset($pacientes_meta['_report_medico'])) ? $pacientes_meta['_report_medico'] : ''; ?>"></div>
                </div>
              </div> 

              <div class="row">
                <div class="col-md-2">
                  <label for="">Comentario de la luz</label>
                </div>
                <div class= "col-md-9">
                  <div class="form-group"><p><?php echo implode("\n\n", $report_comentarios_mes); ?></p></div>
                  <div class="form-group"><textarea <?php echo $disabled; ?>  rows="5" style="height: 100px;" class="form-control" name="comentarios_mes" id="comentarios_mes" ><?php echo (isset($pacientes_meta['_report_comentarios_mes'])) ? $pacientes_meta['_report_comentarios_mes'] : ''; ?></textarea></div>
                </div>
              </div> 

              <div class="row">
                <div class="col-md-2">
                  <label for="">Objetivos del Mes</label>
                </div>
                <div class= "col-md-9">
                  <div class="form-group"><textarea <?php echo $disabled; ?>   style="height: 100px;" class="form-control" name="objetivos_mes" id="objetivos_mes" > <?php echo (isset($pacientes_meta['_report_objetivos_mes'])) ? $pacientes_meta['_report_objetivos_mes'] : ''; ?> </textarea></div>
                </div>
              </div>     

              <div class="row">
                <div class="col-md-2">
                  <label for="">Objetivos Para el Próximo Mes</label>
                </div>
                <div class= "col-md-9">
                  <div class="form-group"><textarea <?php echo $disabled; ?> rows="5"  style="height: 100px;" class="form-control" name="objetivos_prox_mes" id="objetivos_prox_mes"><?php echo (isset($pacientes_meta['_report_objetivos_prox_mes'])) ? $pacientes_meta['_report_objetivos_prox_mes'] : ''; ?></textarea></div>
                </div>
              </div>    

              <div class="row">
                <div class="col-md-2">
                  <label for="">Análisis de Resultados</label>
                </div>
                <div class= "col-md-9">
                  <div class="form-group"><textarea <?php echo $disabled; ?>   rows="5" style="height: 100px;" class="form-control" name="analisis_resultados" id="analisis_resultados" ><?php echo (isset($pacientes_meta['_report_analisis_resultados'])) ? $pacientes_meta['_report_analisis_resultados'] : ''; ?></textarea></div>
                </div>
              </div>           
                           

              <div class="row">
                <div class="col-md-2">
                  <label for="">Principales Logros</label>
                </div>
                <div class= "col-md-9">
                  <div class="form-group"><textarea <?php echo $disabled; ?>  rows="5" style="height: 100px;" class="form-control" name="principales_logros" id="principales_logros" ><?php echo (isset($pacientes_meta['_report_principales_logros'])) ? $pacientes_meta['_report_principales_logros'] : ''; ?></textarea></div>
                </div>
              </div>                     

              <div class="row">
                <div class="col-md-2">
                  <label for="">Principales Oportunidades</label>
                </div>
                <div class= "col-md-9">
                  <div class="form-group"><textarea <?php echo $disabled; ?> rows="5" style="height: 100px;" class="form-control" name="principales_oportunidades" id="principales_oportunidades" ><?php echo (isset($pacientes_meta['_report_principales_oportunidades'])) ? $pacientes_meta['_report_principales_oportunidades'] : ''; ?></textarea></div>
                </div>
              </div>  

              <div class="row">
                <div class="col-md-2">

                  <label for="">Informe Médico </label>
                  </div>
                <div class="col-md-9">
                  <div class="form-group"><textarea <?php echo $disabled; ?> rows="5" style="height: 100px;" class="form-control" name="informe_medico" id="informe_medico" ><?php echo (isset($pacientes_meta['_report_informe_medico'])) ? $pacientes_meta['_report_informe_medico'] : ''; ?></textarea></div>
                </div>
              </div>  

              <div class="row">
                <div class="col-md-2">

                  <label for="">Informe de Rehabilitación </label>
                  </div>
                <div class="col-md-9">
                  <div class="form-group"><textarea <?php echo $disabled; ?> rows="5" style="height: 100px;" class="form-control" name="informe_rehabilitacion" id="informe_rehabilitacion" ><?php echo (isset($pacientes_meta['_report_informe_rehabilitacion'])) ? $pacientes_meta['_report_informe_rehabilitacion'] : ''; ?></textarea></div>
                </div>
              </div>  

              <div class="row">
                <div class="col-md-2">

                  <label for="">Informe Neuropsicológico </label>
                  </div>
                <div class="col-md-9">
                  <div class="form-group"><textarea <?php echo $disabled; ?> rows="5" style="height: 100px;" class="form-control" name="informe_neuropsicologico" id="informe_neuropsicologico" ><?php echo (isset($pacientes_meta['_report_informe_neuropsicologico'])) ? $pacientes_meta['_report_informe_neuropsicologico'] : ''; ?></textarea></div>
                </div>
              </div> 

              <div class="row">
                <div class="col-md-2">

                  <label for="">Notas Internas </label>
                  </div>
                <div class="col-md-9">
                <div class="form-group"><textarea <?php echo $disabled; ?>  rows="5" style="height: 100px;" class="form-control" name="notas_internas" id="notas_internas" ><?php echo (isset($pacientes_meta['_report_notas_internas'])) ? $pacientes_meta['_report_notas_internas'] : ''; ?></textarea></div>
                </div>
              </div> 

              
              <br/>
               <div class="row" >
                <div class="col-md-12">

                    <input <?php echo $disabled; ?> type="hidden" id="wcloud" name="wcloud" value='<?php echo json_encode($word_cloud); ?>'/>
                      <div class="word-cloud" id="word-cloud" style="width: 700px; height: 300px;"></div>
                </div>
              </div>
              <div class= "row">
                <div class= "col-md-12">
                  <label style="margin-top:100px"for="">Acividades que deseas incluir en el reporte:</label><br/>
                    <?php  $i=0; foreach($reports_type as $k=>$type):$i++;  ?>
                      
                        <label style="padding:10px"><input <?php echo $disabled; ?> type="checkbox" name="reports_included[]" id="reports_include_<?php echo $k;?>" value="<?php echo $k;?>" <?php if(in_array($k,$reportes_incluidos_arr)){ echo "checked"; } ?> > <?php echo $type;?></label>
                        <?php if ($i == 4){ echo "<br/>";}?>
                    <?php endforeach; ?>


                    <div class="content-signos-vitales" style="display:<?php echo (in_array('signos_vitales', $reportes_incluidos_arr) ? 'block' : 'none') ?>">
                      <label style="margin-top:10px" for="">Signos vitales que deseas incluir:</label>
                      <br />
                      
                      <?php foreach($signos_vitales AS $signo_vital): ?>
                          <label style="padding:10px">
                            <input type="checkbox" name="signos_vitales_included[]" value="<?php echo $signo_vital; ?>" checked class="checkbox_signos_vitales"> <?php echo $signo_vital; ?>
                          </label>
                      <?php endforeach;  ?>
                    </div>
       
                </div>

              </div>

              <div class= "row">
                    <div class="col-md-6 col-sm-6">
                      
                    </div>
                    <div class="col-md-6 col-sm-6" style="text-align:right !important;">
                      <?php if($save){  ?>
                      <input type= "hidden" id="updateReportVal" value="<?= base_url('admin/reports/update/'); ?>" />

                      <a href="<?= base_url('admin/reports/pdf/'.$paciente->id) ?>" id="updateReport" class="btn btn-info print_report_pdf" report-id="<?php echo $paciente->id; ?>" data-send="0"  data-rel="1" title="Guardar" disabled>Guardar</a>
                      <a href="<?= base_url('admin/reports/pdf/'.$paciente->id) ?>" id="printReport" class="btn btn-info  print_report_pdf" report-id="<?php echo $paciente->id; ?>" data-send="0" data-toggle="tooltip" data-placement="top" title="Imprimir" data-original-title="Imprimir" disabled> Imprimir </a>
                      

                      <?php } ?>
                    </div>
                </div>
        
            

          </form>
      </div> 

          
          <input type="hidden" id="word_cloud-url" value=""/>
          <input type="hidden" id="sintomas1-url" value=""/>
          <input type="hidden" id="medicamento1-url" value=""/>
          <input type="hidden" id="estcognitiva1-url" value=""/>
          <input type="hidden" id="estfisica1-url" value=""/>
          <input type="hidden" id="estotra1-url" value=""/>
          <input type="hidden" id="evac1-url" value=""/>
          <input type="hidden" id="micc1-url" value=""/>
          <input type="hidden" id="from" value="<?php echo $from_day; ?>"/>
          <input type="hidden" id="to" value="<?php echo $to_day; ?>"/>
          <input type="hidden" id="reportes_incluidos" value='<?php echo json_encode($reportes_incluidos_arr) ?>'/>

          <?php $signos = Array('temperatura','tension_arterial','presion_arterial','frecuencia_cardiaca','oxigeno', 'glucosa');
          foreach($signos as $signo){ ?>
            <input type="hidden" id="<?php echo $signo ?>-url" value=""/>


          <?php } ?>
        </div>
  </div>
</div>
