<div class="">
    <div class="page-title">
        <div class="title_left">
            <h3>Pacientes
                <span class="fa fa-heartbeat"></span>
            </h3>
        </div>
        <?php if ($this->session->flashdata('message')): ?>
        <div class="col-lg-12 col-md-12">
            <div class="alert alert-info alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('message')?>
            </div>
        </div>
        <?php endif; ?>

        <?php if ($this->session->flashdata('message_error')): ?>
        <div class="col-lg-12 col-md-12">
            <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('message_error')?>
            </div>
        </div>
        <?php endif; ?>


    </div>

    <div class="clearfix"></div>

    <div class="row">

        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">

                <div class="x_title">
                    <h2>Listado de Pacientes</h2>
                    <?php 
                         /* PARA CONTROLAR ACCESOS*/
                        $editable = false;
                        $disabled = "disabled";
                        $edit_data ="";
                                    
                        if($group_name == 'admin' or $group_name == "supervisor"){
                            $editable = true; 
                            $disabled = "";
                            $edit_data = "edit_data";
                        }
                        if($editable){ ?>
                    <div class="panel_toolbox">
                        <a href="<?php echo  base_url('admin/pacientes/create') ?>" class="btn btn-success">Agregar</a>
                    </div>
                    <?php } ?>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="panel_toolbox">
                        <a href="#" class="btn btn-default filters-toggle" id="filters-toggle">
                            <span class="fa fa-filter"></span> Filtros</a>
                    </div>
                    <div class="clearfix"></div>
                    <div id="filters" class="hidden">
                        <div class="row">


                            <div class="form-group col-md-3 col-sm-3">
                                <label>Status</label>
                                <div class="control-group">

                                    <select name="Status" id="Status" class="form-control">
                                        <option value="">Seleccionar</option>
                                        <option value="1">Activo</option>
                                        <option value="2">Inestable</option>
                                        <option value="3">Suspendido</option>
                                        <option value="4">Cancelado</option>
                                    </select>
                                </div>

                            </div>

                            <div class="form-group col-md-3 col-sm-3">
                                <label>Supervisor</label>
                                <div class="control-group">
                                    <select name="supervisor" id="supervisor" class="form-control">
                                        <option value="">Seleccionar</option>
                                        <?php foreach($supervisors as $supervisor):
                                                            $name = $supervisor->first_name. ' '.$supervisor->last_name; ?>
                                        <option value="<?php echo $supervisor->id; ?>">
                                            <?php echo $name; ?>
                                        </option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>

                            </div>

                            <div class="form-group col-md-4 col-sm-4">
                                <label>Luz</label>
                                <div class="control-group">
                                    <select name="luz" id="luz" class="form-control">
                                        <option value="">Ninguno</option>
                                        <?php foreach($nurses as $nurse ): ?>
                                        <option value="<?php echo $nurse->id; ?>" <?php if ($filtered_luz==$nurse->id){echo "selected";} ?> >
                                            <?php echo $nurse->first_name;?>
                                            <?php echo $nurse->last_name;?>
                                        </option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>


                        </div>

                        <div class="col-md-2 col-sm-2">
                            <br>
                            <a href="<?php echo base_url(); ?>admin/pacientes" class="btn btn-info form-control" id="filter_submit" data-rel="nurse_list">Filtrar</a>
                        </div>
                    </div>

                </div>
            </div>

            <div class="outside-report-table">

                <div class="report-table">

                 <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">

                    <thead>
                        <tr>
                            <th style="width:34%;">Nombre Completo</th>
                            <th style="width:6%;">Teléfonos</th>
                            <th style="width:64%;">
                                    <table style="width:100%; border:0 !important;">
                                            <tr>
                                                <td style="width:15%; text-align:center;">Tars ID</td>
                                                <td style="width:20%; text-align:center;">Tipo plan</td>
                                                <td style="width:50%; text-align:center;">Luz</td>
                                                <td style="width:15%; text-align:center;">Acciones</td>
                                            </tr>
                                    </table>
                            </th>
                        </tr>
                        
                    </thead>

                    <tbody>
                    <?php foreach($pacientes AS $paciente): ?>
                        <tr class="odd gradeX">
                            <td>
                                <?php $name = json_decode($paciente['name']); ?>
                                <span class='first-name-table'><?php echo $name->first_name . " " .$name->second_name; ?></span><br/><span class="last-name-table"><?php echo $name->last_name.' '. $name->second_last_name; ?></span>
                                <br />
                                <a href="<?php echo base_url('admin/pacientes/edit/'.$paciente["id"]) ?>" class="btn btn-default btn-accion"  data-toggle="tooltip" data-placement="top" title="" data-original-title="Editar Paciente">
                                    <i class="fa fa-edit"></i>
                                    Paciente 
                                </a>
                                <a href="<?php echo base_url('admin/pacientes/delete/'.$paciente["id"]) ?>"  class="btn btn-danger btn-eliminar"   data-message="¿Estas seguro que deseas eleminiar el paciente seleccionado ?" data-toggle="tooltip" data-placement="top" title="" data-original-title="Eliminar Paciente">
                                    <i class="fa fa-remove"></i>
                                    Paciente 
                                </a>
                                <a href="<?php echo base_url('admin/services/create/'.$paciente["id"]) ?>" class="btn btn-default btn-accion"  data-toggle="tooltip" data-placement="top" title="" data-original-title="Agregar Servicio">
                                    <i class="fa fa-plus"></i>
                                    Servicio
                                </a>
                            </td>

                            <td><?php echo $paciente['phones']; ?> </td>

                            <td class="td-servicios <?php echo (count($paciente['services']) == 0) ? 'bg-gris' : ''; ?>">
                                <?php if(count($paciente['services']) > 0): ?>
                                <table class="table table-striped table-bordered dt-responsive nowrap sub-table" cellspacing="0" width="100%">
                                    <tbody>
                                        <?php foreach($paciente['services'] AS $servicio): 
                                        ?>
                                        <tr>
                                            <td style="width:15%;"><?php echo (isset($servicio['tars_id'])) ? $servicio['tars_id'] : ''; ?></td>
                                            <td style="width:20%"><?php echo (isset($servicio['id']) && isset($services_meta[$servicio['id']]) && isset($services_meta[$servicio['id']]['tipo_plan'])) ? $services_meta[$servicio['id']]['tipo_plan'] : ''; ?></td>
                                            <td style="width:50%">
                                                <?php 
                                                    if(isset($servicio['id']) && isset($services_meta[$servicio['id']]) && isset($services_meta[$servicio['id']]['schedule'])) {
                                                        foreach($services_meta[$servicio['id']]['schedule'] as $nurse=>$hours){
                                                            echo "<strong> ".$nurse." </strong> <br/>";
                                                            echo implode("",$hours);
                                                        }
                                                    } else {
                                                        echo '&nbsp';
                                                    }
                                                ?>
                                            </td>
                                            <td style="width:15%">
                                                <?php if(isset($servicio['id'])): ?>
                                                <a href="<?php echo base_url('admin/services/view/'.$paciente["id"] . '/' . $servicio['id']) ?>" class="btn btn-default btn-accion"  data-toggle="tooltip" data-placement="top" title="" data-original-title="Ver">
                                                    <i class="fa fa-eye"></i> 
                                                </a>
                                                <a href="<?php echo base_url('admin/services/pdf/'.$paciente["id"] . '/' . $servicio['id']) ?>" class="btn btn-default btn-accion print_pdf" action="print" target="_blank" paciente-id="<?php echo $paciente['id']; ?>" service-id="<?php echo $servicio['id']; ?>" data-toggle="tooltip" data-placement="top" title="" data-original-title="Imprimir">
                                                    <i class="fa fa-print"></i> 
                                                </a>
                                                <a href="<?php echo base_url('admin/services/pdf/'.$paciente["id"]  . '/' . $servicio['id']) ?>" class="btn btn-default btn-accion print_pdf" action="send-get-info"  target="_blank" paciente-id="<?php echo $paciente['id']; ?>" service-id="<?php echo $servicio['id']; ?>" data-toggle="tooltip" data-placement="top" title="" data-original-title="Enviar">
                                                    <i class="fa fa-envelope"></i> 
                                                </a>
                                                <?php if($editable){ ?>
                                                    <a href="<?php echo base_url('admin/services/edit/'.$paciente["id"] .'/' . $servicio['id']) ?>" class="btn btn-default btn-accion"  data-toggle="tooltip" data-placement="top" title="" data-original-title="Editar">
                                                        <i class="fa fa-edit"></i> 
                                                    </a>
                                                    <a href="<?php echo base_url('admin/services/delete/'.$servicio["id"]) ?>" class="btn btn-danger btn-eliminar" data-message="¿Estas seguro que deseas eleminiar el servicio seleccionado ?"  data-toggle="tooltip" data-placement="top" title="" data-original-title="Eliminar">
                                                        <i class="fa fa-remove"></i> 
                                                    </a>
                                                    <?php } 
                                                    endif;
                                                    ?>
                                            </td>
                                        </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                                <?php endif; ?>
                            </td>
                        </tr>
                    </tbody>
                     <?php endforeach; ?>

                 </table>
                </div>
            </div>


        </div>
    </div>
</div>
</div>
</div>

</div>
<!-- /#page-wrapper -->