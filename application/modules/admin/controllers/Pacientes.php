<?php (defined('BASEPATH')) or exit('No direct script access allowed');

class Pacientes extends Admin_Controller
{

    public function __construct()
    {
        parent::__construct();

        if (!$this->ion_auth->logged_in()) {
            $this->ion_auth->logout();
            redirect('auth', 'refresh');
        }

        $authorized = array('admin', 'supervisor', 'excecutives');
        if (!in_array($this->ion_auth->get_users_groups()->row()->name, $authorized)) {
            $this->session->set_flashdata('message', 'You must be an administrator to view the users page.');
            redirect('admin/dashboard');
        }

        $this->load->model('module');
        $this->load->model(array('admin/service'));
        $this->load->model(array('admin/options'));
        $this->load->model(array('admin/paciente'));
        $this->load->library('report_maker');
        $this->load->library('phpmailerlib');
        $this->load->library('servicios_lib');

        $this->load->helper('pacientes');
        $this->load->helper("date");
    }

    /**
     * TODO: Revisar el listado por que no esta apareciendo el paciente al editar debido a que al eliminar la información de contacto hay un inner join ha esta tabla 
     * TODO: Modificar get_services_meta_for_list para que recupere ids con join a pacientes y no a servicios
     * TODO: Revisar funcion get_lat_results
     * TODO: Poner validacion en el listado por tipo de horario si es 3.5 solo poner nombre de la luz o algo asi 
     * TODO: Preguntar a yael que hace esto do_schedule
     * TODO: Relaciones familiar doctor y supervisor ??? 
     * TODO: Posibilidad de quitar services_meta por que esta recuperando info de otro id alparecer
     * TODO: Agregar horario 24x3.5 
     * TODO: Buscar get_data_from_tars y reemplazar por get_data_Service_from_tars para agregar el paciente
     * TODO: Buscar get_registros y reemplazar por get_registros_services para agregar el paciente
     * 
     * Pantalla principal del catalogo de pacientes
     *
     * @return void
     */
    public function index()
    {
        $refresh = 0;
        $luz = false;

        $user_id = $this->ion_auth->get_user_id();
        $group_name = $this->ion_auth->get_users_groups()->row()->name;

        $params = array(
            'user_id' => $user_id, 
            'group_name' => $group_name,
            'filtro' => false
        );
        
        if ($this->input->is_ajax_request()) {
            $refresh = 1;

            $status = $this->input->post("status");
            $luz = $this->input->post("luz");
            $supervisor = $this->input->post("supervisor");

            $params['status'] = $status;
            $params['luz'] = $luz;
            $params['supervisor'] = $supervisor;
            $params['filtro'] = true;
        }

        $data['group_name'] = $group_name;

        $pacientes = & $this->paciente->listado($params);
        $data['pacientes'] = pacientes_agrupar_listado($pacientes);

        $data['filtered_luz'] = $luz;
        $data['supervisors'] = $this->ion_auth->users('supervisor')->result();
        $data['colonies'] = $this->servicios_lib->get_all_colonies();
        $data['nurses'] = $this->ion_auth->users('nurses')->result();
        $data['nurses_status'] = $this->report_maker->get_nurses_status();

        $data['services_meta'] = $this->servicios_lib->get_services_meta_for_list();
        $data['page'] = $this->config->item('ci_my_admin_template_dir_admin') . "pacientes_list";

        $this->load->view($this->_container, $data);

        if ($refresh == 1) {
            return json_encode($data);
        }
    }

    /**
     * Pantalla para crear una nuevo paicente
     *
     * @param [type] $nurse_info
     * @return void
     */
    public function create($nurse_info = null)
    {
        $data['filtered_supervisor'] = 0;
        $data['filtered_luz'] = 0;
        $data['filtered_gender'] = 0;
        $data['filtered_colony'] = "none";
        $data['filtered_personality'] = 0;

        if ($this->input->is_ajax_request()) {

            $query = "select users.* from users,users_groups";
            $query2 = "  users_groups.user_id = users.id
                    and users_groups.group_id = (SELECT id from groups where name='nurses') ";
            if ($this->input->post('luz') != 0) {
                $query2 .= " and users.id = " . $this->input->post('luz');
                $data['filtered_luz'] = $this->input->post('luz');

            }
            if ($this->input->post('supervisor') != 0) {
                $query .= " JOIN users_meta as fs using(user_id)";
                $query2 .= "   AND (fs.meta_key = '_supervisor'
                              AND fs.meta_value =  '" . $this->input->post('supervisor') . "') ";
                $data['filtered_supervisor'] = $this->input->post('supervisor');
            }

            if ($this->input->post('colony') != "") {
                $query .= " JOIN users_meta as fc using(user_id)";
                $query2 .= "   AND (fc.meta_key = '_colony'
                              AND fc.meta_value =  '" . $this->input->post('colony') . "') ";
                $data['filtered_colony'] = $this->input->post('colony');

            }
            if ($this->input->post('gender') != "") {
                $query .= " JOIN users_meta as fg using(user_id)";
                $query2 .= "   AND (fg.meta_key = '_gender'
                              AND fg.meta_value =  '" . $this->input->post('gender') . "' )";
                $data['filtered_gender'] = $this->input->post('gender');
            }
            if ($this->input->post('personality') != "") {
                $query .= " JOIN users_meta as fp using(user_id)";
                $query2 .= "  AND (fp.meta_key = '_personality'
                              AND fp.meta_value =  '" . $this->input->post('personality') . "') ";
                $data['filtered_personality'] = $this->input->post('personality');
            }

            $query = $query . " WHERE " . $query2;
            $query = $this->db->query($query);
            $data['nurses'] = $query->result();
            $data['nurses_status'] = $this->report_maker->get_nurses_status();

            $data['page'] = $this->config->item('ci_my_admin_template_dir_admin') . "pacientes_create";
            $this->load->view($this->_container, $data);
            return json_encode($data);

        } else {
            $authorized = array('admin', 'supervisor');
            if (!in_array($this->ion_auth->get_users_groups()->row()->name, $authorized)) {
                redirect(site_url('admin/reports'), 'refresh');
            }

            if ($this->input->post('name')) {
                $this->service->insert($data);
                redirect('/admin/pacientes', 'refresh');
            }

            $data['nurses'] = $this->ion_auth->users('nurses')->result();
            $data['nurses_status'] = $this->report_maker->get_nurses_status();
            $data['personalities'] = $this->options->get_all('*', array('option_name' => '_personality'));
            $data['colonies'] = $this->servicios_lib->get_all_colonies();

            if ($nurse_info != null) {
                $nurse_arr = explode("-", $nurse_info);
                $data['assigned_nurse'] = $nurse_arr[0];
                $data['assigned_hour'] = $nurse_arr[1];
            } else {
                $data['assigned_nurse'] = -1;
                $data['assigned_hour'] = "00:00";
            }
            $data['deseases'] = $this->options->get_all('*', array('option_name' => '_desease'));
            $data['service_calendar'] = true;

            $data['doctors'] = $this->ion_auth->users('doctors')->result();
            $data['supervisors'] = $this->ion_auth->users('supervisor')->result();
            $data['ejecutivos'] = $this->ion_auth->users('excecutives')->result();
            /*$data['date_range'] = ['date_range(strtotime('monday this week'), strtotime('sunday this week'));']*/
            $data['date_range'] = array('Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado', 'Domingo');
            //$data['time_range'] = time_range('00:00', '24:00');

            $data['page'] = $this->config->item('ci_my_admin_template_dir_admin') . "pacientes_create";

            $this->load->view($this->_container, $data);
        }

    }

    /**
     * Metodo que se encarga de mostar la vista de editar paciente
     *
     * @param [type] $id
     * @return void
     */
    public function edit($id)
    {
        $data['filtered_supervisor'] = 0;
        $data['filtered_luz'] = 0;
        $data['filtered_gender'] = 0;
        $data['filtered_colony'] = "none";
        $data['filtered_personality'] = 0;

        if ($this->input->is_ajax_request()) {
            $query = "select users.* from users,users_groups";
            $query2 = "  users_groups.user_id = users.id
                    and users_groups.group_id = (SELECT id from groups where name='nurses') ";
            if ($this->input->post('luz') != 0) {
                $query2 .= " and users.id = " . $this->input->post('luz');
                $data['filtered_luz'] = $this->input->post('luz');

            }
            if ($this->input->post('supervisor') != 0) {
                $query .= " JOIN users_meta as fs using(user_id)";
                $query2 .= "   AND (fs.meta_key = '_supervisor'
                              AND fs.meta_value =  '" . $this->input->post('supervisor') . "') ";
                $data['filtered_supervisor'] = $this->input->post('supervisor');
            }

            if ($this->input->post('colony') != "") {
                $query .= " JOIN users_meta as fc using(user_id)";
                $query2 .= "   AND (fc.meta_key = '_colony'
                              AND fc.meta_value =  '" . $this->input->post('colony') . "') ";
                $data['filtered_colony'] = $this->input->post('colony');

            }
            if ($this->input->post('gender') != "") {
                $query .= " JOIN users_meta as fg using(user_id)";
                $query2 .= "   AND (fg.meta_key = '_gender'
                              AND fg.meta_value =  '" . $this->input->post('gender') . "' )";
                $data['filtered_gender'] = $this->input->post('gender');
            }
            if ($this->input->post('personality') != "") {
                $query .= " JOIN users_meta as fp using(user_id)";
                $query2 .= "  AND (fp.meta_key = '_personality'
                              AND fp.meta_value =  '" . $this->input->post('personality') . "') ";
                $data['filtered_personality'] = $this->input->post('personality');
            }

            $query = $query . " WHERE " . $query2;
            /*echo  $query;*/
            $query = $this->db->query($query);
            $data['nurses'] = $query->result();
            $data['nurses_status'] = $this->report_maker->get_nurses_status();

            $data['page'] = $this->config->item('ci_my_admin_template_dir_admin') . "pacientes_edit";
            $this->load->view($this->_container, $data);
            return json_encode($data);

        } else {
            $authorized = array('admin', 'supervisor');
            if (!in_array($this->ion_auth->get_users_groups()->row()->name, $authorized)) {
                redirect(site_url('admin/reports'), 'refresh');
            }

            $paciente = $this->paciente->get($id);

            $this->db->select("*");
            $this->db->from("pacientes_relationship");
            $this->db->where(array('paciente_id' => $id, 'relation_term' => 'familiar'));
            $query = $this->db->get();
            $result = $query->result();

            $familiars = array();

            foreach ($result as $res) {
                $familiar_data = $this->ion_auth->user($res->relation_value)->row();
                $familiar_meta = $this->db->select("*");
                $this->db->from("users_meta");
                $this->db->where(array('user_id' => $res->relation_value, 'meta_key' => '_RelationType'));
                $query = $this->db->get();
                $result = $query->result();

                switch ($result[0]->meta_value) {

                    case 'passive-hidden':
                        $relationVal = 'Pasivo';
                        break;
                    case 'payments-show':
                        $relationVal = 'Paga';
                        break;
                    case 'caring-hidden':
                        $relationVal = 'Cuida';
                        break;

                }

                $this->db->from("users_meta");
                /*Relacion con el paciente */
                $this->db->where(array('user_id' => $res->relation_value, 'meta_key' => '_RelationToPatient'));
                $query = $this->db->get();
                $result = $query->result();

                switch ($result[0]->meta_value) {

                    case 'family-show':
                        $relationTP = 'Familiar';
                        break;
                    case 'doctor-hidden':
                        $relationTP = 'Doctor';
                        break;

                }

                $this->db->from("users_meta");
                /*Calificacion luz */
                $this->db->where(array('user_id' => $res->relation_value, 'meta_key' => '_Rating_Luz'));
                $query = $this->db->get();
                $ratingluz = $query->result();

                $this->db->from("users_meta");
                /*Calificacion servicio */
                $this->db->where(array('user_id' => $res->relation_value, 'meta_key' => '_Rating_PM'));
                $query = $this->db->get();
                $ratingserv = $query->result();

                $familiars[] = array('data' => $familiar_data, 'relationType' => $relationVal, 'relationToPatient' => $relationTP, 'calificacion_luz' => $ratingluz[0]->meta_value, 'calificacion_servicio' => $ratingserv[0]->meta_value);

            }
            $data['paciente_familiares'] = $familiars;

            $this->db->select("*");
            $this->db->from("services_meta");
            $this->db->where(array('service_id' => $id));
            $query = $this->db->get();
            $results = $query->result_array();

            $service_meta = array();

            foreach ($results as $result) {
                $service_meta[$result['service_meta']] = $result['service_value'];
            }

            $query_r = $query->result();

            $data['paciente_id'] = $id;
            
            $meds = json_decode($paciente->medicines);
            $newmeds = array();
            foreach ($meds as $m) {
                if (!empty($m[0])) {
                    $newmeds[] = $m;
                }
            }
            $data['service_medicines'] = $newmeds;
            $data['_birthday'] = $paciente->birthday;
            $data['nurses'] = $this->ion_auth->users('nurses')->result();
            $data['nurses_status'] = $this->report_maker->get_nurses_status();
            $data['colonies'] = $this->servicios_lib->get_all_colonies();

            $data['birthday'] = $paciente->birthday;
            $data['deseases'] = $this->options->get_all('*', array('option_name' => '_desease'));
            
            $data['time_range'] = time_range('00:00', '24:00');
            $data['paciente'] = $paciente;
            $data['map_lat'] = $paciente->map_lat;
            $data['map_long'] = $paciente->map_long;
            $data['page'] = $this->config->item('ci_my_admin_template_dir_admin') . "pacientes_edit";

            $this->load->view($this->_container, $data);

        }
    }

    /**
     * Metodo que se encarga de crear y editar un nuevo paciente
     *
     * @return void
     */
    public function save()
    {
        if ($this->input->is_ajax_request()) {

            switch ($this->input->post('action')) {
                case 1:

                    $data = json_decode($this->input->post('data'));
                    $tars_id = $data->tars_id;
                    $name = json_encode(array('first_name' => $data->FirstName, 'second_name' => $data->SecondName, 'last_name' => $data->LastName, 'second_last_name' => $data->SecondLastname));
                    $address = json_encode(array('street' => $data->Street, 'number' => $data->ExternalNumber, 'internal' => $data->InternalNumber, 'colony' => $data->Colony));

                    $city = $data->City;

                    $zip = $data->Zip;

                    $state = $data->State;

                    $country = 'MX';

                    $phones = $data->Phone;

                    $desease = $data->Desease;

                    $medicines = $data->_Medicines;

                    $relation_ids = $data->_RelationIDs;

                    $goals = $data->Goals;

                    $pacientes_data = array(
                        'name' => $name,
                        'address' => $address,
                        'city' => $city,
                        'zip' => $zip,
                        'region' => $state,
                        'country' => $country,
                        'phones' => $phones,
                        'disease' => $desease,
                        'goals' => $goals,
                        'nombre_familia' => $data->nombre_familia,
                        'medicines' => json_encode($data->_Medicines),
                        'map_long' => $data->map_long,
                        'map_lat' => $data->map_lat,
                        'birthday' => $data->Birthdate,
                    );

                    $this->db->insert('pacientes', $pacientes_data); // creating service record

                    $paciente_id = $this->db->insert_id();

                    if ($paciente_id != '' && $paciente_id > 0) { // validate if service_id is not null and is more than 0

                        // contactos tipo familiar
                        if (count($data->_RelationIDs) > 0) {
                            foreach ($data->_RelationIDs as $relation => $value):
                                $this->db->insert('paciente_relationship', array('paciente_id' => $paciente_id, 'relation_term' => 'familiar', 'relation_value' => $value));
                            endforeach;
                        }

                        /* REHACER  LA AGENDA DE HOY A LA SIGUIENTE SEMANA*/
                        //$schedule = $this->servicios_lib->do_schedule();

                        $results = array(
                            'status' => 'success',
                            'action' => 'redirect',
                            'module' => "" . base_url() . 'admin' . "/pacientes",
                            'message' => 'Se registro exitosamente',
                        );

                        echo json_encode($results);
                    }

                    break;

                case 2:

                    $data = json_decode($this->input->post('data'));

                    $paciente_id = $data->paciente_id;

                    $tars_id = $data->tars_id;

                    $name = json_encode(array('first_name' => $data->FirstName, 'second_name' => $data->SecondName, 'last_name' => $data->LastName, 'second_last_name' => $data->SecondLastname));

                    $address = json_encode(array('street' => $data->Street, 'number' => $data->ExternalNumber, 'internal' => $data->InternalNumber, 'colony' => $data->colony));

                    $city = $data->City;

                    $zip = $data->Zip;

                    $state = $data->State;

                    $country = 'MX';

                    $phones = $data->Phone;

                    $desease = $data->Desease;

                    $medicines = $data->_Medicines;

                    $relation_ids = $data->_RelationIDs;

                    $goals = $data->Goals;

                    $paciente_data = array(
                        'name' => $name,
                        'address' => $address,
                        'city' => $city,
                        'zip' => $zip,
                        'region' => $state,
                        'country' => $country,
                        'phones' => $phones,
                        'disease' => $desease,
                        'goals' => $goals,
                        'nombre_familia' => $data->nombre_familia,
                        'medicines' => json_encode($data->_Medicines),
                        'birthday' => $data->Birthdate,
                        'map_long' => $data->map_long,
                        'map_lat' => $data->map_lat,
                    );

                    $this->db->where('id', $paciente_id);
                    $this->db->update('pacientes', $paciente_data);

                    if (count($data->_RelationIDs) > 0) {
                        $this->db->query("DELETE from pacientes_relationship where paciente_id = '" . $paciente_id . "' and relation_term = 'familiar'");

                        foreach ($data->_RelationIDs as $relation => $value):
                            $this->db->insert('services_relationship', array('service_id' => $paciente_id, 'relation_term' => 'familiar', 'relation_value' => $value));
                        endforeach;
                    }

                    /* METER TODOS LOS DATOS EN LA AGENDA*/
                    //$schedule = $this->servicios_lib->do_schedule();

                    $results = array(
                        'status' => 'success',
                        'action' => 'redirect',
                        'module' => "" . base_url() . 'admin' . "/pacientes",
                        'message' => 'Se registro exitosamente',
                    );

                    echo json_encode($results);

                    break;
            }
        } else {
            exit('No direct script access allowed');
        }
    }

    public function delete($id)
    {
        $authorized = array('admin', 'supervisor');
        if (!in_array($this->ion_auth->get_users_groups()->row()->name, $authorized)) {
            redirect(site_url('admin/reports'), 'refresh');
        }
        $this->paciente->delete($id);

        redirect('/admin/pacientes', 'refresh');
    }

    /**
     * Metodo que se encarga de eliminar a los familiares
     *
     * @param [type] $id
     * @return void
     */
    public function delete_familiar($id)
    {
        if ($this->input->is_ajax_request()) {
            $fam_id = $this->input->post('familiar_id');
            $this->db->where(array('paciente_id' => $id, 'relation_term' => 'familiar', 'relation_value' => $fam_id));
            $this->db->delete('pacientes_relationship');
            $results = array(
                'status' => 'success',
                'action' => 'redirect',
                'module' => "" . base_url() . 'admin' . "/pacientes",
                'message' => 'Se borró exitosamente $id',
            );

            echo json_encode($results);
        }
    }

    /**
     * Metodo que se encarga de eliminar un medicina 
     *
     * @param [int] $id
     * @return void
     */
    public function delete_medicina($id)
    {
        if ($this->input->is_ajax_request()) {

            $med_id = $this->input->post('medicinas_id');

            $this->db->select('id, service_value');
            $this->db->from("services_meta");
            $this->db->where(array('service_id' => $id, 'service_meta' => '_medicines'));
            $this->db->limit(1);

            $query = $this->db->get();
            $result = $query->result();

            $medicines = json_decode($result[0]->service_value);

            $new_med = array();
            foreach ($medicines as $med) {
                $this_med_id = strtr(utf8_decode($med[0]), utf8_decode('àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ '), 'aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY-');
                if ($this_med_id != $med_id) {
                    if (!empty($med[0])) {
                        $new_med[] = $med;
                    }
                }

            }
            $this->db->where('id', $result[0]->id);
            $this->db->update('services_meta', array('service_value' => json_encode($new_med)));

            $results = array(
                'status' => 'success',
                'action' => 'redirect',
                'module' => "" . base_url() . 'admin' . "/pacientes",
                'message' => 'Se borró exitosamente ' . $id . " " . $new_med,
            );

            echo json_encode($results);
        }
    }

    /**
     * Metodo que se encarga 
     *
     * @return void
     */
    public function medicine()
    {

        if ($this->input->is_ajax_request()) {

            if ($this->input->post('action') == 'save') {

                switch ($this->input->post('TimesPerDay')) {

                    case 1:
                        $inputs = '<div class="form-group col-md-12 col-sm-12 clockpicker"><input type="text" name="Medicine_Times[]" class="form-control Medicine_Times" /></div>';
                        break;
                    case 2:
                        $inputs = '<div class="form-group col-md-6 col-sm-6 clockpicker"><input type="text" name="Medicine_Times[]" class="form-control Medicine_Times" /></div>';
                        $inputs .= '<div class="form-group col-md-6 col-sm-6 clockpicker"><input type="text" name="Medicine_Times[]" class="form-control Medicine_Times" /></div>';

                        break;
                    case 3:
                        $inputs = '<div class="form-group col-md-4 clockpicker"><input type="text" name="Medicine_Times[]" class="form-control col-md-4 Medicine_Times" /></div>';
                        $inputs .= '<div class="form-group col-md-4 clockpicker"><input type="text" name="Medicine_Times[]" class="form-control col-md-4 Medicine_Times" /></div>';
                        $inputs .= '<div class="form-group col-md-4 clockpicker"><input type="text" name="Medicine_Times[]" class="form-control col-md-4 Medicine_Times" /></div>';

                        break;
                    case 4:
                        $inputs = '<div class="form-group col-md-3 col-sm-3 clockpicker"><input type="text" name="Medicine_Times[]" class="form-control Medicine_Times" /></div>';
                        $inputs .= '<div class="form-group col-md-3 col-sm-3 clockpicker"><input type="text" name="Medicine_Times[]" class="form-control Medicine_Times" /></div>';
                        $inputs .= '<div class="form-group col-md-3 col-sm-3 clockpicker"><input type="text" name="Medicine_Times[]" class="form-control Medicine_Times" /></div>';
                        $inputs .= '<div class="form-group col-md-3 col-sm-3 clockpicker"><input type="text" name="Medicine_Times[]" class="form-control Medicine_Times" /></div>';
                        break;
                }

                $code = '<tr>
                        <td><div class="form-group col-md-12"><input type="text" class="form-control Medicine_Names" name="Medicine_Names[]" /></div></td>
                        <td><div class="form-group col-md-12"><input type="text" class="form-control Medicine_Dosis" name="Medicine_Dosis[]" /></div></td>
                        <td><div class="form-group col-md-12"><input type="text" class="form-control Medicine_TimesperDay" name="Medicine_TimesperDay[]"  disabled value="' . $this->input->post('TimesPerDay') . '"/></div></td>
                        <td class="times">' . $inputs . '</td></tr><script type="text/javascript">$(".clockpicker").clockpicker({
                                            placement: "top",
                                            align: "left",
                                            donetext: "Listo"
                                        });</script>';
                $results = array(
                    'action' => 'append',
                    'element' => 'table-' . $this->input->post('module') . ' > tbody',
                    'code' => $code,
                    'status' => 'true',
                );

                echo json_encode($results);

            } else {

                $this->load->view($this->config->item('ci_my_admin_template_dir_admin') . "modals/_add_medicine_row");

            }

        } else {
            exit('No direct script access allowed');
        }

    }
}
