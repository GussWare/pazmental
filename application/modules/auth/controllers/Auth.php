<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Auth extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library(array('ion_auth', 'form_validation'));
        $this->load->helper(array('url', 'language'));
		$this->load->library('session');
		
		$this->lang->load('auth', 'english');
		$this->lang->load('ion_auth', 'english');

        $this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

        log_message('debug', 'CI My Admin : Auth class loaded');
    }

    public function index()
    {
        if ($this->ion_auth->logged_in()) {
            $this->redirect_page();
            redirect($referred_from, 'refresh');

        } else {
            $data['page'] = $this->config->item('ci_my_admin_template_dir_public') . "login_form";
            $data['module'] = 'auth';

            $this->load->view($this->_container, $data);
        }
    }

    public function login()
    {
        $this->form_validation->set_rules('email', 'Email', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');
        $this->form_validation->set_rules('g-recaptcha-response', 'Captcha', 'callback_recaptcha');

        if ($this->form_validation->run() == true) {
            $remember = (bool) $this->input->post('remember');

            if ($this->ion_auth->login($this->input->post('email'), $this->input->post('password'), $remember)) {
                $this->redirect_page();
                redirect($redirect_page, 'refresh');
            } else {
                $this->session->set_flashdata('message', $this->ion_auth->errors());
                redirect('auth', 'refresh');
            }
        } else {
            $this->session->set_flashdata('message', $this->ion_auth->errors());

            $data['page'] = $this->config->item('ci_my_admin_template_dir_public') . "login_form";
            $data['module'] = 'auth';
			$data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
			$data['message_success'] =  $this->session->flashdata('message_success');

            $this->load->view($this->_container, $data);
        }
    }

    public function forgot_password()
    {
        if($this->config->item('identity', 'ion_auth') != 'email' )
		{
		   $this->form_validation->set_rules('identity', $this->lang->line('forgot_password_identity_label'), 'required');
		}
		else
		{
		   $this->form_validation->set_rules('email', $this->lang->line('forgot_password_validation_email_label'), 'required|valid_email');
		}


		if ($this->form_validation->run() == false)
		{
			// setup the input
			$this->data['email'] = array('name' => 'email',
				'id' => 'email',
			);

			if ( $this->config->item('identity', 'ion_auth') != 'email' ){
				$this->data['identity_label'] = $this->lang->line('forgot_password_identity_label');
			}
			else
			{
				$this->data['identity_label'] = $this->lang->line('forgot_password_email_identity_label');
			}

			// set any errors and display the form
			$data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
            $data['page'] = $this->config->item('ci_my_admin_template_dir_public') . "forgot_form";
            $data['module'] = 'auth';

            $this->load->view($this->_container, $data);
		}
		else
		{
			$identity_column = $this->config->item('identity','ion_auth');
			$identity = $this->ion_auth->where($identity_column, $this->input->post('email'))->users()->row();

			if(empty($identity)) {

	            		if($this->config->item('identity', 'ion_auth') != 'email')
		            	{
		            		$this->ion_auth->set_error('forgot_password_identity_not_found');
		            	}
		            	else
		            	{
		            	   $this->ion_auth->set_error('forgot_password_email_not_found');
		            	}

		                $this->session->set_flashdata('message', $this->ion_auth->errors());
                		redirect("auth/forgot_password", 'refresh');
            		}

			// run the forgotten password method to email an activation code to the user
			$forgotten = $this->ion_auth->forgotten_password($identity->{$this->config->item('identity', 'ion_auth')});

			if ($forgotten)
			{
				$this->load->library('phpmailerlib');

				$nombreUsuario = $identity->first_name . ' ' . $identity->last_name;
				$mensaje = str_replace('%s', $nombreUsuario, $this->config->item('email_forgot_password_personalizado', 'ion_auth'));
				$mensaje = str_replace('%d', base_url('auth/reset_password/' . $forgotten["forgotten_password_code"]), $mensaje);
				$mail_success = $this->phpmailerlib->sendEmail($identity->email, $nombreUsuario, utf8_decode('Recuperar contraseña'), $mensaje, FALSE);

				// if there were no errors
				$this->session->set_flashdata('message_success', 'Correo de recuperación de contraseña enviado con éxito');
				redirect("auth/login", 'refresh'); //we should display a confirmation page here instead of the login page
			}
			else
			{
				$this->session->set_flashdata('message', $this->ion_auth->errors());
				redirect("auth/forgot_password", 'refresh');
			}
		}
    }

    public function reset_password($code)
    {
        if (!$code)
		{
			show_404();
		}

		$user = $this->ion_auth->forgotten_password_check($code);

		if ($user)
		{
			// if the code is valid then display the password reset form

			$this->form_validation->set_rules('new', $this->lang->line('reset_password_validation_new_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[new_confirm]');
			$this->form_validation->set_rules('new_confirm', $this->lang->line('reset_password_validation_new_password_confirm_label'), 'required');

			if ($this->form_validation->run() == false)
			{
                // render
                $data['csrf'] = $this->_get_csrf_nonce();
                $data['code'] = $code;
                $data['user_id'] = $user->id;
                $data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
                $data['page'] = $this->config->item('ci_my_admin_template_dir_public') . "reset_form";
                $data['module'] = 'auth';
                $this->load->view($this->_container, $data);
			}
			else
			{
				// do we have a valid request?
				if ($this->_valid_csrf_nonce() === FALSE || $user->id != $this->input->post('user_id'))
				{

					// something fishy might be up
					$this->ion_auth->clear_forgotten_password_code($code);

					show_error($this->lang->line('error_csrf'));

				}
				else
				{
					// finally change the password
					$identity = $user->{$this->config->item('identity', 'ion_auth')};

					$change = $this->ion_auth->reset_password($identity, $this->input->post('new'));

					if ($change)
					{
						// if the password was successfully changed
						$this->session->set_flashdata('message_success', 'Contraseña reestablecida con éxito');
						redirect("auth/login", 'refresh');
					}
					else
					{
						$this->session->set_flashdata('message', $this->ion_auth->errors());
						redirect('auth/reset_password/' . $code, 'refresh');
					}
				}
			}
		}
		else
		{
			// if the code is invalid then send them back to the forgot password page
			$this->session->set_flashdata('message', $this->ion_auth->errors());
			redirect("auth/forgot_password", 'refresh');
		}
    }

    public function recaptcha($str = "")
    {

        $google_url = "https://www.google.com/recaptcha/api/siteverify";
        $secret = '6Le2xzsUAAAAAOoJUWd2Rgf-Y0JT_2gZlqZRehx3';
        $ip = $_SERVER['REMOTE_ADDR'];
        $url = $google_url . "?secret=" . $secret . "&response=" . $str . "&remoteip=" . $ip;
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_TIMEOUT, 10);
        curl_setopt($curl, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.2.16) Gecko/20110319 Firefox/3.6.16");
        $res = curl_exec($curl);
        curl_close($curl);
        $res = json_decode($res, true);
        //reCaptcha success check
        if ($res['success']) {
            return TRUE;
        } else {
            $this->form_validation->set_message('recaptcha', 'El campo de  {field} me indica que eres un robot. ¿Intentamos de nuevo?');
            return TRUE;
        }

    }

    public function logout()
    {
        $this->ion_auth->logout();

        redirect('auth', 'refresh');
    }

    public function redirect_page()
    {

        $group_name = $this->ion_auth->get_users_groups()->row()->name;
        if ($group_name == 'familiars' or $group_name == "doctors") {
            redirect(site_url('admin/reports'), 'refresh');
        } elseif ($group_name == "nurses") {
            $this->session->set_flashdata('message', "Todavía no hay una vista para tu tipo de usuario.");
            (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

            $data['page'] = $this->config->item('ci_my_admin_template_dir_public') . "login_form";
            $data['module'] = 'auth';
            //$data['message'] = $this->data['message'];

            $this->load->view($this->_container, $data);
        } else {
            redirect(site_url('admin/dashboard'), 'refresh');
        }

    }

    function _get_csrf_nonce()
	{
		$this->load->helper('string');
		$key   = random_string('alnum', 8);
		$value = random_string('alnum', 20);
		$this->session->set_flashdata('csrfkey', $key);
		$this->session->set_flashdata('csrfvalue', $value);

		return array($key => $value);
    }
    
    function _valid_csrf_nonce()
	{
		if ($this->input->post($this->session->flashdata('csrfkey')) !== FALSE &&
			$this->input->post($this->session->flashdata('csrfkey')) == $this->session->flashdata('csrfvalue'))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
}

/* End of file auth.php */
/* Location: ./modules/auth/controllers/auth.php */
