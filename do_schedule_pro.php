
<?php

$link = mysqli_connect('localhost', 'root', '', 'pruebas1_sitiodemo_info');
//$link = mysqli_connect('localhost', 'root', '', 'anapazmentalmx_2018_05_31');
//$link = mysqli_connect('45.33.31.67','anapazmentalmx','622U3eHx','anapazmentalmx');
//$link = mysqli_connect('localhost','webaccess','soy1Guau!','pazmenta_webapp');

/* Comprueba la conexión */
if ($link->connect_errno) {
    printf("Connect failed: %s\n", $link->connect_error);
    exit();
}

defined('TIPO_SERVICIO_TRES_PUNTO_CINCO') or define('TIPO_SERVICIO_TRES_PUNTO_CINCO', 231);

$begin = new DateTime('today');
$end = new DateTime('today');
$end = $end->modify('+8 day');

$period = new DatePeriod(
    $begin,
    new DateInterval('P1D'),
    $end
);

$trans = array("Monday" => "Lunes", "Tuesday" => "Martes",
    "Wednesday" => "Miercoles", "Thursday" => "Jueves",
    "Friday" => "Viernes", "Saturday" => "Sabado",
    "Sunday" => "Domingo");

$daysofweek = array();
foreach ($period as $p) {

    $dayofweek = $p->format("l");
    $date = $p->format("Y-m-d");
    if (!array_key_exists($trans[$dayofweek], $daysofweek)) {
        $daysofweek[$trans[$dayofweek]] = array();
    }
    $daysofweek[$trans[$dayofweek]][] = $date;
}

// $results = $link->query("DELETE from agenda where date(start_time) >= '" . $begin->format("Y-m-d") . "'");
$results = $link->query("SELECT schedule.*, schedule_fechas_arranque.id AS id_schedule_fecha_arranque, schedule_fechas_arranque.fecha_arranque FROM schedule INNER JOIN services ON schedule.service_id = services.id LEFT JOIN schedule_fechas_arranque ON services.id = schedule_fechas_arranque.service_id AND schedule.nurse_id = schedule_fechas_arranque.nurse_id;");

while ($row = $results->fetch_array()) {

    $day = $row['date'];
    $times = json_decode($row['times']);

    $nurse_id = $row['nurse_id'];
    $service_id = $row['service_id'];

    $result_meta = $link->query("SELECT * FROM services_meta WHERE service_meta = '_service_plan' AND service_id = " . $service_id . " LIMIT 1");
    $row_meta = $result_meta->fetch_assoc();

    if (count($row_meta) > 0) {
        if ($row_meta['service_value'] == TIPO_SERVICIO_TRES_PUNTO_CINCO) {
			if(isset($row['fecha_arranque'])) {

                if($row['fecha_arranque'] == '2018-06-25') {
                    $debug = true;
                }

				$date_start = new DateTime($row['fecha_arranque']);
				$date_end = new DateTime($row['fecha_arranque']);
				$date_end = $date_end->modify('+8 day');

				$period_24_x_3_punto_5 = new DatePeriod(
					$date_start,
					new DateInterval('P2D'),
					$date_end 
				);

                $fecha_final = null;

				foreach($period_24_x_3_punto_5 AS $dia) {
					$dia_periodo = $dia->format("Y-m-d");
	
					$start = new DateTime($dia_periodo." ".$times[0]);
					$end = new DateTime($dia_periodo." ".end($times));

					$fecha_final = $end->format("Y-m-d");

					if(!$link->query("INSERT INTO agenda (nurse_id, service_id, start_time, end_time) VALUES ('".$nurse_id."','".$service_id."','".$start->format("Y-m-d H:i:s")."','".$end->format("Y-m-d H:i:s")."')")){ 
						printf("Errormessage: %s\n", $link->error);
						exit();
					}
				}

				if(isset($fecha_final) && isset($row['id_schedule_fecha_arranque'])) {
					if(!$link->query("UPDATE schedule_fechas_arranque SET fecha_arranque = '".$fecha_final."' WHERE id = ". $row['id_schedule_fecha_arranque'] .";")){
						printf("Errormessage: %s\n", $link->error);
						exit();
					}
				}
			}
        } else {
            foreach ($daysofweek[$day] as $d) {

                $start = new DateTime($d . " " . $times[0]);
                $end = new DateTime($d . " " . end($times));

                if (!$link->query("INSERT INTO agenda (nurse_id, service_id, start_time, end_time) VALUES ('" . $nurse_id . "','" . $service_id . "','" . $start->format("Y-m-d H:i:s") . "','" . $end->format("Y-m-d H:i:s") . "')")) {
                    printf("Errormessage: %s\n", $link->error);

                }
            }
        }

    }

}

$results->free();
$link->close();

?>








