<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Paciente extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Metodo que se encarga de recuperar la información del listado de pacientes
     *
     * @return void
     */
    public function &listado($params = array())
    {
        $join_paciente_service = ($params['filtro'] == false) ? "left" : "inner";

        $this->db->select("DISTINCT pacientes.*", false);
        $this->db->select("services.id AS service_id, services.service_type, services.start_date, services.control_records, services.step, services.paciente_id, services.tars_id", false);
        $this->db->from("pacientes");
        $this->db->join("services", "pacientes.id = services.paciente_id", $join_paciente_service);
        $this->db->join("services_relationship", "services.id = services_relationship.service_id", $join_paciente_service);
        

        // filtros por grupo de usuario
        if (isset($params['group_name']) && isset($params['user_id'])) {
            switch ($params['group_name']) {
                case ('supervisor'):
                    $this->db->where("services_relationship.relation_term", 'supervisor');
                    $this->db->where("services_relationship.relation_value", $params['user_id']);
                    break;
                case ('excecutives'):
                    $this->db->where("services_relationship.relation_term", 'excecutives');
                    $this->db->where("services_relationship.relation_value", $params['user_id']);
                    break;
            }
        }

        // filtros por busqueda
        if (isset($params['status']) && $params['status']) {
            $this->db->where("services.status", $params['status']);
        }

        if (isset($params['supervisor']) && $params['supervisor']) {
            $this->db->where("services_relationship.relation_term", 'supervisor');
            $this->db->where("services_relationship.relation_value", $params['supervisor']);
        }

        if (isset($params['luz']) && $params['luz']) {
            $this->db->join("schedule", "schedule.service_id = services.id", "inner");
            $this->db->where("schedule.nurse_id", $params['luz']);
        }

        $query = $this->db->get();

        log_message('debug', 'query pacientes----------------');
        log_message('debug', $this->db->last_query());
        log_message('debug', 'query pacientes----------------');

        $data = array();

        if ($query->num_rows() > 0) {
            $data = $query->result();
        }

        return $data;
    }


    /**
     * Metodo que se encarga de recuperar todos los datos meta de un paciente 
     *
     * @param int $paciente_id
     * @return array
     */
    public function get_data_pacientes_meta($paciente_id) {

        $this->db->select("*");
        $this->db->from("pacientes_meta");
        $this->db->where("pacientes_meta.paciente_id",$paciente_id);

        log_message('debug', 'query get_data_pacientes_meta ----------------');
        log_message('debug', $this->db->last_query());
        log_message('debug', 'query get_data_pacientes_meta ----------------');

        $query = $this->db->get();

        $data = array();

        if($query->num_rows() > 0) {
            $data = $query->result();
        }

        return $data;
    }
}

/* End of file filename.php */
