<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Agenda extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Metodo que se encarga de recuperar 
     */
    public function get_dashboard($params = array())
    {
        $this->db->select("DISTINCT agenda.start_time as start_time, agenda.end_time as end_time,", false);
        $this->db->select("users.first_name as nurse_name, users.last_name as nurse_last,users.id as nurse_id,", false);
        $this->db->select("services.id as service_id, pacientes.id AS paciente_id, pacientes.name as patient_name", false);
        $this->db->from("agenda");
        $this->db->join("users", "agenda.nurse_id = users.id", "inner");
        $this->db->join("services", "agenda.service_id = services.id", "inner");
        $this->db->join("pacientes", "services.paciente_id = pacientes.id", "inner");
        $this->db->where("date(agenda.start_time)", $params['date']);

        if ($params['supervisor']) {
            $this->db->where("services.id = services_relationship.service_id");
            $this->db->where("relation_term", 'supervisor');
            $this->db->where("relation_value", $params['supervisor']);

            if ($params['luz']) {
                $this->db->or_where('users.id', $params['luz']);
            }
        } else {
            if($params['luz']) {
                $this->db->where("users.id", $params['luz']);
            }
            
        }

        if ($params['status']) {
            $this->db->where("services.status", $params['status']);
        } else {
            $this->db->where("services.status", 1);
        }

        switch ($params['group_name']) {
            case ('supervisor'):
                $this->db->where("services.id = services_relationship.service_id ");
                $this->db->where("relation_term", 'supervisor');
                $this->db->where("relation_value", $params['user_id']);
                break;
            case ('excecutives'):
                $this->db->where("services.id = services_relationship.service_id ");
                $this->db->where("relation_term", 'ejecutivo');
                $this->db->where("relation_value", $params['user_id']);
                break;
        }

        $query = $this->db->get();
        $data = array();

        log_message("debug", "--------------- LOG QUERY AGENDA -------------------");
        log_message("debug", $this->db->last_query());
        log_message("debug", "--------------- LOG QUERY AGENDA -------------------");

        if($query->num_rows() > 0) {
            $data = $query->result();
        }

        return $data;
    }
}
